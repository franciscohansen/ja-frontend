inherited frmPesquisa: TfrmPesquisa
  Caption = 'Pesquisar'
  KeyPreview = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  ExplicitWidth = 651
  ExplicitHeight = 338
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseModal: TdxLayoutControl
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 635
    ExplicitHeight = 299
    object edtBusca: TcxTextEdit [0]
      Left = 10
      Top = 10
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
      TextHint = 'Digite os dados para pesquisar'
      Width = 534
    end
    object btnBuscar: TcxButton [1]
      Left = 550
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 1
      OnClick = btnBuscarClick
    end
    object GridLista: TcxTreeList [2]
      Left = 10
      Top = 41
      Width = 615
      Height = 186
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.ShowRoot = False
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 2
      object Grid_Json: TcxTreeListColumn
        Visible = False
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object Grid_Codigo: TcxTreeListColumn
        Caption.Text = 'C'#243'digo'
        Options.Sizing = False
        Width = 75
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object Grid_Descricao: TcxTreeListColumn
        Caption.Text = 'Descri'#231#227'o'
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object btnCarregarMais: TcxButton [3]
      Left = 10
      Top = 233
      Width = 615
      Height = 25
      Caption = 'Carregar Mais...'
      TabOrder = 3
      OnClick = btnCarregarMaisClick
    end
    object btnOk: TcxButton [4]
      Left = 469
      Top = 264
      Width = 75
      Height = 25
      Caption = 'OK'
      TabOrder = 4
      OnClick = btnOkClick
    end
    object btnFechar: TcxButton [5]
      Left = 550
      Top = 264
      Width = 75
      Height = 25
      Caption = 'Fechar'
      TabOrder = 5
      OnClick = btnFecharClick
    end
    object lciBusca: TdxLayoutItem
      Parent = lcg2
      AlignHorz = ahClient
      CaptionOptions.Layout = clTop
      Control = edtBusca
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciBuscar: TdxLayoutItem
      Parent = lcg2
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnBuscar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciLista: TdxLayoutItem
      Parent = lcgBaseModal
      AlignVert = avClient
      CaptionOptions.Layout = clTop
      Control = GridLista
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCarregarMais: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCarregarMais
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciOk: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnOk
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciFechar: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnFechar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      LayoutDirection = ldHorizontal
      Index = 3
    end
    object lcg2: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      LayoutDirection = ldHorizontal
      Index = 0
    end
  end
end
