unit untImprimir;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseModal, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, Dao.Report;

type

  TfrmImprimir = class(TfrmBaseModal)
  private
    FContexto: TContextoImpressao;
    FID: Integer;
    procedure SetContexto(const Value: TContextoImpressao);
    procedure SetID(const Value: Integer);
    { Private declarations }
  public
    property ID : Integer read FID write SetID;
    property Contexto : TContextoImpressao read FContexto write SetContexto;
    { Public declarations }
    class function Imprimir( AID : Integer; AContexto : TContextoImpressao ): Boolean;
  end;


implementation


{$R *.dfm}

{ TfrmImprimir }

class function TfrmImprimir.Imprimir(AID: Integer;
  AContexto: TContextoImpressao): Boolean;
var
  frm : TfrmImprimir;
begin
  frm := TfrmImprimir.Create( nil );
  try
    frm.ID := AID;
    frm.Contexto := AContexto;
    frm.ShowModal;
  finally
    frm.Release;
  end;
end;

procedure TfrmImprimir.SetContexto(const Value: TContextoImpressao);
begin
  FContexto := Value;
end;

procedure TfrmImprimir.SetID(const Value: Integer);
begin
  FID := Value;
end;

end.
