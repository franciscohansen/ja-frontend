inherited frmCad_Vendas: TfrmCad_Vendas
  Caption = 'Vendas'
  ClientHeight = 745
  ClientWidth = 970
  ExplicitWidth = 986
  ExplicitHeight = 784
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Width = 970
    Height = 745
    ExplicitWidth = 970
    ExplicitHeight = 745
    inherited btnSalvar: TcxButton
      Left = 804
      Top = 710
      TabOrder = 91
      ExplicitLeft = 804
      ExplicitTop = 710
    end
    inherited btnFechar: TcxButton
      Left = 885
      Top = 710
      TabOrder = 92
      ExplicitLeft = 885
      ExplicitTop = 710
    end
    inherited edtID: TcxTextEdit
      Left = 24
      Top = 62
      ExplicitLeft = 24
      ExplicitTop = 62
      ExplicitWidth = 75
      Width = 75
    end
    object cbbIndicacao: TcxImageComboBox [3]
      Left = 24
      Top = 107
      Properties.Items = <>
      Properties.OnChange = cbbIndicacaoPropertiesChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 1
      Width = 121
    end
    object edtDataLcto: TcxDateEdit [4]
      Left = 151
      Top = 107
      AutoSize = False
      Properties.DisplayFormat = 'dd/mm/yyyy'
      Properties.EditFormat = 'dd/mm/yyyy'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 2
      Height = 21
      Width = 121
    end
    object cboxComProcurador: TcxCheckBox [5]
      Left = 278
      Top = 111
      Caption = 'Com Procurador'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 3
      OnClick = cboxComProcuradorClick
    end
    object edtIndicador: TcxButtonEdit [6]
      Left = 24
      Top = 152
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 4
      Width = 331
    end
    object edtIndicadorNome: TcxTextEdit [7]
      Left = 361
      Top = 152
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 5
      Text = 'edtIndicadorNome'
      Height = 21
      Width = 331
    end
    object edtIndicadorRg: TcxTextEdit [8]
      Left = 825
      Top = 152
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 7
      Text = 'edtIndicadorRg'
      Height = 21
      Width = 121
    end
    object cbbTipoContrato: TcxImageComboBox [9]
      Left = 24
      Top = 197
      AutoSize = False
      Properties.Items = <>
      Properties.OnChange = cbbTipoContratoPropertiesChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 8
      Height = 21
      Width = 121
    end
    object edtVendedor: TcxButtonEdit [10]
      Left = 151
      Top = 197
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 9
      Text = 'edtVendedor'
      Width = 795
    end
    object cboxNovoCliente: TcxCheckBox [11]
      Left = 24
      Top = 224
      Caption = 'Novo Cliente'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 10
      OnClick = cboxNovoClienteClick
    end
    object edtCliente: TcxButtonEdit [12]
      Left = 24
      Top = 288
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 12
      Text = 'edtCliente'
      Width = 268
    end
    object edtNomeCliente: TcxTextEdit [13]
      Left = 298
      Top = 288
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 13
      Text = 'edtNomeCliente'
      Height = 21
      Width = 267
    end
    object edtClienteRazao: TcxTextEdit [14]
      Left = 698
      Top = 288
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 15
      Text = 'edtClienteRazao'
      Height = 21
      Width = 121
    end
    object edtClienteRg: TcxTextEdit [15]
      Left = 825
      Top = 288
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 16
      Text = 'edtClienteRg'
      Width = 121
    end
    object cboxClienteJuridico: TcxCheckBox [16]
      Left = 24
      Top = 247
      Caption = 'Pessoa Jur'#237'dica'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 11
      OnClick = cboxClienteJuridicoClick
    end
    object edtClienteNascimento: TcxDateEdit [17]
      Left = 24
      Top = 333
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 17
      Height = 21
      Width = 121
    end
    object edtClienteEmail: TcxTextEdit [18]
      Left = 278
      Top = 333
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 19
      Text = 'edtClienteEmail'
      Width = 414
    end
    object cbbClienteEstadoCivil: TcxImageComboBox [19]
      Left = 151
      Top = 333
      AutoSize = False
      Properties.Items = <>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 18
      Height = 21
      Width = 121
    end
    object edtClienteNaturalidade: TcxTextEdit [20]
      Left = 698
      Top = 333
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 20
      Text = 'edtClienteNaturalidade'
      Height = 21
      Width = 121
    end
    object cbbClienteSexo: TcxImageComboBox [21]
      Left = 825
      Top = 333
      AutoSize = False
      Properties.Items = <>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 21
      Height = 21
      Width = 121
    end
    object edtClienteProfissao: TcxButtonEdit [22]
      Left = 24
      Top = 378
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 22
      Text = 'edtClienteProfissao'
      Width = 121
    end
    object edtClienteNomePai: TcxTextEdit [23]
      Left = 151
      Top = 378
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 23
      Text = 'edtClienteNomePai'
      Height = 21
      Width = 395
    end
    object edtClienteNomeMae: TcxTextEdit [24]
      Left = 552
      Top = 378
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 24
      Text = 'edtClienteNomeMae'
      Height = 21
      Width = 394
    end
    object GridC_Fones: TcxTreeList [25]
      Left = 24
      Top = 423
      Width = 922
      Height = 267
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Append.Enabled = False
      OptionsBehavior.ChangeDelay = 1000
      OptionsData.Appending = True
      OptionsData.Inserting = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.ShowRoot = False
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 25
      object GridF_Json: TcxTreeListColumn
        Visible = False
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Padrao: TcxTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Caption.Text = 'Padr'#227'o'
        Options.Sizing = False
        Width = 75
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Tipo: TcxTreeListColumn
        Caption.Text = 'Tipo'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_DDD: TcxTreeListColumn
        Caption.Text = 'DDD'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Fone: TcxTreeListColumn
        Caption.Text = 'Fone'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Ramal: TcxTreeListColumn
        Caption.Text = 'Ramal'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Observacoes: TcxTreeListColumn
        Caption.Text = 'Observa'#231#245'es'
        Width = 100
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object btnCopiarEndereco: TcxButton [26]
      Left = 10000
      Top = 10000
      Width = 120
      Height = 25
      Caption = 'Copiar Endere'#231'o'
      TabOrder = 26
      Visible = False
      OnClick = btnCopiarEnderecoClick
    end
    object edtBanco: TcxButtonEdit [27]
      Left = 10000
      Top = 10000
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 27
      Text = 'edtBanco'
      Visible = False
      Width = 668
    end
    object edtBancoPerc: TcxCalcEdit [28]
      Left = 10000
      Top = 10000
      AutoSize = False
      EditValue = 0.000000000000000000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 28
      Visible = False
      Height = 21
      Width = 121
    end
    object edtBancoPrazo: TcxTextEdit [29]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 29
      Text = 'edtBancoPrazo'
      Visible = False
      Height = 21
      Width = 121
    end
    object edtValorParcela: TcxCalcEdit [30]
      Left = 10000
      Top = 10000
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Properties.OnChange = FinanciamentoChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 30
      Visible = False
      Width = 121
    end
    object edtTotalParcelas: TcxCalcEdit [31]
      Left = 10000
      Top = 10000
      AutoSize = False
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Properties.OnChange = FinanciamentoChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 31
      Visible = False
      Height = 21
      Width = 121
    end
    object edtValorFinanciado: TcxCalcEdit [32]
      Left = 10000
      Top = 10000
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Properties.OnChange = FinanciamentoChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 32
      Visible = False
      Width = 121
    end
    object edtParcelasPagas: TcxCalcEdit [33]
      Left = 10000
      Top = 10000
      AutoSize = False
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Properties.OnChange = FinanciamentoChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 33
      Visible = False
      Height = 21
      Width = 121
    end
    object edtParcelasAtraso: TcxCalcEdit [34]
      Left = 10000
      Top = 10000
      AutoSize = False
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Properties.OnChange = FinanciamentoChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 34
      Visible = False
      Height = 21
      Width = 121
    end
    object edtParcelasRestantes: TcxCalcEdit [35]
      Left = 10000
      Top = 10000
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Properties.OnChange = FinanciamentoChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 35
      Visible = False
      Width = 121
    end
    object edtSaldoPagar: TcxCalcEdit [36]
      Left = 10000
      Top = 10000
      AutoSize = False
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Properties.OnChange = FinanciamentoChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 36
      Visible = False
      Height = 21
      Width = 121
    end
    object edtR_ParcelaReduzida: TcxCalcEdit [37]
      Left = 10000
      Top = 10000
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Properties.OnChange = RENChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 37
      Visible = False
      Width = 121
    end
    object edtR_Saldo: TcxCalcEdit [38]
      Left = 10000
      Top = 10000
      AutoSize = False
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Properties.OnChange = RENChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 40
      Visible = False
      Height = 21
      Width = 121
    end
    object edtR_Economia: TcxCalcEdit [39]
      Left = 10000
      Top = 10000
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 38
      Visible = False
      Width = 121
    end
    object edtR_EconomiaTotal: TcxCalcEdit [40]
      Left = 10000
      Top = 10000
      AutoSize = False
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 41
      Visible = False
      Height = 21
      Width = 121
    end
    object edtP_Nome: TcxTextEdit [41]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 42
      Text = 'edtP_Nome'
      Visible = False
      Width = 851
    end
    object edtP_Nacionalidade: TcxTextEdit [42]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 43
      Text = 'edtP_Nacionalidade'
      Visible = False
      Width = 851
    end
    object edtP_Naturalidade: TcxTextEdit [43]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 44
      Text = 'edtP_Naturalidade'
      Visible = False
      Width = 851
    end
    object edtP_Profissao: TcxTextEdit [44]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 45
      Text = 'edtP_Profissao'
      Visible = False
      Width = 851
    end
    object edtP_RG: TcxTextEdit [45]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 46
      Text = 'edtP_RG'
      Visible = False
      Width = 851
    end
    object edtP_CPF: TcxTextEdit [46]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 47
      Text = 'edtP_CPF'
      Visible = False
      Width = 851
    end
    object edtP_Filiacao: TcxTextEdit [47]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 48
      Text = 'edtP_Filiacao'
      Visible = False
      Width = 851
    end
    object edtP_Email: TcxTextEdit [48]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 49
      Text = 'edtP_Email'
      Visible = False
      Width = 851
    end
    object edtP_Telefone: TcxTextEdit [49]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 50
      Text = 'edtP_Telefone'
      Visible = False
      Width = 851
    end
    object btnDA_Buscar: TcxButton [50]
      Left = 10000
      Top = 10000
      Width = 898
      Height = 25
      Caption = 'Buscar Dados'
      TabOrder = 51
      Visible = False
      OnClick = btnDA_BuscarClick
    end
    object edtCond_Nome: TcxTextEdit [51]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 52
      Text = 'edtCond_Nome'
      Visible = False
      Width = 831
    end
    object edtCond_Cpf: TcxTextEdit [52]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 53
      Text = 'edtCond_Cpf'
      Visible = False
      Width = 831
    end
    object edtCond_Cnh: TcxTextEdit [53]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 54
      Text = 'edtCond_Cnh'
      Visible = False
      Width = 831
    end
    object cbbVeicMarca: TcxComboBox [54]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 55
      Text = 'cbbVeicMarca'
      Visible = False
      Width = 831
    end
    object cbbVeicModelo: TcxComboBox [55]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 56
      Text = 'cbbVeicModelo'
      Visible = False
      Width = 831
    end
    object edtVeicCor: TcxTextEdit [56]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 57
      Text = 'edtVeicCor'
      Visible = False
      Width = 831
    end
    object edtVeicAno: TcxTextEdit [57]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 59
      Text = 'edtVeicAno'
      Visible = False
      Height = 21
      Width = 121
    end
    object edtVeicAnoModelo: TcxTextEdit [58]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 58
      Text = 'edtVeicAnoModelo'
      Visible = False
      Width = 121
    end
    object edtVeicPlaca: TcxTextEdit [59]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 60
      Text = 'edtVeicPlaca'
      Visible = False
      Width = 831
    end
    object edtVeicRenavam: TcxTextEdit [60]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 61
      Text = 'edtVeicRenavam'
      Visible = False
      Width = 831
    end
    object edtVeicChassi: TcxTextEdit [61]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 62
      Text = 'edtVeicChassi'
      Visible = False
      Width = 831
    end
    object edtVeicCrlv: TcxTextEdit [62]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 63
      Text = 'edtVeicCrlv'
      Visible = False
      Width = 831
    end
    object edtCartUltimaFatura: TcxDateEdit [63]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 64
      Visible = False
      Width = 812
    end
    object edtCartBandeira: TcxTextEdit [64]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 65
      Text = 'edtCartBandeira'
      Visible = False
      Width = 812
    end
    object edtCartContrato: TcxTextEdit [65]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 66
      Text = 'edtCartContrato'
      Visible = False
      Width = 812
    end
    object edtEmpContrato: TcxTextEdit [66]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 67
      Text = 'edtEmpContrato'
      Visible = False
      Width = 812
    end
    object mmoDA_Obs: TcxMemo [67]
      Left = 10000
      Top = 10000
      Lines.Strings = (
        'mmoDA_Obs')
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 68
      Visible = False
      Height = 132
      Width = 922
    end
    object edtDataPrimeiraPrestacao: TcxDateEdit [68]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 69
      Visible = False
      Width = 131
    end
    object edtDiasEntreParcelas: TcxSpinEdit [69]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 70
      Visible = False
      Height = 21
      Width = 121
    end
    object btnGerarParcelas: TcxButton [70]
      Left = 10000
      Top = 10000
      Width = 120
      Height = 25
      Caption = 'Gerar Parcelas'
      TabOrder = 71
      Visible = False
      OnClick = btnGerarParcelasClick
    end
    object edtMesesQuitacao: TcxSpinEdit [71]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 72
      Visible = False
      OnExit = edtMesesQuitacaoExit
      Height = 21
      Width = 121
    end
    object edtPrevisaoQuitacao: TcxDateEdit [72]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 73
      Visible = False
      Height = 21
      Width = 121
    end
    object GridParcelas: TcxTreeList [73]
      Left = 10000
      Top = 10000
      Width = 922
      Height = 583
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.ShowRoot = False
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 74
      Visible = False
      object GridP_Documento: TcxTreeListColumn
        Caption.Text = 'Documento'
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridP_Vencimento: TcxTreeListColumn
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mm/yyyy'
        Properties.EditFormat = 'dd/mm/yyyy'
        Caption.Text = 'Vencimento'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridP_Valor: TcxTreeListColumn
        PropertiesClassName = 'TcxCalcEditProperties'
        Properties.DisplayFormat = '#,##0.00'
        Caption.Text = 'Valor'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridP_Json: TcxTreeListColumn
        Visible = False
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object edtValorComissao: TcxCalcEdit [74]
      Left = 10000
      Top = 10000
      EditValue = 0.000000000000000000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 75
      Visible = False
      Width = 121
    end
    object edtIBanco: TcxTextEdit [75]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 76
      Text = 'edtIBanco'
      Visible = False
      Height = 21
      Width = 121
    end
    object edtIAgencia: TcxTextEdit [76]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 77
      Text = 'edtIAgencia'
      Visible = False
      Height = 21
      Width = 121
    end
    object edtIConta: TcxTextEdit [77]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 78
      Text = 'edtIConta'
      Visible = False
      Height = 21
      Width = 121
    end
    object GridDocumentos: TcxTreeList [78]
      Left = 10000
      Top = 10000
      Width = 922
      Height = 628
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsData.Deleting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.ShowRoot = False
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 80
      Visible = False
      object GridD_Entregue: TcxTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Caption.Text = 'Entregue?'
        Options.Sizing = False
        Width = 62
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridD_Descricao: TcxTreeListColumn
        Caption.Text = 'Documento'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridD_Json: TcxTreeListColumn
        Visible = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object edtAdesaoValor: TcxCalcEdit [79]
      Left = 10000
      Top = 10000
      EditValue = 0.000000000000000000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 81
      Visible = False
      Width = 121
    end
    object edtAdesaoDiasParcelas: TcxSpinEdit [80]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 83
      Visible = False
      Height = 21
      Width = 121
    end
    object edtAdesaoNroParcelas: TcxSpinEdit [81]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 84
      Visible = False
      Height = 21
      Width = 121
    end
    object btnAdesaoGerar: TcxButton [82]
      Left = 10000
      Top = 10000
      Width = 120
      Height = 25
      Caption = 'Gerar Parcelas'
      TabOrder = 85
      Visible = False
      OnClick = btnAdesaoGerarClick
    end
    object GridAdesao: TcxTreeList [83]
      Left = 10000
      Top = 10000
      Width = 898
      Height = 522
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.ShowRoot = False
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 86
      Visible = False
      object GridA_Vencimento: TcxTreeListColumn
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mm/yyyy'
        Properties.EditFormat = 'dd/mm/yyyy'
        Caption.Text = 'Vencimento'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridA_Documento: TcxTreeListColumn
        Caption.Text = 'Documento'
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridA_Valor: TcxTreeListColumn
        PropertiesClassName = 'TcxCalcEditProperties'
        Properties.DisplayFormat = '#,##0.00'
        Caption.Text = 'Valor'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridA_Json: TcxTreeListColumn
        Visible = False
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object edtAdesaoPrimeiroVcto: TcxDateEdit [84]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 82
      Visible = False
      Width = 121
    end
    object edtR_NroParcelas: TcxSpinEdit [85]
      Left = 10000
      Top = 10000
      Properties.OnChange = RENChange
      Properties.OnValidate = edtR_NroParcelasPropertiesValidate
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 39
      Visible = False
      Width = 121
    end
    object btnGerarVenda: TcxButton [86]
      Left = 10000
      Top = 10000
      Width = 140
      Height = 25
      Caption = 'Gerar Venda'
      TabOrder = 88
      Visible = False
      OnClick = btnGerarVendaClick
    end
    object btnGerarVendaSemAdesao: TcxButton [87]
      Left = 10000
      Top = 10000
      Width = 140
      Height = 25
      Caption = 'Gerar Venda sem Ades'#227'o'
      TabOrder = 87
      Visible = False
      OnClick = btnGerarVendaClick
    end
    object lbSituacao: TcxLabel [88]
      Left = 60
      Top = 710
      Caption = 'Pendente'
      Style.HotTrack = False
      Style.TransparentBorder = False
    end
    object btnGerarVendaAgora: TcxButton [89]
      Left = 678
      Top = 710
      Width = 120
      Height = 25
      Caption = 'Gerar Venda Agora'
      TabOrder = 90
      OnClick = btnGerarVendaAgoraClick
    end
    object edtIndicadorCpf: TcxMaskEdit [90]
      Left = 698
      Top = 152
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d\d\d\.\d\d\d\.\d\d\d-\d\d'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 6
      Width = 121
    end
    object edtClienteCpf: TcxMaskEdit [91]
      Left = 571
      Top = 288
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d\d\d\.\d\d\d\.\d\d\d-\d\d'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 14
      Width = 121
    end
    object edtPix: TcxTextEdit [92]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 79
      Text = 'edtPix'
      Visible = False
      Width = 121
    end
    inherited lciSalvar: TdxLayoutItem
      Index = 2
    end
    inherited lciFechar: TdxLayoutItem
      Index = 3
    end
    inherited lciID: TdxLayoutItem
      Parent = lcgDadosGerais
      ControlOptions.OriginalWidth = 75
    end
    object lciIndicacao: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'Indica'#231#227'o'
      CaptionOptions.Layout = clTop
      Control = cbbIndicacao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciDataLcto: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'Data Lcto'
      CaptionOptions.Layout = clTop
      Control = edtDataLcto
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciComProcurador: TdxLayoutItem
      Parent = lcg1
      AlignVert = avBottom
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = cboxComProcurador
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 95
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciIndicador: TdxLayoutItem
      Parent = lcgIndicador
      AlignHorz = ahClient
      CaptionOptions.Text = 'Cliente Indicador'
      CaptionOptions.Layout = clTop
      Control = edtIndicador
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciIndicadorNome: TdxLayoutItem
      Parent = lcgIndicador
      AlignHorz = ahClient
      CaptionOptions.Text = 'Nome Cliente Indicador'
      CaptionOptions.Layout = clTop
      Control = edtIndicadorNome
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciIndicadoRg: TdxLayoutItem
      Parent = lcgIndicador
      CaptionOptions.Text = 'RG/IE'
      CaptionOptions.Layout = clTop
      Control = edtIndicadorRg
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciTipoContrato: TdxLayoutItem
      Parent = lcg2
      CaptionOptions.Text = 'Tipo Contrato'
      CaptionOptions.Layout = clTop
      Control = cbbTipoContrato
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciVendedor: TdxLayoutItem
      Parent = lcg2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Vendedor'
      CaptionOptions.Layout = clTop
      Control = edtVendedor
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciNovoCliente: TdxLayoutItem
      Parent = lcgCliente
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = cboxNovoCliente
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 99
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciCliente: TdxLayoutItem
      Parent = lcg4
      AlignHorz = ahClient
      CaptionOptions.Text = 'Cliente'
      CaptionOptions.Layout = clTop
      Control = edtCliente
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciNomeCliente: TdxLayoutItem
      Parent = lcg4
      AlignHorz = ahClient
      CaptionOptions.Text = 'Cliente'
      CaptionOptions.Layout = clTop
      Control = edtNomeCliente
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciClienteRazao: TdxLayoutItem
      Parent = lcg4
      CaptionOptions.Text = 'Raz'#227'o Social'
      CaptionOptions.Layout = clTop
      Control = edtClienteRazao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciClienteRg: TdxLayoutItem
      Parent = lcg4
      CaptionOptions.Text = 'RG/IE'
      CaptionOptions.Layout = clTop
      Control = edtClienteRg
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciClienteJuridico: TdxLayoutItem
      Parent = lcgCliente
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = cboxClienteJuridico
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 110
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciClienteNascimento: TdxLayoutItem
      Parent = lcg5
      CaptionOptions.Text = 'Data Nascimento'
      CaptionOptions.Layout = clTop
      Control = edtClienteNascimento
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciClienteEmail: TdxLayoutItem
      Parent = lcg5
      AlignHorz = ahClient
      CaptionOptions.Text = 'E-mail'
      CaptionOptions.Layout = clTop
      Control = edtClienteEmail
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciClienteEstadoCivil: TdxLayoutItem
      Parent = lcg5
      CaptionOptions.Text = 'Estado Civil'
      CaptionOptions.Layout = clTop
      Control = cbbClienteEstadoCivil
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciClienteNaturalidade: TdxLayoutItem
      Parent = lcg5
      CaptionOptions.Text = 'Natural de'
      CaptionOptions.Layout = clTop
      Control = edtClienteNaturalidade
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciClienteSexo: TdxLayoutItem
      Parent = lcg5
      CaptionOptions.Text = 'Sexo'
      CaptionOptions.Layout = clTop
      Control = cbbClienteSexo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciClienteProfissao: TdxLayoutItem
      Parent = lcg6
      CaptionOptions.Text = 'Profiss'#227'o'
      CaptionOptions.Layout = clTop
      Control = edtClienteProfissao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciClienteNomePai: TdxLayoutItem
      Parent = lcg6
      AlignHorz = ahClient
      CaptionOptions.Text = 'Nome do Pai'
      CaptionOptions.Layout = clTop
      Control = edtClienteNomePai
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciClienteNomeMae: TdxLayoutItem
      Parent = lcg6
      AlignHorz = ahClient
      CaptionOptions.Text = 'Nome da M'#227'e'
      CaptionOptions.Layout = clTop
      Control = edtClienteNomeMae
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciC_Fones: TdxLayoutItem
      Parent = lcgDadosGerais
      AlignVert = avClient
      CaptionOptions.Text = 'Telefones'
      CaptionOptions.Layout = clTop
      Control = GridC_Fones
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object lcgTabs: TdxLayoutGroup
      Parent = lcgContent
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = 0
    end
    object lcgDadosGerais: TdxLayoutGroup
      Parent = lcgTabs
      CaptionOptions.Text = 'Dados B'#225'sicos'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      Index = 0
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgDadosGerais
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object lcg2: TdxLayoutAutoCreatedGroup
      Parent = lcgDadosGerais
      LayoutDirection = ldHorizontal
      Index = 3
    end
    object lcg4: TdxLayoutAutoCreatedGroup
      Parent = lcgCliente
      LayoutDirection = ldHorizontal
      Index = 2
    end
    object lcgCliente: TdxLayoutGroup
      Parent = lcgDadosGerais
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 4
      ShowBorder = False
      Index = 4
    end
    object lcg5: TdxLayoutAutoCreatedGroup
      Parent = lcgCliente
      LayoutDirection = ldHorizontal
      Index = 3
    end
    object lcg6: TdxLayoutAutoCreatedGroup
      Parent = lcgCliente
      LayoutDirection = ldHorizontal
      Index = 4
    end
    object lcgEnderecos: TdxLayoutGroup
      Parent = lcgTabs
      CaptionOptions.Text = 'Endere'#231'os'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 1
    end
    object lciCopiarEndereco: TdxLayoutItem
      Parent = lcgEnderecos
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCopiarEndereco
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 120
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciBanco: TdxLayoutItem
      Parent = lcg7
      AlignHorz = ahClient
      CaptionOptions.Text = 'Banco'
      CaptionOptions.Layout = clTop
      Control = edtBanco
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciBancoPerc: TdxLayoutItem
      Parent = lcg7
      AlignVert = avClient
      CaptionOptions.Text = 'Perc %'
      CaptionOptions.Layout = clTop
      Control = edtBancoPerc
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciBancoPrazo: TdxLayoutItem
      Parent = lcg7
      AlignVert = avClient
      CaptionOptions.Text = 'Prazo'
      CaptionOptions.Layout = clTop
      Control = edtBancoPrazo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciValorParcela: TdxLayoutItem
      Parent = lcg8
      CaptionOptions.Text = 'Valor Parcela'
      CaptionOptions.Layout = clTop
      Control = edtValorParcela
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciTotalParcelas: TdxLayoutItem
      Parent = lcg8
      CaptionOptions.Text = 'Total de Parcelas'
      CaptionOptions.Layout = clTop
      Control = edtTotalParcelas
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciValorFinanciado: TdxLayoutItem
      Parent = lcg9
      CaptionOptions.Text = 'Valor Financiado'
      CaptionOptions.Layout = clTop
      Control = edtValorFinanciado
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciParcelasPagas: TdxLayoutItem
      Parent = lcg9
      CaptionOptions.Text = 'Parcelas Pagas'
      CaptionOptions.Layout = clTop
      Control = edtParcelasPagas
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciParcelasAtraso: TdxLayoutItem
      Parent = lcg9
      CaptionOptions.Text = 'Parcelas em Atraso'
      CaptionOptions.Layout = clTop
      Control = edtParcelasAtraso
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciParcelasRestantes: TdxLayoutItem
      Parent = lcg10
      CaptionOptions.Text = 'Parcelas Restantes'
      CaptionOptions.Layout = clTop
      Control = edtParcelasRestantes
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciSaldoPagar: TdxLayoutItem
      Parent = lcg10
      CaptionOptions.Text = 'Saldo a Pagar'
      CaptionOptions.Layout = clTop
      Control = edtSaldoPagar
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciR_ParcelaReduzida: TdxLayoutItem
      Parent = lcg14
      AlignHorz = ahClient
      AlignVert = avTop
      CaptionOptions.Text = 'Parcela Reduzida'
      CaptionOptions.Layout = clTop
      Control = edtR_ParcelaReduzida
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciR_Saldo: TdxLayoutItem
      Parent = lcg12
      CaptionOptions.Text = 'Total Reduzido'
      CaptionOptions.Layout = clTop
      Control = edtR_Saldo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciR_Economia: TdxLayoutItem
      Parent = lcg14
      CaptionOptions.Text = 'Economia na Parcela'
      CaptionOptions.Layout = clTop
      Control = edtR_Economia
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciR_EconomiaTotal: TdxLayoutItem
      Parent = lcg12
      CaptionOptions.Text = 'Economia Total'
      CaptionOptions.Layout = clTop
      Control = edtR_EconomiaTotal
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciP_Nome: TdxLayoutItem
      Parent = lcgProcurador
      CaptionOptions.Text = 'Nome'
      Control = edtP_Nome
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciP_Nacionalidade: TdxLayoutItem
      Parent = lcgProcurador
      CaptionOptions.Text = 'Nacionalidade'
      Control = edtP_Nacionalidade
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciP_Naturalidade: TdxLayoutItem
      Parent = lcgProcurador
      CaptionOptions.Text = 'Naturalidade'
      Control = edtP_Naturalidade
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciP_Profissao: TdxLayoutItem
      Parent = lcgProcurador
      CaptionOptions.Text = 'Profiss'#227'o'
      Control = edtP_Profissao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciP_RG: TdxLayoutItem
      Parent = lcgProcurador
      CaptionOptions.Text = 'RG'
      Control = edtP_RG
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciP_CPF: TdxLayoutItem
      Parent = lcgProcurador
      CaptionOptions.Text = 'CPF'
      Control = edtP_CPF
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object lciP_Filiacao: TdxLayoutItem
      Parent = lcgProcurador
      CaptionOptions.Text = 'Filia'#231#227'o'
      Control = edtP_Filiacao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 6
    end
    object lciP_Email: TdxLayoutItem
      Parent = lcgProcurador
      CaptionOptions.Text = 'E-mail'
      Control = edtP_Email
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 8
    end
    object lciP_Telefone: TdxLayoutItem
      Parent = lcgProcurador
      CaptionOptions.Text = 'Telefone'
      Control = edtP_Telefone
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 9
    end
    object lciDA_Buscar: TdxLayoutItem
      Parent = lcgCondutor
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnDA_Buscar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciCond_Nome: TdxLayoutItem
      Parent = lcgCondutor
      CaptionOptions.Text = 'Nome'
      Control = edtCond_Nome
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCond_Cpf: TdxLayoutItem
      Parent = lcgCondutor
      CaptionOptions.Text = 'CPF'
      Control = edtCond_Cpf
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciCond_Cnh: TdxLayoutItem
      Parent = lcgCondutor
      CaptionOptions.Text = 'CNH'
      Control = edtCond_Cnh
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciVeicMarca: TdxLayoutItem
      Parent = lcgVeiculo
      CaptionOptions.Text = 'Marca'
      Control = cbbVeicMarca
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciVeicModelo: TdxLayoutItem
      Parent = lcgVeiculo
      CaptionOptions.Text = 'Modelo'
      Control = cbbVeicModelo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciVeicCor: TdxLayoutItem
      Parent = lcgVeiculo
      CaptionOptions.Text = 'Cor'
      Control = edtVeicCor
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciVeicAno: TdxLayoutItem
      Parent = lcg15
      CaptionOptions.Text = '/'
      Control = edtVeicAno
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciVeicAnoModelo: TdxLayoutItem
      Parent = lcg15
      CaptionOptions.Text = 'Ano'
      Control = edtVeicAnoModelo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciVeicPlaca: TdxLayoutItem
      Parent = lcgVeiculo
      CaptionOptions.Text = 'Placa'
      Control = edtVeicPlaca
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciVeicRenavam: TdxLayoutItem
      Parent = lcgVeiculo
      CaptionOptions.Text = 'RENAVAM'
      Control = edtVeicRenavam
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object lciVeicChassi: TdxLayoutItem
      Parent = lcgVeiculo
      CaptionOptions.Text = 'Chassi'
      Control = edtVeicChassi
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 6
    end
    object lciVeicCrlv: TdxLayoutItem
      Parent = lcgVeiculo
      CaptionOptions.Text = 'CRLV do Ano'
      Control = edtVeicCrlv
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 7
    end
    object lciCartUltimaFatura: TdxLayoutItem
      Parent = lcgCartao
      CaptionOptions.Text = 'Data da '#218'ltima Fatura'
      Control = edtCartUltimaFatura
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciCartBandeira: TdxLayoutItem
      Parent = lcgCartao
      CaptionOptions.Text = 'Bandeira'
      Control = edtCartBandeira
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCartContrato: TdxLayoutItem
      Parent = lcgCartao
      CaptionOptions.Text = 'Nro. Contrato'
      Control = edtCartContrato
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciEmpContrato: TdxLayoutItem
      Parent = lcgEmprestimo
      CaptionOptions.Text = 'Nro. Contrato'
      Control = edtEmpContrato
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciDA_Obs: TdxLayoutItem
      Parent = lcgDadosAdicionais
      AlignVert = avClient
      CaptionOptions.Text = 'Observa'#231#245'es'
      CaptionOptions.Layout = clTop
      Control = mmoDA_Obs
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciDataPrimeiraPrestacao: TdxLayoutItem
      Parent = dxLayoutAutoCreatedGroup1
      CaptionOptions.Text = 'Data da Primeira Presta'#231#227'o'
      CaptionOptions.Layout = clTop
      Control = edtDataPrimeiraPrestacao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 131
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciDiasEntreParcelas: TdxLayoutItem
      Parent = dxLayoutAutoCreatedGroup1
      CaptionOptions.Text = 'Dias Entre Parcelas'
      CaptionOptions.Layout = clTop
      Control = edtDiasEntreParcelas
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciGerarParcelas: TdxLayoutItem
      Parent = dxLayoutAutoCreatedGroup1
      AlignVert = avBottom
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnGerarParcelas
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 120
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciMesesQuitacao: TdxLayoutItem
      Parent = dxLayoutAutoCreatedGroup1
      CaptionOptions.Text = 'Meses para Quitar'
      CaptionOptions.Layout = clTop
      Control = edtMesesQuitacao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciPrevisaoQuitacao: TdxLayoutItem
      Parent = dxLayoutAutoCreatedGroup1
      CaptionOptions.Text = 'Previs'#227'o Quita'#231#227'o'
      CaptionOptions.Layout = clTop
      Control = edtPrevisaoQuitacao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciParcelas: TdxLayoutItem
      Parent = lcgParcelas
      AlignVert = avClient
      CaptionOptions.Text = 'Parcelas'
      CaptionOptions.Layout = clTop
      Control = GridParcelas
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciValorComissao: TdxLayoutItem
      Parent = lcgIndicacao
      AlignHorz = ahLeft
      AlignVert = avTop
      CaptionOptions.Text = 'Valor da Comiss'#227'o'
      CaptionOptions.Layout = clTop
      Control = edtValorComissao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciIBanco: TdxLayoutItem
      Parent = lcgIndicacao
      AlignHorz = ahLeft
      AlignVert = avTop
      CaptionOptions.Text = 'Banco'
      CaptionOptions.Layout = clTop
      Control = edtIBanco
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciIAgencia: TdxLayoutItem
      Parent = lcgIndicacao
      AlignHorz = ahLeft
      AlignVert = avTop
      CaptionOptions.Text = 'Ag'#234'ncia'
      CaptionOptions.Layout = clTop
      Control = edtIAgencia
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciIconta: TdxLayoutItem
      Parent = lcgIndicacao
      AlignHorz = ahLeft
      AlignVert = avTop
      CaptionOptions.Text = 'Conta'
      CaptionOptions.Layout = clTop
      Control = edtIConta
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciDocumentos: TdxLayoutItem
      Parent = lcgDocumentos
      AlignVert = avClient
      CaptionOptions.Text = 'Documentos'
      CaptionOptions.Layout = clTop
      Control = GridDocumentos
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciAdesaoValor: TdxLayoutItem
      Parent = lcg13
      CaptionOptions.Text = 'Valor da Ades'#227'o'
      CaptionOptions.Layout = clTop
      Control = edtAdesaoValor
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciAdesaoDiasParcelas: TdxLayoutItem
      Parent = lcg13
      CaptionOptions.Text = 'Dias entre parcelas'
      CaptionOptions.Layout = clTop
      Control = edtAdesaoDiasParcelas
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciAdesaoNroParcelas: TdxLayoutItem
      Parent = lcg13
      CaptionOptions.Text = 'N'#250'mero de Parcelas'
      CaptionOptions.Layout = clTop
      Control = edtAdesaoNroParcelas
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciAdesaoGerar: TdxLayoutItem
      Parent = lcg13
      AlignVert = avBottom
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnAdesaoGerar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 120
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciAdesao: TdxLayoutItem
      Parent = lcgDadosAdesao
      AlignVert = avClient
      CaptionOptions.Text = 'Parcelas Ades'#227'o'
      CaptionOptions.Layout = clTop
      Control = GridAdesao
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcgEndereco: TdxLayoutGroup
      Parent = lcgEnderecos
      CaptionOptions.Text = 'Endere'#231'o'
      ButtonOptions.Buttons = <>
      Index = 0
    end
    object lcgEnderecoCarne: TdxLayoutGroup
      Parent = lcgEnderecos
      CaptionOptions.Text = 'Endere'#231'o Carn'#234
      ButtonOptions.Buttons = <>
      Index = 2
    end
    object lcgSimulacao: TdxLayoutGroup
      Parent = lcgTabs
      CaptionOptions.Text = 'Simula'#231#227'o'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      Index = 2
    end
    object lcg7: TdxLayoutAutoCreatedGroup
      Parent = lcgSimulacao
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg10: TdxLayoutAutoCreatedGroup
      Parent = lcgValoresFinanciamento
      LayoutDirection = ldHorizontal
      Index = 2
    end
    object lcg11: TdxLayoutAutoCreatedGroup
      Parent = lcgSimulacao
      LayoutDirection = ldHorizontal
      Index = 2
    end
    object lcgValoresFinanciamento: TdxLayoutGroup
      Parent = lcgSimulacao
      CaptionOptions.Text = 'Dados do Financiamento'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 1
    end
    object lcg8: TdxLayoutAutoCreatedGroup
      Parent = lcgValoresFinanciamento
      AlignVert = avTop
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg9: TdxLayoutAutoCreatedGroup
      Parent = lcgValoresFinanciamento
      AlignVert = avTop
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object lcgProcurador: TdxLayoutGroup
      Parent = lcgTabs
      CaptionOptions.Text = 'Procurador'
      ButtonOptions.Buttons = <>
      Index = 3
    end
    object lcgP_Endereco: TdxLayoutGroup
      Parent = lcgProcurador
      CaptionOptions.Text = 'Endere'#231'o'
      ButtonOptions.Buttons = <>
      Index = 7
    end
    object lcgDadosAdicionais: TdxLayoutGroup
      Parent = lcgTabs
      CaptionOptions.Text = 'Dados Adicionais'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 4
    end
    object lcgParcelas: TdxLayoutGroup
      Parent = lcgTabs
      CaptionOptions.Text = 'Parcelas'
      ButtonOptions.Buttons = <>
      Index = 5
    end
    object lcgIndicacao: TdxLayoutGroup
      Parent = lcgTabs
      CaptionOptions.Text = 'Indica'#231#227'o'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      Index = 6
    end
    object lcgDocumentos: TdxLayoutGroup
      Parent = lcgTabs
      CaptionOptions.Text = 'Documentos'
      ButtonOptions.Buttons = <>
      Index = 7
    end
    object lcgAdesao: TdxLayoutGroup
      Parent = lcgTabs
      CaptionOptions.Text = 'Ades'#227'o'
      ButtonOptions.Buttons = <>
      Index = 8
    end
    object dxLayoutAutoCreatedGroup1: TdxLayoutAutoCreatedGroup
      Parent = lcgParcelas
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg13: TdxLayoutAutoCreatedGroup
      Parent = lcgDadosAdesao
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg12: TdxLayoutAutoCreatedGroup
      Parent = lcg11
      Index = 2
    end
    object lcg14: TdxLayoutAutoCreatedGroup
      Parent = lcg11
      Index = 0
    end
    object lcgVeiculo: TdxLayoutGroup
      Parent = lcgDadosAdicionais
      CaptionOptions.Text = 'Dados do Ve'#237'culo'
      ButtonOptions.Buttons = <>
      ItemIndex = 3
      Index = 1
    end
    object lcgCartao: TdxLayoutGroup
      Parent = lcgDadosAdicionais
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      ShowBorder = False
      Index = 2
    end
    object lcgEmprestimo: TdxLayoutGroup
      Parent = lcgDadosAdicionais
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 3
    end
    object lcgCondutor: TdxLayoutGroup
      Parent = lcgDadosAdicionais
      CaptionOptions.Text = 'Dados do Condutor'
      ButtonOptions.Buttons = <>
      ItemIndex = 3
      Index = 0
    end
    object lcg15: TdxLayoutAutoCreatedGroup
      Parent = lcgVeiculo
      LayoutDirection = ldHorizontal
      Index = 3
    end
    object lcgDadosAdesao: TdxLayoutGroup
      Parent = lcgAdesao
      AlignVert = avClient
      CaptionOptions.Text = 'Dados para pagamento da Ades'#227'o'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      Index = 0
    end
    object lciAdesaoPrimeiroVcto: TdxLayoutItem
      Parent = lcg13
      CaptionOptions.Text = 'Data da Primeira Parcela'
      CaptionOptions.Layout = clTop
      Control = edtAdesaoPrimeiroVcto
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciR_NroParcelas: TdxLayoutItem
      Parent = lcg11
      CaptionOptions.Text = 'Nro Parcelas'
      CaptionOptions.Layout = clTop
      Control = edtR_NroParcelas
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciGerarVenda: TdxLayoutItem
      Parent = lcg16
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnGerarVenda
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 140
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciGerarVendaSemAdesao: TdxLayoutItem
      Parent = lcg16
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnGerarVendaSemAdesao
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 140
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lcg16: TdxLayoutAutoCreatedGroup
      Parent = lcgDadosAdesao
      LayoutDirection = ldHorizontal
      Index = 2
    end
    object lciSituacao: TdxLayoutItem
      Parent = lcgRodape
      CaptionOptions.Text = 'Situa'#231#227'o:'
      Control = lbSituacao
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciGerarVendaAgora: TdxLayoutItem
      Parent = lcgRodape
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnGerarVendaAgora
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 120
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcgIndicador: TdxLayoutGroup
      Parent = lcgDadosGerais
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object lciIndicadorCpf: TdxLayoutItem
      Parent = lcgIndicador
      CaptionOptions.Text = 'CPF'
      CaptionOptions.Layout = clTop
      Control = edtIndicadorCpf
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciClienteCpf: TdxLayoutItem
      Parent = lcg4
      CaptionOptions.Text = 'CPF/CNPJ'
      CaptionOptions.Layout = clTop
      Control = edtClienteCpf
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciPix: TdxLayoutItem
      Parent = lcgIndicacao
      CaptionOptions.Text = 'PIX'
      CaptionOptions.Layout = clTop
      Control = edtPix
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
  end
end
