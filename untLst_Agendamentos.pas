unit untLst_Agendamentos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseLista, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles,
  dxScrollbarAnnotations, cxTL, cxTLdxBarBuiltInMenu, Vcl.Menus, cxContainer,
  cxEdit, dxLayoutContainer, dxLayoutControlAdapters, dxLayoutcxEditAdapters,
  cxClasses, cxMaskEdit, cxDropDownEdit, cxTextEdit, Vcl.StdCtrls, cxButtons,
  cxInplaceContainer, dxLayoutControl, cxCalendar, Dao.Interfaces,
  Json.Interfaces;

type
  TfrmLst_Agendamentos = class(TfrmBaseLista)
    GridBase_Descricao: TcxTreeListColumn;
    GridBase_Data: TcxTreeListColumn;
    GridBase_Status: TcxTreeListColumn;
    GridBase_Usuario: TcxTreeListColumn;
  private
    { Private declarations }
  protected
    function Dao : IDao; override;
    procedure JsonToNode( AJson : IJson; ANode : TcxTreeListNode ); override;
  public
    { Public declarations }
  end;


implementation

uses
  Dao.Base;

{$R *.dfm}

{ TfrmLst_Agendamentos }

function TfrmLst_Agendamentos.Dao: IDao;
begin
  Result := TDao.New( '/agendamentos' );
end;

procedure TfrmLst_Agendamentos.JsonToNode(AJson: IJson; ANode: TcxTreeListNode);
begin
  inherited;
end;

end.
