inherited frmCad_Acompanhamento: TfrmCad_Acompanhamento
  Caption = 'Acompanhamento'
  ClientHeight = 834
  ClientWidth = 691
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 707
  ExplicitHeight = 873
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseModal: TdxLayoutControl
    Width = 691
    Height = 834
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 691
    ExplicitHeight = 834
    object lbNome: TcxLabel [0]
      Left = 72
      Top = 10
      Caption = 'lbNome'
      Style.HotTrack = False
      Style.TransparentBorder = False
    end
    object lbProcurador: TcxLabel [1]
      Left = 72
      Top = 30
      Caption = 'lbProcurador'
      Style.HotTrack = False
      Style.TransparentBorder = False
    end
    object lbCondutor: TcxLabel [2]
      Left = 72
      Top = 50
      Caption = 'lbCondutor'
      Style.HotTrack = False
      Style.TransparentBorder = False
    end
    object lbVeiculo: TcxLabel [3]
      Left = 72
      Top = 70
      Caption = 'lbVeiculo'
      Style.HotTrack = False
      Style.TransparentBorder = False
    end
    object edtME_Data: TcxDateEdit [4]
      Left = 24
      Top = 263
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 5
      Width = 319
    end
    object edtME_Nro: TcxTextEdit [5]
      Left = 349
      Top = 263
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 6
      Height = 21
      Width = 318
    end
    object edtME_IDDecisao: TcxTextEdit [6]
      Left = 24
      Top = 308
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 7
      Width = 319
    end
    object edtME_ResumoDecisao: TcxTextEdit [7]
      Left = 349
      Top = 308
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 8
      Height = 21
      Width = 318
    end
    object edtME_Forum: TcxButtonEdit [8]
      Left = 24
      Top = 353
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 9
      Width = 319
    end
    object edtME_Oficial: TcxButtonEdit [9]
      Left = 349
      Top = 353
      AutoSize = False
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 10
      Height = 21
      Width = 318
    end
    object cboxCB_Liberacao: TcxCheckBox [10]
      Left = 10000
      Top = 10000
      Caption = 'Aguardando Libera'#231#227'o'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 11
      Visible = False
    end
    object cboxCB_Arquivado: TcxCheckBox [11]
      Left = 10000
      Top = 10000
      Caption = 'Arquivado'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 12
      Visible = False
    end
    object edtCB_Data: TcxDateEdit [12]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 13
      Visible = False
      Width = 192
    end
    object edtCB_ClasseProcesso: TcxTextEdit [13]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 14
      Visible = False
      Height = 21
      Width = 193
    end
    object edtCB_Nro: TcxTextEdit [14]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 15
      Visible = False
      Width = 192
    end
    object edtCB_Forum: TcxButtonEdit [15]
      Left = 10000
      Top = 10000
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 16
      Visible = False
      Width = 292
    end
    object edtCB_Oficial: TcxButtonEdit [16]
      Left = 10000
      Top = 10000
      AutoSize = False
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 17
      Visible = False
      Height = 21
      Width = 291
    end
    object edtCA_Data: TcxDateEdit [17]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 18
      Visible = False
      Width = 292
    end
    object edtCA_Endereco: TcxTextEdit [18]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 19
      Visible = False
      Height = 21
      Width = 291
    end
    object edtCA_Motivo: TcxTextEdit [19]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 20
      Visible = False
      Width = 292
    end
    object edtCA_Oficial: TcxButtonEdit [20]
      Left = 10000
      Top = 10000
      AutoSize = False
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 21
      Visible = False
      Height = 21
      Width = 291
    end
    object mmoCA_Observacoes: TcxMemo [21]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 22
      Visible = False
      Height = 89
      Width = 589
    end
    object edtRE_Data: TcxDateEdit [22]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 23
      Visible = False
      Width = 192
    end
    object edtRE_Forum: TcxButtonEdit [23]
      Left = 10000
      Top = 10000
      AutoSize = False
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 24
      Visible = False
      Height = 21
      Width = 193
    end
    object edtRE_IDDecisao: TcxTextEdit [24]
      Left = 10000
      Top = 10000
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 25
      Visible = False
      Height = 21
      Width = 192
    end
    object mmoRE_Observacoes: TcxMemo [25]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 26
      Visible = False
      Height = 134
      Width = 589
    end
    object cboxEE_Bacenjud: TcxCheckBox [26]
      Left = 10000
      Top = 10000
      Caption = 'BACENJUD'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 27
      Visible = False
    end
    object edtEE_Data: TcxDateEdit [27]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 28
      Visible = False
      Width = 185
    end
    object edtEE_Forum: TcxButtonEdit [28]
      Left = 10000
      Top = 10000
      AutoSize = False
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 29
      Visible = False
      Height = 21
      Width = 185
    end
    object edtEE_IDDecisao: TcxTextEdit [29]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 30
      Visible = False
      Width = 185
    end
    object mmoEE_Observacao: TcxMemo [30]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 31
      Visible = False
      Height = 134
      Width = 643
    end
    object cboxCadastroPush: TcxCheckBox [31]
      Left = 24
      Top = 496
      Caption = 'Cadastro no Push?'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 32
    end
    object GridFollowUp: TcxTreeList [32]
      Left = 24
      Top = 527
      Width = 643
      Height = 221
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 37
      object GridFU_Data: TcxTreeListColumn
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mm/yyyy'
        Properties.EditFormat = 'dd/mm/yyyy'
        Caption.Text = 'Data'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridFU_Resumo: TcxTreeListColumn
        PropertiesClassName = 'TcxRichEditProperties'
        Properties.PlainText = True
        Caption.Text = 'Resumo'
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridFU_Json: TcxTreeListColumn
        Visible = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object mmoObservacoes: TcxMemo [33]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 40
      Visible = False
      Height = 283
      Width = 643
    end
    object lstSituacao: TcxCheckListBox [34]
      Left = 10
      Top = 108
      Width = 671
      Height = 97
      Items = <>
      TabOrder = 4
      OnClick = lstSituacaoClick
      OnEditValueChanged = lstSituacaoClick
    end
    object btnFechar: TcxButton [35]
      Left = 606
      Top = 799
      Width = 75
      Height = 25
      Caption = 'Fechar'
      TabOrder = 50
      OnClick = btnFecharClick
    end
    object btnOk: TcxButton [36]
      Left = 525
      Top = 799
      Width = 75
      Height = 25
      Caption = 'OK'
      TabOrder = 49
      OnClick = btnOkClick
    end
    object cbbTipo: TcxImageComboBox [37]
      Left = 10000
      Top = 10000
      EditValue = 'ACORDO'
      Properties.Items = <
        item
          Description = 'Acordo'
          ImageIndex = 0
          Value = 'ACORDO'
        end
        item
          Description = 'Ades'#227'o'
          Value = 'ADESAO'
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 41
      Visible = False
      Width = 121
    end
    object cbbVencimento: TcxImageComboBox [38]
      Left = 10000
      Top = 10000
      AutoSize = False
      EditValue = 'VENCIDAS'
      Properties.Items = <
        item
          Description = 'Todas'
          ImageIndex = 0
          Value = 'TODAS'
        end
        item
          Description = 'Vencidas/Pagas'
          ImageIndex = 0
          Value = 'VENCIDAS'
        end
        item
          Description = 'A Vencer'
          Value = 'PENDENTES'
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 42
      Visible = False
      Height = 21
      Width = 121
    end
    object btnFiltrar: TcxButton [39]
      Left = 10000
      Top = 10000
      Width = 75
      Height = 25
      Caption = 'Filtrar'
      TabOrder = 43
      Visible = False
      OnClick = btnFiltrarClick
    end
    object GridFinanceiro: TcxTreeList [40]
      Left = 10000
      Top = 10000
      Width = 643
      Height = 238
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsView.ShowRoot = False
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 44
      Visible = False
      object GridF_Codigo: TcxTreeListColumn
        Caption.Text = 'C'#243'digo'
        Width = 75
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Descricao: TcxTreeListColumn
        Caption.Text = 'Descri'#231#227'o'
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Documento: TcxTreeListColumn
        Caption.Text = 'Documento'
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Vcto: TcxTreeListColumn
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mm/yyyy'
        Properties.EditFormat = 'dd/mm/yyyy'
        Caption.Text = 'Vencimento'
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Pgto: TcxTreeListColumn
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mm/yyyy'
        Properties.EditFormat = 'dd/mm/yyyy'
        Caption.Text = 'Pagamento'
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Valor: TcxTreeListColumn
        PropertiesClassName = 'TcxCalcEditProperties'
        Properties.DisplayFormat = '#,##0.00'
        Caption.Text = 'Valor'
        Width = 100
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Recebido: TcxTreeListColumn
        PropertiesClassName = 'TcxCalcEditProperties'
        Properties.DisplayFormat = '#,##0.00'
        Caption.Text = 'Recebido'
        Width = 100
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridF_Json: TcxTreeListColumn
        Visible = False
        Width = 100
        Position.ColIndex = 7
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object GridTelefone: TcxTreeList [41]
      Left = 10000
      Top = 10000
      Width = 643
      Height = 220
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsView.ShowEditButtons = ecsbAlways
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 45
      Visible = False
      object GridT_DDD: TcxTreeListColumn
        Caption.Text = 'DDD'
        Options.Sizing = False
        Options.Editing = False
        Width = 50
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridT_Fone: TcxTreeListColumn
        Caption.Text = 'Telefone'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridT_Observacoes: TcxTreeListColumn
        Caption.Text = 'Observa'#231#245'es'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridT_WhatsApp: TcxTreeListColumn
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Glyph.SourceDPI = 96
            Glyph.Data = {
              89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
              F40000000473424954080808087C086488000000097048597300000B1300000B
              1301009A9C18000005BD494441545885BD966B4C5C651AC77FEF3967A67305CA
              5C283820945A845AB694626DAD9736DED366DDA45E134D4CF593269AFDD08FC6
              F841FDE4266663A2093146E3C66CD6784F6C6BB5B656B650B654280CB69432C0
              00C3CC30F7FB397E00A6D09901BABBF59F4C32F3E439EFEFFFBEEFF33C6704C0
              A1234F3423B26FA169FB2449B27003A5425C82E3AA2A1FEE7CE0D34171E8C813
              CD42CA7409A4B21B09BE56014F389A0C67EE5510D9B7FE6878C813213A11B348
              B2F89B82A6ED4388353F2C090997A51E97A58EB27515C84221918D3313F73212
              1A269E8DAD0A0F4D44015073D90E65AD776E3338B8AF6E3F3B37DC8545672D9A
              A3691A43C15FF961FC3BFA66BBD134AD247C5EB241590D2C0B99FD0D8FF150FD
              A3C84206C0131965243C4C20394B4ECD61D159A9B1D4D2B47E0BCD95AD3457B6
              32121AE6C3C177F1C6264AC0E7255E3876502B882EC8ACB3F262EB613655DC8A
              AAA99C9C38C6D1B1AF98494C15CDD7497A3AAA76B3BFE131EC4627E95C8ACE81
              77F8B1F7FBA27080922760544CFCB5ED556AADF5CCC4BDBCD7FF369EC868E9A3
              02326A9AD3DE1FE99E3ECDC15B9E61AFEB21F6EA0FD01F1D22847BED060482E7
              5A5EA2D65ACF787494B77B5F279A89AC08BFD6C83FDC9DF40F9EA721D5C2FD37
              1F606E38803FE92BC8958A2D70FB863D6C737410498778E7DC1BD7055F54C813
              E19B335F727EF62C8A50D857FB08A248B715189084CCA38D4F01F089BB93B954
              301F7F7ECBCBBC79E7BB6C5EDFB22A7CF1CE7F9EFC81703A4495A99A8D659B57
              37B0D5D686CDE0C01319A577A62B1FDFEB7A90DB37ECC16670707FDD8135C101
              725A96EEE99F0168B56F5FDDC076E74E004E4D7E8FC6D506B9CDD696FFDE6A6B
              C76670AC0A5FD46F738364D40C35963A0CB27165031BCB9B00B810E85B165735
              F5EA0F41C1302A0507C8AA19BC310F024195B9A6B40181C0617492D372F812D3
              CB12FBFDFFC97FFFF6F2675C898CAC09BEA840D20F40B97E7D69033A598F2464
              12D9F8F21D03A7268FE7DBA8A17C1302B16638402A9700609DAC2F6D40D57200
              28A2703C64D4341F5CF83BAAA6D252F927FEDCF8E49AE100B234BF666E8151D4
              4056CD124987312846CC455E38C3C10BFCEBE2470034A6B6D026F6A093740058
              F5653CDDF43C7754DF8D5131153CBB78F491747859BC60AB639111B6D8B6714B
              C5AD9CF375172C7474EC6BFC6373B8128DDC66DB46435923E7677BD9BCBE854A
              839D4A839D6DF60EBEBBF20597C31781F9DAAA36BB0098BDA6B60ABAE0D78562
              EBA8DA530087F93BFFBCEB9F1C1DFB8A542E8959676557F53DCBDA529174C80B
              2703506D7661D15909A5E7F283ADA481EEE95364D50CEDCE3BA8325517C017EF
              DC1D1CE0E3A1F7E999395D30AA47C397B81C1ACEEF7E47D52E0006FDE797CD16
              00B9FDD996D79606D2B91466BD95C6F2262A0DF6FC142B56705935C378F40A7D
              BE1EC62297998A4F7076A68B5E5F571EB4A9A29976E72E92D904473D5F93D3B2
              CBD628FA368C2DEC2898F297842F9586C6547C82A9F8C4B2B8DDE8645FEDC300
              9C9C3C462A972C78B6A8811DCEDD00F44CFF725DADB6542ECBCD3C5CFF17F492
              9E7EFF39DCC181A2790506AACD377193A58E502A484FFF19F0E96873EC6483B9
              863E5F0F9331CF8A60A362A2A36A375BEDED0804EE603F27268E94CC2F30D0EE
              9C2F9821F7108F981FC7D67CB5BA1BCB9B98897BB91872331D9F24920EA1A162
              904DD88C0EEA2C1BD958B1194528E4B42CBF787FA2CFD75D50782B1AD851B51B
              B7DB8D6FCC8FCDE02096897229E4269E89B1D5BE1DA7A91AE735DDB1549AA6E2
              0E0E7066EA14A174B0645E510335E65A22E3717A06BAB934E7E652C8CD4CC29B
              FF7B7DD6D785CB5247ADA501BBD1814967414222954B32970A30191B67347C91
              4436BE2A7851E2D0B18331094C00EBFC16029E3902C9D9158FEDFF2521D4A422
              C17160FF7CB57B6F3874A92449F9B7A4AAF2E180271CFD6F5AED7F91408B6295
              5E913A1FF8743019CEDC2BC9E2241499142BAF72DD1F21A949592F9D902A7477
              7DFBD28973BF0341D193E7DEA986100000000049454E44AE426082}
            Kind = bkGlyph
          end>
        Properties.ViewStyle = vsButtonsOnly
        Properties.OnButtonClick = GridT_WhatsAppPropertiesButtonClick
        Options.Sizing = False
        Width = 50
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridT_PageBot: TcxTreeListColumn
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Caption = 'PageBot'
            Default = True
            Kind = bkEllipsis
          end>
        Properties.ViewStyle = vsButtonsOnly
        Properties.OnButtonClick = GridT_PageBotPropertiesButtonClick
        Visible = False
        Options.Sizing = False
        Width = 50
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridT_Copiar: TcxTreeListColumn
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Caption = 'Copiar'
            Default = True
            Kind = bkText
          end>
        Properties.ViewStyle = vsButtonsOnly
        Properties.OnButtonClick = GridT_CopiarPropertiesButtonClick
        Options.Sizing = False
        Width = 50
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object edtEmail: TcxTextEdit [42]
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 46
      Visible = False
      Width = 431
    end
    object btnEmailCopiar: TcxButton [43]
      Left = 10000
      Top = 10000
      Width = 100
      Height = 25
      Caption = 'Copiar E-mail'
      TabOrder = 47
      Visible = False
      OnClick = btnEmailCopiarClick
    end
    object btnEmailAbrir: TcxButton [44]
      Left = 10000
      Top = 10000
      Width = 100
      Height = 25
      Caption = 'Abrir WebMail'
      TabOrder = 48
      Visible = False
      OnClick = btnEmailAbrirClick
    end
    object btnFollowUp_Carregar: TcxButton [45]
      Left = 361
      Top = 754
      Width = 150
      Height = 25
      Caption = 'Carregar Follow-Ups'
      TabOrder = 38
      OnClick = btnFollowUp_CarregarClick
    end
    object btnFollowUp_Consultar: TcxButton [46]
      Left = 517
      Top = 754
      Width = 150
      Height = 25
      Caption = 'Consultar Follow-Ups'
      TabOrder = 39
      OnClick = btnFollowUp_ConsultarClick
    end
    object btnCopiarNome: TcxButton [47]
      Left = 169
      Top = 496
      Width = 120
      Height = 25
      Caption = 'Copiar Nome'
      TabOrder = 33
      OnClick = mniNomeClick
    end
    object btnCopiarCpf: TcxButton [48]
      Left = 295
      Top = 496
      Width = 120
      Height = 25
      Caption = 'Copiar CPF'
      TabOrder = 34
      OnClick = mniCPFClick
    end
    object btnCopiarRenavam: TcxButton [49]
      Left = 421
      Top = 496
      Width = 120
      Height = 25
      Caption = 'Copiar RENAVAM'
      TabOrder = 35
      OnClick = mniRENAVAMClick
    end
    object btnCopiarPlaca: TcxButton [50]
      Left = 547
      Top = 496
      Width = 120
      Height = 25
      Caption = 'Copiar Placa'
      TabOrder = 36
      OnClick = mniPlacaClick
    end
    inherited lcgBaseModal: TdxLayoutGroup
      ItemIndex = 6
    end
    object lciNome: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Cliente:'
      Control = lbNome
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciProcurador: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Procurador:'
      Control = lbProcurador
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCondutor: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Condutor:'
      Control = lbCondutor
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciVeiculo: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Ve'#237'culo:'
      Control = lbVeiculo
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lcgDados: TdxLayoutGroup
      Parent = lcgBaseModal
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = 5
    end
    object lcgME: TdxLayoutGroup
      Parent = lcgDados
      AlignVert = avClient
      CaptionOptions.Text = 'Mandado Expedido'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 0
    end
    object lcgCB: TdxLayoutGroup
      Parent = lcgDados
      CaptionOptions.Text = 'Com Busca'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      Index = 1
    end
    object lcgCA: TdxLayoutGroup
      Parent = lcgDados
      CaptionOptions.Text = 'Carro Apreendido'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      Index = 2
    end
    object lcgRE: TdxLayoutGroup
      Parent = lcgDados
      CaptionOptions.Text = 'RENAJUD'
      ButtonOptions.Buttons = <>
      Index = 3
    end
    object lcgEE: TdxLayoutGroup
      Parent = lcgDados
      CaptionOptions.Text = 'Em Execu'#231#227'o'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 4
    end
    object lciME_Data: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Data'
      CaptionOptions.Layout = clTop
      Control = edtME_Data
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciME_Nro: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Nro'
      CaptionOptions.Layout = clTop
      Control = edtME_Nro
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciME_IDDecisao: TdxLayoutItem
      Parent = lcg2
      AlignHorz = ahClient
      CaptionOptions.Text = 'ID Decis'#227'o'
      CaptionOptions.Layout = clTop
      Control = edtME_IDDecisao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciME_ResumoDecisao: TdxLayoutItem
      Parent = lcg2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Resumo Decisao'
      CaptionOptions.Layout = clTop
      Control = edtME_ResumoDecisao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciME_Forum: TdxLayoutItem
      Parent = lcg3
      AlignHorz = ahClient
      CaptionOptions.Text = 'F'#243'rum'
      CaptionOptions.Layout = clTop
      Control = edtME_Forum
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciME_Oficial: TdxLayoutItem
      Parent = lcg3
      AlignHorz = ahClient
      CaptionOptions.Text = 'Oficial de Justi'#231'a'
      CaptionOptions.Layout = clTop
      Control = edtME_Oficial
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCB_Liberacao: TdxLayoutItem
      Parent = lcg4
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cboxCB_Liberacao
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 80
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciCB_Arquivado: TdxLayoutItem
      Parent = lcg4
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cboxCB_Arquivado
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 109
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCB_Data: TdxLayoutItem
      Parent = lcg5
      AlignHorz = ahClient
      CaptionOptions.Text = 'Data'
      CaptionOptions.Layout = clTop
      Control = edtCB_Data
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciClasseProcesso: TdxLayoutItem
      Parent = lcg5
      AlignHorz = ahClient
      CaptionOptions.Text = 'Classe Processo'
      CaptionOptions.Layout = clTop
      Control = edtCB_ClasseProcesso
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCB_Nro: TdxLayoutItem
      Parent = lcg5
      AlignHorz = ahClient
      CaptionOptions.Text = 'Nro. Processo'
      CaptionOptions.Layout = clTop
      Control = edtCB_Nro
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciCB_Forum: TdxLayoutItem
      Parent = lcg6
      AlignHorz = ahClient
      CaptionOptions.Text = 'F'#243'rum'
      CaptionOptions.Layout = clTop
      Control = edtCB_Forum
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciCB_Oficial: TdxLayoutItem
      Parent = lcg6
      AlignHorz = ahClient
      CaptionOptions.Text = 'Ofical de Justi'#231'a'
      CaptionOptions.Layout = clTop
      Control = edtCB_Oficial
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCA_Data: TdxLayoutItem
      Parent = lcg7
      AlignHorz = ahClient
      CaptionOptions.Text = 'Data'
      CaptionOptions.Layout = clTop
      Control = edtCA_Data
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciCA_Endereco: TdxLayoutItem
      Parent = lcg7
      AlignHorz = ahClient
      CaptionOptions.Text = 'Endere'#231'o'
      CaptionOptions.Layout = clTop
      Control = edtCA_Endereco
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCA_Motivo: TdxLayoutItem
      Parent = lcg8
      AlignHorz = ahClient
      CaptionOptions.Text = 'Motivo'
      CaptionOptions.Layout = clTop
      Control = edtCA_Motivo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciCA_Oficial: TdxLayoutItem
      Parent = lcg8
      AlignHorz = ahClient
      CaptionOptions.Text = 'Oficial de Justi'#231'a'
      CaptionOptions.Layout = clTop
      Control = edtCA_Oficial
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCA_Observacoes: TdxLayoutItem
      Parent = lcgCA
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'Observa'#231#245'es'
      CaptionOptions.Layout = clTop
      Control = mmoCA_Observacoes
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciRE_Data: TdxLayoutItem
      Parent = lcg9
      AlignHorz = ahClient
      CaptionOptions.Text = 'Data'
      CaptionOptions.Layout = clTop
      Control = edtRE_Data
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciRE_Forum: TdxLayoutItem
      Parent = lcg9
      AlignHorz = ahClient
      CaptionOptions.Text = 'F'#243'rum'
      CaptionOptions.Layout = clTop
      Control = edtRE_Forum
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciRE_IDDecisao: TdxLayoutItem
      Parent = lcg9
      AlignHorz = ahClient
      CaptionOptions.Text = 'ID Decis'#227'o'
      CaptionOptions.Layout = clTop
      Control = edtRE_IDDecisao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciRE_Observacoes: TdxLayoutItem
      Parent = lcgRE
      AlignVert = avClient
      CaptionOptions.Text = 'Observa'#231#245'es'
      CaptionOptions.Layout = clTop
      Control = mmoRE_Observacoes
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciEE_Bacenjud: TdxLayoutItem
      Parent = lcg10
      AlignVert = avBottom
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cboxEE_Bacenjud
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 70
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciEE_Data: TdxLayoutItem
      Parent = lcg10
      AlignHorz = ahClient
      CaptionOptions.Text = 'Data'
      CaptionOptions.Layout = clTop
      Control = edtEE_Data
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciEE_Forum: TdxLayoutItem
      Parent = lcg10
      AlignHorz = ahClient
      CaptionOptions.Text = 'F'#243'rum'
      CaptionOptions.Layout = clTop
      Control = edtEE_Forum
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciEE_IDDecisao: TdxLayoutItem
      Parent = lcg10
      AlignHorz = ahClient
      CaptionOptions.Text = 'ID Decis'#227'o'
      CaptionOptions.Layout = clTop
      Control = edtEE_IDDecisao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciEE_Observacao: TdxLayoutItem
      Parent = lcgEE
      AlignVert = avClient
      CaptionOptions.Text = 'Observa'#231#245'es'
      CaptionOptions.Layout = clTop
      Control = mmoEE_Observacao
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgME
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg2: TdxLayoutAutoCreatedGroup
      Parent = lcgME
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object lcg3: TdxLayoutAutoCreatedGroup
      Parent = lcgME
      LayoutDirection = ldHorizontal
      Index = 2
    end
    object lcg4: TdxLayoutAutoCreatedGroup
      Parent = lcgCB
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg5: TdxLayoutAutoCreatedGroup
      Parent = lcgCB
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object lcg6: TdxLayoutAutoCreatedGroup
      Parent = lcgCB
      LayoutDirection = ldHorizontal
      Index = 2
    end
    object lcg7: TdxLayoutAutoCreatedGroup
      Parent = lcgCA
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg8: TdxLayoutAutoCreatedGroup
      Parent = lcgCA
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object lcg9: TdxLayoutAutoCreatedGroup
      Parent = lcgRE
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg10: TdxLayoutAutoCreatedGroup
      Parent = lcgEE
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcgBase: TdxLayoutGroup
      Parent = lcgBaseModal
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = 6
    end
    object lcgFollowUps: TdxLayoutGroup
      Parent = lcgBase
      CaptionOptions.Text = 'Follow-Ups'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      Index = 0
    end
    object lcgObservacoes: TdxLayoutGroup
      Parent = lcgBase
      CaptionOptions.Text = 'Observa'#231#245'es'
      ButtonOptions.Buttons = <>
      Index = 1
    end
    object lcgFinanceiro: TdxLayoutGroup
      Parent = lcgBase
      CaptionOptions.Text = 'Financeiro'
      ButtonOptions.Buttons = <>
      Index = 2
    end
    object lcgMaisDados: TdxLayoutGroup
      Parent = lcgBase
      CaptionOptions.Text = 'Mais Dados'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 3
    end
    object lciCadastroPush: TdxLayoutItem
      Parent = lcg11
      AlignHorz = ahLeft
      AlignVert = avTop
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cboxCadastroPush
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 108
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciFollowUp: TdxLayoutItem
      Parent = lcgFollowUps
      AlignVert = avClient
      CaptionOptions.Text = 'cxTreeList1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = GridFollowUp
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg11: TdxLayoutAutoCreatedGroup
      Parent = lcgFollowUps
      AlignVert = avTop
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lciObservacoes: TdxLayoutItem
      Parent = lcgObservacoes
      AlignVert = avClient
      CaptionOptions.Text = 'cxMemo1'
      CaptionOptions.Visible = False
      Control = mmoObservacoes
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciSituacao: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Situa'#231#245'es'
      CaptionOptions.Layout = clTop
      Control = lstSituacao
      ControlOptions.OriginalHeight = 97
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciFechar: TdxLayoutItem
      Parent = lcg12
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnFechar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciOk: TdxLayoutItem
      Parent = lcg12
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnOk
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lcg12: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      LayoutDirection = ldHorizontal
      Index = 7
    end
    object lciTipo: TdxLayoutItem
      Parent = lcg13
      CaptionOptions.Text = 'Tipo'
      CaptionOptions.Layout = clTop
      Control = cbbTipo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciVencimento: TdxLayoutItem
      Parent = lcg13
      CaptionOptions.Text = 'Vencimento'
      CaptionOptions.Layout = clTop
      Control = cbbVencimento
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciFiltrar: TdxLayoutItem
      Parent = lcg13
      AlignVert = avBottom
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnFiltrar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lcg13: TdxLayoutAutoCreatedGroup
      Parent = lcgFinanceiro
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lciFinanceiro: TdxLayoutItem
      Parent = lcgFinanceiro
      AlignVert = avClient
      CaptionOptions.Text = 'cxTreeList1'
      CaptionOptions.Visible = False
      Control = GridFinanceiro
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciFones: TdxLayoutItem
      Parent = lcgMaisDados
      AlignVert = avClient
      CaptionOptions.Text = 'Telefones'
      CaptionOptions.Layout = clTop
      Control = GridTelefone
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciEmail: TdxLayoutItem
      Parent = lcg14
      AlignHorz = ahClient
      CaptionOptions.Text = 'E-mail'
      CaptionOptions.Layout = clTop
      Control = edtEmail
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciEmailCopiar: TdxLayoutItem
      Parent = lcg14
      AlignVert = avBottom
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnEmailCopiar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 100
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciEmailAbrir: TdxLayoutItem
      Parent = lcg14
      AlignVert = avBottom
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnEmailAbrir
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 100
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lcg14: TdxLayoutAutoCreatedGroup
      Parent = lcgMaisDados
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object lciFollowUp_Carregar: TdxLayoutItem
      Parent = lcg15
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnFollowUp_Carregar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 150
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciFollowUp_Consultar: TdxLayoutItem
      Parent = lcg15
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnFollowUp_Consultar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 150
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg15: TdxLayoutAutoCreatedGroup
      Parent = lcgFollowUps
      LayoutDirection = ldHorizontal
      Index = 2
    end
    object lciCopiarNome: TdxLayoutItem
      Parent = lcg11
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCopiarNome
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 120
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCopiarCpf: TdxLayoutItem
      Parent = lcg11
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCopiarCpf
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 120
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciCopiarRenavam: TdxLayoutItem
      Parent = lcg11
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCopiarRenavam
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 120
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciCopiarPlaca: TdxLayoutItem
      Parent = lcg11
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCopiarPlaca
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 120
      ControlOptions.ShowBorder = False
      Index = 4
    end
  end
end
