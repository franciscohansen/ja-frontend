unit untPainelAdministracao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseGeral, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutControlAdapters, Vcl.Menus, Vcl.StdCtrls, cxButtons;

type
  TfrmPainelAdministracao = class(TfrmBaseGeral)
    btnBancos: TcxButton;
    lciBancos: TdxLayoutItem;
    btnDocumentos: TcxButton;
    lciDocumentos: TdxLayoutItem;
    btnConfiguracoes: TcxButton;
    lciConfiguracoes: TdxLayoutItem;
    btnUsuarios: TcxButton;
    lciUsuarios: TdxLayoutItem;
    btnCorrigeJuridico: TcxButton;
    lciCorrigeJuridico: TdxLayoutItem;
    procedure btnBancosClick(Sender: TObject);
    procedure btnDocumentosClick(Sender: TObject);
    procedure btnConfiguracoesClick(Sender: TObject);
    procedure btnUsuariosClick(Sender: TObject);
    procedure btnCorrigeJuridicoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
  untLst_Bancos, untLst_Documentos, untConfiguracoes, Dao.Base,
  Dao.Configuracao, untUsuarios, Dao.Juridico;

{$R *.dfm}

procedure TfrmPainelAdministracao.btnBancosClick(Sender: TObject);
var
  frm : TfrmLst_Bancos;
begin
  inherited;
  frm := TfrmLst_Bancos.Create( Self.Owner );
  frm.Show;
end;

procedure TfrmPainelAdministracao.btnConfiguracoesClick(Sender: TObject);
var
  frm : TfrmConfiguracoes;
begin
  inherited;
  frm := TfrmConfiguracoes.Create( Self.Owner );
  frm.Update( TConfiguracaoDao.New.Dao.FindById( 1 ) );
end;

procedure TfrmPainelAdministracao.btnCorrigeJuridicoClick(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  try
    TJuridicoDao.New.CorrigeJuridico;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmPainelAdministracao.btnDocumentosClick(Sender: TObject);
var
  frm : TfrmLst_Documentos;
begin
  inherited;
  frm := TfrmLst_Documentos.Create( Self.Owner );
  frm.Show;
end;

procedure TfrmPainelAdministracao.btnUsuariosClick(Sender: TObject);
var
  frm : TfrmUsuarios;
begin
  inherited;
  frm := TfrmUsuarios.Create( Self );
  try
    frm.ShowModal;
  finally
    frm.Release;
  end;
end;

end.
