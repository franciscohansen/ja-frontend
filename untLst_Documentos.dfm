inherited frmLst_Documentos: TfrmLst_Documentos
  Caption = 'frmLst_Documentos'
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    inherited GridBase: TcxTreeList
      object GridBase_Descricao: TcxTreeListColumn
        Caption.Text = 'Descri'#231#227'o'
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Tipo: TcxTreeListColumn
        Caption.Text = 'Tipo'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
  end
end
