unit untUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseModal, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, cxCustomData, cxStyles, dxScrollbarAnnotations, cxTL,
  cxTLdxBarBuiltInMenu, cxInplaceContainer, dxLayoutControlAdapters, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxCheckBox, Json.Interfaces;

type
  TfrmUsuarios = class(TfrmBaseModal)
    GridUsuarios: TcxTreeList;
    lciUsuarios: TdxLayoutItem;
    btnSalvar: TcxButton;
    lciSalvar: TdxLayoutItem;
    btnFechar: TcxButton;
    lciFechar: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    GridU_Nome: TcxTreeListColumn;
    GridU_Vendas: TcxTreeListColumn;
    GridU_Juridico: TcxTreeListColumn;
    GridU_Negociacoes: TcxTreeListColumn;
    GridU_Json: TcxTreeListColumn;
    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
  Json.Base, Dao.Base, Util.Lists;

{$R *.dfm}

procedure TfrmUsuarios.btnFecharClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmUsuarios.btnSalvarClick(Sender: TObject);
var
  i: Integer;
  n : TcxTreeListNode;
  j : IJson;
begin
  inherited;
  for i := 0 to GridUsuarios.Count -1 do begin
    n := GridUsuarios.Items[ i ];
    j := TJson.New( n.Values[ GridU_Json.ItemIndex ] )
      .AddOrSetBoolean( 'acessaVendas', n.Values[ GridU_Vendas.ItemIndex ] )
      .AddOrSetBoolean( 'acessaJuridico', n.Values[ GridU_Juridico.ItemIndex ] )
      .AddOrSetBoolean( 'acessaNegociacoes', n.Values[ GridU_Negociacoes.ItemIndex ] );
    TDao.New( '/usuario' ).Save( j );
  end;
  ModalResult := mrOk;
end;

procedure TfrmUsuarios.FormShow(Sender: TObject);
var
  l : IList<IJson>;
  j : IJson;
  i : Integer;
begin
  inherited;
  try
    GridUsuarios.BeginUpdate;
    GridUsuarios.Clear;
    l := TList<IJson>.New( TDao.New( '/usuario' ).FindAll(0, 0) );
    for i := 0 to l.Count -1 do begin
      j := l.Item( i );
      with GridUsuarios.Add do begin
        Values[ GridU_Nome.ItemIndex        ] := j.Item( 'nome' ).AsString;
        Values[ GridU_Vendas.ItemIndex      ] := j.Item( 'acessaVendas' ).AsBoolean;
        Values[ GridU_Juridico.ItemIndex    ] := j.Item( 'acessaJuridico' ).AsBoolean;
        Values[ GridU_Negociacoes.ItemIndex ] := j.Item( 'acessaNegociacoes' ).AsBoolean;
        Values[ GridU_Json.ItemIndex        ] := j.AsJsonString;
      end;
    end;
  finally
    GridUsuarios.EndUpdate;
  end;
end;

end.
