unit Forms.Interfaces;

interface

uses
  System.SysUtils, System.Classes, System.Variants, Json.Interfaces;
type
  IListCallback = interface
    ['{2784A6CF-CD06-480E-A441-20806F8DF9DF}']
    function DoCallback( AJson : IJson ) : IListCallBack;
  end;
  IFormCadastro = interface
    ['{E656EF91-9F22-40A8-91BF-BD1A79ADD875}']
    function Insert : IFormCadastro;
    function Update( AJson : IJson ) : IFormCadastro;
    function Open( AJson : IJson ) : IFormCadastro;
    function Callback( ACallback : IListCallback ) : IFormCadastro;
  end;

implementation

end.
