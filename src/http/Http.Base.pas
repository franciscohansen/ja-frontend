unit Http.Base;

interface

uses
  Http.Interfaces, REST.Client, System.SysUtils, System.JSON, IPPeerClient, REST.Types, System.Classes,
   Util.Lists, REST.Authenticator.Basic;

(***********************************************************************
 *                                                                     *
 *  Essas classes s�o SEALED porque s�o imut�veis (exceto a TUrlParam) *
 *  e devem permanecer assim. Todo o setter da classe deve permanecer  *
 *  no seu construtor;                                                 *
 *                                                                     *
 ***********************************************************************)

type
  THttpRequest = class sealed( TInterfacedObject, IHttpRequest )
  private
    FUrl: IUrl;
    procedure PoeParams( Req: TRESTRequest );
    constructor Create( AUrl: IUrl );
    function ConvertMethod( AMethod: THttpMethod ): TRESTRequestMethod;
    function Execute( AMethod: THttpMethod ): IHttpResponse;
  public
    class function New( Url: IUrl ): IHttpRequest;
    function DoGet: IHttpResponse;
    function DoPost: IHttpResponse;
    function DoPut: IHttpResponse;
    function DoDelete: IHttpResponse;
  end;
  THttpResponse = class sealed( TInterfacedObject, IHttpResponse )
  private
    FCode: integer;
    FValue: string;
    constructor Create( Code: integer; Value: String );
  public
    class function New( Code: integer; Value: String ): IHttpResponse;
    function Code: integer;
    function Status : THttpStatus;
    function Value: string;
    function ToString: string;
  end;

  TUrl = class sealed( TInterfacedObject, IUrl )
  private
    FParams: IUrlParams;
    FUrl: string;
    FAuth : TCustomAuthenticator;
    constructor Create( AUrl: string; Params: IUrlParams; AAuth : TCustomAuthenticator );
  public
    class function New( AUrl: string; Params: IUrlParams; AAuth : TCustomAuthenticator = nil ): IUrl; overload;
    class function New( AUrl: string ): IUrl; overload;
    destructor Destroy; override;
    function Value: string;
    function Params: IUrlParams;
    function Authenticator : TCustomAuthenticator;
  end;
  TUrlParams = class( TInterfacedObject, IUrlParams )
  private
    FParamsWithSlash: IList<String>;
    FParamsWithName,
    FHeader: IMap<String,String>;
    constructor Create;
  public
    class function New: IUrlParams;
//    class function NewWithToken( ACfg : IConfiguracao ): IUrlParams; overload;
    destructor Destroy; override;
    function AddParam( s: string ): IUrlParams; overload;
    function AddParam( Name, Value: string ): IUrlParams; overload;
    function RemoveParam( Name : string ) :IUrlParams;
    function AddHeader( Key: string; Value: string ): IUrlParams;
    function RemoveHeader( Key : string ) : IUrlParams;
    function GetParams: IMap<String,String>;
    function GetParamsToUrl: string;
    function IsParamsToUrl: boolean;
    function GetHeader: IMap<String,String>;
  end;

implementation

{ THttpRequest }

uses  Cfg.Constantes, Exceptions;

function THttpRequest.ConvertMethod(AMethod: THttpMethod): TRESTRequestMethod;
begin
  case AMEthod of
    httpGET    : Result := rmGET;
    httpPOST   : Result := rmPOST;
    httpPUT    : Result := rmPUT;
    httpDELETE : Result := rmDELETE;
  else
    Raise Exception.Create( 'HttpMethod ' + Integer( AMethod ).ToString + ' n�o implementado' );
  end;
end;

constructor THttpRequest.Create(AUrl: IUrl);
begin
  FUrl := AUrl;
end;

function THttpRequest.DoDelete: IHttpResponse;
begin
  Result := Execute( httpDELETE );
end;

function THttpRequest.DoGet: IHttpResponse;
begin
  Result := Execute( httpGET );
end;

function THttpRequest.DoPost: IHttpResponse;
begin
  Result := Execute( httpPOST );
end;

function THttpRequest.DoPut: IHttpResponse;
begin
  Result := Execute( httpPUT );
end;

function THttpRequest.Execute(AMethod: THttpMethod): IHttpResponse;
var
  AClient: TRestClient;
  Req: TRESTRequest;
  sKey, sUrl: string;
  wET : TWinInetExceptionType;
begin
  Req := nil;
  AClient := TRestClient.Create( nil );
  try
    sUrl := FUrl.Value;
    if not sUrl.StartsWith( 'http://' ) then
      sUrl := 'http://' + sUrl;
    AClient.BaseURL := sUrl;
    if Assigned( FUrl.Authenticator ) then
      AClient.Authenticator := FUrl.Authenticator;

    Req := TRestRequest.Create( nil );
    Req.Client := AClient;
    Req.Method := ConvertMethod( AMethod );
    Req.Timeout := 300000; //60000;

    if FUrl.Params <> nil then begin
      if FUrl.Params.GetHeader <> nil then begin
        for sKey in FUrl.Params.GetHeader.KeysToArray do begin
          Req.AddParameter(
            sKey,
//            FUrl.Params.GetHeader.Item( sKey ),
            UTF8Encode( FUrl.Params.GetHeader.Item( sKey ) ),
//            TConverter.EncodeUtf8( FUrl.Params.GetHeader.Items[ sKey ] ),
            pkHTTPHEADER
          );
        end;
      end;
    end;

    if AMethod in [ httpGET, httpDELETE ] then begin
      if ( FUrl.Params <> nil ) and FUrl.Params.IsParamsToUrl then
        AClient.BaseUrl := AClient.BaseUrl + FUrl.Params.GetParamsToUrl
      else begin
        AClient.BaseURL := AClient.BaseURL;
        PoeParams( Req );
      end;
    end
    else begin
      AClient.ContentType := 'application/x-www-formurlencoded';
      PoeParams( Req );
//      if FUrl.Params.GetParams <> nil then begin
//        for sKey in FUrl.Params.GetParams.Keys do begin
//          Req.AddParameter(
//            sKey,
//            TConverter.EncodeUtf8( FUrl.Params.GetParams.Items[ sKey ] )
//          );
//        end;
//      end;
    end;

    try
      Req.Execute;
      Result := THttpResponse.New(
        Req.Response.StatusCode,
        Req.Response.Content
      );
    except
      on e: exception do begin
        if e.ClassName = 'ERESTException' then begin // se � erro "esperado" manda como HttpResponse;
          if ( Req.Response.StatusCode = 0 ) and ( TWinInetExceptionType.FromString( E.Message ) <> WININET_UNKNOWN_ERROR ) then begin

            wET := TWinInetExceptionType.FromString( E.Message );
            case wET of
              INTERNET_CANNOT_CONNECT: begin
                raise WinInetError.Create( INTERNET_CANNOT_CONNECT,
                    'Houve um erro ao conectar-se ao servidor do H�bil 10.' + #13#10 +
                    'Verifique se o mesmo encontra-se em execu��o e tente novamente.'   );
              end;
              INTERNET_TIMEOUT: begin
                raise WinInetError.Create( INTERNET_TIMEOUT,
                    'O Servidor demorou muito tempo para responder. ' + #13#10 +
                    'Tente novamente.'
                );
              end
            else
              raise WinInetError.Create( TWinInetExceptionType.FromString( E.Message ), E.Message )
            end;
//            Result := THttpResponse.New(
//              TWinInetExceptionType.FromString( E.Message ).Code,
//              E.Message
//            );
          end
          else begin
            Result := THttpResponse.New(
              Req.Response.StatusCode,
              Req.Response.Content
            );
          end;
        end
        else // sen�o estoura o erro e quem for usar que trate;
          Raise e;
      end;
    end;
    case Result.Status  of
      stCONFLICT: raise EUserAlreadyConnectedError.Create( 'Seu usu�rio j� est� conectado em outra esta��o!' );
      stLOCKED  : raise ELicenceNumberError.Create( 'O N�mero de usu�rios logados ultrapassou o n�mero de licen�as do sistema!' );
    end;
  finally
    AClient.Free;
    if Assigned( Req ) then
      Req.Free;
  end;
end;

class function THttpRequest.New(Url: IUrl): IHttpRequest;
begin
  Result := THttpRequest.Create( Url );
end;

procedure THttpRequest.PoeParams( Req: TRESTRequest );
var
  sKey: string;

  function EncodeUtf8( s: string ): string;
  var
    UTF8Str : UTF8String;
    I : Integer;
  begin
    UTF8Str := UTF8String( s );
    SetLength( Result, Length( UTF8Str ) );
    for i := 1 to Length( UTF8Str ) do
      Result[ I ] := Char( Ord( UTF8Str[ I ] ) );
  end;

begin
  if ( FUrl.Params <> nil ) and ( FUrl.Params.GetParams <> nil ) then begin
    for sKey in FUrl.Params.GetParams.KeysToArray do begin
      Req.AddParameter(
        sKey,
        EncodeUtf8( FUrl.Params.GetParams.Item( sKey ) )
      );
    end;
  end;
end;

{ THttpResponse }

function THttpResponse.Code: integer;
begin
  Result := FCode;
end;

constructor THttpResponse.Create(Code: integer; Value: String);
begin
  inherited Create;
  FCode := Code;
  FValue := Value;
end;

class function THttpResponse.New(Code: integer;
  Value: String): IHttpResponse;
begin
  Result := THttpResponse.Create( Code, Value );
end;

function THttpResponse.Status: THttpStatus;
begin
  Result := THttpStatus.FromCode( FCode );
end;

function THttpResponse.ToString: string;
begin
  Result := 'Code: ' + FCode.ToString + ', Value: ' + FValue;
end;

function THttpResponse.Value: string;
begin
  Result := FValue;
end;

{ TUrl }

function TUrl.Authenticator: TCustomAuthenticator;
begin
  Result := FAuth;
end;

constructor TUrl.Create( AUrl: string; Params: IUrlParams; AAuth : TCustomAuthenticator );
begin
  FUrl    := AUrl;
  FParams := Params;
  FAuth   := AAuth;
end;

destructor TUrl.Destroy;
begin
  if Assigned( FAuth ) then
    FAuth.Free;
  inherited;
end;

class function TUrl.New(AUrl: string; Params: IUrlParams; AAuth : TCustomAuthenticator): IUrl;
begin
  Result := TUrl.Create( AUrl, Params, AAuth );
end;

class function TUrl.New(AUrl: string): IUrl;
begin
  Result := TUrl.Create( AUrl, nil, nil );
end;

function TUrl.Params: IUrlParams;
begin
  Result := FParams;
end;

function TUrl.Value: string;
begin
  Result := FUrl;
end;

{ TUrlParams }

function TUrlParams.AddParam(s: string): IUrlParams;
begin
  Result := Self;
  if FParamsWithSlash = nil then
    FParamsWithSlash := TList<String>.New;
  FParamsWithSlash.Add( s );
end;

function TUrlParams.AddParam(Name, Value: string): IUrlParams;
begin
  Result := Self;
  if FParamsWithName = nil then
    FParamsWithName := TMap<String,String>.New;
  FParamsWithName.AddOrReplace( Name, Value );
end;

function TUrlParams.AddHeader(Key, Value: string): IUrlParams;
begin
  Result := Self;
  if ( FHeader = nil ) then
    FHeader := TMap<String,String>.New;
  FHeader.AddOrReplace( Key, Value );
end;

constructor TUrlParams.Create;
begin
  inherited Create;
//  FParamsWithSlash := nil;
//  FParamsWithName := nil;
//  FHeader := nil;
end;

destructor TUrlParams.Destroy;
begin
  {if Assigned( FParamsWithSlash ) then begin
    FParamsWithSlash.TrimExcess;
    FParamsWithSlash.Free;
  end;
  if Assigned( FHeader ) then begin
    FHeader.TrimExcess;
    FHeader.Free;
  end;
  if Assigned( FParamsWithName ) then begin
    FParamsWithName.TrimExcess;
    FParamsWithName.Free;
  end;}
end;

function TUrlParams.GetParams: IMap<String, String>;
begin
  Result := FParamsWithName;
end;

function TUrlParams.GetParamsToUrl: string;
var
  s: string;
begin
  Result := '';
  if FParamsWithSlash <> nil then begin
    for s in FParamsWithSlash.ToArray do
      Result := Result + '/' + s;
  end
  else begin
    if FParamsWithName <> nil then begin
      Result := Result + '?';
      for s in FParamsWithName.KeysToArray do
        Result := Result + s + '=' + FParamsWithName.Item( s ) + '&';
      Delete( Result, Length( Result ), 1 );
    end;
  end;
  Result := Result.Trim;
end;

function TUrlParams.IsParamsToUrl: boolean;
begin
  Result := GetParams = nil;
end;

function TUrlParams.GetHeader: IMap<String,String>;
begin
  Exit( FHeader );
end;

class function TUrlParams.New: IUrlParams;
begin
  Result := TUrlParams.Create;
end;

{class function TUrlParams.NewWithToken( ACfg : IConfiguracao ): IUrlParams;
begin
  Result := TUrlParams.New
    .AddHeader( TAppConstantes.Token, ACfg.Token );
end;}

function TUrlParams.RemoveHeader(Key: string): IUrlParams;
begin
  Result := Self;
  if Assigned( FHeader ) and FHeader.ContainsKey( Key ) then
    FHeader.Delete( Key );
end;

function TUrlParams.RemoveParam(Name: string): IURLParams;
begin
  Result := Self;
  if Assigned( FParamsWithName ) and FParamsWithName.ContainsKey ( Name ) then
    FParamsWithName.Delete( Name );
end;

end.
