unit Http.Interfaces;

interface

uses
  System.Classes, SysUtils, Util.Lists, Rest.Client;

type
  THttpMethod = ( httpGET, httpPOST, httpPUT, httpDELETE );
  THttpStatus = (
                  {$REGION ' STATUS ' }
                //1xx - INFORMATIONAL
                stCONTINUE,                           //100
                stSWITCHING_PROTOCOLS,                //101
                stPROCESSING,                         //102
                //2xx - SUCCESSS
                stOK,                                 //200
                stCREATED,                            //201
                stACCEPTED,                           //202
                stNON_AUTHORITATIVE_INFO,             //203
                stNO_CONTENT,                         //204
                stRESET_CONTENT,                      //205
                stPARTIAL_CONTENT,                    //206
                stMULTI_STATUS,                       //207
                stALREADY_REPORTED,                   //208
                stIM_USED,                            //226
                //3xx REDIRECTION
                stMULTIPLE_CHOICES,                   //300
                stMOVED_PERMANENTLY,                  //301
                stFOUND,                              //302
                stSEE_OTHER,                          //303
                stNOT_MODIFIED,                       //304
                stUSE_PROXY,                          //305
                stTEMPORARY_REDIRECT,                 //307
                stPERMANENT_REDIRECT,                 //308
                //4xx - CLIENT ERROR
                stBAD_REQUEST,                        //400
                stUNAUTHORIZED,                       //401
                stPAYMENT_REQUIRED,                   //402
                stFORBIDDEN,                          //403
                stNOT_FOUND,                          //404
                stMETHOD_NOT_ALLOWED,                 //405
                stNOT_ACCEPTABLE,                     //406
                stPROXY_AUTHENTICATION_REQUIRED,      //407
                stREQUEST_TIMEOUT,                    //408
                stCONFLICT,                           //409
                stGONE,                               //410
                stLENGTH_REQUIRED,                    //411
                stPRECONDITION_FAILED,                //412
                stPAYLOAD_TOO_LARGE,                  //413
                stREQUEST_URI_TOO_LONG,               //414
                stUNSUPPORTED_MEDIA_TYPE,             //415
                stREQUESTED_RANGE_NOT_SATISFIABLE,    //416
                stEXPECTATION_FAILED,                 //417
                stIM_A_TEAPOT,                        //418
                stMISDIRECTED_REQUEST,                //421
                stUNPROCESSABLE_ENTITY,               //422
                stLOCKED,                             //423
                stFAILED_DEPENDENCY,                  //424
                stUPGRADE_REQUIRED,                   //426
                stPRECONDITION_REQUIRED,              //428
                stTOO_MANY_REQUESTS,                  //429
                stREQUEST_HEADER_FIELDS_TOO_LARGE,    //431
                stCONNECTION_CLOSED_WITHOUT_RESPONSE, //444
                stUNAVAILABLE_FOR_LEGAL_REASONS,      //451
                stCLIENT_CLOSED_REQUEST,              //499
                //5xx - SERVER ERROR
                stINTERNAL_SERVER_ERROR,              //500
                stNOT_IMPLEMENTED,                    //501
                stBAD_GATEWAY,                        //502
                stSERVICE_UNAVAILABLE,                //503
                stGATEWAY_TIMEOUT,                    //504
                stHTTP_VERSION_NOT_SUPPORTED,         //505
                stVARIANT_ALSO_NEGOTIATES,            //506
                stINSUFFICIENT_STORAGE,               //507
                stLOOP_DETECTED,                      //508
                stNOT_EXTENDED,                       //510
                stNETWORK_AUTHENTICATION_REQUIRED,    //511
                stNETWORK_CONNECT_TIMEOUT_ERROR       //599
                {$ENDREGION}
                );

  IHttpResponse = interface
    ['{5901F785-2AA6-4C1D-B4C8-A2D36CE6A9F1}']
    function Code: integer;
    function Status : THttpStatus;
    function Value: string;
    function ToString: string;
  end;

  IHttpRequest = interface
    ['{706DFA36-E6D2-4A2A-B91C-563FB9C6F58F}']
    function Execute( AMethod: THttpMethod ): IHttpResponse;
    function DoGet: IHttpResponse;
    function DoPost: IHttpResponse;
    function DoPut: IHttpResponse;
    function DoDelete: IHttpResponse;
  end;

  IUrlParams = interface
    ['{616DD287-7192-433C-A8A1-86D1A0129A85}']
    function AddParam( s: string ): IUrlParams; overload;
    function AddParam( Name, Value: string ): IUrlParams; overload;
    function RemoveParam( Name : string ) :IUrlParams;
    function AddHeader( Key: string; Value: string ): IUrlParams;
    function RemoveHeader( Key : string ) : IUrlParams;
    function GetParams: IMap<String,String>;
    function GetParamsToUrl: string;
    function GetHeader: IMap<String,String>;
    function IsParamsToUrl: boolean;
  end;

  IUrl = interface
    ['{B15D97E3-8EA2-45F2-B6DF-88817176F7EC}']
    function Value: string;
    function Params: IUrlParams;
    function Authenticator : TCustomAuthenticator;
  end;

  THttpStatusHelper = record helper for THttpStatus
  public
    function Code : Integer;
    class function FromCode( ACode : Integer ) : THttpStatus; static;
  end;

  IIndyRequest = interface
  ['{337663E3-F8D9-4CA9-BA4E-CC8BAEEBE32D}']
    function URL( URL : string ) : IIndyRequest;
    function Method( Method : THttpMethod ) : IIndyRequest;
    function Header( Key, Value : String ) : IIndyRequest;
    function FieldParam( Key, Value : string; ContentType : String = '' ) : IIndyRequest;
    function FileParam( Key, Value : string; ContentType : string = '' ) : IIndyRequest;
    function Execute : IIndyRequest;
    function Response : IHttpResponse;
    function ResponseStream : TStream;
  end;

implementation
  uses
    Exceptions;
{ THttpStatusHelper }

function THttpStatusHelper.Code: Integer;
begin
  case Self of
    stCONTINUE                          : Exit( 100 );
    stSWITCHING_PROTOCOLS               : Exit( 101 );
    stPROCESSING                        : Exit( 102 );
    stOK                                : Exit( 200 );
    stCREATED                           : Exit( 201 );
    stACCEPTED                          : Exit( 202 );
    stNON_AUTHORITATIVE_INFO            : Exit( 203 );
    stNO_CONTENT                        : Exit( 204 );
    stRESET_CONTENT                     : Exit( 205 );
    stPARTIAL_CONTENT                   : Exit( 206 );
    stMULTI_STATUS                      : Exit( 207 );
    stALREADY_REPORTED                  : Exit( 208 );
    stIM_USED                           : Exit( 226 );
    stMULTIPLE_CHOICES                  : Exit( 300 );
    stMOVED_PERMANENTLY                 : Exit( 301 );
    stFOUND                             : Exit( 302 );
    stSEE_OTHER                         : Exit( 303 );
    stNOT_MODIFIED                      : Exit( 304 );
    stUSE_PROXY                         : Exit( 305 );
    stTEMPORARY_REDIRECT                : Exit( 307 );
    stPERMANENT_REDIRECT                : Exit( 308 );
    stBAD_REQUEST                       : Exit( 400 );
    stUNAUTHORIZED                      : Exit( 401 );
    stPAYMENT_REQUIRED                  : Exit( 402 );
    stFORBIDDEN                         : Exit( 403 );
    stNOT_FOUND                         : Exit( 404 );
    stMETHOD_NOT_ALLOWED                : Exit( 405 );
    stNOT_ACCEPTABLE                    : Exit( 406 );
    stPROXY_AUTHENTICATION_REQUIRED     : Exit( 407 );
    stREQUEST_TIMEOUT                   : Exit( 408 );
    stCONFLICT                          : Exit( 409 );
    stGONE                              : Exit( 410 );
    stLENGTH_REQUIRED                   : Exit( 411 );
    stPRECONDITION_FAILED               : Exit( 412 );
    stPAYLOAD_TOO_LARGE                 : Exit( 413 );
    stREQUEST_URI_TOO_LONG              : Exit( 414 );
    stUNSUPPORTED_MEDIA_TYPE            : Exit( 415 );
    stREQUESTED_RANGE_NOT_SATISFIABLE   : Exit( 416 );
    stEXPECTATION_FAILED                : Exit( 417 );
    stIM_A_TEAPOT                       : Exit( 418 );
    stMISDIRECTED_REQUEST               : Exit( 421 );
    stUNPROCESSABLE_ENTITY              : Exit( 422 );
    stLOCKED                            : Exit( 423 );
    stFAILED_DEPENDENCY                 : Exit( 424 );
    stUPGRADE_REQUIRED                  : Exit( 426 );
    stPRECONDITION_REQUIRED             : Exit( 428 );
    stTOO_MANY_REQUESTS                 : Exit( 429 );
    stREQUEST_HEADER_FIELDS_TOO_LARGE   : Exit( 431 );
    stCONNECTION_CLOSED_WITHOUT_RESPONSE: Exit( 444 );
    stUNAVAILABLE_FOR_LEGAL_REASONS     : Exit( 451 );
    stCLIENT_CLOSED_REQUEST             : Exit( 499 );
    stINTERNAL_SERVER_ERROR             : Exit( 500 );
    stNOT_IMPLEMENTED                   : Exit( 501 );
    stBAD_GATEWAY                       : Exit( 502 );
    stSERVICE_UNAVAILABLE               : Exit( 503 );
    stGATEWAY_TIMEOUT                   : Exit( 504 );
    stHTTP_VERSION_NOT_SUPPORTED        : Exit( 505 );
    stVARIANT_ALSO_NEGOTIATES           : Exit( 506 );
    stINSUFFICIENT_STORAGE              : Exit( 507 );
    stLOOP_DETECTED                     : Exit( 508 );
    stNOT_EXTENDED                      : Exit( 510 );
    stNETWORK_AUTHENTICATION_REQUIRED   : Exit( 511 );
    stNETWORK_CONNECT_TIMEOUT_ERROR     : Exit( 599 );
  else
    raise EEnumNotImplementedError<THttpStatus>.Create( Ord( Self ) );
  end;
end;

class function THttpStatusHelper.FromCode(ACode: Integer): THttpStatus;
begin
  case ACode of
    100: Exit( stCONTINUE );
    101: Exit( stSWITCHING_PROTOCOLS );
    102: Exit( stPROCESSING );
    200: Exit( stOK );
    201: Exit( stCREATED );
    202: Exit( stACCEPTED );
    203: Exit( stNON_AUTHORITATIVE_INFO );
    204: Exit( stNO_CONTENT );
    205: Exit( stRESET_CONTENT );
    206: Exit( stPARTIAL_CONTENT );
    207: Exit( stMULTI_STATUS );
    208: Exit( stALREADY_REPORTED );
    226: Exit( stIM_USED );
    300: Exit( stMULTIPLE_CHOICES );
    301: Exit( stMOVED_PERMANENTLY );
    302: Exit( stFOUND );
    303: Exit( stSEE_OTHER );
    304: Exit( stNOT_MODIFIED );
    305: Exit( stUSE_PROXY );
    307: Exit( stTEMPORARY_REDIRECT );
    308: Exit( stPERMANENT_REDIRECT );
    400: Exit( stBAD_REQUEST );
    401: Exit( stUNAUTHORIZED );
    402: Exit( stPAYMENT_REQUIRED );
    403: Exit( stFORBIDDEN );
    404: Exit( stNOT_FOUND );
    405: Exit( stMETHOD_NOT_ALLOWED );
    406: Exit( stNOT_ACCEPTABLE );
    407: Exit( stPROXY_AUTHENTICATION_REQUIRED );
    408: Exit( stREQUEST_TIMEOUT );
    409: Exit( stCONFLICT );
    410: Exit( stGONE );
    411: Exit( stLENGTH_REQUIRED );
    412: Exit( stPRECONDITION_FAILED );
    413: Exit( stPAYLOAD_TOO_LARGE );
    414: Exit( stREQUEST_URI_TOO_LONG );
    415: Exit( stUNSUPPORTED_MEDIA_TYPE );
    416: Exit( stREQUESTED_RANGE_NOT_SATISFIABLE );
    417: Exit( stEXPECTATION_FAILED );
    418: Exit( stIM_A_TEAPOT );
    421: Exit( stMISDIRECTED_REQUEST );
    422: Exit( stUNPROCESSABLE_ENTITY );
    423: Exit( stLOCKED );
    424: Exit( stFAILED_DEPENDENCY );
    426: Exit( stUPGRADE_REQUIRED );
    428: Exit( stPRECONDITION_REQUIRED );
    429: Exit( stTOO_MANY_REQUESTS );
    431: Exit( stREQUEST_HEADER_FIELDS_TOO_LARGE );
    444: Exit( stCONNECTION_CLOSED_WITHOUT_RESPONSE );
    451: Exit( stUNAVAILABLE_FOR_LEGAL_REASONS );
    499: Exit( stCLIENT_CLOSED_REQUEST );
    500: Exit( stINTERNAL_SERVER_ERROR );
    501: Exit( stNOT_IMPLEMENTED );
    502: Exit( stBAD_GATEWAY );
    503: Exit( stSERVICE_UNAVAILABLE );
    504: Exit( stGATEWAY_TIMEOUT );
    505: Exit( stHTTP_VERSION_NOT_SUPPORTED );
    506: Exit( stVARIANT_ALSO_NEGOTIATES );
    507: Exit( stINSUFFICIENT_STORAGE );
    508: Exit( stLOOP_DETECTED );
    510: Exit( stNOT_EXTENDED );
    511: Exit( stNETWORK_AUTHENTICATION_REQUIRED );
    599: Exit( stNETWORK_CONNECT_TIMEOUT_ERROR );
  else
    raise EEnumNotImplementedError<THttpStatus>.Create(ACode);
  end;
end;

end.
