unit Json.Base;

interface

uses
  System.SysUtils, DateUtils, System.JSON, Json.Interfaces, Variants, Classes, Winapi.Windows;

type
  TJson = class( TInterfacedObject, IJson )
  private
    FJson: TJsonObject;
    const FORMAT_DATE_TIME = 'yyyy-mm-dd hh:nn:ss.zzz';
    const FORMAT_DATE = 'yyyy-mm-dd';
    function SetJsonValue( Add: boolean; Name: string; JsonValue: TJsonValue ): IJson;
    function ListToArrayJson( List: TArray<IJson> ): System.JSON.TJsonArray;
    function VarListToArrayJson( List : TArray<Variant> ) : System.JSON.TJsonArray;
  public
    constructor Create( Json: string );
    class function New( Json: String ): IJson; overload;
    class function New( Json: IJson ): IJson; overload;
    class function New: IJson; overload;
//    class function NewList( Json: String ): TArray<IJson>;
    class function ArrayToString( JsonArray: TArray<IJson> ): String;
    destructor Destroy; override;
    function AsJsonString: string;
    function Value: TJsonObject;
    function SetString( Name: string; Value: string ): IJson;
    function SetInt( Name: string; Value: integer ): IJson;
    function SetFloat( Name: string; Value: real ): IJson;
    function SetDate( Name: string; Value: TDate ): IJson;
    function SetDateTime( Name: string; Value: TDateTime ): IJson;
    function SetJson( Name: string; Value: IJson ): IJson;
    function SetJsonArray( Name: string; Value: TArray<IJson> ): IJson;
    function SetVarArray( Name : string; Value : TArray<Variant> ) : IJson;
    function SetBoolean( Name: string; Value: boolean ): IJson;
    function SetNull( Name: String ): IJson;
    function AddString( Name: string; Value: string ): IJson;
    function AddInt( Name: string; Value: integer ): IJson;
    function AddFloat( Name: string; Value: real ): IJson;
    function AddDate( Name: string; Value: TDate ): IJson;
    function AddDateTime( Name: string; Value: TDateTime ): IJson;
    function AddJson( Name: string; Value: IJson ): IJson;
    function AddJsonArray( Name: string; Value: TArray<IJson> ): IJson;
    function AddVarArray( Name : string; Value: TArray<Variant> ) : IJson;
    function AddBoolean( Name: string; Value: boolean ): IJson;
    function RemoveItem( Name : string ) : IJson;
    function Add( Name: string ): IJson;
    function AddNull( Name : string ) : IJson;
    function Item( Name: string ): IValue; overload;
    function Item( Index: integer ): IValue; overload;
    function Count: integer;
    function IsEmpty: boolean;
    function Contains( Name : String ) : boolean;
    function ToString : string;
    function SaveToFile( AFileName: String ) : IJson;
    function ID: integer;
    function AddOrSetString( Name : string; Value : string ) : IJson;
    function AddOrSetInt( Name : string; Value : Integer ) : IJson;
    function AddOrSetFloat( Name : string; Value : Real ) : IJson;
    function AddOrSetDate( Name : string; Value : TDate ) : IJson;
    function AddOrSetDateTime( Name : string; Value : TDateTime ) : IJson;
    function AddOrSetJson( Name : string; Value : IJson ) : IJson;
    function AddOrSetJsonArray( Name : string; Value : TArray<IJson> ) : IJson;
    function AddOrSetVarArray( Name : string; Value : TArray<Variant> ) : IJson;
    function AddOrSetBoolean( Name : string; Value : Boolean ) : IJson;
    function AddOrSetNull( Name : string ) : IJson;
  end;

  TJsonArray = class
  public
    class function New( Json: String ): TArray<IJson>; overload;
    class function New( Json: IJson ): TArray<IJson>; overload;
  end;

  TValue = class( TInterfacedObject, IValue )
  private
    FName: string;
    FValue: TJsonValue;
  private type
    TFormat = class
      class function FormattedStrToDate( AString: String; ADateFormat: string = 'yyyy-mm-dd'; ADateSep: char = '-' ): TDate;
      class function FormattedStrToDateTime( AString : string; ADateFormat : string = 'yyyy-mm-dd';
                ATimeFormat : string = 'hh:nn:ss.zzz'; ADateSep : char = '-'; ATimeSep : char = ':' ): real;
    end;
  public
    constructor Create( Name: String; Value: TJsonValue );
    class function New( Name: String; Value: TJsonValue ): IValue;
    function AsString: string;
    function AsInteger: integer;
    function AsFloat: real;
    function AsDate: TDate;
    function AsDateTime: TDateTime;
    function AsBoolean: boolean;
    function AsJson: IJson;
    function AsList: TArray<IJson>;
    function AsArray: TArray<Variant>;
    function PropertyName: string;
  end;

implementation

{ TJson }

uses Exceptions, Util.Lists;

function TJson.AddFloat(Name: string; Value: real): IJson;
begin
  Result := Self.SetJsonValue( true, Name, TJSONNumber.Create( Value ) );
end;

function TJson.AddInt(Name: string; Value: integer): IJson;
begin
  Result := Self.SetJsonValue( true, Name, TJSONNumber.Create( Value ) );
end;

function TJson.AddString(Name, Value: string): IJson;
begin
  Result := Self.SetJsonValue( true, Name, TJsonString.Create( Value ) );
end;

function TJson.AddNull(Name: string): IJson;
begin
  Result := Self;
  Self.SetJsonValue( true, Name, TJSONNull.Create );
end;

function TJson.AddOrSetBoolean(Name: string; Value: Boolean): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetBoolean( Name, Value )
  else
    Self.AddBoolean( Name, Value );
end;

function TJson.AddOrSetDate(Name: string; Value: TDate): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetDate( Name, Value )
  else
    Self.AddDate( Name, Value );
end;

function TJson.AddOrSetDateTime(Name: string; Value: TDateTime): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetDateTime( Name, Value )
  else
    Self.AddDateTime( Name, Value );
end;

function TJson.AddOrSetFloat(Name: string; Value: Real): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetFloat( Name, Value )
  else
    Self.AddFloat( Name, Value );
end;

function TJson.AddOrSetInt(Name: string; Value: Integer): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetInt( Name, Value )
  else
    Self.AddInt( Name, Value );
end;

function TJson.AddOrSetJson(Name: string; Value: IJson): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetJson( Name, Value )
  else
    Self.AddJson( Name, Value );
end;

function TJson.AddOrSetJsonArray(Name: string; Value: TArray<IJson>): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetJsonArray( Name, Value )
  else
    Self.AddJsonArray( Name, Value );
end;

function TJson.AddOrSetNull(Name: string): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetNull( Name )
  else
    Self.AddNull( Name );
end;

function TJson.AddOrSetString(Name, Value: string): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetString( Name, Value )
  else
    Self.AddString( Name, Value );
end;

function TJson.AddOrSetVarArray(Name: string; Value: TArray<Variant>): IJson;
begin
  Result := Self;
  if Self.Contains( Name ) then
    Self.SetVarArray( Name, Value )
  else
    Self.AddVarArray( Name, Value );
end;

function TJson.Add(Name: string): IJson;
begin
  Result := Self.AddNull( Name );
end;

function TJson.AddBoolean(Name: string; Value: boolean): IJson;
begin
  Result := Self;
  if Value then
    SetJsonValue( true, Name, TJsonTrue.Create )
  else
    SetJsonValue( true, Name, TJSONFalse.Create );
end;

function TJson.AddJson(Name: string; Value: IJson ): IJson;
begin
  if Value <> nil then
    Result := Self.SetJsonValue( true, Name, TJsonObject.ParseJSONValue( Value.AsJsonString ) as TJsonObject )
  else
    Result := Self.SetJsonValue( true, Name, TJSONNull.Create );
end;

function TJson.AddDate(Name: string; Value: TDate): IJson;
begin
  if FormatDateTime( 'dd/mm/yyyy', Value ) = '00/00/0000' then
    Result := AddNull( Name )
  else
    Result := Self.SetJsonValue( true, Name, TJsonString.Create( FormatDateTime( FORMAT_DATE, Value ) ) );
end;

function TJson.AddDateTime(Name: string; Value: TDateTime): IJson;
begin
  if FormatDateTime( 'dd/mm/yyyy hh:nn:ss.zzz', Value ) = '00/00/0000 00:00:00.000' then
    Result := AddNull( Name )
  else
    Result := Self.SetJsonValue( true, Name, TJsonString.Create( FormatDateTime( FORMAT_DATE_TIME, Value ) ) );
end;

function TJson.AsJsonString: string;
var
  b : TBytes;
begin
  SetLength( b, 0 );
  SetLength( b, FJson.EstimatedByteSize );
  FJson.ToBytes( b, 0 );
  while b[ High( b ) ] <> Ord( '}' )  do
    SetLength( b, Length( b ) -1 );

  Result := TEncoding.UTF8.GetString( b );
end;

function TJson.Contains(Name: String): boolean;
var
  Value: IValue;
  s: string;
begin
  Result := False;
  if Name.Contains( '.' ) then try
    Value := nil;
    for s in Name.Split( [ '.' ] ) do begin
      if Value = nil then
        Value := TValue.New( s, FJson.getValue( s ) )
      else begin
        if not Value.AsJson.Contains( s ) then
          Exit( False )
        else
          Value := TValue.New( s, Value.AsJson.Value.GetValue( s ) );
      end;
    end;
    Result := Value <> nil;
  except
    on E:EMissingJsonPropertyError  do
      Exit( False );
  end
  else try
    if Self.Item( Name ) <> nil then
      Result := True;
  except
    on E:EMissingJsonPropertyError  do
      Exit( False );
  end;
end;

function TJson.Count: integer;
begin
  Result := FJson.Count;
end;

constructor TJson.Create(Json: string);
begin
  inherited Create;
  if ( Json.Trim <> '' ) and ( Json.Trim <> '""' ) then begin
    FJson := TJSONObject.ParseJSONValue( Json ) as TJsonObject;
    if FJson = nil then
      Raise EInvalidJsonStringError.Create( Json );
  end
  else
    FJson := TJsonObject.Create;
  FJson.Owned := false;
end;

destructor TJson.Destroy;
begin
//  try
    if ( FJson <> nil ) {and not FJson.Owned }then begin
      FreeAndNil( FJson );
    end;
    inherited Destroy;
//  except
    //https://i.redd.it/19j4arlrh0vz.jpg
    //spotify:track:7jA5OcunWRSYXq98puhAX8
//  end;
end;

function TJson.RemoveItem(Name: string): IJson;
var
  p : TJSONPair;
begin
  Result := Self;
  if FJson.GetValue( Name ) <> nil  then begin
    p := FJson.RemovePair( Name );
    p.Free;
  end;
end;

function TJson.SetJsonValue(Add: boolean; Name: string; JsonValue: TJsonValue ): IJson;
var
  Pair: TJsonPair;
  OldValue: TJsonValue;
  Caminho, Prop: string;
begin
  Result := Self;
  if Name.Contains( '.' ) then begin
    Caminho := Copy( Name, 1, Name.LastIndexOf( '.' ) );
    Prop := Copy( Name, Name.LastIndexOf( '.' ) + 2 );
    OldValue := Self.Item( Caminho ).AsJson.Value.GetValue( Prop );
  end
  else
    OldValue := FJson.GetValue( Name );
  if OldValue = nil then begin
    if Add then begin
      FJson.AddPair( TJSONPair.Create( Name, JsonValue ) );
    end
    else
      Raise EMissingJsonPropertyError.Create( Name );
  end
  else begin
    // n�o existe uma forma de alterar o JSON
    // s� matando e colocando outro no lugar;
    // parab�ns aos envolvidos;
    Pair := FJson.RemovePair( Name );
    Pair.Free;
    FJson.AddPair( TJsonPair.Create( Name, JsonValue ) );
  end;
end;

function TJson.SetNull(Name: String): IJson;
begin
  Result := Self;
  SetJsonValue( false, Name, TJsonNull.Create );
end;

function TJson.SetJsonArray(Name: string; Value: TArray<IJson>): IJson;
begin
  Result := Self;
  if Length( Value ) > 0 then
    SetJsonValue( false, Name, ListToArrayJson( Value ) )
  else
    SetJsonValue( false, Name, TJSONNull.Create );
end;

function TJson.SetVarArray(Name: string; Value: TArray<Variant>): IJson;
begin
  Result := Self;
  if Length( Value ) > 0 then
    SetJsonValue( false, Name, VarListToArrayJson( Value ) )
  else
    SetJsonValue( false, Name, TJSONNull.Create );
end;

function TJson.ToString: string;
begin
  Result := FJson.ToString;
end;

function TJson.ID: integer;
begin
  if not Self.Contains( 'id' ) then
    Exit( 0 );
  Result := Self.Item( 'id' ).AsInteger;
end;

function TJson.IsEmpty: boolean;
begin
  Result := FJson.Count = 0;
end;

function TJson.Item(Index: integer): IValue;
begin
  Result := TValue.New( FJson.Pairs[ Index ].JsonString.Value, FJson.Pairs[ Index ].JsonValue );
end;

function TJson.Item(Name: string): IValue;
var
  Value: IValue;
  s: string;
begin
  if Name.Contains( '.' ) then begin
    Value := nil;
    for s in Name.Split( [ '.' ] ) do begin
      if Value = nil then
        Value := TValue.New( s, FJson.getValue( s ) )
      else
        Value := TValue.New( s, Value.AsJson.Value.GetValue( s ) );
    end;
    Result := Value;
  end
  else begin
    if FJson.GetValue( Name ) = nil then
      Raise EMissingJsonPropertyError.Create( Name );
    Result := TValue.New( Name, FJson.GetValue( Name ) );
  end;
end;

function TJson.ListToArrayJson(List: TArray<IJson>): System.JSON.TJsonArray;
var
  Item: IJson;
begin
  Result := System.JSON.TJsonArray.Create;
  for Item in List do begin
    if Item <> nil then
      Result.Add( Item.Value );
  end;
end;

function TJson.VarListToArrayJson(List: TArray<Variant>): System.JSON.TJsonArray;
var
  v : Variant;
begin
  Result := System.JSON.TJsonArray.Create;
  for v in List do begin
    Result.Add( VarToStrDef( v, '' ) );
  end;
end;

class function TJson.New(Json: String): IJson;
begin
  Result := TJson.Create( Json );
end;

class function TJson.New: IJson;
begin
  Result := TJson.Create( '' );
end;

class function TJson.New(Json: IJson): IJson;
begin
  if Json = nil then
    Exit( TJson.New );
  Result := TJson.Create( Json.AsJsonString );
end;

(*class function TJson.NewList(Json: String): TArray<IJson>;
var
  Itens: TJsonArray;
  obj: TJsonObject;
  i: integer;
  StringJson: string;
  b : TBytes;
begin
  StringJson := '';
  try
    Itens := TJsonObject.ParseJSONValue( TEncoding.UTF8.GetBytes( json ), 0 ) as TJSONArray;
    if Itens <> nil then try
      SetLength( Result, Itens.Count );
      for i := 0 to Itens.Count - 1 do begin
        obj := Itens.Items[ i ] as TJsonObject;
        SetLength( b, 0 );
        SetLength( b, obj.EstimatedByteSize );
        obj.ToBytes( b, 0 );

        while b[ High( b ) ] <> Ord( '}' ) do begin
          SetLength( b, Length( b ) -1 );
        end;

        StringJson  := TEncoding.UTF8.GetString( b );
        Result[ i ] := TJson.New( StringJson );
      end;
    finally
      Itens.Free;
    end;
  except
    on e: exception do begin
      Raise Exception.Create( 'Json: ' + StringJson + #13#10 + e.Message );
    end;
  end;
end;*)

function TJson.SaveToFile(AFileName: String): IJson;
var
  sFN : string;
begin
  Result := Self;
  with TStringList.Create do try
    Text := Self.AsJsonString;
    ForceDirectories( ExtractFilePath(AFileName));
    sFN := AFileName;
    if not sFN.EndsWith( '.json' ) then
      sFN := sFN + '.json';
    SaveToFile( sFN );
  finally
    Free;
  end;
end;

function TJson.SetBoolean(Name: string; Value: boolean): IJson;
begin
  Result := Self;
  if Value then
    SetJsonValue( false, Name, TJsonTrue.Create )
  else
    SetJsonValue( false, Name, TJSONFalse.Create );
end;

function TJson.SetInt(Name: string; Value: integer): IJson;
begin
  Result := Self.SetJsonValue( false, Name, TJSONNumber.Create( Value ) )
end;

function TJson.SetString(Name, Value: string): IJson;
begin
  Result := Self.SetJsonValue( false, Name, TJSONString.Create( Value ) );
end;

function TJson.SetJson(Name: string; Value:IJson): IJson;
begin
  Result := Self;
  //Value.Value.Owned := true;
  if Value = nil then
    Value := TJson.New;
  SetJsonValue( false, Name, TJsonObject.ParseJSONValue( Value.AsJsonString ) as TJsonObject )//Value.Value );
end;

function TJson.SetDate(Name: string; Value: TDate): IJson;
begin
  if Value <= 0 then // red#2026, o fechamento do caixa estava vindo preenchido, ent�o n�o pegava todos os caixas
    Result := Self.SetJsonValue( false, Name, TJSONNull.Create )
  else
    Result := Self.SetJsonValue( false, Name, TJsonString.Create( FormatDateTime( FORMAT_DATE, Value ) ) );
end;

function TJson.SetDateTime(Name: string; Value: TDateTime): IJson;
begin
  if Value <= 0 then // red#2026, o fechamento do caixa estava vindo preenchido, ent�o n�o pegava todos os caixas
    Result := Self.SetJsonValue( false, Name, TJSONNull.Create )
  else
    Result := Self.SetJsonValue( false, Name, TJSONString.Create( FormatDateTime( FORMAT_DATE_TIME, Value ) ) );
end;

function TJson.SetFloat(Name: string; Value: real): IJson;
begin
  Result := Self.SetJsonValue( false, Name, TJSONNumber.Create( Value ) );
end;

function TJson.Value: TJsonObject;
begin
  // crio um outro objeto porque o FJson � destru�do no final;
  Result := TJSONObject.ParseJSONValue( AsJsonString ) as TJsonObject;
end;


function TJson.AddJsonArray(Name: string; Value: TArray<IJson>): IJson;
begin
  Result := Self;
  if Length( Value ) > 0 then
    SetJsonValue( true, Name, ListToArrayJson( Value ) )
  else
    SetJsonValue( true, Name, TJSONNull.Create );
end;

function TJson.AddVarArray(Name: string; Value: TArray<Variant>): IJson;
begin
  Result := Self;
  if Length( Value ) > 0 then
    Self.SetJsonValue( True, Name, VarListToArrayJson( Value ) )
  else
    Self.SetJsonValue( True, Name, TJSONNull.Create );
end;

class function TJson.ArrayToString(JsonArray: TArray<IJson>): String;
var
  j: IJson;
begin
  Result := '';
  for j in JsonArray do begin
    Result := Result + j.AsJsonString + ',';
  end;
  System.Delete( Result, Result.Length, 1 );
  Result := '[' + Result + ']';
end;

{ TValue }

function TValue.AsBoolean: boolean;
begin
  if FValue.Null then
    Exit( false );

  Result := FValue.Value.ToLower = 'true';
end;

function TValue.AsDate: TDate;
begin
  if ( FValue.Null ) or
     ( FValue.Value = '' ) or
     ( FValue.Value= '0000-00-00 00:00:00.000' ) or
     ( FValue.Value = '00:00:00' ) or
     ( FValue.Value = '0000-00-00' ) or
     ( FValue.Value = '00:00:00.000' ) then
    Exit( 0 );
  Result := TFormat.FormattedStrToDate( FValue.Value );// StrToDate( FValue.Value );
end;

function TValue.AsDateTime: TDateTime;
begin
  if ( FValue.Null ) or
     ( FValue.Value = '' ) or
     ( FValue.Value= '0000-00-00 00:00:00.000' ) or
     ( FValue.Value = '00:00:00' ) or
     ( FValue.Value = '0000-00-00' ) or
     ( FValue.Value = '00:00:00.000' ) then
    Exit( 0 );
  Result := TFormat.FormattedStrToDateTime( FValue.Value ); //StrToDateTime( FValue.Value );
end;

function TValue.AsFloat: real;
begin
  if FValue.Null or ( FValue.Value = '' ) then
    Exit( 0 );
  Result := StrToFloat(
    StringReplace( FValue.Value, '.', ',', [ rfReplaceAll ] )
  );
end;

function TValue.AsInteger: integer;
begin
  if ( FValue.Null ) or ( FValue.Value = '' ) then
    Exit( 0 );
  Result := StrToInt( FValue.Value );
end;

function TValue.AsJson: IJson;
var
  s: string;
begin
  if ( FValue.Null ) or ( FValue.ToString = '[]' ) then
    s := ''
  else
    s := FValue.ToJSON; //ToString;
  Result := TJson.New( s );
end;

function TValue.AsArray: TArray<Variant>;
var
  jArray : System.JSON.TJsonArray;
  i : Integer;
begin
  SetLength( Result, 0 );
  if FValue is System.JSON.TJsonArray then begin
    jArray := FValue as System.JSON.TJsonArray;
    SetLength( Result, jArray.Count );
    for i := 0 to jArray.Count -1 do begin
      Result[ i ] := jArray.Items[ i ].Value;
    end;
  end;
end;

function TValue.AsList: TArray<IJson>;
var
  jArray: System.JSON.TJsonArray;
  i: integer;
begin
  SetLength( Result, 0 ); //CHICO - Pus esse SetLength aqui, porque se o item anterior fosse um JsonArray e o Atual tamb�m,
                          //e se o atual estiver vazio ou nulo, ele retornava o array anterior o.O
                          ////spotify:track:49qvGgyUS9HY3ZK86AuRZx
  if FValue is System.JSON.TJsonArray then begin
    jArray := FValue as System.JSON.TJsonArray;
    SetLength( Result, jArray.Count );
    for i := 0 to jArray.Count - 1 do begin
      Result[ i ] := TJson.New( jArray.Items[ i ].ToJson ); // red#2255, .ToString vem cagado, tem que ser .ToJson pros caracteres especiais virem direito
    end;
  end;
end;

function TValue.AsString: string;
begin
  if (FValue.Null) then
    Exit( '' );
  Result := FValue.Value;
end;

constructor TValue.Create(Name: String; Value: TJsonValue );
begin
  inherited Create;
  FName  := Name;
  if Value <> nil then
    FValue := Value
  else
    FValue := TJsonValue.Create;
end;

class function TValue.New(Name: String; Value: TJsonValue): IValue;
begin
  Result := TValue.Create( Name, Value );
end;

function TValue.PropertyName: string;
begin
  Result := FName;
end;

{ TJsonArray }

class function TJsonArray.New(Json: String): TArray<IJson>;
//var
//  Jsons: TlkJSONbase;
//  i: Integer;
//  Ret: IList<IJson>;
//  UTF8String : RawByteString;
//begin
//  Ret := TList<IJson>.New;
//  UTF8String := UTF8Encode( Json);
//  SetCodePage( UTF8String, 0, False );
//  Jsons := TlkJSON.ParseText( UTF8String );
//  if Jsons = nil then
//    Exit( Ret.ToArray );
//  try
//    for i := 0 to Pred(Jsons.Count) do
//      Ret.Add( TJson.New( TlkJSON.GenerateText( TlkJSONobject( Jsons.Child[ i ] ) ) ) );
//  finally
//    Jsons.Free;
//  end;
//  Result := Ret.ToArray;
var
  Itens: System.JSON.TJsonArray;
  obj: TJsonObject;
  i: integer;
  StringJson: string;
  b : TBytes;
begin
  StringJson := '';
  try
    Itens := TJsonObject.ParseJSONValue( TEncoding.UTF8.GetBytes( json ), 0 ) as System.JSON.TJSONArray;
    if Itens <> nil then try
      SetLength( Result, Itens.Count );
      for i := 0 to Itens.Count - 1 do begin
        obj := Itens.Items[ i ] as TJsonObject;
        SetLength( b, 0 );
        SetLength( b, obj.EstimatedByteSize );
        obj.ToBytes( b, 0 );

        while b[ High( b ) ] <> Ord( '}' ) do begin
          SetLength( b, Length( b ) -1 );
        end;

        StringJson  := TEncoding.UTF8.GetString( b );
        Result[ i ] := TJson.New( StringJson );
      end;
    finally
      Itens.Free;
    end;
  except
    on e: exception do begin
      Raise Exception.Create( 'Json: ' + StringJson + #13#10 + e.Message );
    end;
  end;

end;

class function TJsonArray.New(Json: IJson): TArray<IJson>;
var
  s: string;
begin
  s := '';
  if Json <> nil then begin
    s := Json.AsJsonString;
  end;
  if not s.StartsWith( '[' ) then
    s := '[' + s + ']';
  Result := TJsonArray.New( s );
end;

{ TValue.TFormat }

class function TValue.TFormat.FormattedStrToDate(AString,
  ADateFormat: string; ADateSep: char): TDate;
var
  d: TDateTime;
  dia, mes, ano: word;
begin
  d := TFormat.FormattedStrToDateTime( AString, ADateFormat, 'hh:nn:ss.zzz', ADateSep, ':' );
  DecodeDate( d, ano, mes, dia );
  Result := EncodeDate( ano, mes, dia );
end;

class function TValue.TFormat.FormattedStrToDateTime(AString, ADateFormat,
  ATimeFormat: string; ADateSep, ATimeSep: char): real;
//var
//  fmt : TFormatSettings;
var
  sData,
  sHora: string;
  sSec: string;
  Year, Month, Day, Hour, Min, Sec, MiliSec: word;
  RMili : Double;
begin
(********************************************************
 *                                                      *
 *  Fiz a fun��o retornar DOUBLE ao inv�s de TDATETIME  *
 *  porque o TDateTime tava cortando os milisegundos    *
 *                                                      *
 ********************************************************)
  AString := AString
    .Replace( 'T', ' ' )
    .Replace( 'Z', '' );

  sData   := AString.Split( [ ' ' ] )[ 0 ];
  sHora   := '';
  Hour    := 0;
  Min     := 0;
  Sec     := 0;
  MiliSec := 0;
  Year    := 0;
  Month   := 0;
  Day     := 0;

  if sData.Contains( '-' ) then begin
    with TList<String>.New( sData.Split( [ '-' ] ) ) do begin
      Year  := Item( 0 ).ToInteger();
      Month := Item( 1 ).ToInteger();
      Day   := Item( 2 ).ToInteger();
    end;
  end;

  if ( Length( AString.Split( [ ' ' ] ) ) = 2 ) or ( sData.Contains( ':' ) ) then begin // � data e hora;
    if ( Length( AString.Split( [ ' ' ] ) ) = 2 ) then
      sHora := AString.Split( [ ' ' ] )[ 1 ]
    else
      sHora := sData; //Se caiu aqui, veio s� a hora do servidor...
    sHora := sHora.Replace( '.', ':' );

    with TList<String>.New( sHora.Split( [ ':' ] ) ) do begin
    //11 00 45-03 00
      Hour := Item( 0 ).ToInteger;//11
      Min  := Item( 1 ).ToInteger;//00
      sSec := Item( 2 );//45-03

      if ( sSec.Contains( '-' ) ) then begin
        with TList<String>.New( sSec.Split( [ '-' ] ) ) do begin
          Sec :=  StrToIntDef( Item( 0 ), 0 ); // red#2271, botei um strToIntDef, os segundos n�o s�o t�o importantes assim
        end;
      end
      else begin
        Sec := StrToIntDef( Item( 2 ), 0 ); // red#2271, botei um strToIntDef, os segundos n�o s�o t�o importantes assim
      end;
      if Count = 4 then begin
        if StrToInt( Item( 3 ) )> 1000 then begin
          RMili := StrToInt(MiliSec.ToString) / 1000000;
          if RMili <= 0 then begin
            MiliSec := 0;
          end
          else begin
            MiliSec := StrToIntDef( RMili.ToString, 0 );
          end;
        end
        else
           MiliSec := StrToIntDef( Item( 3 ), 0 );
      end;
    end;
  end;
  if ( Year > 0 ) and ( Month > 0 ) and ( Day > 0 ) then
    Result := EncodeDateTime( Year, Month, Day, Hour, Min, Sec, MiliSec )
  else
    Result := EncodeTime( Hour, Min, Sec, MiliSec );

  {fmt := TFormatSettings.Create( GetThreadLocale ); //GetLocaleFormatSettings( GetThreadLocale, fmt );
  fmt.TimeSeparator   := ATimeSep;
  fmt.DateSeparator   := ADateSep;
  fmt.ShortDateFormat := ADateFormat;
  fmt.LongTimeFormat  := ATimeFormat;
  Result := StrToDateTime( AString, fmt );
}

end;

end.
