unit Json.Interfaces;

interface

uses
  Generics.Collections, System.JSON, System.SysUtils;

type
  IValue = interface;
  IJson = interface;
  TKSJsonArray = TArray<IJson>;

  IJson = interface
    ['{D421C693-1C30-400F-8FD4-111B42B55947}']
    function AsJsonString: string;
    function Value: TJsonObject;
    function SetString( Name: string; Value: string ): IJson;
    function SetInt( Name: string; Value: integer ): IJson;
    function SetFloat( Name: string; Value: real ): IJson;
    function SetDate( Name: string; Value: TDate ): IJson;
    function SetDateTime( Name: string; Value: TDateTime ): IJson;
    function SetJson( Name: string; Value: IJson ): IJson;
    function SetJsonArray( Name: string; Value: TArray<IJson> ): IJson;
    function SetVarArray( Name : string; Value : TArray<Variant> ) : IJson;
    function SetBoolean( Name: string; Value: boolean ): IJson;
    function SetNull( Name: string ): IJson;
    function AddString( Name: string; Value: string ): IJson;
    function AddInt( Name: string; Value: integer ): IJson;
    function AddFloat( Name: string; Value: real ): IJson;
    function AddDate( Name: string; Value: TDate ): IJson;
    function AddDateTime( Name: string; Value: TDateTime ): IJson;
    function AddJson( Name: string; Value: IJson ): IJson;
    function AddJsonArray( Name: string; Value: TArray<IJson> ): IJson;
    function AddVarArray( Name : string; Value: TArray<Variant> ) : IJson;
    function AddBoolean( Name: string; Value: boolean ): IJson;
    function RemoveItem( Name : string ) : IJson;
    function Add( Name: string ): IJson;
    function AddNull( Name : string ) : IJson;
    function Item( Name: string ): IValue; overload;
    function Item( Index: integer ): IValue; overload;
    function Count: integer;
    function IsEmpty: boolean;
    function Contains( Name : String ) : boolean;
    function SaveToFile( AFileName: String ) : IJson;
    function ToString : string;
    function ID: integer;

    function AddOrSetString( Name : string; Value : string ) : IJson;
    function AddOrSetInt( Name : string; Value : Integer ) : IJson;
    function AddOrSetFloat( Name : string; Value : Real ) : IJson;
    function AddOrSetDate( Name : string; Value : TDate ) : IJson;
    function AddOrSetDateTime( Name : string; Value : TDateTime ) : IJson;
    function AddOrSetJson( Name : string; Value : IJson ) : IJson;
    function AddOrSetJsonArray( Name : string; Value : TArray<IJson> ) : IJson;
    function AddOrSetVarArray( Name : string; Value : TArray<Variant> ) : IJson;
    function AddOrSetBoolean( Name : string; Value : Boolean ) : IJson;
    function AddOrSetNull( Name : string ) : IJson;
  end;

  IValue = interface
    ['{8251A8A7-6D49-4F7B-B9AD-86378D639E05}']
    function PropertyName: string;
    function AsString: string;
    function AsInteger: integer;
    function AsFloat: real;
    function AsDate: TDate;
    function AsDateTime: TDateTime;
    function AsBoolean: boolean;
    function AsJson: IJson;
    function AsList: TArray<IJson>;
    function AsArray: TArray<Variant>;
  end;

implementation

end.
