unit Json.Builder;

interface

uses
  Json.Base, Json.Interfaces, Classes, SysUtils, Model.Enums;

type
  TJsonBuilder = class
  private
    class function PessoaContatoConjuge: IJson; static;
    class function EmpresaContador: IJson; static;
    class function EmpresaEmissaoFiscalNFe: IJson; static;
    class function EmpresaEmissaoFiscalNFCe: IJson; static;
    class function EmpresaMensagem: IJson; static;
    class function EmpresaImpostos: IJson; static;
    class function ContaComuns( Empresa: IJson; Tipo: TTipoLcto ): IJson;
  public
    class function Pessoa( Empresa: IJson ): IJson;
    class function Endereco: IJson;
    class function Usuario: IJson;
    class function PessoaCliente : IJson;
    class function UsuarioGrupo: IJson;
    class function PessoaClienteEmprego: IJson; static;
    class function PessoaClienteFiliacao: IJson; static;
    class function PessoaClienteConjuge : IJson; static;
    class function PessoaFornecedor: IJson; static;
    class function PessoaFuncionario: IJson; static;
    class function PessoaFuncionarioDadosPonto: IJson; static;
    class function PessoaFuncionarioDadosMobile : IJson; static;
    class function PessoaContato: IJson; static;
    class function PessoaTransportadora: IJson; static;
    class function Classe: IJson;
    class function Profissao: IJson;
    class function CampoUsuario: IJson;
    class function Backup : IJson;
    class function BackupGoogleDriveConfiguration : IJson;
    class function BackupFTPConfiguration : IJson;
    class function BackupDropboxConfiguration : IJson;
    class function Familia : IJson;
    class function Grupo : IJson;
    class function SubGrupo : IJson;
    class function Fabricante : IJson;
    class function CentroCusto: IJson;
    class function RecibosRapidos( Empresa : IJson ) : IJson;
    class function ContaCaixa: IJson;
    class function Empresa : IJson;
    class function EmpresaEmissaoFiscal: IJson; static;
    class function Caixa( Empresa : IJson ): IJson;
    class function Cartao( Empresa : IJson ): IJson;
    class function Banco: IJson;
    class function ContaBancaria( Empresa : IJson ): IJson;
    class function Cfop : IJson;
    class function FormaPgto: IJson;
    class function ContaPagar( Empresa: IJson ): IJson;
    class function ContaReceber( Empresa: IJson ): IJson;
    class function ContaReceberAgendamento: IJson;
    class function Convenio( Empresa : IJson ) : IJson;
    class function Historicos( Empresa : IJson ) : IJson;
    class function LctoDinheiro( Empresa: IJson ): IJson;
    class function LctoBancario( Empresa: IJson ): IJson;
    class function LctoCartao( Empresa: IJson ): IJson;
    class function PlanoPgto( Empresa : IJson ) : IJson;
    class function LctoConvenio( Empresa: IJson ): IJson;
    class function LctoCheque( Empresa: IJson ): IJson;
    class function LctoCredito( Empresa: IJson ): IJson;
    class function ConfiguracoesEmpresa: IJson;
    class function ConfiguracoesSistema: IJson;
    class function ConfiguracaoEmail: IJson;
    class function ConfiguracaoJuros: IJson;
    class function ConfiguracaoUsuario: IJson;
    class function ConfiguracaoRelatorios : IJson;
    class function ConfiguracaoNuvemShop: IJson;
    class function ConfiguracaoIderis: IJson;
    class function ConfiguracaoMagento : IJson;
    class function Observacao : IJson;
    class function ChequeSuspeito : IJson;
    class function ValeFuncionario( Empresa: IJson ): IJson;
    class function MalaDireta : IJson;
    class function Grade : IJson;
    class function LctoAntecipado( Empresa: IJson ): IJson;
    class function ConfiguracaoLocal( MACAddress : string ) : IJson;
    //class function Item( Empresa : IJson ) : IJson;
//    class function ItemDetalhes( Empresa : IJson ) : IJson;
//    class function ItemEstoque : IJson;
//    class function ItemPrecos : IJson;
    class function RegistroLicenca: IJson;
    class function LctoFinanceiroDetalhe( AContaCaixa, ACentroCusto : IJson; AValor : Real ) : IJson;
    class function Recados : IJson;
    class function Tarefas( AEmpresa : IJson ) : IJson;
    class function ItemObservacao( AEmpresa : IJson  ) : IJson;
    class function FollowUp( AEmpresa : IJson) : IJson;
    class function FollowUpDadosEmail : IJson; static;
    class function Boleto( AEmpresa : IJson ) : IJson;
    class function BoletoCedente( AEmpresa : IJson ) : IJson;
    class function BoletoOutrosDados : IJson;
    class function BoletoValoresPadrao : IJson;
    class function BoletoDadosWeePay : IJson;
    class function FechamentoCaixaGaveta : IJson;
    class function Equipamento( APessoa : IJson ) : IJson;
    class function TipoAtendimento : IJson;
    class function Modalidade : IJson;
    class function Status : IJson;
    class function Manutencao : IJson;
    class function Veiculo( APessoa : IJson ) : IJson;
    class function Acompanhamento: IJson ;
    class function Integracoes: IJson;
    class function TabelaPreco( AEmpresa: IJson ): IJson;
    class function OrdemProducao( AEmpresa: IJson ): IJson;
    class function OrdemProducaoDetalhe: IJson;
    class function ConfiguracaoEmpresaPDV( AEmpresa : IJson ) : IJson;
    class function LctoFinanceiroComum( Empresa: IJson ): IJson;
  end;

implementation

{ TJsonBuilder }


class function TJsonBuilder.Caixa( Empresa : IJson ): IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .AddJson( 'empresa', Empresa )
    .Add( 'descricao' )
    .AddDate( 'abertura', Date )
    .Add( 'fechamento' )
    .Add( 'usuarios' )
    .Add( 'grupos' )
end;

class function TJsonBuilder.CampoUsuario: IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .Add( 'janela' )
    .Add( 'nomeAmigavel' )
    .Add( 'ordem' )
    .Add( 'pesquisaEm' )
    .Add( 'opcoes' )
    .Add( 'mostraCampo' );
end;

class function TJsonBuilder.Cartao( Empresa : IJson ): IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .AddJson( 'empresa', Empresa )
    .AddString( 'tipo', ctCredito.Value )
    .Add( 'descricao' )
    .Add( 'administradora' )
    .Add( 'contato' )
    .Add( 'telefone' )
    .AddString( 'tipoRede', crPadrao.Value )
    .Add( 'taxa' )
    .Add( 'diaRecebimento' )
    .Add( 'carencia' )
    .Add( 'contaBancaria' )
    .Add( 'credenciadora' )
    .Add( 'credenciadoraCnpj' )
    .AddJson( 'endereco', TJsonBuilder.Endereco );
end;

class function TJsonBuilder.CentroCusto: IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .Add( 'descricao' )
    .AddBoolean( 'inativo', false )
    .Add( 'pai' );
//    .AddJson( 'pai', TJson.New.Add( 'id' )  );
end;

class function TJsonBuilder.Cfop: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'codigo' )
    .Add( 'nome' )
    .AddBoolean( 'alteraEstoque', true )
    .AddBoolean( 'efetuaLctoFinanceiro', true )
    .Add( 'incluiIpiNoIcms' )
    .Add( 'calculaCustoNaEntrada' )
    .AddBoolean( 'incluiValoresAproximados', true )
    .Add( 'usaPrecoCustoNaSaida' )
    .Add( 'somaValorEmOutros' )
    .Add( 'excluiMovimentacaoSped' )
    .Add( 'pedePisCofins' )
    .Add( 'operacaoInterestadual' )
    .Add( 'codigoNFS' )
    .Add( 'tipo' )
    .Add( 'aliquotaSimplesNacional' )
    .Add( 'textoAnexo' )
    .Add( 'operacaoInterestadual' )
    .Add( 'naoCalculaDifal' )
    .Add( 'spedIpm' )
end;

class function TJsonBuilder.ChequeSuspeito: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'banco' )
    .Add( 'data' )
    .Add( 'nroInicial' )
    .Add( 'nroFinal' )
    .Add( 'agencia' )
    .Add( 'conta' )
    .Add( 'proprietario' )
    .Add( 'observacoes' );
end;

class function TJsonBuilder.Acompanhamento: IJson;
begin
  Result := TJson.New
    .Add( 'data' )
    .Add( 'tecnico' )
    .Add( 'servico' )
    .Add( 'horaInicial' )
    .Add( 'horaFinal' );
end;

class function TJsonBuilder.Backup: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'descricao' )
    .Add( 'destino' )
    .AddNull( 'horaBackup' )
    .Add( 'diaDaSemana' )
    .Add( 'diasDaSemana' )
    .Add( 'backupFtp' )
    .AddJson( 'ftpConfiguration', BackupFTPConfiguration )
    .Add( 'backupGoogleDrive' )
    .AddJson( 'googleDriveConfiguration', BackupGoogleDriveConfiguration )
    .Add( 'backupDropbox' )
    .AddJson( 'dropboxConfiguration', BackupDropboxConfiguration );
end;

class function TJsonBuilder.BackupDropboxConfiguration: IJson;
begin
  Result := TJson.New
    .AddInt( 'idInterno', 0 )
    .Add( 'account' )
    .Add( 'path' );
end;

class function TJsonBuilder.BackupFTPConfiguration: IJson;
begin
  Result := TJson.New
    .AddInt( 'idInterno', 0 )
    .Add( 'account' )
    .Add( 'path' );
end;

class function TJsonBuilder.BackupGoogleDriveConfiguration: IJson;
begin
  Result := TJson.New
    .AddInt( 'idInterno', 0 )
    .Add( 'account' )
    .Add( 'parentId' )
    .Add( 'filePath' );
end;

class function TJsonBuilder.Banco: IJson;
begin
  Result := TJson.New
    .Add( 'codigo' )
    .Add( 'descricao' );
end;

class function TJsonBuilder.Boleto(AEmpresa: IJson): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddJson( 'empresa', AEmpresa )
    .Add( 'descricao' )
    .Add( 'banco' )
    .Add( 'conta' )
    .Add( 'contaDigito' )
    .Add( 'agencia' )
    .Add( 'agenciaDigito' )
    .Add( 'cedente' )
    .Add( 'cedenteDigito' )
    .Add( 'carteira' )
    .Add( 'carteiraVariacao' )
    .Add( 'convenio' )
    .AddBoolean( 'usaNossoNro', False )
    .Add( 'nossoNro' )
    .Add( 'codTransmissao' )
    .Add( 'relatorioPadrao' )
    .AddString( 'tipoIntegracao', 'NENHUM' )
    .AddJson( 'dadosCedente', BoletoCedente( AEmpresa ) )
    .AddJson( 'outrosDados', BoletoOutrosDados )
    .AddJson( 'valoresPadrao', BoletoValoresPadrao )
    .AddJson( 'dadosWeePay', BoletodadosWeePay );
end;

class function TJsonBuilder.BoletoCedente(AEmpresa: IJson): IJson;
begin
  Result := TJson.New
    .AddString( 'cedenteNome', AEmpresa.Item( 'nome' ).AsString )
    .AddString( 'cedenteCpfCnpj', AEmpresa.Item( 'cpfCnpj' ).AsString )
    .AddString( 'cedenteEmail', AEmpresa.Item( 'email' ).AsString )
    .AddJson( 'endereco', AEmpresa.Item( 'endereco' ).AsJson );
  case TTipoPessoa.FromString( AEmpresa.Item( 'tipoPessoa' ).AsString ) of
    tpFisica  : Result.AddString( 'tipoInscricao', 'P_FISICA' );
    tpJuridica: Result.AddString( 'tipoInscricao', 'P_JURIDICA' );
  else
    Result.AddString( 'tipoInscricao', 'P_OUTRO' );
  end;
end;

class function TJsonBuilder.BoletoDadosWeePay: IJson;
begin
  Result := TJson.New
    .Add( 'sellerId' )
    .Add( 'secretKey' )
    .Add( 'mcc' )
    .Add( 'bank' )
    .Add( 'documento' )
    .Add( 'documentoFileName' );
end;

class function TJsonBuilder.BoletoOutrosDados: IJson;
begin
  Result := TJson.New
    .AddInt( 'diasProtesto', 0 )
    .AddInt( 'tamanhoNossoNro', 0 )
    .AddBoolean( 'aceiteDocumento', False )
    .AddString( 'tipoCarteira', 'SIMPLES' )
    .AddString( 'tipoEmissao', 'CLIENTE_EMITE' )
    .Add( 'contrato' )
    .Add( 'registroBanco' )
    .Add( 'tipoIdentificador' )
    .AddInt( 'ultimoSeq', 0 )
    .AddBoolean( 'usaMultaPadrao', False )
    .Add( 'multaPerc' )
    .Add( 'moraDiaria' )
    .AddInt( 'diasBaixaDevolucao', 0 )
    .AddBoolean( 'usarMoraPercArquivoRemessa', False )
    .AddString( 'tipoDocumento', 'TRADICIONAL' )
    .AddString( 'tipoImpressao', 'PADRAO' );
end;

class function TJsonBuilder.BoletoValoresPadrao: IJson;
begin
  Result := TJson.New
    .AddString( 'localPagamento', 'PAG�VEL EM QUALQUER BANCO AT� O VENCIMENTO' )
    .Add( 'especie' )
    .Add( 'instrucoes01' )
    .Add( 'instrucoes02' )
    .AddString( 'caracteristica', 'SIMPLES' )
    .Add( 'instrucoes' )
    .Add( 'itauInstrucoes01' )
    .Add( 'itauInstrucoes02' );
end;

class function TJsonBuilder.Classe: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'descricao' )
    .Add( 'tipo' );
end;

class function TJsonBuilder.ContaBancaria( Empresa : IJson ): IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .AddJson( 'empresa', Empresa )
    .Add( 'abertura' )
    .Add( 'descricao' )
    .Add( 'conta' )
    .Add( 'agencia' )
    .Add( 'banco' )
    .Add( 'gerente' )
    .Add( 'email' )
    .Add( 'homePage' )
    .Add( 'telefone' )
    .Add( 'fax' )
    .Add( 'limite' )
    .Add( 'vencimento' );
end;

class function TJsonBuilder.ContaCaixa: IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .Add( 'descricao' )
    .Add( 'centroCusto' )
    .AddBoolean( 'incluiNoFechamento', true )
    .AddBoolean( 'raiz', false )
    .Add( 'tipo' )
    .Add( 'idPai' )
    .Add( 'tipoDaConta' )
    .Add( 'codigoContabil' )
    .Add( 'codigoArvore' )
    .Add( 'padraoPara' )
    .AddJson(
      'opcoesSped',
      TJson.New
        .Add( 'codigo' )
        .Add( 'indicador' )
    )
end;



class function TJsonBuilder.ContaComuns(Empresa: IJson; Tipo: TTipoLcto): IJson;
begin
  Result := TJsonBuilder.LctoFinanceiroComum( Empresa )
    .Add( 'previsaoBaixa' )
    .Add( 'pessoa' )
    .Add( 'previsao' )
    .Add( 'documento' )
    .Add( 'portador' )
    .Add('prioridade')
    .Add( 'detalhes' )
    .Add( 'referenciaRenegociacaoMae' )
    .Add( 'referenciaRenegociacaoFilha' )
    .Add( 'obs' )
    .Add( 'icms' )
    .Add( 'ipi' )
    .Add( 'ir' )
    .Add( 'iss' )
    .Add( 'pis' )
    .Add( 'cofins' )
    .Add( 'csll' )
    .Add( 'inss' )
    .Add( 'anexos' );

  Result.SetString( 'tipo', Tipo.Value );
end;

class function TJsonBuilder.Convenio( Empresa : IJson ): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddJson( 'empresa', Empresa )
    .Add( 'descricao' )
    .AddInt( 'diaRecebimento', 0 )
    .AddInt( 'carencia', 0 )
    .Add( 'contato' )
    .Add( 'telefone' )
    .Add( 'contaBancaria' );
end;



class function TJsonBuilder.ContaPagar(Empresa: IJson): IJson;
begin
  Result := ContaComuns( Empresa, tlPagamento )
    .Add( 'naoPossuiDuplicata' )
    .Add( 'banco' )
    .Add( 'juros' )
    .Add( 'multa' )
    .Add( 'desconto' )
    .Add( 'ateDia' )
    .AddJson( 'cfgJuros', TJsonBuilder.ConfiguracaoJuros )
end;

class function TJsonBuilder.ContaReceber(Empresa: IJson): IJson;
begin
  Result := ContaComuns( Empresa, tlRecebimento )
    .Add( 'vendedor' )
    .AddBoolean( 'descontada', false )
    .Add( 'dataDesconto' )
    .AddJson( 'agendamento', TJsonBuilder.ContaReceberAgendamento )
    .AddJson( 'dadosBoleto', TJson.New
      .Add( 'boleto' )
      .Add( 'nossoNro' )
      .Add( 'linhaDigitavel' )
      .Add( 'codigoBarras' )
    );
end;

class function TJsonBuilder.ContaReceberAgendamento: IJson;
begin
  Result := TJson.New
    .Add( 'contato' )
    .Add( 'previsao' )
    .Add( 'obs' )
end;

class function TJsonBuilder.ConfiguracaoJuros : IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'juroMensal' )
    .AddString( 'tipoJuros', jurSimples.Value )
    .Add( 'diasCarenciaJuros' )
    .Add( 'incluiCarenciaJuros' )
    .Add( 'multa' )
    .Add( 'cobrarMultaApos' )
    .Add( 'desconto' )
    .Add( 'descontoDiasAntesVencimento' )
    .AddBoolean( 'habilitado', False );
end;

class function TJsonBuilder.ConfiguracaoLocal(MACAddress: string): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddString( 'macAddress', MACAddress )
    .Add( 'avisarEstoqueZero' )
    .Add( 'avisarEstoqueMinimo' )
    .Add( 'avisarEstoqueNegativo' )
    .Add( 'bloqueiaVendasEstoqueZero' )
    .Add( 'compensaLctoBancarioAutomaticamente' )
    .Add( 'usaEnterNaPesquisa' );
end;

class function TJsonBuilder.ConfiguracaoMagento: IJson;
begin
  Result := TJson.New
    .Add( 'urlLoja' )
    .Add( 'consumerKey' )
    .Add( 'consumerSecret' )
    .Add( 'acessToken' )
    .Add( 'acessTokenSecret'  )
    .AddString( 'usarCategoria', 'NENHUM' )
end;

class function TJsonBuilder.ConfiguracaoRelatorios: IJson;
begin
  Result := TJson.New.AddInt( 'id', 0 )
    .Add( 'contexto' )
    .Add( 'relatorios' );
end;

class function TJsonBuilder.ConfiguracaoUsuario: IJson;
begin
  Result := TJson.New
    .Add( 'usuario' )
    .AddJsonArray( 'atalhosPersonalizados', TArray<IJson>.Create() );
end;

class function TJsonBuilder.ConfiguracaoEmail: IJson;
begin
  Result := TJson.New()
    .AddBoolean( 'usaGerenciadorPadrao', true )
    .Add( 'perfilMalaDireta' );
end;

class function TJsonBuilder.ConfiguracaoEmpresaPDV(AEmpresa: IJson): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddJson( 'empresa', AEmpresa )
    .AddBoolean( 'emitirBoletos', false )
    .Add( 'idBoletoPadrao' );
end;

class function TJsonBuilder.ConfiguracaoIderis: IJson;
begin
  Result := TJson.New()
    .AddString( 'categoria', 'NENHUM' )
    .AddString( 'subCategoria', 'NENHUM' )
    .AddString( 'departamento', 'NENHUM' )
    .AddString( 'marca', 'NENHUM' )
end;

class function TJsonBuilder.ConfiguracoesEmpresa : IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'usaImagemFundo' )
    .Add( 'imagemFundoPath' )
    .Add( 'imagemFundo' )
    .AddNull( 'cfgJuros' )
    .AddNull( 'cfgEmpresaNFe' )
    .AddNull( 'cfgEmpresaNFSe' )
    .Add( 'inicioExpediente' )
    .Add( 'fimExpediente' )
    .Add( 'pdvTabelaPreco' )
    .AddJson( 'mensagens', EmpresaMensagem );
end;

class function TJsonBuilder.ConfiguracoesSistema: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddJson( 'cfgJuros', ConfiguracaoJuros )
    .AddJson( 'cfgEmail', ConfiguracaoEmail )
    .Add( 'permiteItemProduzidoNaComposicao' )
    .Add( 'cfgMovimento' )
    .Add( 'cfgRelatorios' )
    .Add( 'bloqueiaPessoaSemCpfCnpj' )
    .Add( 'naoPermiteCpfCnpjDuplicado' )
    .AddInt( 'precisaoPesquisa', 3 )
    .AddInt( 'casasDecimais', 2 )
    .Add( 'ufsComFatorIcms' )
    .Add( 'emailLimiteEmissoes' )
    .Add( 'pedeLimiteDeCreditoAoCadastrarCliente' )
    .Add( 'validaNcmAoCadastrarItem' )
    .AddJson( 'nuvemShop', ConfiguracaoNuvemShop )
    .AddJson( 'magento', ConfiguracaoMagento )
    .AddJson( 'ideris', ConfiguracaoIderis )
    .Add( 'statusPadraoCotacao' )
    .Add( 'statusPadraoOrdemServico')
    .AddString( 'mascaraPadrao', 'NONE' );
end;

class function TJsonBuilder.Empresa: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'tipoPessoa' )
    .Add( 'tipoEmpresa' )
    .Add( 'razaoSocial' )
    .Add( 'nome' )
    .Add( 'cpfCnpj' )
    .Add( 'rgIe' )
    .Add( 'im' )
    .Add( 'cnae' )
    .Add( 'email' )
    .Add( 'crt' )
    .Add( 'crtISSQN' )
    .Add( 'ieSt' )
    .Add( 'homePage' )
    .Add( 'produtosImpostos' )
    .AddJson( 'impostos', EmpresaImpostos )
    .AddJson( 'cfgEmpresa', ConfiguracoesEmpresa )
    .AddJson( 'endereco', Endereco )
    .Add( 'telefones' )
    .AddJson( 'contador', EmpresaContador )
    .AddJson( 'mensagens', EmpresaMensagem )
    .AddJson( 'emissaoFiscal', EmpresaEmissaoFiscal )
end;

class function TJsonBuilder.EmpresaContador : IJson;
begin
  Result := TJson.New
    .Add( 'nome' )
    .Add( 'cpf' )
    .Add( 'crc' )
    .Add( 'cnpj' )
    .Add( 'telefone' )
    .Add( 'fax' )
    .Add( 'email' )
    .AddJson( 'endereco', Endereco );
end;

class function TJsonBuilder.EmpresaEmissaoFiscal: IJson;
begin
  Result := TJson.New()
    .Add( 'empAkHomologacao' )
    .Add( 'empAkProducao' )
    .Add( 'pathCertificado' )
    .Add( 'senhaCertificado' )
    .Add( 'justificativaInutilizacao' )
    .Add( 'justificativaCancelamento' )
    .Add( 'justificativaContingencia' )
    .Add( 'orientacaoDanfe' )
    .Add( 'impressaoTributos' )
    .Add( 'infoComplementarNoVerso' )
    .Add( 'dataHoraNaImpressao' )
    .Add( 'reaproveitarDocsRejeitados' )
    .Add( 'descontoIcms' )
    .Add( 'usuario' )
    .Add( 'senha' )
    .AddJson( 'nfe', EmpresaEmissaoFiscalNFe );
end;

class function TJsonBuilder.EmpresaEmissaoFiscalNFCe: IJson;
begin
  Result := TJson.New
    .Add( 'tokenCsc' )
    .Add( 'tokenId' )
    .Add( 'inutilizarDocumentosRejeitados' )
    .Add( 'inutilizarPulos' )
    .Add( 'possuiLeituraX' )
    .Add( 'ultimoNSU' );
end;

class function TJsonBuilder.EmpresaEmissaoFiscalNFe: IJson;
begin
  Result := TJson.New
    .Add( 'inutilizarAutomaticamenteDocsRejeitados' )
    .Add( 'inutilizarPulosDeNumeracao' )
    .Add( 'nsu' );
end;

class function TJsonBuilder.EmpresaImpostos: IJson;
begin
  Result := TJson.New( )
    .Add( 'calculaFecp' )
    .Add( 'optanteSimplesNacional' )
    .Add( 'aliquotaSimplesNacional' )
    .Add( 'iss' )
    .Add( 'irrf' )
    .Add( 'csll' )
    .Add( 'cofins' )
    .Add( 'pis' )
    .Add( 'custoFixo' )
    .Add( 'icmsFrete' )
    .Add( 'retemImpostos' )
end;

class function TJsonBuilder.EmpresaMensagem : IJson;
begin
  Result := TJson.New
    .Add( 'optSimplesFederal' )
    .Add( 'optSimplesEstadual' )
    .Add( 'optSimplesNacional' )
    .Add( 'msgSimplesFederal' )
    .Add( 'msgSimplesEstadual' )
    .Add( 'msgSubsTrib' )
    .Add( 'msgBCReduzida' )
    .Add( 'msgIsento' )
    .Add( 'msgSuspensao' )
    .Add( 'msgDiferimento' )
    .Add( 'msgOutros' )
    .Add( 'msgTextoAnexo' );//red#1770
end;

class function TJsonBuilder.Endereco: IJson;
begin
  Result := TJson.New
    .AddString( 'logradouro', '' )
    .AddString( 'numero', '' )
    .AddString( 'complemento', '' )
    .AddString( 'bairro', '' )
    .AddString( 'cep', '' )
    .AddString( 'pais', '' )
    .AddString( 'uf', '' )
    .AddString( 'municipio', '' )
end;

class function TJsonBuilder.Equipamento( APessoa : IJson ) : IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddString( 'descricao', '' )
    .AddString( 'marca', ''  )
    .AddString( 'modelo', '' )
    .Add( 'custo' )
    .AddString( 'nroSerie', '' )
    .AddString( 'anoFabricacao','' )
    .AddJson( 'cliente', APessoa ) ;
end;

class function TJsonBuilder.Fabricante: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'descricao' )
    .Add( 'observacao' );
end;

class function TJsonBuilder.Familia: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'descricao' );
end;

class function TJsonBuilder.FechamentoCaixaGaveta: IJson;
begin
  Result := TJson.New
    .AddFloat( 'abDinheiro', 0 )
    .AddFloat( 'abCheque', 0 )
    .AddFloat( 'refDinheiro', 0 )
    .AddFloat( 'refCheque', 0 )
    .AddFloat( 'sanDinheiro', 0 )
    .AddFloat( 'sanCheque', 0 )
    .AddFloat( 'vDinheiro', 0 )
    .AddFloat( 'vCheque', 0 )
    .AddFloat( 'vChequePre', 0 )
    .AddFloat( 'vCredito', 0 )
    .AddFloat( 'vCartao', 0 )
    .AddFloat( 'vConvenio', 0 )
    .AddFloat( 'vBanco', 0 )
    .AddFloat( 'vpCartao', 0 )
    .AddFloat( 'vpConvenio', 0 )
    .AddFloat( 'vpCrediario', 0 )
    .AddFloat( 'vpBanco', 0 )
    .AddFloat( 'eDinheiro', 0 )
    .AddFloat( 'eCheque', 0 )
    .AddFloat( 'eChequePre', 0 )
    .AddFloat( 'recDinheiro', 0 )
    .AddFloat( 'recCheque', 0 )
    .AddFloat( 'recChequePre', 0 )
    .AddFloat( 'recCartao', 0 )
    .AddFloat( 'recBanco', 0 )
    .AddFloat( 'recConvenio', 0 )
    .AddFloat( 'troco', 0 );
end;

class function TJsonBuilder.FollowUp(AEmpresa : IJson): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddJson( 'empresa', AEmpresa )
    .Add( 'cliente' )
    .Add( 'funcionario' )
    .Add( 'motivo' )
    .AddString( 'tipo', 'TELEFONE' )
    .Add( 'assunto' )
    .AddString( 'prioridade', tpMedia.Value  )
    .AddString( 'situacao', 'PENDENTE'  )
    .AddDate( 'dataCadastro', Date )
    .Add( 'resumoF' )
    .AddBoolean( 'proximoContato', false )
    .AddDate( 'proximoContatoData', Date )
    .AddDate( 'proximoContatoHora', Time )
    .AddJson( 'followUpEmail' , FollowUpDadosEmail );
end;

class function TJsonBuilder.FollowUpDadosEmail: IJson;
begin
   Result := TJson.New
    .Add( 'textoEmail' )
    .Add( 'assuntoEmail' )
    .AddJson( 'cfgEmail', ConfiguracaoEmail );
end;

class function TJsonBuilder.FormaPgto: IJson;
begin
  Result := TJson.New()
    .AddInt( 'id', 0 )
    .Add( 'descricao' )
    .AddString( 'tipo', 'RECEBIMENTO' )
    .Add( 'pedeDocumentos' )
    .Add( 'forcaDocumentos' )
    .Add( 'usaTransferencia' )
    .Add( 'contaCaixa' )
    .Add( 'considerar' )
end;

class function TJsonBuilder.Grade: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'descricao' )
    .Add( 'tituloLinhas' )
    .Add( 'tituloColunas' )
    .Add( 'linhas' )
    .Add( 'colunas' );
end;

class function TJsonBuilder.Grupo: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'descricao' );
end;

class function TJsonBuilder.Historicos(Empresa: IJson): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddJson( 'empresa', Empresa )
    .Add( 'descricao' );
end;

class function TJsonBuilder.Integracoes: IJson;
begin
  Result := TJson.New()
    .Add( 'idNuvemShop' )
    .AddBoolean( 'naoEnviarNuvemShop', false )
    .Add( 'linkImagemNuvemShop' )
    .Add( 'idMagento')
    .AddBoolean('naoEnviarMagento', false)
    .AddBoolean( 'naoEnviarIderis', false )
    .Add( 'linkIderis' )
end;

class function TJsonBuilder.ItemObservacao( AEmpresa : IJson ): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'descricao' )
    .AddJson( 'empresa', AEmpresa );
end;

class function TJsonBuilder.LctoAntecipado(Empresa: IJson): IJson;
begin

end;

class function TJsonBuilder.LctoBancario(Empresa: IJson): IJson;
begin

end;

class function TJsonBuilder.LctoCartao(Empresa: IJson): IJson;
begin

end;

class function TJsonBuilder.LctoCheque(Empresa: IJson): IJson;
begin
  Result := TJsonBuilder.LctoFinanceiroComum( Empresa )
    .Add( 'numero' )
    .Add( 'conta' )
    .Add( 'agencia' )
    .Add( 'banco' )
    .Add( 'preDatado' )
    .Add( 'bomPara' )
    .Add( 'devolvido' )
    .Add( 'custodiado' )
    .Add( 'obs' )
end;

class function TJsonBuilder.LctoConvenio(Empresa: IJson): IJson;
begin
//  Result := TJsonBuilder.LctoFinanceiroComum( Empresa, fppdConvenio )
//    .Add( 'convenio' );
end;

class function TJsonBuilder.LctoCredito(Empresa: IJson): IJson;
begin
//  Result := TJsonBuilder.LctoFinanceiroComum( Empresa, fppdCredito )
//    .Add( 'pessoa' );
end;

class function TJsonBuilder.LctoDinheiro(Empresa: IJson): IJson;
begin
//  Result := TJsonBuilder.LctoFinanceiroComum( Empresa, fppdDinheiro )
//    .Add( 'caixa' )
//    .Add( 'documento' );
end;

//class function TJsonBuilder.LctoFinanceiroComum(Empresa: IJson; PedeDados: TFormaPgtoPedeDados ): IJson;
//begin
//  Result := TJson.New
//    .AddInt( 'id', 0 )
//    .AddJson( 'empresa', Empresa )
//    .AddDate( 'cadastro', Date )
//    .Add( 'baixa' )
//    .Add( 'vencimento' )
//    .Add( 'valor' )
//    .Add( 'descricao' )
//    .AddString( 'tipo', tlRecebimento.Value )
//    .AddBoolean( 'cancelado', false )
//    .Add( 'referencia' )
//    .Add( 'referenciaPagamento' )
//    .AddString( 'pedeDados', PedeDados.Value )
//    .Add( 'detalhes' );
//
//end;

class function TJsonBuilder.LctoFinanceiroComum(Empresa: IJson): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddJson( 'empresa', Empresa )
    .AddDate( 'cadastro', Date )
    .Add( 'baixa' )
    .Add( 'vencimento' )
    .Add( 'valor' )
    .Add( 'descricao' )
    .AddString( 'tipo', tlRecebimento.Value )
    .AddBoolean( 'cancelado', false )
    .Add( 'referencia' )
    .Add( 'referenciaPagamento' )
    .Add( 'pedeDados' )
    .Add( 'detalhes' );
end;

class function TJsonBuilder.LctoFinanceiroDetalhe(AContaCaixa,
  ACentroCusto: IJson; AValor: Real): IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .AddJson( 'contaCaixa', AContaCaixa )
    .AddJson( 'centroCusto', ACentroCusto )
    .AddFloat( 'valor', AValor );
end;

class function TJsonBuilder.MalaDireta: IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .Add( 'nome' )
    .Add( 'email' )
    .Add( 'senha' )
    .Add( 'host' )
    .Add( 'porta' )
    .Add( 'tipoAuth' );
end;

class function TJsonBuilder.Manutencao: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'descricao' );
end;

class function TJsonBuilder.Modalidade: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'descricao' );
end;

class function TJsonBuilder.ConfiguracaoNuvemShop: IJson;
begin
  Result := TJson.New()
    .AddString( 'usarCategoria', 'NENHUM' )
    .Add( 'tabelaPreco' )
end;

class function TJsonBuilder.Observacao: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'observacao' );
end;

class function TJsonBuilder.OrdemProducao(AEmpresa: IJson): IJson;
begin
  Result := TJson.New()
    .AddInt( 'id', 0 )
    .AddJson( 'empresa', AEmpresa )
    .Add( 'descricao' )
    .Add( 'responsavel' )
    .AddDate( 'inicio', Date )
    .Add( 'termino' )
    .AddString( 'tipo', TOrdemProducaoTipo.optImediata.Value )
    .AddString( 'status', TOrdemProducaoStatus.opsPendente.Value )
    .Add( 'observacoes' )
    .Add( 'item' )
    .AddFloat( 'qtde', 1 )
    .Add( 'qtdeFabricada' )
    .Add( 'anexos' )
    .Add( 'detalhes' )
    .AddBoolean(
      'atualizaEstoqueAoFinalizar', False
//      TCfgFactory.GetInstance.ConfiguracaoSistema.AsJson.Item( 'ordemProducaoAtualizaEstoqueAoFinalizar' ).AsBoolean
    )
end;

class function TJsonBuilder.OrdemProducaoDetalhe: IJson;
begin
  Result := TJson.New()
    .Add( 'ordemProducao' )
    .Add( 'item' )
    .Add( 'itemRefDetalhe' )
    .Add( 'qtde' )
    .Add( 'qtdeFabricada' )
end;

class function TJsonBuilder.Pessoa( Empresa: IJson ): IJson;
var
  empArray : TArray<IJson>;
begin
  empArray := TArray<IJson>.Create( Empresa );

  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .AddJsonArray( 'empresas', empArray )
    .AddString( 'tipoPessoa', 'FISICA' )
    .AddString( 'classificacaoDestinatario', TClassificacaoDestinatario.cdNenhum.Value )
    .AddString( 'consumidorFinal', TConsumidorFinal.cfNaoDefinido.Value )
    .AddBoolean( 'usaCpf', false )
    .Add( 'razaoSocial' )
    .Add( 'nome' )
    .Add( 'cpfCnpj' )
    .Add( 'rgIe' )
    .Add( 'im' )
    .Add( 'optanteSn' )
    .Add( 'email' )
    .Add( 'homePage' )
    .Add( 'classe' )
    .Add( 'suframa' )
    .Add( 'telefones' )
    .Add( 'anexos' )
    .Add( 'contribuinteIcms' )
    .AddDate( 'dataCadastro', Date )
    .Add( 'dataNascimentoFundacao' )
    .Add( 'sexo' )
    .Add( 'estadoCivil' )
    .Add( 'obs' )
    .Add( 'retemIss' )
    .AddBoolean( 'inativo', False )
    .AddBoolean( 'cliente', true )
    .AddBoolean( 'fornecedor', true )
    .AddBoolean( 'transportadora', false )
    .AddBoolean( 'funcionario', false )
    .AddBoolean( 'contato', false )
    .AddJson( 'endereco', Endereco )
    .AddJson( 'enderecoCobranca', Endereco )
    .AddJson( 'enderecoEntrega', Endereco )
    .AddString( 'enderecoEmissao', TEnderecoEmissaoTipo.enderPrincipal.Value )
    .AddFloat( 'credito', 0 )
    .AddFloat( 'creditoCheque', 0 )
    .Add( 'dadosCliente' )
    .AddJson( 'dadosFornecedor', PessoaFornecedor )
    .Add( 'dadosFuncionario' )
    .Add( 'dadosContato' )
    .Add( 'dadosTransportadora' )
    .AddBoolean( 'optanteSn', False )
    .Add( 'responsavel' )
    .Add( 'alertas' )
    .AddString( 'avatar', '' )
    .AddInt( 'idxAvatar', 1 );
end;

class function TJsonBuilder.PessoaCliente: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'naturalDe' )
    .AddDate( 'clienteDesde', Date )
    .Add( 'tempoResidencia' )
    .Add( 'sedePropria' )
    .Add( 'valorAluguel' )
    .Add( 'carteiraProfissional' )
    .Add( 'atividadePrincipal' )
    .Add( 'vendedorPadrao' )
    .Add( 'transportadoraPadrao' )
    .Add( 'codigoInterno' )
    .AddJson( 'emprego', PessoaClienteEmprego )
    .AddJson( 'filiacao', PessoaClienteFiliacao )
    .AddJson( 'dadosConjuge', PessoaClienteConjuge )
    .Add( 'autorizados' )
    .Add( 'contatos' )
    .Add( 'dependentes' )
    .Add( 'veiculos' )
    .Add( 'referenciasComerciais' )
    .Add( 'referenciasPessoais' )
    .Add( 'bensCliente' )
    .Add( 'avalista' )
    .Add( 'telefoneAvalista' )
    .Add( 'numeroCartao' )
    .Add( 'diaVcto' )
    .Add( 'validadeCartao' )
    .Add( 'avisarCartaCobranca' )
    .Add( 'avisarVencimentos' )
    .Add( 'avisarPromocoes' )
    .Add( 'avisarOutros' )
    .Add( 'avisarAniversarios' )
    .Add( 'limite' )
    .Add( 'planosPgto' )
    .Add( 'limiteCheque' )
    .Add( 'tabelaPreco' )
    .AddJson( 'cfgJuros', ConfiguracaoJuros );
end;

class function TJsonBuilder.PessoaClienteConjuge: IJson;
begin
  Result := TJson.New
      .AddInt( 'id', 0 )
      .AddInt( 'idInterno', 0 )
//      .Addjson( 'empresa', Empresa )
      .Add( 'nome' )
      .Add( 'nomeMae' )
      .Add( 'nomePai' )
      .Add( 'cpf' )
      .Add( 'rg' )
      .Add( 'dataNascimento' )
      .Add( 'dataCasamento' )
      .Add( 'empresaTrabalha' )
      .Add( 'cargo' )
      .Add( 'profissao' )
      .Add( 'telefoneComercial' )
      .Add( 'dataAdmissao' )
      .AddFloat( 'salario', 0 );
end;

class function TJsonBuilder.PessoaClienteEmprego : IJson;
begin
  Result := TJson.New
      .AddInt( 'id', 0 )
      .AddInt( 'idInterno', 0 )
//      .AddJson( 'empresa', Empresa )
      .Add( 'nomeEmpresa' )
      .Add( 'cargo' )
      .Add( 'profissao' )
      .Add( 'dataAdmissao' )
      .Add( 'telefoneComercial' )
      .Add( 'salario')
      .AddJson( 'endereco', Endereco );
end;

class function TJsonBuilder.PessoaClienteFiliacao : IJson;
begin
  Result := TJson.New
      .AddInt( 'id', 0 )
      .AddInt( 'idInterno', 0 )
//      .AddJson( 'empresa', Empresa )
      .Add( 'nomePai')
      .Add( 'nomeMae' )
      .Add( 'nascimentoPai' )
      .Add( 'nascimentoMae' )
      .Add( 'profissaoPai' )
      .Add( 'profissaoMae' )
      .Add( 'empresaPai' )
      .Add( 'empresaMae' )
      .Add( 'telefoneComercialPai' )
      .Add( 'telefoneComercialMae' )
      .AddJson( 'endereco', Endereco );
end;

class function TJsonBuilder.PessoaContato: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .AddBoolean( 'confidencial', false )
    .Add( 'empresaTrabalha' )
    .Add( 'secao' )
    .Add( 'departamento' )
    .Add( 'emailEmpresa' )
    .Add( 'homePageEmpresa' )
    .AddJson( 'enderecoEmpresa', Endereco )
    .Add( 'caixaPostal' )
    .AddJson( 'conjuge', PessoaContatoConjuge );
end;

class function TJsonBuilder.PessoaContatoConjuge: IJson;
begin
  Result := TJson.New
    .AddInt( 'id',  0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'nome' )
    .Add( 'dataNascimento' )
    .Add( 'dataCasamento' )
    .Add( 'nomePai' )
    .Add( 'nomeMae' )
    .Add( 'obs' );
end;

class function TJsonBuilder.PessoaFornecedor : IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'ramoAtividade' )
    .Add( 'banco' )
    .Add( 'agencia' )
    .Add( 'conta' )
    .Add( 'acrescimoFinanceiroMedio' )
    .AddNull( 'representantes' );
end;

class function TJsonBuilder.PessoaFuncionarioDadosMobile: IJson;
begin
  Result := TJson.New
    .AddBoolean( 'usaApp', false )
    .Add( 'email' )
    .Add( 'senha' );
end;

class function TJsonBuilder.PessoaFuncionario: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'dataAdmissao' )
    .Add( 'dataSaida' )
    .AddNull( 'funcao' )
    .Add( 'codigoBarras' )
    .Add( 'cargaHoraria' )
    .Add( 'salarioFixo' )
    .Add( 'percComissao' )
    .Add( 'carteiraProfissional' )
    .Add( 'pis' )
    .Add( 'habilitacao' )
    .Add( 'tipoSanguineo' )
    .AddJson( 'dadosPonto', PessoaFuncionarioDadosPonto )
    .AddJson( 'dadosApp', PessoaFuncionarioDadosMobile );

end;

class function TJsonBuilder.PessoaFuncionarioDadosPonto: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'senha' )
    .AddBoolean( 'somenteUmTurno', false )
    .AddBoolean( 'horarioLivre', false )
    .AddNull( 'turnos' );
end;

class function TJsonBuilder.PessoaTransportadora: IJson;
begin
   Result := TJson.New
    .Add( 'placa' )
    .Add( 'antt' );
end;

class function TJsonBuilder.PlanoPgto(Empresa: IJson): IJson;
var
 empresas : TArray<IJson>;
begin
  empresas := TArray<IJson>.Create( Empresa );

  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .AddJsonArray( 'empresas', empresas )
    .Add( 'descricao' )
    .Add( 'descricaoCupom' )
    .AddString( 'tipo', TPlanoPgtoTipo.ppVista.Value ) // red#2031, inicializando o tipo
    .Add( 'nroPagamentos' )
    .AddInt( 'diasPrimeiraPrestacao', 0 )
    .AddInt( 'diasEntrePrestacoes', 30 )
    .AddFloat( 'comissaoPadrao', 100.00 )
    .AddFloat( 'multiplicador', 1.0 )
    .Add( 'formasPgto' )
    .Add( 'parcelas' )
    .Add( 'diasParcelas' )
    .Add( 'ignoraMesAtualVencimento' );

end;

{class function TJsonBuilder.Item(Empresa: IJson): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'descricao' )
    .AddDate( 'dataCadastro', Date )
    .Add( 'almoxarifado' )
    .Add( 'ncm' )
    .Add( 'cest' )
    .Add( 'gtin' )
    .Add( 'tipo' )
    .Add( 'tipoControle' )
    .Add( 'fabricacao' )
    .Add( 'classificacaoOrigem' )
    .Add( 'grade' )
    .Add( 'classe' )
    .Add( 'familia' )
    .Add( 'grupo' )
    .Add( 'subGrupo' )
    .Add( 'fabricante' )
    .Add
    .Add( 'fornecedores' )
end;

class function TJsonBuilder.ItemDetalhes(Empresa: IJson): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddJson( 'empresa', Empresa )
    .Add( 'unidade' )
    .AddJson( 'estoque', ItemEstoque )
    .AddJson( 'precos', ItemPrecos );
end;

class function TJsonBuilder.ItemEstoque: IJson;
begin
  Result := TJson.New
          .Add( 'usaMultiplo' )
          .Add( 'multiplo' )
          .Add( 'estoqueMinimo' )
          .Add( 'estoqueIndisponivel' )
          .Add( 'estoquePrevisto' )
          .Add( 'estoqueAtual' );
end;

class function TJsonBuilder.ItemPrecos: IJson;
begin
  Result := TJson.New
          .Add( 'precoCusto' )
          .Add( 'precoVenda' )
          .Add( 'precoCustoReal' )
          .Add( 'precoCustoMedio' )
          .Add( 'precoCustoIndice' )
          .Add( 'precoCustoLetras' );
end;}

class function TJsonBuilder.Profissao: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'descricao' );
end;

class function TJsonBuilder.Recados: IJson;
begin
  Result := TJson.New
    .Add( 'de' )
    .AddInt( 'para', 0 )
    .Add( 'assunto' )
    .Add( 'msg' )
    .AddBoolean( 'confidencial', False );
end;

class function TJsonBuilder.RecibosRapidos(Empresa: IJson): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .AddJson( 'empresa', Empresa )
    .AddBoolean( 'estouPagando', False )
    .AddDate( 'data', Date )
    .Add( 'valor' )
    .Add( 'nomeSacado' )
    .Add( 'nomeEmitente' )
    .AddJson( 'enderecoSacado', Endereco )
    .Add( 'cpfCnpjEmitente' )
    .AddJson( 'enderecoEmitente', Endereco )
    .Add( 'referente' );

  if Assigned( Empresa ) then begin
    if Empresa.Contains( 'endereco' ) and ( not Empresa.Item( 'endereco' ).AsJson.IsEmpty ) then begin
      Result.SetJson( 'enderecoEmitente', Empresa.Item( 'endereco' ).AsJson );
    end;
    Result.SetString( 'nomeEmitente', Empresa.Item( 'nome' ).AsString )
      .SetString( 'cpfCnpjEmitente', Empresa.Item( 'cpfCnpj' ).AsString );
  end;
end;

class function TJsonBuilder.RegistroLicenca: IJson;
begin
  Result := TJson.New
    .Add( 'expDate' )
    .Add( 'nroLicencas' )
    .Add( 'acessos' );
end;

class function TJsonBuilder.Status: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'descricao' )
    .AddInt( 'cor', 0 );
end;

class function TJsonBuilder.SubGrupo: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .AddInt( 'idInterno', 0 )
    .Add( 'descricao' )
    .Add( 'obs' );
end;

class function TJsonBuilder.TabelaPreco(AEmpresa: IJson): IJson;
begin
  Result := TJson.New()
    .AddInt( 'id', 0 )
    .Add( 'descricao' )
    .AddJson( 'empresa', AEmpresa )
end;

class function TJsonBuilder.Tarefas( AEmpresa : IJson ): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'descricao' )
    .AddBoolean( 'concluida', False )
    .Add( 'observacao' )
    .Add( 'duracao' )
    .Add( 'concluidaEm' )
    .AddString( 'prioridade', tpMedia.Value )
    .AddJson( 'empresa', AEmpresa )
end;

class function TJsonBuilder.TipoAtendimento: IJson;
begin
    Result := TJson.New
      .AddInt( 'id', 0 )
      .Add( 'descricao' );
end;

class function TJsonBuilder.Usuario: IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'login' )
    .Add( 'nome' )
    .Add( 'email' )
    .Add( 'lembreteSenha' )
    .Add( 'senha' )
    .AddInt( 'indexAvatar', 0 )
    .Add( 'acessaAppGraficos' )
    .AddBoolean( 'sysAdmin', false )
    .AddJson( 'endereco', Endereco )
    .AddJson( 'acessoUsuario', TJson.New
      .Add( 'empresas' )
      .Add( 'grupos' ) )
end;

class function TJsonBuilder.UsuarioGrupo: IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .Add( 'nome' )
    .Add( 'descricao' )
    .Add( 'permissoes' )
    .Add( 'permissoesPDV' )
    .Add( 'permissoesNFCe' )
    .Add( 'relatorios' )
    .Add( 'graficos' )
    .Add( 'obs' )
    .Add( 'finalizaOrcamentoSemVenda' )
    .Add( 'finalizaOrdemServicoSemVenda' )
end;

class function TJsonBuilder.ValeFuncionario(Empresa: IJson): IJson;
begin
  Result := TJson.New
    .Add( 'id' )
    .AddJson( 'empresa', Empresa )
    .AddDate( 'cadastro', Date )
    .Add( 'funcionario' )
    .AddString( 'tipo', 'Vales' )
    .Add( 'descricao' )
    .Add( 'contaCaixa' )
    .Add( 'centroCusto' )
    .Add( 'valor' );
end;

class function TJsonBuilder.Veiculo(APessoa: IJson): IJson;
begin
  Result := TJson.New
    .AddInt( 'id', 0 )
    .Add( 'descricao' )
    .Add( 'placa' )
    .Add( 'marca' )
    .Add( 'renavam' )
    .Add( 'ano' )
    .Add( 'cor')
    .AddJson( 'cliente', APessoa );
end;

end.
