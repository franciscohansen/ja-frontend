unit Cfg.Constantes;

interface

type
  TAppConstantes = class
  public
    class function Build        : string;
    class function Token        : string;
    class function MascaraCPF   : string;
    class function MascaraCNPJ  : string;
    class function MascaraCEP   : string;
    class function MascaraCNAE  : string;
    class function MascaraIP    : string;
    class function PathApp      : string;
    class function PathIni      : String;
    class function PathIniH10   : string;
    class function FormatFloat( CasasDecimais: integer = 2 ) : String;
    class function Percentual   : String;
    class function SearchParams : String;
    class function EmpresaAtiva : String;
    class function OrderParams  : String;
    class function Search       : String;
    class function FieldName    : String;
    class function FiscalLaudo  : string;
    class function FiscalVersao : String;
    class function MaintenanceToken : String;
    class function P_SIZE : Integer;
  end;

implementation

{ TAppConstantes }

class function TAppConstantes.Build: string;
begin
  Result := '21.02.16.1634b'
end;

class function TAppConstantes.EmpresaAtiva: String;
begin
  Result := 'empresa-ativa';
end;

class function TAppConstantes.FieldName: String;
begin
  Result := 'fieldName';
end;

class function TAppConstantes.FiscalLaudo: string;
begin
  Result := 'IFL0282016';
end;

class function TAppConstantes.FiscalVersao: String;
begin
  Result := '10.0';
end;

class function TAppConstantes.FormatFloat( CasasDecimais: integer = 2 ): String;
var
  i: Integer;
begin
  if CasasDecimais < 2 then
    CasasDecimais := 2;
  Result := '#,#0.0';
  for i := 1 to CasasDecimais - 1 do begin
    Insert( '#', Result, Pos( ',', Result ) + 1 );
    Insert( '0', Result, Pos( '.', Result ) + 1 );
  end;
end;

class function TAppConstantes.MaintenanceToken: String;
begin
  Result := 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzZXJ2ZXIubWFuYWdlbWVudCIsImlzcyI6IktTSDEwIiwib3JpZ2VtIjoiUkVUQUdVQVJEQSJ9.ABwIqTKiwg3VmNZMLWnB-HwrAlKnpQ2RZhbGgYCaYNw';
end;

class function TAppConstantes.MascaraCEP: string;
begin
  Result := '\d\d\d\d\d' + '-' + '\d\d\d';
end;

class function TAppConstantes.MascaraCNAE: string;
begin
  Result := '\d\d\d\d''-''\d''/''\d\d';
end;

class function TAppConstantes.MascaraCNPJ: string;
begin
  Result := '\d\d''.''\d\d\d''.''\d\d\d''/''\d\d\d\d''-''\d\d';
end;

class function TAppConstantes.MascaraCPF: string;
begin
  Result := '\d\d\d''.''\d\d\d''.''\d\d\d''-''\d\d';
end;

class function TAppConstantes.MascaraIP: string;
begin
  Result := '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)';
end;

class function TAppConstantes.OrderParams: String;
begin
  Result := 'order-params';
end;

class function TAppConstantes.PathApp: string;
begin
  Result := 'C:\ProgramData\Koinonia Software\Habil_JA\';
end;

class function TAppConstantes.PathIni: String;
begin
  Result := TAppConstantes.PathApp + 'ini.cfg';
end;

class function TAppConstantes.PathIniH10: string;
begin
  Result := 'C:\ProgramData\Koinonia Software\Habil10\cfg.ini';
end;

class function TAppConstantes.Percentual: String;
begin
  Result := '0.##%';
end;

class function TAppConstantes.P_SIZE: Integer;
begin
  Result := 50;
end;

class function TAppConstantes.Search: String;
begin
  Result := 'search';
end;

class function TAppConstantes.SearchParams: String;
begin
  Result := 'search-params';
end;

class function TAppConstantes.Token: string;
begin
  Result := 'X-Auth-Token';
end;


end.

