unit Cfg;

interface

uses
  System.SysUtils, Classes, Json.Interfaces, System.IniFiles;

type
  ICfg = interface
    ['{E37D9E68-BF78-4D83-B435-720AA40E31CE}']
    function Empresa : IJson;
    function Usuario : IJson;
    function SetEmpresa( AEmpresa : IJson ) : ICfg;
    function SetUsuario( AUsuario : IJson ) : ICfg;

    function GetIniValue( Sessao, Chave: string ): String;
    function SetIniValue( Sessao, Chave: string; Valor: String ): ICfg;
    function API : string;
    function H10API : string;
    function Host : string;
    function H10Host : string;
    function Port: Integer;
    function H10Port : Integer;
    function SetToken( AToken : string ) : ICfg;
    function Token : string;
    function ComputerId : string;
    function GetIniValueH10( Sessao, Chave: string ): String;
    function SetIniValueH10( Sessao, Chave: string; Valor: String ): ICfg;

    function ComissaoPadrao : Real;
    function PercentualQuitacao: Real;

    function Admin              : Boolean;
    function AcessaVendas       : Boolean;
    function AcessaJuridico     : Boolean;
    function AcessaNegociacoes  : Boolean;
  end;

  TCfg = class( TInterfacedObject, ICfg )
  strict private class var _instance : ICfg;
  private
    FEmpresa,
    FUsuario : IJson;
    FIni, FIniH10 : TIniFile;
    FToken : String;
    constructor Create;
  public
    class function GetInstance : ICfg;
    destructor Destroy; override;
    function Empresa : IJson;
    function Usuario : IJson;
    function SetEmpresa( AEmpresa : IJson ) : ICfg;
    function SetUsuario( AUsuario : IJson ) : ICfg;
    function API : string;
    function H10API : string;
    function GetIniValue( Sessao, Chave: string ): String;
    function SetIniValue( Sessao, Chave: string; Valor: String ): ICfg;
    function Host : string;
    function H10Host : string;
    function Port: Integer;
    function H10Port : Integer;
    function SetToken( AToken : string ) : ICfg;
    function Token : string;
     function ComputerId : string;
    function GetIniValueH10( Sessao, Chave: string ): String;
    function SetIniValueH10( Sessao, Chave: string; Valor: String ): ICfg;

    function ComissaoPadrao     : Real;
    function PercentualQuitacao : Real;

    function Admin              : Boolean;
    function AcessaVendas       : Boolean;
    function AcessaJuridico     : Boolean;
    function AcessaNegociacoes  : Boolean;
  end;

implementation

uses
  Cfg.Constantes;
{ TCfg }

function TCfg.AcessaJuridico: Boolean;
begin
  Result := Admin or Usuario.Item( 'acessaJuridico' ).AsBoolean;
end;

function TCfg.AcessaNegociacoes: Boolean;
begin
  Result := Admin or Usuario.Item( 'acessaNegociacoes' ).AsBoolean;
end;

function TCfg.AcessaVendas: Boolean;
begin
  Result := Admin or Usuario.Item( 'acessaVendas' ).AsBoolean;
end;

function TCfg.Admin: Boolean;
begin
  Result := Usuario.Item( 'administrador' ).AsBoolean;
end;

function TCfg.API: string;
begin
  Result := 'http://' + Host + ':' + IntToStr( Port );
end;

function TCfg.ComissaoPadrao: Real;
begin
  Result := 0;
end;

function TCfg.ComputerId: string;
begin
  Result := GetIniValueH10( 'IDENTIFICACAO', 'PC_UID' );
  if Result = '' then begin
    Result := GUIDToString( TGUID.NewGuid );
    SetIniValueH10( 'IDENTIFICACAO', 'PC_UID', Result );
  end;
end;

constructor TCfg.Create;
begin
  inherited Create;
  ForceDirectories( TAppConstantes.PathApp );
  FIni := TIniFile.Create( TAppConstantes.PathIni );
  FIniH10 := TIniFile.Create( TAppConstantes.PathIniH10 );
end;

destructor TCfg.Destroy;
begin
  FIni.Free;
  inherited;
end;

function TCfg.Empresa: IJson;
begin
  Result := FEmpresa;
end;

function TCfg.GetIniValue(Sessao, Chave: string): String;
begin
  Result := FIni.ReadString( Sessao, Chave, '' );
  Result := Trim( Result );
end;

function TCfg.GetIniValueH10(Sessao, Chave: string): String;
begin
  Result := FIniH10.ReadString( Sessao, Chave, '' );
end;

class function TCfg.GetInstance: ICfg;
begin
  if not Assigned( TCfg._instance ) then
    _instance := Create;
  Result := TCfg._instance;
end;

function TCfg.H10API: string;
begin
  Result := Trim( 'http://' + H10Host + ':' + IntToStr( H10Port ) );
end;

function TCfg.H10Host: string;
begin
  Result := GetIniValue( 'CONEXAO', 'H10_HOST' );
  if Result.IsEmpty then
    Result := 'localhost';
end;

function TCfg.H10Port: Integer;
begin
  Result := StrToIntDef( Trim( GetIniValue('CONEXAO', 'H10_PORT' ) ), 4567 );
end;

function TCfg.Host: string;
begin
  Result := GetIniValue( 'CONEXAO', 'HOST' );
  if Result.IsEmpty then
    Result := 'localhost';
end;

function TCfg.PercentualQuitacao: Real;
begin
  Result := 0;
end;

function TCfg.Port: Integer;
begin
  Result := StrToIntDef( GetIniValue( 'CONEXAO', 'PORT' ), 4052 );
end;

function TCfg.SetEmpresa(AEmpresa: IJson): ICfg;
begin
  Result := Self;
  FEmpresa := AEmpresa;
end;

function TCfg.SetIniValue(Sessao, Chave, Valor: String): ICfg;
begin
  Result := Self;
  FIni.WriteString( Sessao, Chave, Valor );
end;

function TCfg.SetIniValueH10(Sessao, Chave, Valor: String): ICfg;
begin
  Result := Self;
  FIniH10.WriteString( Sessao, Chave, Valor );
end;

function TCfg.SetToken(AToken: string): ICfg;
begin
  Result := Self;
  FToken := AToken;
end;

function TCfg.SetUsuario(AUsuario: IJson): ICfg;
begin
  Result := Self;
  FUsuario := AUsuario;
end;

function TCfg.Token: string;
begin
  Result := FToken;
end;

function TCfg.Usuario: IJson;
begin
  Result := FUsuario;
end;

end.
