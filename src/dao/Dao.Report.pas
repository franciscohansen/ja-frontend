unit Dao.Report;

interface

uses
  Dao.Interfaces, Http.Interfaces, System.Classes, System.SysUtils,
  Json.Interfaces;
type
  TContextoImpressao = ( cxtVendas, cxtJuridico );
  IReportDao = interface( IDaoWithBaseDao<IDao> )
    function ListByContexto( AContexto : TContextoImpressao ) : TArray<IJson>;
  end;
  TReportDao = class( TInterfacedObject, IReportDao)
  private const URI = 'report';
  private
    FDao : IDao;
    constructor Create;
  public
    class function New : IReportDao;
    function Dao : IDao;
    function ListByContexto( AContexto : TContextoImpressao ) : TArray<IJson>;
  end;


implementation

uses
  Dao.Base, Http.Base;

{ TReportDao }

constructor TReportDao.Create;
begin
  inherited Create;
  FDao := TDao.New( URI );
end;

function TReportDao.Dao: IDao;
begin
  Result := FDao;
end;

function TReportDao.ListByContexto(
  AContexto: TContextoImpressao): TArray<IJson>;
begin

end;

class function TReportDao.New: IReportDao;
begin
  Result := Create;
end;

end.
