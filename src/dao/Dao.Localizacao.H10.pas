unit Dao.Localizacao.H10;

interface

uses
  Http.Interfaces, Dao.Interfaces, Json.Interfaces, System.SysUtils,
  System.Classes;

type
  ILocalizacaoDao = interface
    ['{E17443AB-628F-42D9-A90D-B92EFFEB36D8}']
    function PesquisaCEP( ACep : string ) : IJson;
  end;

  TLocalizacaoDao = class( TInterfacedObject, ILocalizacaoDao )
  private
    function BaseUrl : string;
    function BaseParams : IUrlParams;
  public
    class function New : ILocalizacaoDao;
    function PesquisaCEP( ACep : string ) : IJson;
  end;

implementation

uses
  Http.Base, Cfg.Constantes, Cfg, Dao.Base.H10, Exceptions, Json.Base,
  Util.Format;

{ TLocalizacaoDao }

function TLocalizacaoDao.BaseParams: IUrlParams;
begin
  Result := TUrlParams.New
    .AddHeader( TAppConstantes.Token, TCfg.GetInstance.Token );
end;

function TLocalizacaoDao.BaseUrl: string;
begin
   Result := TCfg.GetInstance.H10API;
  Result := Result + '/localizacao';
end;

class function TLocalizacaoDao.New: ILocalizacaoDao;
begin
  Result := Create;
end;

function TLocalizacaoDao.PesquisaCEP(ACep: string): IJson;
var
  r : IHttpResponse;
  sUrl : String;
begin
  sUrl := BaseUrl + '/cep/' + TFormat.ClearFormat( ACep );
  r := THttpRequest.New(
    TUrl.New(
      sUrl,
      BaseParams
    )
  ).DoGet;
  if r.Status = stOK then
    Result := TJson.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

end.
