unit Dao.Base.H10;

interface

uses
  Dao.Interfaces, Http.Interfaces, Json.Interfaces, System.Classes,
  System.SysUtils;
type
  IDaoH10 = interface( IDao )
    ['{50E6DBE8-5661-449A-8824-1F837F6D1F5A}']
    function FindByIDs( AIDs : TArray<Integer> ) : TArray<IJson>;
  end;

  TDaoH10 = class( TInterfacedObject, IDaoH10 )
  private
    FUri : String;
    constructor Create( AUri : string );
    function BaseUrl : string;
    function BaseParams : IUrlParams;
  public
    class function New( AUri : string ) : IDaoH10;
    function NewRecord : IJson;
    function Save( AJson : IJson ) : IJson;
    function FindAll( AStart, ALength : Integer ) : TArray<IJson>; overload;
    function FindAll( ASearch : string; AStart, ALength : Integer ) :TArray<IJson>; overload;
    function FindAll( ASearch : string; AStart, ALength : Integer; AParams : IURlParams) :TArray<IJson>; overload;
    function RecordCount : Integer; overload;
    function RecordCount( ASearch : string ): Integer; overload;
    function RecordCount( ASearch : string; AParams : IUrlParams ) : Integer; overload;
    function FindById( AID : Integer ) : IJson;
    function FindByIdHabil( AID : Integer ) : IJson;
    function Delete( AID : Integer ): Boolean;
    function Print(AID : integer): string;
    function FindByIDs( AIDs : TArray<Integer> ) : TArray<IJson>;
  end;


implementation

uses
  Json.Base, Http.Base, Exceptions, Dao.Base, Cfg, Cfg.Constantes;

{ TDaoH10 }

function TDaoH10.BaseParams: IUrlParams;
begin
  Result := TUrlParams.New
    .AddHeader( TAppConstantes.Token, TCfg.GetInstance.Token );
end;

function TDaoH10.BaseUrl: string;
begin
  Result := TCfg.GetInstance.H10API;
  if not FUri.StartsWith( '/' ) then
    Result := Result + '/';
  Result := Result + FUri;
end;

constructor TDaoH10.Create(AUri: string);
begin
  inherited Create;
  FUri := AUri;
end;

function TDaoH10.Delete(AID: Integer): Boolean;
begin

end;

function TDaoH10.FindAll(ASearch: string; AStart,
  ALength: Integer): TArray<IJson>;
begin

end;

function TDaoH10.FindAll(AStart, ALength: Integer): TArray<IJson>;
begin
  Result := FindAll( '', AStart, ALength, nil );
end;

function TDaoH10.FindAll(ASearch: string; AStart, ALength: Integer;
  AParams: IURlParams): TArray<IJson>;
var
  r : IHttpResponse;
  p : IUrlParams;
  s : string;
begin
  p := BaseParams.AddParam( 'search', ASearch );

  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/all/' + IntToStr( AStart ) + '/' + IntToStr( ALength ),
      p
    )
  ).DoGet;
  case r.Status of
    stOK        : Result := TJsonArray.New( r.Value );
    stNO_CONTENT: Result := [];
  else
    raise EHttpStatusNotExpectedError.Create( r.Code, r.Value );
  end;
end;

function TDaoH10.FindById(AID: Integer): IJson;
var
  r : IHttpResponse;
  s : string;
begin
  s := BaseUrl + '/' + IntToStr( AID );
  r := THttpRequest.New(
    TUrl.New(
      s,
      BaseParams
    )
  ).DoGet;
  case r.Status of
    stOK        : Result := TJson.New( r.Value );
    stNO_CONTENT: Result := nil;
  else
    raise EHttpStatusNotExpectedError.Create( r.Code, r.Value );
  end;
end;

function TDaoH10.FindByIdHabil(AID: Integer): IJson;
begin

end;

function TDaoH10.FindByIDs(AIDs: TArray<Integer>): TArray<IJson>;
var
  Resp: IHttpResponse;
  s: string;
  id: integer;
begin
  s := '';
  for id in AIDs do
    s := s + id.toString + ',';
  System.Delete( s, s.Length, 1 );
  Resp := THttpRequest.New(
    TUrl.New(
      BaseUrl,
      BaseParams.AddParam( 'ids[]', s )
    )
  ).DoPost;
  if Resp.Code in [ 200, 204 ] then
    Result := TJsonArray.New( Resp.Value )
  else
    raise EHttpStatusNotExpectedError.Create(Resp.Code, Resp.Value);
end;

class function TDaoH10.New(AUri: string): IDaoH10;
begin
  Result := Create( AUri );
end;

function TDaoH10.NewRecord: IJson;
begin

end;

function TDaoH10.Print(AID: integer): string;
begin

end;

function TDaoH10.RecordCount(ASearch: string): Integer;
begin

end;

function TDaoH10.RecordCount: Integer;
begin

end;

function TDaoH10.RecordCount(ASearch: string; AParams: IUrlParams): Integer;
begin

end;

function TDaoH10.Save(AJson: IJson): IJson;
begin

end;

end.
