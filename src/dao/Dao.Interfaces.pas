unit Dao.Interfaces;

interface

uses
  System.SysUtils, Classes, Json.Interfaces, Http.Interfaces;

type
  IDao = interface
    ['{68A76E98-5D95-4465-B6E3-16C3538119D9}']
    function NewRecord : IJson;
    function Save( AJson : IJson ) : IJson;
    function FindAll( AStart, ALength : Integer ) : TArray<IJson>; overload;
    function FindAll( ASearch : string; AStart, ALength : Integer ) :TArray<IJson>; overload;
    function FindAll( ASearch : string; AStart, ALength : Integer; AParams : IURlParams ) :TArray<IJson>; overload;
    function RecordCount : Integer; overload;
    function RecordCount( ASearch : string ): Integer; overload;
    function RecordCount( ASearch : string; AParams : IUrlParams ) : Integer; overload;
    function FindById( AID : Integer ) : IJson;
    function FindByIdHabil( AID : Integer ) : IJson;
    function Delete( AID : Integer ): Boolean;
    function Print( AID : Integer ) : string;
  end;

  IDaoWithBaseDao<T> = interface
    ['{12E6E8B5-17FB-47A5-891F-981363C61BBE}']
    function Dao : T;
  end;

implementation

end.
