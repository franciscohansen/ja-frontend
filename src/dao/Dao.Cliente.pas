unit Dao.Cliente;

interface

uses
  System.SysUtils, System.Classes, Json.Interfaces, Http.Interfaces, Exceptions,
  Dao.Interfaces;
type
  IClienteDao = interface( IDaoWithBaseDao<IDao> )
    ['{764A0072-FE37-4AE8-A461-70A656B3C6BF}']
    function UFs : TArray<Variant>;
    function Telefones( AID : Integer ) : TArray<IJson>;
    function DocumentValidation( ADocument: String ) : Boolean;
    function FindByDocumento( ADocument : string ) : IJson;
  end;
  TClienteDao = class( TInterfacedObject, IClienteDao )
  const
      URI = '/cliente';
  private
    FDao : IDao;
    constructor Create;
    function BaseURL : String;
  public
    class function New : IClienteDao;
    function Dao : IDao;
    function UFs : TArray<Variant>;
    function Telefones( AID : Integer ) : TArray<IJson>;
    function DocumentValidation( ADocument: String ) : Boolean;
    function FindByDocumento( ADocument : string ) : IJson;
  end;
  EDocumentConflictException = class(Exception)

  end;

implementation

uses
  Json.Base, Http.Base, Dao.Base, Cfg.Constantes, Cfg, Util.Converter;

{ TClienteDao }

function TClienteDao.BaseURL: String;
begin
  Result := TCfg.GetInstance.API + URI;
end;

constructor TClienteDao.Create;
begin
  inherited Create;
  FDao := TDao.New( URI );
end;

function TClienteDao.Dao: IDao;
begin
  Result := FDao;
end;

function TClienteDao.DocumentValidation(ADocument: String): Boolean;
var
  r : IHttpResponse;
begin
  Result := False;
  r := THttpRequest.New(
    TUrl.New(
      BaseURL + '/documento/validate',
      TUrlParams.New
        .AddParam( 'documento', ADocument )
    )
  ).DoPost;
  if r.Status = stOK then
    Result := AnsiLowerCase( r.Value ) = 'true'
  else if r.Status = stBAD_REQUEST then
    raise EDocumentConflictException.Create(r.Value);
end;

function TClienteDao.FindByDocumento(ADocument: string): IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseURL + '/documento/locate/' + ADocument,
      nil

    )
  ).DoGet;
  if r.Status = stOK then
    Result := TJson.New( r.Value )
  else
    Result := nil;
end;

class function TClienteDao.New: IClienteDao;
begin
  Result := Create;
end;

function TClienteDao.Telefones(AID: Integer): TArray<IJson>;
var
  r : IHttpResponse;
  j : IJson;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseURL + '/' + IntToStr( AID ) + '/telefones',
      nil
    )
  ).DoGet;
  if r.Status = stOK then begin
    Result := TJsonArray.New( r.Value );
  end
  else
    Result := [];
end;

function TClienteDao.UFs: TArray<Variant>;
var
  r : IHttpResponse;
  j : IJson;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseURL + '/ufs',
      nil
    )
  ).DoGet;
  if r.Status = stOK then begin
    j := TJson.New( r.Value );
    Result := j.Item( 'ufs' ).AsArray;
  end
  else
    Result := [];
end;

end.
