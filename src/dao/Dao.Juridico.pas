unit Dao.Juridico;

interface

uses
  System.SysUtils, System.Classes, Json.Interfaces, Http.Interfaces,
  Dao.Interfaces, Model.Enums.JA;
type
  IJuridicoDao = interface( IDaoWithBaseDao<IDao> )
    ['{786E9A46-96AA-486F-BED4-F9961DD6B258}']
    function LocateFindAll( ASearch : string; AStart, ALength : Integer; AParams : IURlParams ) :TArray<IJson>;
    function LocateRecordCount( ASearch : string; AParams : IUrlParams ) : Integer;
    function SituacaoAtiva( AID : Integer ) : IJson;
    function Visualizacoes( AID : Integer ) : IJson;
    function MudaSituacao( AID : Integer; ASituacao : TJuridicoSituacao; AAtivo: Boolean ) : IJson;
    function MarcaLeitura( AID : Integer ) : IJson;
    function CorrigeJuridico : Boolean;
  end;

  IJuridicoChildDao = interface( IDaoWithBaseDao<IDao> )
  ['{4E214CC8-69C2-4D30-B13E-4CC891B06856}']
    function FindByIdJuridico( AID : Integer ) : TArray<IJson>;
  end;

  TJuridicoDao = class( TInterfacedObject, IJuridicoDao )
  const
    URI = '/juridico';
  private
    FDao : IDao;
    constructor Create;
    function BaseUrl : string;
  public
    class function New : IJuridicoDao;
    function Dao : IDao;
    function LocateFindAll( ASearch : string; AStart, ALength : Integer; AParams : IURlParams ) :TArray<IJson>; virtual;
    function LocateRecordCount( ASearch : string; AParams : IUrlParams ) : Integer; virtual;
    function SituacaoAtiva( AID : Integer ) : IJson;
    function Visualizacoes( AID : Integer ) : IJson;
    function MudaSituacao( AID : Integer; ASituacao : TJuridicoSituacao; AAtivo: Boolean ) : IJson;
    function MarcaLeitura( AID : Integer ) : IJson;
    function CorrigeJuridico : Boolean;
  end;

  TJuridicoSituacaoDao = class( TInterfacedObject, IJuridicoChildDao )
  private
    FDao : IDao;
    FUri : String;
    constructor Create;
    function BaseURI : string;
  public
    class function New : IJuridicoChildDao;
    function FindByIdJuridico( AID : Integer ) : TArray<IJson>;
    function Dao : IDao;
  end;



implementation

uses
  Json.Base, Http.Base, Exceptions, Dao.Base, Cfg, Cfg.Constantes,
  Util.Converter;

{ TJuridicoDao }

function TJuridicoDao.BaseUrl: string;
begin
  Result := TCfg.GetInstance.API + URI;
end;

function TJuridicoDao.CorrigeJuridico: Boolean;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/corrige-situacoes'
    )
  ).DoPost;
  if r.Status = stOK then
    Result := True
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

constructor TJuridicoDao.Create;
begin
  inherited Create;
  FDao := TDao.New( URI );
end;

function TJuridicoDao.Dao: IDao;
begin
  Result := FDao;
end;

function TJuridicoDao.LocateFindAll(ASearch: string; AStart, ALength: Integer;
  AParams: IURlParams): TArray<IJson>;
var
  r : IHttpResponse;
  p : IUrlParams;
  s : string;
begin
  p := TUrlParams.New.AddParam( 'search', ASearch );
  if Assigned( AParams ) then begin
    if Assigned( AParams.GetParams ) then begin
      for s in AParams.GetParams.KeysToArray do begin
        p.AddParam( s, AParams.GetParams.Item( s ) );
      end;
    end;
    if Assigned( AParams.GetHeader ) then begin
      for s in AParams.GetHeader.KeysToArray do begin
        p.AddHeader( s, AParams.GetHeader.Item( s ) );
      end;
    end;
  end;
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/locate/' + IntToStr( AStart ) + '/' + IntToStr( ALength ),
      p
    )
  ).DoGet;
  case r.Status of
    stOK        : Result := TJsonArray.New( r.Value );
    stNO_CONTENT: Result := [];
  else
    raise EHttpStatusNotExpectedError.Create( r.Code, r.Value );
  end;
end;

function TJuridicoDao.LocateRecordCount(ASearch: string;
  AParams: IUrlParams): Integer;
var
  r : IHttpResponse;
  p : IUrlParams;
  s : string;
begin
  p := TUrlParams.New.AddParam( 'search', ASearch );
  if Assigned( AParams ) then begin
    if Assigned( AParams.GetParams ) then begin
      for s in AParams.GetParams.KeysToArray do begin
        p.AddParam( s, AParams.GetParams.Item( s ) );
      end;
    end;
    if Assigned( AParams.GetHeader ) then begin
      for s in AParams.GetHeader.KeysToArray do begin
        p.AddHeader( s, AParams.GetHeader.Item( s ) );
      end;
    end;
  end;
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/locate/count',
      p
    )
  ).DoGet;
  case r.Status of
    stOK        : Result := StrToIntDef( r.Value, 0 );
    stNO_CONTENT: Result := 0;
  else
    raise EHttpStatusNotExpectedError.Create( r.Code, r.Value );
  end;
end;

function TJuridicoDao.MarcaLeitura(AID: Integer): IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(BaseUrl + '/marca-lido/' + IntToStr( AID ),
    TUrlParams.New
      .AddHeader( 'id-usuario', TCfg.GetInstance.Usuario.ID.ToString )
    )
  ).DoPost;
  if r.Status = stOK then
    Result := TJson.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);

end;

function TJuridicoDao.MudaSituacao(AID: Integer; ASituacao: TJuridicoSituacao;
  AAtivo: Boolean): IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(BaseUrl + '/muda-situacao/' + IntToStr( AID ),
    TUrlParams.New
      .AddHeader( 'situacao', ASituacao.Value )
      .AddHeader( 'ativo', TJavaConverter.ToBoolean( AAtivo ) )
    )
  ).DoPost;
  if r.Status = stOK then
    Result := TJson.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

class function TJuridicoDao.New: IJuridicoDao;
begin
  Result := Create;
end;

function TJuridicoDao.SituacaoAtiva(AID: Integer): IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New( BaseUrl + '/situacao-ativa/' + IntToStr( AID ),
      nil
    )
  ).DoGet;
  if r.Status = stOK then begin
    Result := TJson.New( r.Value );
  end;
end;

function TJuridicoDao.Visualizacoes(AID: Integer): IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New( BaseUrl + '/visualizacoes/' + IntToStr( AID ),
      TUrlParams.New.AddHeader( 'id-usuario', IntToStr( TCfg.GetInstance.Usuario.ID ) )
    )
  ).DoGet;
  if r.Status = stOK then begin
    Result := TJson.New( r.Value );
  end;
end;

{ TJuridicoSituacaoDao }

function TJuridicoSituacaoDao.BaseURI: string;
begin
  Result := TCfg.GetInstance.API + FUri;
end;

constructor TJuridicoSituacaoDao.Create;
begin
  inherited Create;
  FUri := '/juridico-situacao';
  FDao := TDao.New( FURI );
end;

function TJuridicoSituacaoDao.Dao: IDao;
begin
  Result := Fdao;
end;

function TJuridicoSituacaoDao.FindByIdJuridico(AID: Integer): TArray<IJson>;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New( BaseURI + '/id-juridico/' + IntToStr( AID ) )
  ).DoGet;
  if r.Status = stOk then
    Result := TJsonArray.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);

end;

class function TJuridicoSituacaoDao.New: IJuridicoChildDao;
begin
  Result := Create;
end;

end.
