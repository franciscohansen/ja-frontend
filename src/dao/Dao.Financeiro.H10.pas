unit Dao.Financeiro.H10;

interface

uses
  System.SysUtils, System.Classes, Dao.Interfaces, Dao.Base.H10,
  Http.Interfaces, Json.Interfaces;

type
  IDaoFinanceiroH10 = interface( IDaoWithBaseDao<IDaoH10> )
    ['{8578D8DD-B7E3-467B-A536-E23792CB4C2B}']
    function FindByReferencia( AReferencia: Integer ) : TArray<IJson>;
  end;
  TDaoFinanceiroH10 = class( TInterfacedObject, IDaoFinanceiroH10 )
  private
    FDao : IDaoH10;
    FReceber : Boolean;
    constructor Create( AReceber : Boolean );
    function BaseURL : string;
  public
    class function New( AReceber : Boolean = true ) : IDaoFinanceiroH10;
    function FindByReferencia( AReferencia: Integer ) : TArray<IJson>;
    function Dao : IDaoH10;
  end;

implementation

uses
  Http.Base, Cfg, Cfg.Constantes, Exceptions, Json.Base;

{ TDaoFinanceiroH10 }



{ TDaoFinanceiroH10 }

function TDaoFinanceiroH10.BaseURL: string;
begin
  Result := TCfg.GetInstance.H10API;
  if FReceber then
    Result := Result + '/conta-receber'
  else
    Result := Result + '/conta-pagar';
end;

constructor TDaoFinanceiroH10.Create(AReceber: Boolean);
begin
  inherited Create;
  if AReceber then
    FDao := TDaoH10.New( '/conta-receber' )
  else
    FDao := TDaoH10.New( '/conta-pagar' );
  FReceber := AReceber;
end;

function TDaoFinanceiroH10.Dao: IDaoH10;
begin
  Result := FDao;
end;

function TDaoFinanceiroH10.FindByReferencia(
  AReferencia: Integer): TArray<IJson>;
var
  r : IHttpResponse;
  j : IJson;
begin
  j := TJson.New
    .AddString( 'paramName', 'referencia' )
    .AddVarArray( 'paramValue', [ AReferencia ] )
    .AddString( 'compareType', 'EQUAL' );

  r := THttpRequest.New(
    TUrl.New( BaseURL + '/all/0/0',
      TUrlParams.New
        .AddHeader( TAppConstantes.SearchParams, '[' + j.AsJsonString + ']' )
        .AddHeader( TAppConstantes.EmpresaAtiva, IntToStr( TCfg.GetInstance.Empresa.ID ) )
        .AddHeader( TAppConstantes.Token, TCfg.GetInstance.Token )
    )
  ).DoGet;
  case r.Status of
    stOK,
    stNO_CONTENT: begin
      Result := TJsonArray.New( r.Value );
    end;
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
  end;

end;


class function TDaoFinanceiroH10.New(AReceber: Boolean): IDaoFinanceiroH10;
begin
  Result := Create( AReceber );
end;

end.
