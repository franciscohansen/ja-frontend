unit Dao.Simulacao;

interface

uses
  System.SysUtils, System.Classes, Dao.Interfaces, Json.Interfaces,
  Http.Interfaces;

type
  ISimulacaoDao = interface( IDaoWithBaseDao<IDao> )
    ['{5F347297-83AD-4DB8-A398-1DB880DE2642}']
    function GeraVenda( AID: Integer; ASomenteParcelas : Boolean ) : IJson;
    function Telefones( AID : Integer ) : TArray<IJson>;
    function Documentos( AID : Integer ) : TArray<IJson>;
    function Parcelas( AID : Integer ) : TArray<IJson>;
    function Inativar( AID : Integer ) : Boolean;
    function CriaJuridico( AID : Integer ) : Boolean;
  end;

  TSimulacaoDao = class( TInterfacedObject, ISimulacaoDao )
  private
    FDao : IDao;
    constructor Create;
    function BaseUrl: String;
    function BaseParams : IUrlParams;
  public
    class function New : ISimulacaoDao;
    function Dao : IDao;
    function GeraVenda( AID: Integer; ASomenteParcelas : Boolean ) : IJson;
    function Telefones( AID : Integer ) : TArray<IJson>;
    function Documentos( AID : Integer ) : TArray<IJson>;
    function Parcelas( AID : Integer ) : TArray<IJson>;
    function Inativar( AID : Integer ) : Boolean;
    function CriaJuridico( AID : Integer ) : Boolean;
  end;

implementation

uses
  Dao.Base, Json.Base, Http.Base, Cfg, Exceptions;

{ TSimulacaoDao }

function TSimulacaoDao.BaseParams: IUrlParams;
begin
  Result := TUrlParams.New;
end;

function TSimulacaoDao.BaseUrl: String;
begin
  Result := TCfg.GetInstance.API + '/contatos';
end;

constructor TSimulacaoDao.Create;
begin
  inherited Create;
  FDao := TDao.New( '/contatos' );
end;

function TSimulacaoDao.CriaJuridico(AID: Integer): Boolean;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/' + IntToStr( AID ) + '/cria-juridico',
      BaseParams
    )
  ).DoPost;
  Result := r.Status = stOK;
end;

function TSimulacaoDao.Dao: IDao;
begin
  Result := FDao;
end;

function TSimulacaoDao.Documentos(AID: Integer): TArray<IJson>;
var
  r : IHttpResponse;
begin
  Result := [];
  if AID = 0 then
    Exit;
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/' + IntToStr( AID ) + '/documentos',
      TUrlParams.New
    )
  ).DoGet;
  if ( r.Status = stOk ) or ( r.Status = stNO_CONTENT ) then
    Result := TJsonArray.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);

end;

function TSimulacaoDao.GeraVenda(AID: Integer;
  ASomenteParcelas: Boolean): IJson;
var
  r : IHttpResponse;
  s : string;
begin
  if ASomenteParcelas then
    s := 'true'
  else
    s := 'false';
  r := THttpRequest.New(
    TUrl.New( BaseUrl + '/gera-venda',
      BaseParams.AddParam('id', IntToStr( AID ) )
        .AddParam( 'somente-parcelas', s)
    )
  ).DoPost;
  if r.Status = stOk then begin
    Result := TJson.New( r.Value );
  end
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

function TSimulacaoDao.Inativar(AID: Integer): Boolean;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/inativar/' + IntToStr( AID ),
      BaseParams
    )
  ).DoPost;
  Result := r.Status = stOK;
end;

class function TSimulacaoDao.New: ISimulacaoDao;
begin
  Result := Create;
end;

function TSimulacaoDao.Parcelas(AID: Integer): TArray<IJson>;
var
  r : IHttpResponse;
begin
  Result := [];
  if AID = 0 then
    Exit;
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/' + IntToStr( AID ) + '/parcelas',
      TUrlParams.New
    )
  ).DoGet;
  if ( r.Status = stOk ) or ( r.Status = stNO_CONTENT ) then
    Result := TJsonArray.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

function TSimulacaoDao.Telefones(AID: Integer): TArray<IJson>;
var
  r : IHttpResponse;
begin
  Result := [];
  if AID = 0 then
    Exit;
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/' + IntToStr( AID ) + '/telefones',
      TUrlParams.New
    )
  ).DoGet;
  if ( r.Status = stOk ) or ( r.Status = stNO_CONTENT ) then
    Result := TJsonArray.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

end.
