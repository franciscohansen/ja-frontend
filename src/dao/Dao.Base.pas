unit Dao.Base;

interface
uses
  Dao.Interfaces, Http.Interfaces, Json.Interfaces, System.StrUtils, System.SysUtils;
type
  TDao = class( TInterfacedObject, IDao )
  private
    FUri : String;
    constructor Create( AUri : string );
    function BaseUrl : string;
    function BaseParams : IUrlParams;
  public
    class function New( AUri : string ) : IDao;
    function NewRecord : IJson;
    function Save( AJson : IJson ) : IJson;
    function FindAll( AStart, ALength : Integer ) : TArray<IJson>; overload;
    function FindAll( ASearch : string; AStart, ALength : Integer ) :TArray<IJson>; overload;
    function FindAll( ASearch : string; AStart, ALength : Integer; AParams : IURlParams) :TArray<IJson>; overload;
    function RecordCount : Integer; overload;
    function RecordCount( ASearch : string ): Integer; overload;
    function RecordCount( ASearch : string; AParams : IUrlParams ) : Integer; overload;
    function FindById( AID : Integer ) : IJson;
    function FindByIdHabil( AID : Integer ) : IJson;
    function Delete( AID : Integer ): Boolean;
    function Print( AID : Integer ) : string;
  end;

implementation
uses
  Http.Base, Cfg, Exceptions, Json.Base;

{ TDao }

function TDao.BaseParams: IUrlParams;
begin
  Result := TUrlParams.New;
  if Assigned( TCfg.GetInstance.Empresa ) then
    Result.AddParam( 'empresa', IntToStr( TCfg.GetInstance.Empresa.ID ) );
end;

function TDao.BaseUrl: string;
begin
  Result := TCfg.GetInstance.API;
  if not FUri.StartsWith( '/' ) then
    Result := Result + '/';
  Result := Result + FUri;
end;

constructor TDao.Create(AUri: string);
begin
  inherited Create;
  FUri := AUri;
end;

function TDao.Delete(AID: Integer): Boolean;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/' + AID.ToString,
      TUrlParams.New
    )
  ).DoDelete;
  Result := r.Status = stOK;
end;

function TDao.FindAll(AStart, ALength: Integer): TArray<IJson>;
begin
  Result := FindAll( '', AStart, ALength );
end;

function TDao.FindAll(ASearch: string; AStart, ALength: Integer): TArray<IJson>;
begin
  Result := FindAll( ASearch, AStart, ALength, nil );
end;

function TDao.FindAll(ASearch: string; AStart, ALength: Integer;
  AParams: IURlParams): TArray<IJson>;
var
  r : IHttpResponse;
  p : IUrlParams;
  s : string;
begin
  p := BaseParams.AddParam( 'search', ASearch );
  if Assigned( AParams ) then begin
    if Assigned( AParams.GetParams ) then begin
      for s in AParams.GetParams.KeysToArray do begin
        p.AddParam( s, AParams.GetParams.Item( s ) );
      end;
    end;
    if Assigned( AParams.GetHeader ) then begin
      for s in AParams.GetHeader.KeysToArray do begin
        p.AddHeader( s, AParams.GetHeader.Item( s ) );
      end;
    end;
  end;
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/' + IntToStr( AStart ) + '/' + IntToStr( ALength ),
      p
    )
  ).DoGet;
  case r.Status of
    stOK        : Result := TJsonArray.New( r.Value );
    stNO_CONTENT: Result := [];
  else
    raise EHttpStatusNotExpectedError.Create( r.Code, r.Value );
  end;

end;

function TDao.FindById(AID: Integer): IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/' + IntToStr( AID ),
      BaseParams
    )
  ).DoGet;
  case r.Status of
    stOK        : Result := TJson.New( r.Value );
    stNO_CONTENT: Result := nil;
  else
    raise EHttpStatusNotExpectedError.Create( r.Code, r.Value );
  end;
end;

function TDao.FindByIdHabil(AID: Integer): IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/id-habil/' + IntToStr( AID ),
      BaseParams
    )
  ).DoGet;
  case r.Status of
    stOK        : Result := TJson.New( r.Value );
    stNO_CONTENT: Result := nil;
  else
    raise EHttpStatusNotExpectedError.Create( r.Code, r.Value );
  end;
end;

class function TDao.New(AUri: string): IDao;
begin
  Result := Create( AUri );
end;

function TDao.NewRecord: IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New( BaseUrl + '/novo' )
  ).DoGet;
  if r.Status = stOK then
    Result := TJson.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

function TDao.Print(AID: Integer): string;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/' + IntToStr( AID ) + '/print',
      BaseParams
    )
  ).DoPost;
  if r.Status = stOk then
    Result := r.Value
  else
  begin
    Result := '';
  end;
end;

function TDao.RecordCount(ASearch: string; AParams: IUrlParams): Integer;
var
  r : IHttpResponse;
  p : IUrlParams;
  s : string;
begin
  p := BaseParams.AddParam( 'search', ASearch );
  if Assigned( AParams ) then begin
    if Assigned( AParams.GetParams ) then begin
      for s in AParams.GetParams.KeysToArray do begin
        p.AddParam( s, AParams.GetParams.Item( s ) );
      end;
    end;
    if Assigned( AParams.GetHeader ) then begin
      for s in AParams.GetHeader.KeysToArray do begin
        p.AddHeader( s, AParams.GetHeader.Item( s ) );
      end;
    end;
  end;
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/count',
      p
    )
  ).DoGet;
  case r.Status of
    stOK        : Result := StrToIntDef( r.Value, 0 );
    stNO_CONTENT: Result := 0;
  else
    raise EHttpStatusNotExpectedError.Create( r.Code, r.Value );
  end;
end;

function TDao.RecordCount: Integer;
begin
  Result := RecordCount( '' );
end;

function TDao.RecordCount(ASearch: string): Integer;
begin
  Result := RecordCount( ASearch, nil );
end;

function TDao.Save(AJson: IJson): IJson;
var
  r : IHttpResponse;
  s : string;
begin
//  if ( FUri <> '/usuario' )  then begin
//    AJson.AddOrSetJson( 'usuario', TJson.New.AddInt( 'id', TCfg.GetInstance.Usuario.ID ) )
//      .AddOrSetJson( 'empresa', TJson.New.AddInt( 'id', TCfg.GetInstance.Empresa.ID ));
//  end;
  s := BaseUrl + '/save';
  r := THttpRequest.New(
    TUrl.New( s,
      BaseParams
        .AddParam( 'obj', AJson.AsJsonString )
    )
  ).DoPost;
  if ( r.Status = stOk ) or ( r.Status = stCREATED ) then begin
    Result := TJson.New( r.Value );
  end
  else
    raise EHttpStatusNotExpectedError.Create( r.Code, r.Value );
end;



end.
