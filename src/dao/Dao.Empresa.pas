unit Dao.Empresa;

interface

uses
  System.SysUtils, System.Classes, Json.Interfaces, Http.Interfaces,
  Dao.Interfaces;
type
  IH10EmpresaDao = interface( IDaoWithBaseDao<IDao> )
    ['{A7EA0E78-ABB5-4869-9FE1-232E9DCF9D6E}']
  end;

  TH10EmpresaDao = class( TInterfacedObject, IH10EmpresaDao )
  private
    FDao : IDao;
    constructor Create;
  public
    class function New : IH10EmpresaDao;
    function Dao : IDao;
  end;

implementation



uses
  Json.Base, Http.Base, Cfg.Constantes, Cfg, Exceptions, Dao.Base.H10;

{ TH10EmpresaDao }

constructor TH10EmpresaDao.Create;
begin
  inherited Create;
  FDao := TDaoH10.New( '/empresa' );
end;

function TH10EmpresaDao.Dao: IDao;
begin
  Result := FDao;
end;

class function TH10EmpresaDao.New: IH10EmpresaDao;
begin
  Result := Create;
end;

end.
