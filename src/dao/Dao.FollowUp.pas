unit Dao.FollowUp;

interface

uses
  System.SysUtils, System.Classes, Json.Interfaces, Http.Interfaces,
  Dao.Interfaces, System.DateUtils;

type
  IFollowUpDao = interface( IDaoWithBaseDao<IDao> )
    ['{BA869BE1-77E5-46AA-82BE-D7C02D9C79E9}']
    function FindByIdJuridico( AID : Integer ) : TArray<IJson>;
    function FindForConsulta( AIDCliente : Integer; ADataInicial, ADataFinal : TDateTime ) : TArray<IJson>;
    function FindMotivos : TArray<IJson>;
  end;
  TFollowUpDao = class( TInterfacedObject, IFollowUpDao )
  private
    FDao : IDao;
    FUri : string;
    constructor Create;
    function BaseUrl : String;
  public
    class function New : IFollowUpDao;
    function FindByIdJuridico( AID : Integer ) : TArray<IJson>;
    function FindForConsulta( AIDCliente : Integer; ADataInicial, ADataFinal : TDateTime ) : TArray<IJson>;
    function FindMotivos : TArray<IJson>;
    function Dao : IDao;
  end;

implementation

uses
  Json.Base, Http.Base, Exceptions, Dao.Base.H10, Dao.Base, Cfg.Constantes, Cfg,
  Util.Converter;

{ TFollowUpMapDao }

function TFollowUpDao.BaseUrl: String;
begin
  Result := TCfg.GetInstance.API + FUri;
end;

constructor TFollowUpDao.Create;
begin
  inherited Create;
  FUri := '/follow-up';
  FDao := TDao.New( FUri );
end;

function TFollowUpDao.Dao: IDao;
begin
  Result := FDao;
end;

function TFollowUpDao.FindByIdJuridico(AID: Integer): TArray<IJson>;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/juridico/' + IntToStr( AID ),
      TUrlParams.New
        .AddParam( 'empresa', IntToStr( TCfg.GetInstance.Empresa.ID ) )
    )
  ).DoGet;
  if r.Status = stOK then
    Result := TJsonArray.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

function TFollowUpDao.FindForConsulta(AIDCliente: Integer; ADataInicial,
  ADataFinal: TDateTime): TArray<IJson>;
var
  r : IHttpResponse;
begin
  Result := Dao.FindAll( '', 0, 0, TUrlParams.New
    .AddHeader( 'id-cliente', IntToStr( AIDCliente ) )
    .AddHeader( 'data-inicial', TJavaConverter.ToDate( StartOfTheDay( ADataInicial ) ) )
    .AddHeader( 'data-final', TJavaConverter.ToDate( EndOfTheDay( ADataFinal ) ) )
  );
end;

function TFollowUpDao.FindMotivos: TArray<IJson>;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseUrl + '/motivos',
      TUrlParams.New
    )
  ).DoGet;
  if r.Status = stOK then begin
    Result := TJsonArray.New( r.Value )
  end
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

class function TFollowUpDao.New: IFollowUpDao;
begin
  Result := Create;
end;

end.
