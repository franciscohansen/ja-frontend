unit Dao.Login;

interface

uses
  System.SysUtils, System.Classes, Dao.Interfaces, Json.Interfaces;



type
  ILoginDao = interface( IDaoWithBaseDao<IDao> )
    ['{695887D6-337B-4188-A4B3-B1EE1C3788B6}']
    function ConectaCNPJ( ACnpj : string ) : Boolean;
    function ConectaUsuarioSenha( ACnpj, AUsuario, ASenha : string ) : Boolean;
  end;

  TLoginDao = class( TInterfacedObject, ILoginDao )
  private
    FDao : IDao;
    function BaseURLH10 : string;
    function BaseURL: string;

    function FindByIdH10( AId : integer ) : IJson;
    function SalvaDadosUsuario( AJson : IJson; AUsuario : string ) : Boolean;
    function SalvaDadosEmpresa( AJson : IJson ) : Boolean;

    constructor Create;
  public
    class function New : ILoginDao;
    function Dao : IDao;
    function ConectaCNPJ( ACnpj : string ) : Boolean;
    function ConectaUsuarioSenha( ACnpj, AUsuario, ASenha : string ) : Boolean;
  end;

implementation
uses
  Http.Interfaces, Http.Base, Cfg.Constantes, Cfg, View.MessageBox,
  Json.Base, Dao.Base, Exceptions, Dao.Base.H10;

{ TLoginDao }

function TLoginDao.BaseURL: string;
begin
  Result := TCfg.GetInstance.API + '/usuario';
end;

function TLoginDao.BaseURLH10: string;
begin
  Result := Trim( TCfg.GetInstance.H10API );
end;

function TLoginDao.ConectaCNPJ(ACnpj: string): Boolean;
var
  Response : IHttpResponse;
begin
  Result := False;
  Response := THttpRequest.New(
      TUrl.New(
        BaseURLH10 + '/auth/cnpj',
        TUrlParams.New
          .AddParam( 'cnpj', ACnpj )
          .AddParam( 'build', TCfg.GetInstance.GetIniValue('HABIL', 'BUILD' ) )
      )
    ).DoPost;
  case Response.Status of
    stOk                    : begin
      if Response.Value <> '' then begin
        TMsgWarning.New( Response.Value.Replace( '\n', #13#10, [ rfReplaceAll ] ), True ).Show;
      end;
      Result := True;
    end;
    stPRECONDITION_FAILED   : begin
      TMsgError.New( 'O registro para este CNPJ j� foi ativado em outra m�quina.' + #13#10 +
                      'Entre em contato com nosso departamento de vendas para resolver o problema. ' + #13#10 +
                      '(46) 3225-6234' ).Show;
    end;
    stUNAUTHORIZED          : TMsgInformation.New( 'Dados de login inv�lidos!' ).Show;
    stPAYMENT_REQUIRED      : TMsgInformation.New( 'Licen�a expirada! Entre em contato com o departamento de vendas!' ).Show;
    stFAILED_DEPENDENCY     : begin
      TMsgError.New( 'A vers�o do execut�vel est� diferente da vers�o do servidor.' + #13#10 +
                     '� necess�rio efetuar a atualiza��o antes de prosseguir.' + #13#10 +
                     'Vers�o do Execut�vel: ' + TAppConstantes.Build + #13#10 +
                     'Vers�o do Servidor:' + Response.Value ).Show;
    end;
    stINTERNAL_SERVER_ERROR : TMsgError.New( 'Erro ao fazer login!' + #13#13 + Response.Value ).Show;
  else
    raise Exception.Create( 'Retorno n�o esperado (' + Response.Code.ToString + ')' );
  end;
end;

function TLoginDao.ConectaUsuarioSenha(ACnpj, AUsuario, ASenha: string): Boolean;
var
  Response : IHttpResponse;
  Json, jUsuario : IJson;
begin
  Response := THttpRequest.New(
      TUrl.New(
        BaseURLH10 + '/auth/login',
        TUrlParams.New
          .AddParam( 'cnpj', ACnpj )
          .AddParam( 'login', Trim( AUsuario ) )
          .AddParam( 'senha', ASenha )
          .AddParam( 'mac-address', TCfg.GetInstance.ComputerId )
          .AddParam( 'force-disconnect', 'false' )
          .AddParam( 'nome-computador', TCfg.GetInstance.ComputerId )
          .AddParam( 'software', 'INTEGRADOR' )
      )
    ).DoPost;
    case Response.Status of
      stUNAUTHORIZED     : TMsgInformation.New( 'Usu�rio e/ou senha inv�lidos!' ).Show;
      stFORBIDDEN        : TMsgInformation.New( 'Sem permiss�o para acessar nesse hor�rio!' ).Show;
      stOk               : begin
        Json := TJson.New( Response.Value );
        TCfg.GetInstance.SetToken( Json.Item( 'token' ).AsString );

        SalvaDadosUsuario( Json, AUsuario );
        Result := True;
      end;
      stINTERNAL_SERVER_ERROR : TMsgError.New( Response.Value ).Show;
    else
      TMsgError.New( '$AS118 Tipo ' + Response.Code.ToString + ' n�o esperado' ).Show;
    end;
end;

constructor TLoginDao.Create;
begin
  inherited Create;
  FDao := TDao.New( '/usuario' );
end;

function TLoginDao.Dao: IDao;
begin
  Result := FDao;
end;

function TLoginDao.FindByIdH10(AId: integer): IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      BaseURLH10 + '/usuario/' + IntToStr( AId ),
      TUrlParams.New
        .AddHeader( TAppConstantes.Token, TCfg.GetInstance.Token )
    )
  ).DoGet;
  if r.Status = stOK then begin
    Result := TJson.New( r.Value );
  end
  else begin
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
  end;
end;

class function TLoginDao.New: ILoginDao;
begin
  Result := Create;
end;

function TLoginDao.SalvaDadosEmpresa(AJson: IJson): Boolean;
var
  empresas: TArray<IJson>;
  emp: IJson;
begin
  empresas := TDaoH10.New( '/empresa' ).FindAll( 0, 0 );
  for emp in empresas do begin
    TDao.New( '/empresa' )
      .Save(
        TJson.New
          .AddInt( 'id', emp.ID )
          .AddString( 'nome', emp.Item( 'nome' ).AsString )
          .AddString( 'cpfCnpj', emp.Item( 'cpfCnpj' ).AsString )
      );
  end;
  Result := True;
end;

function TLoginDao.SalvaDadosUsuario(AJson: IJson; AUsuario: string): Boolean;
var
  jUsuario, jH10 : IJson;
begin
  jUsuario := Dao.FindByIdHabil( AJson.Item( 'idUsuario' ).AsInteger );
  jH10 := FindByIdH10( AJson.Item( 'idUsuario' ).AsInteger );

  if not Assigned( jUsuario ) then
    jUsuario := Dao.NewRecord;
  jUsuario
    .AddOrSetString( 'nome', AJson.Item( 'nomeUsuario' ).AsString )
    .AddOrSetString( 'login', AUsuario )
    .AddOrSetString( 'token', AJson.Item( 'token' ).AsString )
    .AddOrSetInt( 'idHabil', AJson.Item( 'idUsuario' ).AsInteger );

  if Assigned( jH10 ) then
    jUsuario.AddOrSetBoolean( 'administrador', jH10.Item( 'sysAdmin' ).AsBoolean );

  jUsuario := Dao.Save( jUsuario );
  TCfg.GetInstance.SetUsuario( jUsuario );
end;

end.
