unit Dao.Configuracao;

interface

uses
  System.SysUtils, System.Classes, Dao.Interfaces, Http.Interfaces,
  Json.Interfaces;

type
  IConfiguracaoDao = interface( IDaoWithBaseDao<IDao> )
    ['{D2C0FEDE-ABE7-44F3-AF5A-3B56DDCE438B}']
    function Get : IJson;
  end;
  TConfiguracaoDao = class( TInterfacedObject, IConfiguracaoDao )
  private
    FDao : IDao;
    function BaseURI : string;
    function BaseParams : IUrlParams;
    constructor Create;
  public
    class function New : IConfiguracaoDao;
    function Dao : IDao;
    function Get: IJson;
  end;

implementation

uses
  Dao.Base, Http.Base, Cfg, Json.Base, Exceptions;

{ TConfiguracaoDao }

function TConfiguracaoDao.BaseParams: IUrlParams;
begin
  Result := TUrlParams.New;
end;

function TConfiguracaoDao.BaseURI: string;
begin
  Result := TCfg.GetInstance.API + '/configuracao';
end;

constructor TConfiguracaoDao.Create;
begin
  inherited Create;
  FDao := TDao.New( '/configuracao' );
end;

function TConfiguracaoDao.Dao: IDao;
begin
  Result := FDao;
end;


function TConfiguracaoDao.Get: IJson;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New( BaseURI + '/get',
      BaseParams
    )
  ).DoGet;
  if r.Status = stOK then
    Result := TJson.New( r.Value )
  else
    raise EHttpStatusNotExpectedError.Create(r.Code, r.Value);
end;

class function TConfiguracaoDao.New: IConfiguracaoDao;
begin
  Result := Create;
end;

end.
