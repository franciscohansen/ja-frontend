unit Util.ConsultaLocalizacao.Interfaces;

interface

uses
  Model.Endereco.Interfaces;

type
  IConsultaCep = interface
    ['{F7CECCC4-E4A6-4721-BF74-FC730E417760}']
    function Achou: boolean;
    function Endereco: IEndereco;
  end;

  IConsultaUF = interface
  ['{99C83731-287C-49F8-9D77-3E66427FE7CB}']
    function Achou : boolean;
    function Municipios : TArray<IMunicipioIBGE>;
  end;

  IConsultaPaises = interface
  ['{22CF2BEC-6F93-4BB0-A44C-251BCDD651BD}']
    function Paises : TArray<IPaisIBGE>;
  end;



implementation

end.
