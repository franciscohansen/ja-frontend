unit Util.ConsultaLocalizacao.Base;

interface

uses Util.ConsultaLocalizacao.Interfaces, System.SysUtils, Model.Endereco.Interfaces, Model.Enums, Json.Interfaces;

type
  TConsultaCep = class( TInterfacedObject, IConsultaCep )
  private
    FEndereco: IEndereco;
    FAchou: boolean;
    procedure Consulta( Cep: string );
    procedure PreencheEndereco( JsonString: string );
  public
    constructor Create( CEP: string );
    class function New( Cep: string ): IConsultaCep;
    function Achou: boolean;
    function Endereco: IEndereco;
  end;

  TConsultaUF = class( TInterfacedObject, IConsultaUF )
  protected
    constructor Create( UF : string ); overload;
  private
    FUF : string;
    FAchou : boolean;
    FMunicipios : TArray<IMunicipioIBGE>;
    procedure Consulta;
    procedure CarregaListaMunicipios( JsonArray : TArray<IJson> );
  public
    class function New( UF : TUF ) : IConsultaUF; overload;
    class function New( UF : String ) : IConsultaUF; overload;
    function Achou : boolean;
    function Municipios : TArray<IMunicipioIBGE>;
  end;

  TConsultaPaises = class( TInterfacedObject, IConsultaPaises )
  private
    FPaises : TArray<IPaisIBGE>;
    procedure Consulta;
    procedure CarregaListaPaises( JsonArray : TArray<IJson> );
  public
    constructor Create();
    class function New : IConsultaPaises; overload;
    function Paises : TArray<IPaisIBGE>;
  end;


implementation

uses Model.Endereco.Base, Http.Base, Http.Interfaces, Util.Format,
  Json.Base, Cfg.Constantes, Cfg;

{ TConsultaCep }

function TConsultaCep.Achou: boolean;
begin
  Result := FAchou;
end;

procedure TConsultaCep.Consulta(Cep: string);
var
  Resp: IHttpResponse;
begin
  Resp := THttpRequest.New(
    TUrl.New(
      'https://viacep.com.br/ws/',
      TUrlParams.New
        .AddParam( TFormat.ClearFormat( Cep, '' ) )
        .AddParam( 'json' )
    )
  ).DoGet;
  if Resp.Status = stOK then begin
    PreencheEndereco( Resp.Value );
  end;
end;

constructor TConsultaCep.Create(CEP: string);
begin
  inherited Create;
  FEndereco := nil;
  FAchou := false;
  Consulta( Cep );
end;

function TConsultaCep.Endereco: IEndereco;
begin
  Result := FEndereco;
end;

class function TConsultaCep.New(Cep: string): IConsultaCep;
begin
  Result := TConsultaCep.Create( Cep );
end;

procedure TConsultaCep.PreencheEndereco(JsonString: string);
var
  Retorno: IJson;
begin
  if not JsonString.Contains( '"erro":' ) then begin
    Retorno := TJson.New( JsonString );
    FEndereco := TEndereco.New(
      TJson.New
        .AddString( 'logradouro', Retorno.Item( 'logradouro' ).AsString )
        .AddString( 'complemento', Retorno.Item( 'complemento' ).AsString )
        .AddString( 'numero', '' )
        .AddString( 'bairro', Retorno.Item( 'bairro' ).AsString )
        .AddString( 'cep', Retorno.Item( 'cep' ).AsString )
        .AddString( 'uf', Retorno.Item( 'uf' ).AsString )
        .AddString( 'municipio', Retorno.Item( 'localidade' ).AsString )
        .AddString( 'pais', 'Brasil' )
    );
    FAchou := true;
  end;
end;

{ TConsultaUF }

function TConsultaUF.Achou: boolean;
begin
  Result := FAchou;
end;

procedure TConsultaUF.CarregaListaMunicipios( JsonArray : TArray<IJson> );
var
  m : IMunicipioIBGE;
  i,l : Integer;
  j : IJson;
begin
  l := Length( JsonArray );
  SetLength( FMunicipios, l );
  for i := 0 to l -1 do begin
    j := JsonArray[i];
    m := TMunicipioIBGE.New( j );
    FMunicipios[ i ] := m;
  end;
end;

procedure TConsultaUF.Consulta;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New( TUrl.New( TCfg.GetInstance.H10API + '/localizacao/lista-municipios/' ,
    TUrlParams.New
      .AddParam( FUF )
      .AddHeader( TAppConstantes.Token, TCfg.GetInstance.Token )
    )
  )
  .DoGet;
  {r := THttpRequestBuilder.New
    .UrlDestino( urlLocalizacao )
    .Url( 'lista-municipios' )
    .UrlParams(
        TUrlParams.New
          .AddParam( FUF )
    ).Builder.DoGet; }
  FAchou := r.Status = stOK;
  if FAchou then begin
    CarregaListaMunicipios( TJsonArray.New( r.Value ) );
  end;
end;

constructor TConsultaUF.Create(UF: string);
begin
  inherited Create;
  FUF := UF;
  Consulta;
end;

function TConsultaUF.Municipios: TArray<IMunicipioIBGE>;
begin
  Result := FMunicipios;
end;

class function TConsultaUF.New(UF: String ): IConsultaUF;
begin
  Result := Create( UF );
end;

class function TConsultaUF.New(UF: TUF): IConsultaUF;
begin
  Result := Create( UF.AsShortString );
end;


{ TConsultaPaises }

procedure TConsultaPaises.CarregaListaPaises(JsonArray: TArray<IJson>);
var
  i, l : Integer;
  j : IJson;
  p : IPaisIBGE;
begin
  l := Length( JsonArray );
  SetLength( FPaises, l );
  for i := 0 to l -1 do begin
    j := JsonArray[ i ];
    p := TPaisIBGE.New( j );
    FPaises[ i ] := p;
  end;
end;

procedure TConsultaPaises.Consulta;
var
  r : IHttpResponse;
begin
  r := THttpRequest.New(
    TUrl.New(
      TCfg.GetInstance.H10API + '/localizacao/lista-paises',
      TUrlParams.New
        .AddHeader( TAppConstantes.Token, TCfg.GetInstance.Token )
    )
  ).DoGet;
  {r := THttpRequestBuilder.New
    .UrlDestino( urlLocalizacao )
    .Url( 'lista-paises' )
    .Builder.DoGet; }
  if r.Status = stOk then begin
    CarregaListaPaises( TJsonArray.New( r.Value ) );
  end;
end;

constructor TConsultaPaises.Create();
begin
  inherited Create;
end;

class function TConsultaPaises.New: IConsultaPaises;
begin
  Result := TConsultaPaises.Create;
end;

function TConsultaPaises.Paises: TArray<IPaisIBGE>;
begin
  Consulta;
  Result := FPaises;
end;

end.
