unit Util.Format;

interface

uses
  System.SysUtils, System.Classes, cxTL, Winapi.Windows, cxMaskEdit, cxTextEdit, cxEdit, cxCalc, cxCalendar, cxControls,
  cxSpinEdit, cxCheckBox, Vcl.ComCtrls,
  Generics.Collections, DateUtils, cxButtonEdit, {View.Patterns, }System.RegularExpressions, System.RegularExpressionsAPI,
  System.RegularExpressionsConsts, System.RegularExpressionsCore,
  System.Variants;

type
  IGridFormat = interface
    ['{A90B387D-46A6-4AE8-8A5B-B6E79D4E186B}']
    function ColumnsAutoWidth( b: boolean ): IGridFormat;
    function Editable( b: boolean ): IGridFormat;
    function Deletable( b : boolean ) : IGridFormat;
    function ShowNavigationBar( b: boolean ): IGridFormat;
    function ShowRoot( Show: boolean ): IGridFormat;
    function ShowHeader( Show: boolean ): IGridFormat;
    function ShowBorder( Show: boolean ): IGridFormat;
    function MultiSelect( AMultiSelect: boolean ): IGridFormat;
    function Insertable( b: boolean ): IGridFOrmat;
  end;

  TGridFormat = class( TInterfacedObject, IGridFormat )
  private
    FGrids: TArray<TcxTreeList>;
    //FPattern: IFormPattern;
    procedure FormataColunasPeloTipoDeDado;
    constructor Create( Grids: array of TcxTreeList );
    type TPercorreGrids = reference to procedure ( Grid: TcxTreeList );
    procedure PercorreGrids( Funcao: TPercorreGrids );
  public
    class function New( Grid: TcxTreeList ): IGridFormat; overload;
    class function New( Grids: array of TcxTreeList ): IGridFormat; overload;
    function ColumnsAutoWidth( b: boolean ): IGridFormat;
    function Editable( b: boolean ): IGridFormat;
    function Deletable( b : boolean ) : IGridFormat;
    function Insertable( b: boolean ): IGridFOrmat;
    function ShowNavigationBar( b: boolean ): IGridFormat;
    function ShowRoot( Show: boolean ): IGridFormat;
    function ShowHeader( Show: boolean ): IGridFormat;
    function ShowBorder( Show: boolean ): IGridFormat;
    function MultiSelect( AMultiSelect: boolean ): IGridFormat;
  end;

  IEditFormat = interface
    ['{170ECDC1-055F-41C7-938E-3951973ED710}']
    function ReadOnly( ReadOnly : boolean ) : IEditFormat;
    function IsPassword( IsPassword : boolean ) : IEditFormat;
  end;

  IMaskEditFormat = interface( IEditFormat )
  ['{B0255C43-5A16-488A-8848-302975E3081A}']
    function MaskKind( AMaskKind : TcxEditMaskKind ) : IMaskEditFormat;
    function Mask( AMask : string ) : IMaskEditFormat;
  end;

  TMaskEditFormat = class( TInterfacedObject, IMaskEditFormat )
  protected
    constructor Create( MaskEdit : TcxMaskEdit );
  private
    FEdit : TcxMaskEdit;
  public
    function ReadOnly( ReadOnly : boolean ) : IEditFormat;
    function IsPassword( IsPassword : boolean ) : IEditFormat;
    function MaskKind( AMaskKind : TcxEditMaskKind ) : IMaskEditFormat;
    function Mask( AMask : string ) : IMaskEditFormat;
    class function New( MaskEdit : TcxMaskEdit ) : IMaskEditFormat; overload;
    class function New( ButtonEdit : TcxButtonEdit ) : IMaskEditFormat; overload;
  end;

  TFormat = class
  public
    class function CpfCnpj( s: string ): String;
    class function Cnae( s : string ) : string;
    class function IP( s : string ) : string;
    class function Cep( s: string ): String;
    class function Date( d: TDate; Format: string = '' ): string;
    class function DateTime( d: TDateTime; Format: string = '' ): string;
    class function DiaDaSemana( d: TDate; Abreviado: boolean ): string;
    class function Float( f: real; Formato: string = '' ): string;
    class function FilePath( s : string ) : string;
    class function RichTextToStr( s: string ): string;
    class function ClearFormat( s: string; Caracters: string = '' ): string;
    class function FormatGrid( Grid: TcxTreeList ): IGridFormat; overload;
    class function FormatGrid( Grids: array of TcxTreeList ): IGridFormat; overload;
    class function FormattedStrToDateTime( AString : string; ADateFormat : string = 'yyyy-mm-dd';
                ATimeFormat : string = 'hh:nn:ss.zzz'; ADateSep : char = '-'; ATimeSep : char = ':' ): TDateTime;
    class function FormattedStrToDate( AString: String; ADateFormat: string = 'yyyy-mm-dd'; ADateSep: char = '-' ): TDate;
    class function NullCheck( AVar : Variant; ADefault : Variant ) : Variant;
  end;

  TDateFormat = class
  public
    class function InicioDoDia( Date : TDateTime ) : TDateTime;
    class function FimDoDia( Date : TDateTime ) : TDateTime;
  end;

implementation

{ TFormat }

uses Cfg.Constantes;

class function TFormat.Cep(s: string): String;
begin
  if s.Trim.IsEmpty then
    Exit( '' );

  s := ClearFormat( s.Trim );
  Insert( '-', s, 6 );
  Result := s;
end;

class function TFormat.ClearFormat(s, Caracters: string): string;
var
  i: integer;
begin
  if Caracters = '' then
    Caracters := '~`*()!@#$%^&*-+\|?/<>:;",.[]{}- ';
  for i := 1 to Length( Caracters ) do begin
    while Pos( Caracters[ i ], s ) > 0 do
      Delete( s, Pos( Caracters[ i ], s ), 1 );
  end;
  Result := s;
end;

class function TFormat.Cnae(s: string): string;
begin
  if ( s = '' ) then
    Exit( '' );
  s := ClearFormat( s.Trim );
  Insert( '-', s, 5 );
  Insert( '/', s, 7 );
  Result := s;
end;

class function TFormat.IP(s: string): string;
//var
//  rx : TRegExpr;
//  st : TMemoryStream;
//  matches : TRegExprMatches;
//  m : TMatch;
//  i : Integer;
begin
//  if s.Trim.IsEmpty then
//    Exit( '' );
//  Result := '';
//  st := TMemoryStream.Create;
//  rx := TRegExpr.Create;
//  rx.Source  := PWideChar( TAppConstantes.MascaraIP );
//  rx.Pattern   := PWideChar( s.Trim );
//  rx.SaveToStream( st );
//  SetString( Result, PChar(st.Memory), st.Size div SizeOf( Char ) );
end;


class function TFormat.NullCheck(AVar, ADefault: Variant): Variant;
begin
  Result := ADefault;
  if ( not VarIsNull( AVar ) ) and ( not VarIsEmpty( AVar ) ) then
    Result := AVar;
end;

class function TFormat.CpfCnpj(s: string): String;
begin
  if ( s = '' ) then
    Exit( '' );

  Result := ClearFormat( s.Trim );
  case Result.Length of
    11: begin
      Insert( '.', Result, 4 );
      Insert( '.', Result, 8 );
      Insert( '-', Result, 12 );
    end;
    14: begin
      Insert( '.', Result, 3 );
      Insert( '.', Result, 7 );
      Insert( '/', Result, 11 );
      Insert( '-', Result, 16 );
    end;
  else
    Exit( s );
  end;
end;

class function TFormat.Date(d: TDate; Format: string): string;
begin
  Result := '';
  if d > 0 then begin
    if Format = '' then
      Format := 'dd/mm/yyyy';
    Result := FormatDateTime( Format, d );
  end;
end;

class function TFormat.DateTime(d: TDateTime; Format: string): string;
begin
  Result := '';
  if d > 0 then begin
    if Format = '' then
      Format := 'dd/mm/yyyy hh:nn:ss';
    Result := FormatDateTime( Format, d )
  end;
end;

class function TFormat.DiaDaSemana(d: TDate; Abreviado: boolean): string;
begin
  case DayOfWeek( d ) of
    1: Result := 'Domingo';
    2: Result := 'Segunda-feira';
    3: Result := 'Ter�a-feira';
    4: Result := 'Quarta-feira';
    5: Result := 'Quinta-feira';
    6: Result := 'Sexta-feira';
    7: Result := 'S�bado';
  end;
  if Abreviado then
    Result := Copy( Result, 1, 3 );
end;

class function TFormat.FilePath(s: string): string;
begin
  s := s.Replace( '\', '/', [ rfReplaceAll ] );
  Result := s;
end;

class function TFormat.Float(f: real; Formato: string): string;
begin
  if Formato = '' then
    Formato := TAppConstantes.FormatFloat;
  Result := FormatFloat( Formato, f );
end;

class function TFormat.FormatGrid( Grid: TcxTreeList ): IGridFormat;
begin
  Result := TGridFormat.New( Grid );
end;

class function TFormat.FormatGrid(
  Grids: array of TcxTreeList): IGridFormat;
begin
  Result := TGridFormat.New( Grids );
end;

class function TFormat.FormattedStrToDate(AString, ADateFormat: string;
  ADateSep: char): TDate;
var
  d: TDateTime;
  dia, mes, ano: word;
begin
  d := TFormat.FormattedStrToDateTime( AString, ADateFormat, 'hh:nn:ss.zzz', ADateSep, ':' );
  DecodeDate( d, ano, mes, dia );
  Result := EncodeDate( ano, mes, dia );
end;

class function TFormat.FormattedStrToDateTime(AString, ADateFormat,
  ATimeFormat: string; ADateSep, ATimeSep: char): TDateTime;
var
  fmt : TFormatSettings;
begin
  fmt := TFormatSettings.Create( GetThreadLocale ); //GetLocaleFormatSettings( GetThreadLocale, fmt );
  fmt.TimeSeparator   := ATimeSep;
  fmt.DateSeparator   := ADateSep;
  fmt.ShortDateFormat := ADateFormat;
  fmt.LongTimeFormat  := ATimeFormat;
  Result := StrToDateTime( AString, fmt );
end;


class function TFormat.RichTextToStr(s: string): string;
var
  RTFConverter   : TRichEdit;
  MyStringStream : TStringStream;
begin
  try
    RTFConverter := TRichEdit.CreateParented( HWND_MESSAGE );
    RTFConverter.WordWrap := false;
    try
      MyStringStream := TStringStream.Create( s );

      RTFConverter.Lines.LoadFromStream( MyStringStream );
      RTFConverter.PlainText := true;
      Result := RTFConverter.Lines.Text;
    finally
      MyStringStream.Free;
    end;
  finally
    RTFConverter.Free;
  end;
end;

{ TGridFormat }

function TGridFormat.ColumnsAutoWidth(b: boolean): IGridFormat;
begin
  Result := Self;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    g.OptionsView.ColumnAutoWidth := b;
  end );
end;

constructor TGridFormat.Create(Grids: array of TcxTreeList);
var
  List: TList<TcxTreeList>;
  g: TcxTreeList;
begin
  inherited Create;
  //FPattern := TFormPattern.New;
  // vergonha de ter feito isso, mas n�o achei uma forma de transformar a porra
  // de um 'array of' em um 'TArray<T>', ent�o fiz essa obra de arte abaixo
  // me julguem!
  List := TList<TcxTreeList>.Create();
  try
    for g in Grids do
      List.Add( g );
    FGrids := List.ToArray;
  finally
    List.Free;
  end;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    g.OptionsView.CellEndEllipsis := true;
    g.OptionsBehavior.HeaderHints := true;
  end );
  Self
    .Editable( false )
    .ShowRoot( false )
    .ShowBorder( true );
  FormataColunasPeloTipoDeDado;
end;

function TGridFormat.Deletable(b: boolean): IGridFormat;
begin
  Result := Self;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    with g do begin
      OptionsData.Deleting  := b;
      Navigator.Visible := b;
      Navigator.Buttons.ConfirmDelete := False;
      with Navigator.Buttons do begin
        Delete.Visible       := b;
      end;
    end;
  end );
end;

function TGridFormat.Editable(b: boolean): IGridFormat;
begin
  Result := Self;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    with g do begin
      OptionsData.Editing   := b;
      OptionsData.Inserting := b;
      OptionsData.Deleting  := b;

      OptionsSelection.CellSelect := b;

      Navigator.Visible := b;

      with Navigator.Buttons do begin
        Delete.Visible       := b;
        Insert.Visible       := b;//b;

        ConfirmDelete        := false;
        Append.Visible       := false;//false;
        Cancel.Visible       := false;
        Edit.Visible         := false;
        Filter.Visible       := false;
        First.Visible        := false;
        GotoBookmark.Visible := false;
        Last.Visible         := false;
        Next.Visible         := false;
        NextPage.Visible     := false;
        Post.Visible         := false;
        Prior.Visible        := false;
        PriorPage.Visible    := false;
        Refresh.Visible      := false;
        SaveBookmark.Visible := false;
      end;
    end;
  end );
end;

procedure TGridFormat.FormataColunasPeloTipoDeDado;
begin
  PercorreGrids( procedure( g: TcxTreeList )
  var
    i: integer;
  begin
    for i := 0 to g.ColumnCount - 1 do begin
      with g.Columns[ i ] do begin
        if DataBinding.ValueType = 'Float' then begin
          Width := 100;
          Options.Sizing := false;
          Caption.AlignHorz := taRightJustify;
          PropertiesClass := TcxCalcEditProperties;
          Caption.AlignHorz := taRightJustify;
          with TcxCalcEditProperties( g.Columns[ i ].Properties ) do begin
            Alignment.Horz := taRightJustify;

            DisplayFormat := TAppConstantes.FormatFloat;
            Precision := Length( Copy( DisplayFormat, Pos( '.', DisplayFormat ) + 1 ) ) + 3;  // isso � para evitar que apare�a valores no formato cient�fico
          end;
          //FPattern.SetCasasDecimais( g.Columns[ i ] );
        end
        else if DataBinding.ValueType = 'DateTime' then begin
          Width := 100;
          Options.Sizing := false;
          Caption.AlignHorz := taCenter;
          PropertiesClass := TcxDateEditProperties;
          Caption.AlignHorz := taCenter;
          TcxDateEditProperties( Properties ).Alignment.Horz := taCenter;
        end
        else if DataBinding.ValueType = 'Integer' then begin
          Width := 60;
          Options.Sizing := false;
          Caption.AlignHorz := taRightJustify;
          PropertiesCLass := TcxSpinEditProperties;
          TcxSpinEditProperties( Properties ).Alignment.Horz := taRightJustify;
        end
        else if DataBinding.ValueType = 'Boolean' then begin
          Caption.AlignHorz := taCenter;
          PropertiesClass := TcxCheckBoxProperties;
        end;
      end;
    end;
  end );
end;

function TGridFormat.Insertable(b: boolean): IGridFOrmat;
begin
  Result := Self;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    g.OptionsData.Inserting := b;
  end );
end;

function TGridFormat.MultiSelect(AMultiSelect: boolean): IGridFormat;
begin
  Result := Self;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    g.OptionsSelection.MultiSelect := AMultiSelect;
  end );
end;

class function TGridFormat.New(Grids: array of TcxTreeList ): IGridFormat;
begin
  Result := TGridFormat.Create( Grids );
end;

procedure TGridFormat.PercorreGrids(Funcao: TPercorreGrids);
var
  g: TcxTreeList;
begin
  for g in FGrids do
    Funcao( g );
end;

class function TGridFormat.New(Grid: TcxTreeList ): IGridFormat;
begin
  Result := TGridFormat.Create( TArray<TcxTreeList>.Create( Grid ) );
end;

function TGridFormat.ShowBorder(Show: boolean): IGridFormat;
begin
  Result := Self;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    if Show then
      g.BorderStyle := cxcbsDefault
    else
      g.BorderStyle := cxcbsNone;
  end );
end;

function TGridFormat.ShowHeader(Show: boolean): IGridFormat;
begin
  Result := Self;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    g.OptionsView.Headers := Show;
  end );
end;

function TGridFormat.ShowNavigationBar(b: boolean): IGridFormat;
begin
  Result := Self;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    g.Navigator.Visible := b;
    // se n�o mostra a barra, n�o deixa inserir nem apagar, s� editar os registros
    with g.OptionsData do begin
      Inserting := b;
      Deleting  := b;
    end;
  end );
end;

function TGridFormat.ShowRoot(Show: boolean): IGridFormat;
begin
  Result := Self;
  PercorreGrids( procedure( g: TcxTreeList )
  begin
    g.OptionsView.ShowRoot := Show;
  end );
end;

{ TMaskEditFormat }

constructor TMaskEditFormat.Create(MaskEdit: TcxMaskEdit);
begin
  inherited Create;
  FEdit := MaskEdit;
end;

function TMaskEditFormat.IsPassword(IsPassword: boolean): IEditFormat;
begin
  Result := Self as IEditFormat;
  if IsPassword then
    FEdit.Properties.EchoMode := eemPassword
  else
    FEdit.Properties.EchoMode := eemNormal;
end;

function TMaskEditFormat.Mask(AMask: string): IMaskEditFormat;
begin
  Result := Self;
  FEdit.Properties.EditMask := AMask;
end;

function TMaskEditFormat.MaskKind(
  AMaskKind: TcxEditMaskKind): IMaskEditFormat;
begin
  Result := Self;
  FEdit.Properties.MaskKind := AMaskKind;
end;

class function TMaskEditFormat.New(
  ButtonEdit: TcxButtonEdit): IMaskEditFormat;
begin
  Result := Create( TcxMaskEdit( ButtonEdit ) );
end;

class function TMaskEditFormat.New(MaskEdit: TcxMaskEdit): IMaskEditFormat;
begin
  Result := Create( MaskEdit );
end;

function TMaskEditFormat.ReadOnly(ReadOnly: boolean): IEditFormat;
begin
  Result := Self as IEditFormat;
  FEdit.Properties.ReadOnly := ReadOnly;
end;

{ TDateFormat }

class function TDateFormat.FimDoDia(Date: TDateTime): TDateTime;
begin
  Result := RecodeTime( Date, 23, 59, 59, 999 );
end;

class function TDateFormat.InicioDoDia(Date: TDateTime): TDateTime;
begin
  Result := RecodeTime( Date, 0, 0, 0, 0 );
end;

end.
