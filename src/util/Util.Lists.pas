unit Util.Lists;

interface

uses
  Generics.Collections;

type

  TFilterFunction<T> = reference to function( obj : T ) : boolean;
  TForEachFunction<T> = reference to function( obj : T ) : boolean;


  IList<T> = interface
    ['{B8F51CF8-D8D5-43AE-8C80-2CFC3A95457E}']
    function Add( Item: T ): IList<T>;
    function AddAll( Items : TArray<T> ) : IList<T>;
    function ToArray: TArray<T>;
    function Item( i: integer ): T;
    function Replace( i: integer; Item: T ): IList<T>;
    function Count: integer;
    function Delete( i: integer ): IList<T>;
    function Contains( Item: T ): boolean;
    function Filter( AFilter : TFilterFunction<T> ) : IList<T>;
    function ForEach( AForEach : TForEachFunction<T> ) : IList<T>;
    function Find( AFilter : TFilterFunction<T> ) : T;
    function IsEmpty : Boolean;
    function Clear : IList<T>;
  end;

  IMap<TKey,TValue> = interface
    ['{568145E0-D3F0-492C-B4BE-9F34AD5F77C3}']
    function Clear : IMap<TKey,TValue>;
    function Add( Key: TKey; Value: TValue ): IMap<TKey, TValue>;
    function Item( Key: TKey ): TValue;
    function Count: integer;
    function KeysToArray: TArray<TKey>;
    function ValuesToArray: TArray<TValue>;
    function ContainsKey( Key: TKey ): boolean;
    function Delete( Key: TKey ): boolean;
    function AddOrReplace( Key: TKey; Value: TValue ): IMap<TKey, TValue>;
  end;

  TList<T> = class( TInterfacedObject, IList<T> )
  private
    FList: Generics.Collections.TList<T>;
  public
    constructor Create;
    destructor Destroy; override;
    class function New: IList<T>; overload;
    class function New( IniciaCom: array of T ): IList<T>; overload;
    class function New( IniciaCom: T ): IList<T>; overload;
    function Add( Item: T ): IList<T>;
    function AddAll( Items : TArray<T> ) : IList<T>;
    function ToArray: TArray<T>;
    function Item( i: integer ): T;
    function Replace( i: integer; Item: T ): IList<T>;
    function Count: integer;
    function Delete( i: integer ): IList<T>;
    function Contains( Item: T ): boolean;
    function Clear : IList<T>;
    class function Sort( Arr: TArray<T> ): TArray<T>;
    function Filter( AFilter : TFilterFunction<T> ) : IList<T>;
    function ForEach( AForEach : TForEachFunction<T> ) : IList<T>;
    function Find( AFilter : TFilterFunction<T> ) : T;
    function IsEmpty : Boolean;
  end;

  TMap<TKey, TValue> = class( TInterfacedObject, IMap<TKey, TValue> )
  private
    FItens: TDictionary<TKey, TValue>;
  public
    constructor Create;
    class function New: IMap<TKey,TValue>;
    class function Clone( AMap : IMap<TKey, TValue> ) : IMap<TKey, TValue>;
    destructor Destroy; override;
    function Add( Key: TKey; Value: TValue ): IMap<TKey, TValue>;
    function Item( Key: TKey ): TValue;
    function Count: integer;
    function KeysToArray: TArray<TKey>;
    function ValuesToArray: TArray<TValue>;
    function ContainsKey( Key: TKey ): boolean;
    function AddOrReplace( Key: TKey; Value: TValue ): IMap<TKey, TValue>;
    function Delete( Key: TKey ): boolean;
    function Clear : IMap<TKey,TValue>;
  end;

implementation

{ TList<T> }

function TList<T>.Add(Item: T): IList<T>;
begin
  Result := Self;
  FList.Add( Item );

end;

function TList<T>.AddAll(Items: TArray<T>): IList<T>;
var
  item : T;
begin
  Result := Self;
  for item in Items do
    FList.Add( item );
end;

function TList<T>.Clear: IList<T>;
begin
  Result := Self;
  FList.Clear;
  FList.TrimExcess;
end;

function TList<T>.Contains(Item: T): boolean;
begin
  Result := FList.Contains( Item );
end;

function TList<T>.Count: integer;
begin
  Result := FList.Count;
end;

constructor TList<T>.Create;
begin
  inherited Create;
  FList := Generics.Collections.TList<T>.Create;
end;

function TList<T>.Delete(i: integer): IList<T>;
begin
  Result := Self;
  FList.Delete( i );
end;

destructor TList<T>.Destroy;
begin
  FList.TrimExcess;
  FList.Free;
  inherited Destroy;
end;

function TList<T>.Filter(AFilter: TFilterFunction<T>): IList<T>;
var
  i : integer;
begin
  Result := TList<T>.New;
  for i := 0 to FList.Count -1 do begin
    if AFilter( FList.Items[ i ] ) then
      Result.Add( FList.Items[ i ] );
  end;
end;

function TList<T>.Find(AFilter: TFilterFunction<T>): T;
var
  i : integer;
begin
  for i := 0 to FList.Count -1 do begin
    if AFilter( FList.Items[ i ] ) then
      Exit( FList.Items[ i ] );
  end;
end;

function TList<T>.ForEach(AForEach: TForEachFunction<T>): IList<T>;
var
  i : Integer;
begin
  Result := Self;
  for i := 0 to FList.Count -1 do begin
    if AForEach( FList.Items[ i ] ) then
      Break;
  end;
end;

function TList<T>.IsEmpty: Boolean;
begin
  Result := Self.Count = 0;
end;

function TList<T>.Item(i: integer): T;
begin
  Result := FList.Items[ i ];
end;

class function TList<T>.New(IniciaCom: T): IList<T>;
begin
  Result := TList<T>.New( [ IniciaCom ] );
end;

class function TList<T>.New(IniciaCom: array of T): IList<T>;
var
  a: T;
begin
  Result := TList<T>.Create;
  for a in IniciaCom do
    Result.Add( a );
end;

function TList<T>.Replace(i: integer; Item: T): IList<T>;
begin
  Result := Self;
  FList.Items[ i ] := Item;
end;

class function TList<T>.Sort(Arr: TArray<T>): TArray<T>;
var
  AList: Generics.Collections.TList<T>;
begin
  AList := Generics.Collections.TList<T>.Create;
  try
    AList.AddRange( Arr );
    AList.Sort;
    Result := AList.ToArray;
  finally
    AList.Free;
  end;
end;

class function TList<T>.New: IList<T>;
begin
  Result := TList<T>.Create;
end;

function TList<T>.ToArray: TArray<T>;
begin
  Result := FList.ToArray;
end;

{ TMap<TKey, TValue> }

function TMap<TKey, TValue>.Add(Key: TKey;
  Value: TValue): IMap<TKey, TValue>;
begin
  Result := Self;
  FItens.Add( Key, Value );
end;

function TMap<TKey, TValue>.AddOrReplace(Key: TKey;
  Value: TValue): IMap<TKey, TValue>;
begin
  Result := Self;
  FItens.AddOrSetValue( Key, Value );
end;

function TMap<TKey, TValue>.Count: integer;
begin
  Result := FItens.Count;
end;

constructor TMap<TKey, TValue>.Create;
begin
  inherited Create;
  FItens := TDictionary<TKey,TValue>.Create;
end;

function TMap<TKey, TValue>.Delete(Key: TKey): boolean;
begin
  FItens.Remove( Key );
  Result := true;
end;

destructor TMap<TKey, TValue>.Destroy;
begin
  FItens.Free;
  inherited;
end;

function TMap<TKey, TValue>.Item(Key: TKey): TValue;
begin
  Result := FItens.Items[ Key ];
end;

function TMap<TKey, TValue>.Clear: IMap<TKey, TValue>;
begin
  Result := Self;
  FItens.Clear;
end;

class function TMap<TKey, TValue>.Clone(AMap: IMap<TKey, TValue>): IMap<TKey, TValue>;
var
  key : TKey;
begin
  Result := TMap<TKey, TValue>.Create;
  for key in AMap.KeysToArray do
    Result.Add( key, AMap.Item( key ) );
end;

function TMap<TKey, TValue>.ContainsKey(Key: TKey): boolean;
begin
  Result := FItens.ContainsKey( Key );
end;

function TMap<TKey, TValue>.KeysToArray: TArray<TKey>;
begin
  Result := FItens.Keys.ToArray
end;

class function TMap<TKey, TValue>.New: IMap<TKey, TValue>;
begin
  Result := TMap<TKey,TValue>.Create;
end;

function TMap<TKey, TValue>.ValuesToArray: TArray<TValue>;
begin
  Result := FItens.Values.ToArray;
end;

end.
