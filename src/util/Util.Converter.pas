unit Util.Converter;

interface

uses
  System.SysUtils, System.Classes, Util.Format;

type

  TJavaConverter = class
  public
    class function ToDate( d : TDateTime; EnviaHoraTambem: boolean = true ): string;
    class function ToBoolean( b: boolean ): string;
    class function ToDouble( d: double ): string;
    class function FromDouble( s : string ) : Real;
  end;

implementation

{ TJavaConverter }

class function TJavaConverter.FromDouble(s: string): Real;
var
  fs : TFormatSettings;
begin
  fs.DecimalSeparator := '.';
  Result := StrToFloat( s, fs );
end;

class function TJavaConverter.ToBoolean(b: boolean): string;
begin
  if b then
    Result := 'true'  // tem que ser minúsculo;
  else
    Result := 'false';
end;

class function TJavaConverter.ToDate(d: TDateTime;
  EnviaHoraTambem: boolean): string;
var
  sFormat: String;
begin
  Result := '';
  sFormat := 'yyyy-mm-dd';
  if EnviaHoraTambem then
    sFormat := sFormat + ' hh:nn:ss.zzz';
  if d > 0 then
    Result := FormatDateTime( sFormat, d );
end;

class function TJavaConverter.ToDouble(d: double): string;
begin
  Result := TFormat.Float( d )
    .Replace( '.', '', [ rfReplaceAll ] )
    .Replace( ',', '.', [ rfReplaceAll ] );
end;

end.
