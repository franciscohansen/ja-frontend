unit Util.View;

interface

uses
  System.SysUtils, Vcl.Controls, cxCheckBox, Vcl.Forms, dxLayoutContainer, Classes, dxLayoutControl, Winapi.Windows, Winapi.Messages,
  View.EmProcessamento, FileManager.Base, cxImage, Util.Converter, StrUtils;

type
  TScreenCursorProcedure = reference to procedure;

  TView = class
    class procedure PoeCursor( Owner: TForm; Funcao: TScreenCursorProcedure );
    class procedure CtrlState( cbox: TObject; Controls: array of TControl; Invertido: boolean = false ); overload;
    class procedure CtrlState( cbox: TObject; Control: TControl; Invertido: boolean = false); overload;
    class procedure PoeProcessamento( Owner: TForm; Funcao : TProcessamentoProc; Caption: string = ''; UsaThread: boolean = true );
  end;

  TAnonymousNotifyEvent = class( TComponent )
  private
    FProc : TProc<TObject>;
    constructor Create( AOwner : TComponent; AProc : TProc<TObject> );
  public
    class function New( AOwner : TComponent; AProc : TProc<TObject> ): TNotifyEvent;
  published
    procedure Event( Sender : TObject );
  end;

implementation

{ TView }

class procedure TView.CtrlState(cbox: TObject; Controls: array of TControl; Invertido: boolean);
var
  c: TControl;
begin
  for c in Controls do begin
    if Invertido then
      c.Enabled := not TcxCheckBox( cbox ).Checked
    else
      c.Enabled := TcxCheckBox( cbox ).Checked;
  end;
  if Controls[ 0 ].Enabled then try
    TWinControl( Controls[ 0 ] ).SetFocus;
  except
    // se der zebra, tudo bem
  end;
end;

class procedure TView.CtrlState(cbox: TObject; Control: TControl; Invertido: boolean);
begin
  TView.CtrlState( cbox, [ Control ], Invertido );
end;

class procedure TView.PoeCursor(Owner: TForm; Funcao: TScreenCursorProcedure);
var
  tagMSG: MSG;
begin
  Screen.Cursor := crHourGlass;
  try
    Funcao;
  finally
    if Owner <> nil then begin
      repeat
        // esse c�digo � pra limpar o cache dos cliques do mouse
        // tudo que o usu�rio clicou enquanto tava "travado" aqui ele limpa
      until PeekMessageA( tagMSG, Owner.Handle, WM_MOUSEFIRST, WM_MOUSELAST, PM_REMOVE ) = False;
    end;
    Screen.Cursor := crDefault;
  end;
end;

class procedure TView.PoeProcessamento(Owner: TForm; Funcao: TProcessamentoProc; Caption: string; UsaThread: boolean);
var
  tagMSG: MSG;
  h : THandle;
  AErro: string;
begin
  Screen.Cursor := crHourGlass;
  try
    if Assigned( Owner ) then
      Owner.Enabled := false;
    TfrmEmProcessamento.Process( Owner, Funcao, AErro, Caption, UsaThread );
  finally
    if Assigned( Owner ) then begin
      Owner.Enabled := true;
      h := Owner.Handle;
    end;

    repeat
      // esse c�digo � pra limpar o cache dos cliques do mouse
      // tudo que o usu�rio clicou enquanto tava "travado" aqui ele limpa
    until PeekMessageA( tagMSG, h, WM_MOUSEFIRST, WM_MOUSELAST, PM_REMOVE ) = False;
    Screen.Cursor := crDefault;
  end;
  if not AErro.IsEmpty then
    raise Exception.Create( AErro );
end;

{ TAnonymousNotifyEvent }

constructor TAnonymousNotifyEvent.Create(AOwner: TComponent;
  AProc: TProc<TObject>);
begin
  inherited Create( AOwner );
  FProc := AProc;
end;

procedure TAnonymousNotifyEvent.Event(Sender: TObject);
begin
  FProc( Sender );
end;

class function TAnonymousNotifyEvent.New(AOwner: TComponent;
  AProc: TProc<TObject>): TNotifyEvent;
begin
  Result := TAnonymousNotifyEvent.Create( AOwner, AProc ).Event;
end;

end.
