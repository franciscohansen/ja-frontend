unit Util.Strings;

interface

uses
  System.SysUtils;

type
  TStringUtils = class
  private
    class function Cryp( src: string; encrypt: boolean ): string;
  public
    class function TiraAcentos( s: string ): string;
    class function Criptografa( s: string ): string;
    class function Descriptografa( s: string ): string;
  end;

implementation

{ TStringUtils }

class function TStringUtils.Criptografa(s: string): string;
begin
  Result := Cryp( s, true );
end;

class function TStringUtils.Cryp(src: string; encrypt: boolean): string;
const
  Key = 'rv=]=^7QSfY&^j?CWjE\]BZRFa KPNS~[9jEDz@_@H_Y_!TMFSh';
var
  KeyLen    : integer;
  KeyPos    : integer;
  offset    : integer;
  dest      : string;
  SrcPos    : integer;
  SrcAsc    : integer;
  TmpSrcAsc : integer;
  Range     : integer;
begin
  KeyLen := Length( Key );
  KeyPos := 0;
  SrcPos := 0;
  SrcAsc := 0;
  Range := 256;
  if Encrypt then begin
    Randomize;
    offset := Random( Range );
    dest := format( '%1.2x', [ offset ] );
    for SrcPos := 1 to Length( Src ) do begin
      SrcAsc := ( Ord( Src[ SrcPos ] ) + offset ) mod 255;
      if KeyPos < KeyLen then
        KeyPos := KeyPos + 1
      else
        KeyPos := 1;
      SrcAsc := SrcAsc xor Ord( Key[ KeyPos ] );
      dest := dest + format( '%1.2x', [ SrcAsc ] );
      offset := SrcAsc;
    end;
  end
  else begin
    try
      offset := StrToInt( '$' + Copy( Src, 1, 2 ) );
      SrcPos := 3;
      repeat
        SrcAsc := StrToInt( '$' + Copy( Src, SrcPos, 2 ) );
        if KeyPos < KeyLen then
          KeyPos := KeyPos + 1
        else
          KeyPos := 1;
        TmpSrcAsc := SrcAsc xor Ord( Key[ KeyPos ] );
        if TmpSrcAsc <= offset then
          TmpSrcAsc := 255 + TmpSrcAsc - offset
        else
          TmpSrcAsc := TmpSrcAsc - offset;
        dest := dest + chr( TmpSrcAsc );
        offset := SrcAsc;
        SrcPos := SrcPos + 2;
      until SrcPos >= Length( Src );
    except
      // LOGAR ESSES ERROS...
    end;
  end;
  Result := dest;
end;

class function TStringUtils.Descriptografa(s: string): string;
begin
  Result := Cryp( s, false );
end;

class function TStringUtils.TiraAcentos(s: string): string;
var
  Acent, NAcent : string;
  aa, bb        : integer;
begin
  Acent  := '������������������������������������������';
  NAcent := 'aAaAaAaAaAeEeEeEEeiIiIiIoOoOoOoOOouUuUuUcC';
  for aa := 1 to Length( s ) do begin
    if ( s[ aa ] = '&' ) then
      Delete( s, aa, 1 );
    for bb := 1 to Length( Acent ) do
      if s[ aa ] = Acent[ bb ] then
        s[ aa ] := NAcent[ bb ];
  end;
  Result := s;
end;

end.
