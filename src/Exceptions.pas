unit Exceptions;

interface

uses
  System.SysUtils, System.TypInfo;

type
  EEnumNotImplementedError<E: record> = class( Exception )
  public
    constructor Create( Enum: integer );
  end;

  EValidationError = class( Exception )
  public
    constructor Create( Msg: string );
  end;

  EUnauthorizedError = class( Exception )
  public
    constructor Create;
  end;

  EServerError = class( Exception )
  public
    Constructor Create( Msg: String );
  end;

  EMissingPropertyError = class( Exception )
  public
    constructor Create( AClass: TClass; AProperty: String );
  end;

  EMissingJsonPropertyError = class( Exception )
  public
    constructor Create( AProperty: string );
  end;

  EInvalidJsonStringError = class( Exception )
  public
    constructor Create( AJsonString: string );
  end;

  EHttpStatusNotExpectedError = class( Exception )
  private
    FCode : Integer;
  public
    constructor Create( Code: integer; Msg: string );
    function Code : Integer;
  end;

  EReportError = class( Exception )
  public
    constructor Create( Msg: string );
  end;

  ERegisterError = class( Exception )
  public
    constructor Create( Msg: String );
  end;

  EMailError = class ( Exception )
  public
    constructor Create( Msg : string );

  end;

  TWinInetExceptionType = ( INTERNET_OUT_OF_HANDLES, INTERNET_TIMEOUT, INTERNET_EXTENDED_ERROR, INTERNET_INTERNAL_ERROR,
                            INTERNET_INVALID_URL, INTERNET_UNRECOGNIZED_SCHEME, INTERNET_NAME_NOT_RESOLVED,
                            INTERNET_PROTOCOL_NOT_FOUND, INTERNET_INVALID_OPTION, INTERNET_BAD_OPTION_LENGTH,
                            INTERNET_OPTION_NOT_SETTABLE, INTERNET_SHUTDOWN, INTERENT_INCORRECT_USER_NAME,
                            INTERNET_INCORRECT_PASSWORD, INTERNET_LOGIN_FAILURE, INTERNET_INVALID_OPERATION,
                            INTERNET_OPERATION_CANCELLED, INTERNET_INCORRECT_HANDLE_TYPE, INTERNET_INCORRECT_HANDLE_STATE,
                            INTERNET_NOT_PROXY_REQUEST, INTERNET_REGISTRY_VALUE_NOT_FOUND, INTERNET_BAD_REGISTRY_PARAMETER,
                            INTERNET_NO_DIRECT_ACCESS, INTERNET_NO_CONTEXT, INTERNET_NO_CALLBACK, INTERNET_REQUEST_PENDING,
                            INTERNET_INCORRECT_FORMAT, INTERNET_ITEM_NOT_FOUND, INTERNET_CANNOT_CONNECT,
                            INTERNET_CONNECTION_ABORTED, INTERNET_CONNECTION_RESET, INTERNET_FORCE_RETRY,
                            INTERNET_INVALID_PROXY_REQUEST, INTERNET_HANDLE_EXISTS, INTERNET_SEC_CERT_DATE_INVALID,
                            INTERNET_SEC_CERT_CN_INVALID, INTERNET_HTTP_TO_HTTPS_ON_REDIR, INTERNET_HTTPS_TO_HTTP_ON_REDIR, INTERNET_MIXED_SECURITY,
                            INTERNET_CHG_POST_IS_NON_SECURE, INTERENT_POST_IS_NON_SECURE,
                            FTP_TRANSFER_IN_PROGRESS, FTP_DROPPED,
                            GOPHER_PROTOCOL_ERROR, GOPHER_NOT_FILE, GOPHER_DATA_ERROR, GOPHER_END_OF_DATA,
                            GOPHER_INVALID_LOCATOR, GOPHER_INCORRECT_LOCATOR_TYPE, GOPHER_NOT_GOPHER_PLUS,
                            GOPHER_ATTRIBUTE_NOT_FOUND, GOPHER_UNKNOWN_LOCATOR,
                            HTTP_HEADER_NOT_FOUND, HTTP_DOWNLEVEL_SERVER, HTTP_INVALID_SERVER_RESPONSE,
                            HTTP_INVALID_HEADER, HTTP_INVALID_QUERY_REQUEST, HTTP_HEADER_ALREADY_EXISTS,
                            HTTP_REDIRECT_FAILED, WININET_UNKNOWN_ERROR );

  TWinInetExceptionTypeHelper = record helper for TWinInetExceptionType
  public
    function Code : Integer;
    class function FromString( AMsg : string ) : TWinInetExceptionType; static;
  end;
  WinInetError = class ( Exception )
  private
    FErrorType: TWinInetExceptionType;
    FErrorMessage: String;
    procedure SetErrorMessage(const Value: String);
    procedure SetErrorType(const Value: TWinInetExceptionType);

  public
    constructor Create( ErrorType : TWinInetExceptionType ; Msg : string );
    property ErrorType : TWinInetExceptionType read FErrorType write SetErrorType;
    property ErrorMessage : String read FErrorMessage write SetErrorMessage;
  end;
  EUserAlreadyConnectedError = class( Exception )
    constructor Create( AMessage : string );
  end;
  ELicenceNumberError = class( Exception )
    constructor Create( AMessage : string );
  end;
  EUnsuportedOperationError = class( Exception )
    constructor Create( AMessage : string ); overload;
    constructor Create( Classe, Rotina : string ); overload;
  end;
  EVarNotAssignedError = class( Exception )
    constructor Create( AMessage: String );
  end;

implementation

{ EUnauthorizedError }

constructor EUnauthorizedError.Create;
begin
  inherited Create( 'Acesso n�o autorizado' );
end;

{ EServerError }

constructor EServerError.Create(Msg: String);
begin
  inherited Create( Msg );
end;

{ EMissingPropertyError }

constructor EMissingPropertyError.Create(AClass: TClass;
  AProperty: String);
begin
  inherited Create( 'Classe ' + AClass.ClassName + ', Propriedade ' + AProperty + ' n�o implementada!' );
end;

{ EMissingJsonPropertyError }

constructor EMissingJsonPropertyError.Create(AProperty: string);
begin
  inherited Create( 'Propriedade ' + AProperty + ' n�o encontrada' );
end;

{ EInvalidJsonStringError }

constructor EInvalidJsonStringError.Create(AJsonString: string);
begin
  inherited Create( 'Json Inv�lido ' + AJsonString );
end;

{ EEnumNotImplemented }

constructor EEnumNotImplementedError<E>.Create(Enum: integer);
var
  Info: PTypeInfo;
  EnumName: string;
  ClassName: string;
begin
  Info := TypeInfo( E );
  EnumName := GetEnumName( Info, Enum );
  ClassName := String( Info.Name );
  inherited Create( 'Valor ' + EnumName + ' n�o implementado para ' + ClassName );
end;

{ EHttpStatusNotExpectedError }

function EHttpStatusNotExpectedError.Code: Integer;
begin
  Result := FCode;
end;

constructor EHttpStatusNotExpectedError.Create(Code: integer;
  Msg: string);
begin
  inherited Create( 'Retorno ' + Code.ToString + ' n�o esperado! Msg: ' + Msg );
  FCode := Code;
end;

{ EReportError }

constructor EReportError.Create(Msg: string);
begin
  inherited Create( Msg );
end;

{ TWinInetExceptionTypeHelper }

function TWinInetExceptionTypeHelper.Code: Integer;
begin
  case Self of
    INTERNET_OUT_OF_HANDLES           : Exit( 12001 );
    INTERNET_TIMEOUT                  : Exit( 12002 );
    INTERNET_EXTENDED_ERROR           : Exit( 12003 );
    INTERNET_INTERNAL_ERROR           : Exit( 12004 );
    INTERNET_INVALID_URL              : Exit( 12005 );
    INTERNET_UNRECOGNIZED_SCHEME      : Exit( 12006 );
    INTERNET_NAME_NOT_RESOLVED        : Exit( 12007 );
    INTERNET_PROTOCOL_NOT_FOUND       : Exit( 12008 );
    INTERNET_INVALID_OPTION           : Exit( 12009 );
    INTERNET_BAD_OPTION_LENGTH        : Exit( 12010 );
    INTERNET_OPTION_NOT_SETTABLE      : Exit( 12011 );
    INTERNET_SHUTDOWN                 : Exit( 12012 );
    INTERENT_INCORRECT_USER_NAME      : Exit( 12013 );
    INTERNET_INCORRECT_PASSWORD       : Exit( 12014 );
    INTERNET_LOGIN_FAILURE            : Exit( 12015 );
    INTERNET_INVALID_OPERATION        : Exit( 12016 );
    INTERNET_OPERATION_CANCELLED      : Exit( 12017 );
    INTERNET_INCORRECT_HANDLE_TYPE    : Exit( 12018 );
    INTERNET_INCORRECT_HANDLE_STATE   : Exit( 12019 );
    INTERNET_NOT_PROXY_REQUEST        : Exit( 12020 );
    INTERNET_REGISTRY_VALUE_NOT_FOUND : Exit( 12021 );
    INTERNET_BAD_REGISTRY_PARAMETER   : Exit( 12022 );
    INTERNET_NO_DIRECT_ACCESS         : Exit( 12023 );
    INTERNET_NO_CONTEXT               : Exit( 12024 );
    INTERNET_NO_CALLBACK              : Exit( 12025 );
    INTERNET_REQUEST_PENDING          : Exit( 12026 );
    INTERNET_INCORRECT_FORMAT         : Exit( 12027 );
    INTERNET_ITEM_NOT_FOUND           : Exit( 12028 );
    INTERNET_CANNOT_CONNECT           : Exit( 12029 );
    INTERNET_CONNECTION_ABORTED       : Exit( 12030 );
    INTERNET_CONNECTION_RESET         : Exit( 12031 );
    INTERNET_FORCE_RETRY              : Exit( 12032 );
    INTERNET_INVALID_PROXY_REQUEST    : Exit( 12033 );
    INTERNET_HANDLE_EXISTS            : Exit( 12036 );
    INTERNET_SEC_CERT_DATE_INVALID    : Exit( 12037 );
    INTERNET_SEC_CERT_CN_INVALID      : Exit( 12038 );
    INTERNET_HTTP_TO_HTTPS_ON_REDIR   : Exit( 12039 );
    INTERNET_HTTPS_TO_HTTP_ON_REDIR   : Exit( 12040 );
    INTERNET_MIXED_SECURITY           : Exit( 12041 );
    INTERNET_CHG_POST_IS_NON_SECURE   : Exit( 12042 );
    INTERENT_POST_IS_NON_SECURE       : Exit( 12043 );
    FTP_TRANSFER_IN_PROGRESS          : Exit( 12110 );
    FTP_DROPPED                       : Exit( 12111 );
    GOPHER_PROTOCOL_ERROR             : Exit( 12130 );
    GOPHER_NOT_FILE                   : Exit( 12131 );
    GOPHER_DATA_ERROR                 : Exit( 12132 );
    GOPHER_END_OF_DATA                : Exit( 12133 );
    GOPHER_INVALID_LOCATOR            : Exit( 12134 );
    GOPHER_INCORRECT_LOCATOR_TYPE     : Exit( 12135 );
    GOPHER_NOT_GOPHER_PLUS            : Exit( 12136 );
    GOPHER_ATTRIBUTE_NOT_FOUND        : Exit( 12137 );
    GOPHER_UNKNOWN_LOCATOR            : Exit( 12138 );
    HTTP_HEADER_NOT_FOUND             : Exit( 12150 );
    HTTP_DOWNLEVEL_SERVER             : Exit( 12151 );
    HTTP_INVALID_SERVER_RESPONSE      : Exit( 12152 );
    HTTP_INVALID_HEADER               : Exit( 12153 );
    HTTP_INVALID_QUERY_REQUEST        : Exit( 12154 );
    HTTP_HEADER_ALREADY_EXISTS        : Exit( 12155 );
    HTTP_REDIRECT_FAILED              : Exit( 12156 );
    WININET_UNKNOWN_ERROR             : Exit( 00000 );
  end;
end;

class function TWinInetExceptionTypeHelper.FromString(
  AMsg: string): TWinInetExceptionType;
var
  et : TWinInetExceptionType;
begin
  Result := WININET_UNKNOWN_ERROR;
  for et := Low( TWinInetExceptionType ) to High( TWinInetExceptionType ) do begin
    if Pos( IntToStr( et.Code ), AMsg ) > 0 then
      Exit( et );
  end;
end;

{ WinInetError }

constructor WinInetError.Create(ErrorType: TWinInetExceptionType; Msg: string);
begin
  inherited Create( Msg + ' [' + IntToStr( ErrorType.Code ) + ']' );
  FErrorType    := ErrorType;
  FErrorMessage := Msg;
end;

procedure WinInetError.SetErrorMessage(const Value: String);
begin
  FErrorMessage := Value;
end;

procedure WinInetError.SetErrorType(const Value: TWinInetExceptionType);
begin
  FErrorType := Value;
end;

{ EUserAlradyConnectedException }

constructor EUserAlreadyConnectedError.Create(AMessage: string);
begin
  inherited Create( AMessage );

end;

{ ELicenceNumberException }

constructor ELicenceNumberError.Create(AMessage: string);
begin
  inherited Create( AMessage );

end;

{ EUnsuportedOperationError }

constructor EUnsuportedOperationError.Create(AMessage: string);
begin
  inherited Create( AMessage );
end;

constructor EUnsuportedOperationError.Create(Classe, Rotina: string);
begin
  inherited Create( Classe + '::' + Rotina + ' not yet implemented' );
end;
{ ERegisterError }

constructor ERegisterError.Create(Msg: String);
begin
  inherited Create( Msg );
end;

{ EVarNotAssignedError }

constructor EVarNotAssignedError.Create(AMessage: String);
begin
  inherited Create( AMessage );
end;

{ EMailError }

constructor EMailError.Create(Msg: string);
begin
  inherited Create( Msg );
end;

{ EValidationError }

constructor EValidationError.Create(Msg: string);
begin
  inherited Create( Msg );
end;

end.
