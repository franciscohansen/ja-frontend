unit Model.Endereco.Interfaces;

interface
  uses
    Model.Enums, Json.Interfaces;

type
  IEndereco = interface
    ['{1BA41093-FB73-490A-82BA-50858E4A6419}']
    function Logradouro: string;
    function Numero: string;
    function Complemento: string;
    function Bairro: string;
    function Cep: string;
    function Pais: string;
    function UF: string;
    function Municipio: string;
    function Vazio : boolean;
    function Json : IJson;
  end;

  IMunicipioIBGE = interface
    function Nome : string;
    function Codigo : integer;
    function Uf : TUF;
  end;

  IPaisIBGE = interface
    function Nome : string;
    function Codigo : integer;
  end;

implementation

end.
