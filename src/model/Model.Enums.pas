unit Model.Enums;

interface

uses
  cxImageComboBox, Vcl.Graphics, System.Classes, System.SysUtils, System.StrUtils, Variants;

type
  TTipoPessoa = ( tpFisica, tpJuridica, tpMicroEmpresa, tpAmbas );
  TCrt = ( crtNone, crt1, crt2, crt3 );
  TCrtISSQN = ( crtNENHUM, crtISSQN1, crtISSQN2, crtISSQN3, crtISSQN4, crtISSQN5, crtISSQN6 );
  TSexoPessoa = ( sxMasculino, sxFeminino, sxOutro, sxNaoSeAplica );
  TEstadoCivil = ( ecCasado, ecSolteiro, ecViuvo, ecAmasiado, ecDesquitado, ecOutro, ecNaoSeAplica );
  TUF = ( ufNenhum, ufAC, ufAL, ufAM, ufAP, ufBA, ufCE, ufDF, ufES, ufGO, ufMA, ufMG, ufMS, ufMT, ufPA, ufPB,
          ufPE, ufPI, ufPR, ufRJ, ufRN, ufRO, ufRR, ufRS, ufSC, ufSE, ufSP, ufTO, ufEX );
  TTipoSanguineo = ( tsAPositivo, tsANegativo, tsBPositivo, tsBNegativo, tsABPositivo, tsABNegativo,
                     tsOPositivo, tsONegativo );
  TTipoClasse = ( tcPessoa, tcItem );
  TDiaDaSemana = ( dsSegunda, dsTerca, dsQuarta, dsQuinta, dsSexta, dsSabado, dsDomingo );
  TTipoLcto = ( tlRecebimento, tlPagamento );
  TCartaoTipo = ( ctCredito, ctDebito );
  TCartaoRede = ( crPadrao, crTecBan, crCrediShop, crHiperCard, crSiTef );
  TTipoCfop = ( cfopSaida, cfopEntrada, cfopRemessa, cfopRetorno );
  TPlanoPgtoTipo = ( ppVista, ppPrazo );
  TJurosTipo = ( jurSimples, jurComposto );
  TIcmsCst = ( icms_00, icms_10, icms_20, icms_30, icms_40, icms_41, {icms_45,} icms_50, icms_51, icms_60, icms_70,
               {icms_80, icms_81,} icms_90, icms_part10, icms_part90, icms_rep41, icms_rep60, {icms_icmsoutrasuf, icms_icmssn,}
               icms_101, icms_102, icms_103, icms_201, icms_202, icms_203, icms_300, icms_400, icms_500, icms_900  );


  TAcaoRapidaModo = ( armCriarESalvarJson, armCriarJson );
  TIpiCst = ( ipi_00, ipi_01, ipi_02, ipi_03, ipi_04, ipi_05, ipi_49, ipi_50, ipi_51, ipi_52, ipi_53, ipi_54, ipi_55, ipi_99 );
  TIpiEnquadramento = ( ipienq_001, ipienq_002, ipienq_003, ipienq_004, ipienq_005, ipienq_006, ipienq_007, ipienq_101, ipienq_102,
                        ipienq_103, ipienq_104, ipienq_105, ipienq_106, ipienq_107, ipienq_108, ipienq_109, ipienq_110, ipienq_111,
                        ipienq_112, ipienq_113, ipienq_114, ipienq_115, ipienq_116, ipienq_117, ipienq_118, ipienq_119, ipienq_120,
                        ipienq_121, ipienq_122, ipienq_123, ipienq_124, ipienq_125, ipienq_126, ipienq_127, ipienq_128, ipienq_129,
                        ipienq_130, ipienq_131, ipienq_132, ipienq_133, ipienq_134, ipienq_135, ipienq_136, ipienq_137, ipienq_138,
                        ipienq_139, ipienq_140, ipienq_141, ipienq_142, ipienq_143, ipienq_144, ipienq_145, ipienq_146, ipienq_147,
                        ipienq_148, ipienq_149, ipienq_150, ipienq_151, ipienq_152, ipienq_153, ipienq_154, ipienq_155, ipienq_156,
                        ipienq_157, ipienq_158, ipienq_159, ipienq_160, ipienq_161, ipienq_162, ipienq_301, ipienq_302, ipienq_303,
                        ipienq_304, ipienq_305, ipienq_306, ipienq_307, ipienq_308, ipienq_309, ipienq_310, ipienq_311, ipienq_312,
                        ipienq_313, ipienq_314, ipienq_315, ipienq_316, ipienq_317, ipienq_318, ipienq_319, ipienq_320, ipienq_321,
                        ipienq_322, ipienq_323, ipienq_324, ipienq_325, ipienq_326, ipienq_327, ipienq_328, ipienq_329, ipienq_330,
                        ipienq_331, ipienq_332, ipienq_333, ipienq_334, ipienq_335, ipienq_336, ipienq_337, ipienq_338, ipienq_339,
                        ipienq_340, ipienq_341, ipienq_342, ipienq_343, ipienq_344, ipienq_345, ipienq_346, ipienq_347, ipienq_348,
                        ipienq_349, ipienq_350, ipienq_351, ipienq_601, ipienq_602, ipienq_603, ipienq_604, ipienq_605, ipienq_606,
                        ipienq_607, ipienq_608, ipienq_999 );

  TPisCst = ( pis_01, pis_02, pis_03, pis_04, pis_05, pis_06, pis_07, pis_08, pis_09, pis_49, pis_50, pis_51, pis_52, pis_53, pis_54,
              pis_55, pis_56, pis_60, pis_61, pis_62, pis_63, pis_64, pis_65, pis_66, pis_67, pis_70, pis_71, pis_72, pis_73, pis_74,
              pis_75, pis_98, pis_99 );
  TCofinsCst = ( cofins_01, cofins_02, cofins_03, cofins_04, cofins_05, cofins_06, cofins_07, cofins_08, cofins_09, cofins_49, cofins_50,
                 cofins_51, cofins_52, cofins_53, cofins_54, cofins_55, cofins_56, cofins_60, cofins_61, cofins_62, cofins_63, cofins_64,
                 cofins_65, cofins_66, cofins_67, cofins_70, cofins_71, cofins_72, cofins_73, cofins_74, cofins_75, cofins_98, cofins_99 );
  TEnderecoEmissaoTipo = ( enderPrincipal, enderCobranca, enderEntrega );
  TClassificacaoDestinatario = ( cdNenhum, cdContribuinte, cdContribuinteIsento, cdNaoContribuinte );
  TConsumidorFinal = ( cfNaoDefinido, cfNao, cfSim );

  TTipoMovimentacao = ( tmCompra, tmVenda, tmOrcamento, tmOrdemServico, tmOrdemCompra, tmCotacao );

  TCompraEVendaTipo = ( cvtCompra, cvtVenda, cvtDevolucaoFornecedor, cvtRetornoMercadorias, cvtTransferenciaEstoque,
                        cvtRemessaFornecedor, cvtDevolucaoCliente, cvtCompraAlmoxarifado, cvtComplementar, cvtAjuste );

  TCompraEVendaSituacao = ( cvsPendente, cvsEstoqueBaixado, cvsFaturado );
  TRelatorioAgendadoStatus = ( agQueued, agRunning, agCompleted, agFailed );

  TContentType = ( imgPng, imgJpeg, imgBmp, appOctetStream, appPDF, appXML, imgGif, appSWF, appRTF, textHtml );

  TNFSePadrao = ( nfsepOutro, nfsepBoanf, nfsepIpm, nfsepAssessorPublico, nfsepNFPSe, nfsepBetha, nfsepNfPaulistana, nfsepIssNet );

  TLctoAntecipadoOrigem = ( laoManual, laoOrdemCompra, laoOrcamento, laoOrdemServico, laoCompra, laoVenda, laoPDV, laoDevolucao, laoRctoConta, laoPgtoConta );

  TOrdemCompraStatus = ( ocsPendente, ocsLiberado, ocsRejeitado, ocsCancelado, ocsFinalizado );

  TModFrete = ( mfContaEmitente, mfContaDestinatario, mfContaTerceiros, mfSemFrete, mfProprioRemetente, mfProprioDestinatario  );

  TTipoEmpresa = ( teMatriz, teFilial, teDeposito, teEscritorio, teProdutorRural );

  TAutorizadoTipo = ( atTodasCompras, atSomenteEstaCompra );

  TModeloNota = ( mn1, mn1A, mn2, mn3, mn3A, mn6, mn21, mn22, mn55, mn65 );

  TNewsTipo = ( newsNews, newsBanner, newsMensagem );
  TNewsFiltroEmissao = ( nfeAmbos, nfeEmite, nfeNaoEmite );
  TNewsBannerPrioridade = ( nbpBaixa, nbpMedia, nbpAlta );

  TTarefaPrioridade = ( tpUrgente, tpAlta, tpMedia, tpBaixa, tpInsignificante );

  TBoletoTipoCarteira   = ( btcSimples, btcRegistrada, btcEletronica );
  TBoletoTipoEmissao    = ( bteClienteEmite, bteBancoEmite, bteBancoReemite, bteBancoNaoReemite  );
  TBoletoCNAB           = ( cnab240, cnab400 );
  TBoletoTipoDocumento  = ( btdTradicional, btdEscritural );
  TBoletoCaracTitulo    = ( ticSimples, ticVinculado, ticCaucionado, ticDescontado, ticVendor );
  TBoletoTipoInscricao  = ( btiFisica, btiJuridica, btiOutros );

  TNFRefTipo = ( nfrNFE_NFCE, nfr1_1A, nfrProdutorRural, nfrCupomFiscal, nfrCte );
  TModRefNFE = ( rnProdutor, rnAvulsa, rnModelo2 );
  TModRefECF = ( reM2B, reM2C, reM2D );

  TTipoOrcamento = ( toOrcamento, toCondicional );
  TOrigemOrcamento = ( ooManual, ooForcaVenda, ooNuvemShop );
  TTipoPapelImpressao = ( piBobina, piA4, piCarta, piOficio, piOficio2, piOficio9, piTabloide );

  TPrioridadeOrdemServico = ( tposUrgente, tposAlta, tposMedia, tposBaixa );

  TOrdemProducaoTipo = ( optImediata, optEmProducao );
  TOrdemProducaoStatus = ( opsPendente, opsEmAndamento, opsFinalizado, opsCancelado );

  TImportacaoViaTransp = ( vtMaritima, vtFluvial, vtLacustre, vtAerea, vtPostal, vtFerroviaria, vtRodoviaria, vtConduto, vtMeiosProprios, vtEntradaSaidaFicta );
  TImportacaoIntermedio = ( intContaPropria, intContaEOrdem, intEncomenda );

  TPrioridadeOrdemServicoHelper = record helper for TPrioridadeOrdemServico
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: String ): TPrioridadeOrdemServico; static;
    class procedure FillCbb( ACbb : TcxImageComboBox ); static;
  end;

  TModeloNotaHelper = record helper for TModeloNota
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: String ): TModeloNota; static;
  end;

  TTarefaPrioridadeHelper = record helper for TTarefaPrioridade
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: String ): TTarefaPrioridade; static;
  end;

  TOrdemProducaoTipoHelper = record helper for TOrdemProducaoTipo
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: String ): TOrdemProducaoTipo; static;
  end;
  TOrdemProducaoStatusHelper = record helper for TOrdemProducaoStatus
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: String ): TOrdemProducaoStatus; static;
  end;


  TTipoEmpresaHelper = record helper for TTipoEmpresa
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: String ): TTipoEmpresa; static;
  end;

  TModFreteHelper = record helper for TModFrete
  public
    function Value: string;
    function Description: string;
    class function ByValue ( Value: string ): TModFrete; static;
  end;

  TTipoMovimentacaoHelper = record helper for TTipoMovimentacao
  public
    function Description : string;
    function Value : string;
    class function ByValue( ATipo : string ) : TTipoMovimentacao; static;
  end;

  TCompraEVendaTipoHelper = record helper for TCompraEVendaTipo
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: string ): TCompraEVendaTipo; static;
  end;

  TOrdemCompraStatusHelper = record helper for TOrdemCompraStatus
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: string ): TOrdemCompraStatus; static;
  end;


  TCompraEVendaSituacaoHelper = record helper for TCompraEVendaSituacao
  public
    function Value: String;
    function Description: string;
    class function ByValue( Value: string ): TCompraEVendaSituacao; static;
  end;

  TNFSePadraoHelper = record helper for TNFSePadrao
  public
    function Value: String;
    function Description: string;
    class function ByValue( Value: string ): TNFSePadrao; static;
  end;

  TCartaoTipoHelper = record helper for TCartaoTipo
  public
    function Value: string;
    function Descricao: string;
    class function ByValue( Value: string ): TCartaoTipo; static;
  end;

  TLctoAntecipadoOrigemHelper = record helper for TLctoAntecipadoOrigem
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: String ): TLctoAntecipadoOrigem; static;
  end;

  TCartaoRedeHelper = record helper for TCartaoRede
  public
    function Value: string;
    function Descricao: string;
    class function ByValue( Value: string ): TCartaoRede; static;
  end;

  TTipoLctoHelper = record helper for TTipoLcto
  public
    function Value: string;
    function Description: string;
    class function ByValue( Value: string ): TTipoLcto; overload; static;
    class function ByValue( Value: variant; ValorDefault: TTipoLcto ): TTipoLcto; overload; static;
    class function ByDescription( Description: string ): TTipoLcto; static;
  end;

  TTipoPessoaHelper = record helper for TTipoPessoa
  private
    const Names : array[ TTipoPessoa ] of String = ( 'F�sica', 'Jur�dica', 'Micro Empresa', 'Ambas' );
    const Values: array[ TTipoPessoa ] of String = ( 'FISICA', 'JURIDICA', 'MICRO_EMPRESA', 'AMBAS' );
  public
    function AsString : string;
    function AsInteger : integer;
    function Value     : string;
    class procedure FillCbb( ACbb : TcxImageComboBox; AUseAmbas : boolean = False ); static;
    class function FromCbb( ACbb : TcxImageComboBox ) : TTipoPessoa; static;
    class function ByValue( Value: string ): TTipoPessoa; static;
    class function FromString( AStr : String ) : TTipoPessoa; static;
  end;
  TCrtHelper = record helper for TCrt
  private
    const Names : array[ TCrt ] of string = ( '', 'Simples Nacional', 'Simples Nacional - excesso de sublimite de receita bruta', 'Regime Normal' );
    const Values: array[ TCrt ] of string = ( '', 'CRT_1', 'CRT_2', 'CRT_3' );
  public
    function Descricao : string;
    function Value     : string;
    //function AsInteger : integer;
    class procedure FillCbb( ACbb : TcxImageComboBox );static;
    class function FromCbb( ACbb : TcxImageComboBox ) : TCrt;static;
    class function FromInteger( AInt : Integer ) : TCrt; static;
    class function FromValue( Crt: string ): TCrt; static;
  end;
  TSexoPessoaHelper = record helper for TSexoPessoa
  private
    const
      Names : array[ TSexoPessoa ] of String = ( 'Masculino', 'Feminino', 'Outro', 'N�o se Aplica' );
      Values: array [ TSexoPessoa ] of string = ( 'MASCULINO', 'FEMININO', 'OUTRO', 'NAO_SE_APLICA' );
  public
    function AsString : String;
    function Value : string;
    function AsInteger : Integer;
    class procedure FillCbb( ACbb : TcxImageComboBox );static;
    class function FromCbb( ACbb : TcxImageComboBox ) : TSexoPessoa;static;
    class function ByValue( Value: string ): TSexoPessoa; static;
  end;
  TEstadoCivilHelper = record helper for TEstadoCivil
    private
      const
        Names : array[ TEstadoCivil ] of string = ( 'Casado', 'Solteiro', 'Vi�vo', 'Amasiado', 'Desquitado', 'Outro', 'N�o se aplica' );
        Values : array[ TEstadoCivil ] of string =( 'CASADO', 'SOLTEIRO', 'VIUVO', 'AMASIADO','DESQUITADO', 'OUTRO', 'NAO_SE_APLICA' );
    public
      function AsString : string;
      function Value : string;
      function AsInteger : integer;
      class procedure FillCbb( ACbb : TcxImageComboBox );static;
      class function FromCbb( ACbb : TcxImageComboBox ) : TEstadoCivil;static;
  end;
  TUFHelper = record helper for TUF
    private
      const
        Siglas  : array[ TUF ] of string = ( '', 'AC', 'AL', 'AM', 'AP', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MG', 'MS', 'MT',
                                             'PA', 'PB', 'PE', 'PI', 'PR', 'RJ', 'RN', 'RO', 'RR', 'RS', 'SC', 'SE', 'SP', 'TO',
                                             'EX' );
        Names   : array[ TUF ] of string = ( 'Nenhum', 'Acre', 'Alagoas', 'Amazonas', 'Amap�', 'Bahia', 'Cear�', 'Distrito Federal',
                                             'Espirito Santo', 'Goi�s', 'Maranh�o', 'Minas Gerais', 'Mato Grosso do Sul', 'Mato Grosso',
                                             'Par�', 'Para�ba', 'Pernambuco', 'Piau�', 'Paran�', 'Rio de Janeiro', 'Rio Grande do Norte',
                                             'Rond�nia', 'Roraima', 'Rio Grande do Sul', 'Santa Catarina', 'Sergipe', 'S�o Paulo', 'Tocantins',
                                             'Exterior' );
        Codigos : array[ TUF ] of integer =( 0, 12, 27, 16, 13, 29, 23, 53, 32, 52, 21, 31, 50, 51, 15, 25, 26, 22, 41, 33, 24, 11, 14,
                                             43, 42, 28, 35, 17, 999 );
    public
      function AsShortString  : string;
      function AsString       : string;
      function AsInteger      : integer;
      class procedure FillCbb( ACbb : TcxImageComboBox ); static;
      class function FromCbb( ACbb : TcxImageComboBox ) : TUF; static;
      class function FromString( S : string ) : TUF; static;
      class function FromCodigo( I : Integer ) : TUF; static;
  end;
  TTipoSanguineoHelper = record helper for TTipoSanguineo
    private
      const
        Siglas  : array [ TTipoSanguineo ] of string = ( 'A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-' );
        Names   : array [ TTipoSanguineo ] of string = ( 'A Positivo', 'A Negativo', 'B Positivo', 'B Negativo',
                                                          'AB Positivo', 'AB Negativo', 'O Positivo', 'O Negativo' );
        Values  : array [ TTipoSanguineo ] of string = ( 'A_POSITIVO', 'A_NEGATIVO', 'B_POSITIVO', 'B_NEGATIVO',
                                                         'AB_POSITIVO', 'AB_NEGATIVO', 'O_POSITIVO', 'O_NEGATIVO' );
    public
      function Value          : string;
      function AsShortString  : string;
      function AsString       : string;
      function AsInteger      : integer;
      class procedure FillCbb( ACbb : TcxImageComboBox ); static;
      class function FromCbb( ACbb : TcxImageComboBox ) : TTipoSanguineo; static;
  end;
  TTipoClasseHelper = record helper for TTipoClasse
    private
      const
        Names   : array [ TTipoClasse ] of string = ( 'Pessoa', 'Item' );
        Values  : array [ TTipoClasse ] of string = ( 'PESSOA', 'ITEM' );
    public
      function AsInteger : Integer;
      function AsString : String;
      function Value: string;
      class procedure FillCbb( ACbb : TcxImageComboBox ); static;
      class function FromCbb( ACbb : TcxImageComboBox ) : TTipoClasse; static;
  end;
  TDiaDaSemanaHelper = record helper for TDiaDaSemana
  public
    function AsInteger : Integer;
    function AsString : string;
    function Value : String;
    class function ValueOf( ADia : Integer ) : TDiaDaSemana; overload; static;
    class function ValueOf( ADia : string ) : TDiaDaSemana; overload; static;
  end;
  TCrtISSQNHelper = record helper for TCrtISSQN
  private
    const
      Names: array [ TCrtISSQN ] of string = ( 'Nenhum', 'Micro Empresa Municipal', 'Estimativa', 'Sociedade de Profissionais',
                                               'Cooperativa', 'MEI - Simples Nacional', 'ME EPP - Simples Nacional' );
      Values:array [ TCrtISSQN ] of string = ( 'NENHUM', 'CRT_ISSQN_1', 'CRT_ISSQN_2', 'CRT_ISSQN_3', 'CRT_ISSQN_4', 'CRT_ISSQN_5', 'CRT_ISSQN_6' );
  public
    function Descricao : string;
    function Value     : string;
    function AsInteger : integer;
    class procedure FillCbb ( ACbb : TcxImageComboBox ); static;
    class function FromCbb( ACbb : TcxImageComboBox ) : TCrtISSQN; static;
  end;

  TCfopTipoHelper = record helper for TTipoCfop
  private
    const
      Names : array [ TTipoCfop ] of string = ( 'Sa�das', 'Entradas', 'Remessas', 'Retornos' );
      Values: array [ TTipoCfop ] of string = ( 'CFOP_SAIDA', 'CFOP_ENTRADA', 'CFOP_REMESSA', 'CFOP_RETORNO' );
  public
    function Descricao : string;
    function Value : string;
    class procedure FillCbb( ACbb : TcxImageComboBox ); static;
    class function FromCbb( ACbb : TcxImageComboBox ) : TTipoCfop; static;
  end;
  TPlanoPgtoTipoHelper = record helper for TPlanoPgtoTipo
  private
    const
      Names : array [ TPlanoPgtoTipo ] of string = ( '� Vista', 'A Prazo' );
      Values : array[ TPlanoPgtoTipo ] of String = ( 'VISTA', 'PRAZO' );
  public
    function Descricao : string;
    function Value : string;
    class procedure FillCbb( ACbb : TcxImageComboBox ); static;
    class function FromCbb( ACbb : TcxImageComboBox ) : TPlanoPgtoTipo; static;
    class function FromString( AString : string ) : TPlanoPgtoTipo; static;
  end;

  TJurosTipoHelper = record helper for TJurosTipo
  public
    function Descricao : string;
    function Value : string;
    class procedure FillCbb( ACbb : TcxImageComboBox ); static;
    class function FromCbb( ACbb : TcxImageComboBox ) : TJurosTipo; static;
  end;

  TIcmsCstHelper = record helper for TIcmsCst
  public
    function Descricao : string;
    function Numero : string;
    function Value : string;
    class procedure FillCbb( ACbb : TcxImageComboBox ); static;
    class function ValueOf( AValue : string ) : TIcmsCst; static;
  end;
  TIpiCstHelper = record helper for TIpiCst
  public
    function Descricao : string;
    function Value : string;
    class procedure FillCbb( ACbb : TcxImageComboBox ); static;
  end;
  TIpiEnquadramentoHelper = record helper for TIpiEnquadramento
  public
    function Descricao : string;
    function Value : string;
    class procedure FillCbb( ACbb : TcxImageComboBox ); static;
  end;
  TPisCstHelper = record helper for TPisCst
  public
    function Descricao : string;
    function Value : string;
    class procedure FillCbb( ACbb : TcxImageComboBox ); static;
  end;
  TCofinsCstHelper = record helper for TCofinsCst
  public
    function Descricao : string;
    function Value : string;
    class procedure FillCbb( ACbb : TcxImageComboBox ); static;
  end;
  TEnderecoEmissaoTipoHelper = record helper for TEnderecoEmissaoTipo
  public
    function Descricao : string;
    function Value : string;
  end;
  TClassificacaoDestinatarioHelper = record helper for TClassificacaoDestinatario
  public
    function Descricao  : string;
    function Value      : string;
  end;
  TConsumidorFinalHelper = record helper for TConsumidorFinal
  public
    function Description: string;
    function Value: string;
    class function ByValue( Value: string ): TConsumidorFinal; static;
  end;

  TRelatorioAgendadoStatusHelper = record helper for TRelatorioAgendadoStatus
  public
    function Descricao : string;
    function Value     : string;
    class function FromValue( Value : string ) : TRelatorioAgendadoStatus; static;
  end;

  TContentTypeHelper = record helper for TContentType
  private
    const
      Values : array [ TContentType ] of String = ( 'IMG_PNG', 'IMG_JPEG', 'IMG_BMP', 'OCTET_STREAM', 'PDF', 'XML', 'IMG_GIF', 'SWF', 'RTF', 'HTML'  );
      Names: array [ TContentType ] of String = ( 'image/x-png', 'image/pjpeg', 'image/bmp', 'application/octet-stream',
                                                  'application/pdf', 'application/xml', 'image/gif', 'application/x-shockwave-flash', 'application/rtf', 'text/html' );
  public
    function Value : String;
    function Descricao : string;
    class function FromValue( AValue : string ) : TContentType; static;
    class function FromDescricao( ADescricao : string ) : TContentType; static;
    class function FromFileExt( AFileExt : string ) : TContentType; static;
  end;

  TAutorizadoTipoHelper = record helper for TAutorizadoTipo
  public
    function Value : string;
    function Descricao : string;
    class function ByValue( AValue : string ) : TAutorizadoTipo; static;

  end;

  TNewsTipoHelper = record helper for TNewsTipo
  public
    function Value : String;
    function Descricao : String;
    class function ByValue( AValue : string ) : TNewsTipo; static;
  end;
  TNewsFiltroEmissaoHelper = record helper for TNewsFiltroEmissao
  public
    function Value : String;
    function Descricao : String;
    class function ByValue( AValue : string ) : TNewsFiltroEmissao; static;
  end;
  TNewsBannerPrioridadeHelper = record helper for TNewsBannerPrioridade
  public
    function Value : String;
    function Descricao : String;
    class function ByValue( AValue : string ) : TNewsBannerPrioridade; static;
  end;


//   TBoletoTipoCarteira   = ( btcSimples, btcRegistrada, btcEletronica );
//  TBoletoTipoEmissao    = ( bteBancoEmite, bteBancoNaoReemite, bteBancoReemite, bteClienteEmite );
//  TBoletoCNAB           = ( cnab240, cnab400 );
//  TBoletoTipoDocumento  = ( btdTradicional, btdEscritural );
//TBoletoCaracTitulo    = ( ticSimples, ticVinculado, ticCaucionado, ticDescontado, ticVendor );
  TBoletoTipoCarteiraHelper = record helper for TBoletoTipoCarteira
  public
    function Description : String;
    function Value : String;
    class function ByValue( AValue : string ) : TBoletoTipoCarteira; static;
  end;

  TBoletoTipoEmissaoHelper = record helper for TBoletoTipoEmissao
  public
    function Description : String;
    function Value : String;
    class function ByValue( AValue : string ) : TBoletoTipoEmissao; static;
  end;

  TBoletoTipoDocumentoHelper = record helper for TBoletoTipoDocumento
  public
    function Description : String;
    function Value : String;
    class function ByValue( AValue : string ) : TBoletoTipoDocumento; static;
  end;
  TBoletoCaracTituloHelper = record helper for TBoletoCaracTitulo
  public
    function Description : String;
    function Value : String;
    class function ByValue( AValue : string ) : TBoletoCaracTitulo; static;
  end;
//  TBoletoTipoInscricao  = ( btiFisica, btiJuridica, btiOutros );
  TBoletoTipoInscricaoHelper = record helper for TBoletoTipoInscricao
  public
    function Description : String;
    function Value : String;
    class function ByValue( AValue : string ) : TBoletoTipoInscricao; static;
  end;

//  TNFRefTipo = ( nfrNFE_NFCE, nfr1_1A, nfrProdutorRural, nfrCupomFiscal, nfrCte );
  TNFRefTipoHelper = record helper for TNFRefTipo
  public
    function Description  : string;
    function Value        : string;
    class function ByValue( AValue : string ) : TNFRefTipo; static;
  end;

//  TModRefNFE = ( rnProdutor, rnAvulsa, rnModelo2 );
  TModRefNFeHelper = record helper for TModRefNFE
  public
    function Description  : string;
    function Value        : string;
    class function ByValue( AValue : string ) : TModRefNFE; static;
  end;
//  TModRefECF = ( reM2B, reM2C, reM2D );
  TModRefECFHelper = record helper for TModRefECF
  public
    function Description  : string;
    function Value        : string;
    class function ByValue( AValue : string ) : TModRefECF; static;
  end;

  TTipoOrcamentoHelper = record helper for TTipoOrcamento
  public
    function Description : String;
    function Value       : string;
    class function ByValue( AValue : string ) : TTipoOrcamento; static;
  end;

  TOrigemOrcamentoHelper = record helper for TOrigemOrcamento
  public
    function Description : String;
    function Value       : string;
    class function ByValue( AValue : string ) : TOrigemOrcamento; static;
  end;

//  TTipoPapelImpressao = ( piBobina, piA4, piCarta, piOficio, piOficio2, piOficio9, piTabloide );
  TTipoPapelImpressaoHelper = record helper for TTipoPapelImpressao
  public
    function Description : string;
    function Value : string;
    function X : Integer;
    function Y : Integer;
    class function ByValue( AValue : string ) : TTipoPapelImpressao; static;
  end;

  TImportacaoViaTranspHelper = record helper for TImportacaoViaTransp
  public
    function Description: string;
    function Value : string;
    class function ValueOf( AValue : string ) : TImportacaoViaTransp; static;
  end;

  TImportacaoIntermedioHelper = record helper for TImportacaoIntermedio
  public
    public
    function Description: string;
    function Value : string;
    class function ValueOf( AValue : string ) : TImportacaoIntermedio; static;
  end;

  procedure Cbb_Add( ACbb : TcxImageComboBox; ADesc : String; AValue : Variant );
  procedure Cbb_Begin( ACbb : TcxImageComboBox );
  procedure Cbb_Terminate( ACbb : TcxImageComboBox );

implementation

uses Exceptions;

function TTipoPessoaHelper.AsInteger: integer;
begin
  Result := Ord( Self );
end;

function TTipoPessoaHelper.AsString: string;
begin
  Result := Names[ Self ];
end;

class function TTipoPessoaHelper.ByValue(Value: string): TTipoPessoa;
begin
  for Result := Low( TTipoPessoa ) to High( TTipoPessoa ) do
    if Result.Value = Value then
      Break;
end;

class procedure TTipoPessoaHelper.FillCbb(ACbb: TcxImageComboBox; AUseAmbas : boolean);
var
  t : TTipoPessoa;
begin
  try
    Cbb_Begin( ACbb );
    for t := Low( TTipoPessoa ) to High( TTipoPessoa ) do begin
      if ( t = tpAmbas ) and not AUseAmbas then
        Continue;
      Cbb_Add( ACbb, t.AsString, t.Value );
    end;
  finally
    Cbb_Terminate( ACbb );
  end;
end;

class function TTipoPessoaHelper.FromCbb(ACbb: TcxImageComboBox): TTipoPessoa;
var
  t : TTipoPessoa;
begin
  for t := Low( TTipoPessoa ) to High( TTipoPessoa ) do
    if ACbb.EditValue = t.Value then
      Exit( t );
//  Result := TTipoPessoa( ACbb.EditValue );
end;

class function TTipoPessoaHelper.FromString(AStr: String): TTipoPessoa;
var
  p : TTipoPessoa;
begin
  for p := Low( TTipoPessoa ) to High( TTipoPessoa ) do
    if p.Value = AStr then
      Exit( p );
end;

function TTipoPessoaHelper.Value: string;
begin
  Result := Values[ Self ];
end;

{ TCrtHelper }

//function TCrtHelper.AsInteger: integer;
//begin
//  Result := Ord( Self );
//end;
//
function TCrtHelper.Descricao: string;
begin
  Result := Names[ Self ];
end;

class procedure TCrtHelper.FillCbb(ACbb: TcxImageComboBox);
var
  t : TCrt;
begin
  try
    Cbb_Begin( ACbb );
    for t := Low( TCrt ) to High( TCrt ) do
      Cbb_Add( ACbb, t.Descricao, t.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

class function TCrtHelper.FromCbb(ACbb: TcxImageComboBox): TCrt;
var
  crt : TCrt;
begin
  for crt := Low( TCrt ) to High( TCrt ) do
    if crt.Value = ACbb.EditValue then
      Exit( crt );
end;

class function TCrtHelper.FromInteger(AInt: Integer): TCrt;
begin
  Result := TCrt( AInt - 1 );
end;

class function TCrtHelper.FromValue(Crt: string): TCrt;
begin
  for Result := Low( TCrt ) to High( TCrt ) do
    if Result.Value = Crt then
      Break;
end;

function TCrtHelper.Value: string;
begin
  Result := Values[ Self ];
end;

procedure Cbb_Add( ACbb : TcxImageComboBox; ADesc : String; AValue : Variant );
begin
  with ACbb.Properties.Items.Add do begin
    Description := ADesc;
    Value       := AValue;
  end;
end;

procedure Cbb_Begin( ACbb : TcxImageComboBox );
begin
  ACbb.Properties.BeginUpdate;
  ACbb.Properties.Items.Clear;
end;
procedure Cbb_Terminate( ACbb : TcxImageComboBox );
begin
  ACbb.Properties.EndUpdate;
end;

{ TSexoPessoaHelper }

function TSexoPessoaHelper.AsInteger: Integer;
begin
  Result := Ord( Self );
end;

function TSexoPessoaHelper.AsString: String;
begin
  Result := Names[ Self ];
end;

class function TSexoPessoaHelper.ByValue(Value: string): TSexoPessoa;
begin
  if Value = '' then
    Exit( sxNaoSeAplica );
  for Result := Low( TSexoPessoa ) to High( TSexoPessoa ) do
    if Result.Value = Value then
      Break;
end;

class procedure TSexoPessoaHelper.FillCbb(ACbb: TcxImageComboBox);
var
  s : TSexoPessoa;
begin
  try
    Cbb_Begin( ACbb );
    for s := Low( TSexoPessoa ) to High( TSexoPessoa ) do
      Cbb_Add( ACbb, s.AsString, s.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

class function TSexoPessoaHelper.FromCbb(
  ACbb: TcxImageComboBox): TSexoPessoa;
var
  s : TSexoPessoa;
begin
  for s := Low( TSexoPessoa ) to High( TSexoPessoa ) do
    if s.Value = ACbb.EditValue then
      Exit( s );
end;

function TSexoPessoaHelper.Value: string;
begin
  Result := Values[ Self ];
end;

{ TEstadoCivilHelper }

function TEstadoCivilHelper.AsInteger: integer;
begin
  Result := Ord( Self );
end;

function TEstadoCivilHelper.AsString: string;
begin
  Result := Names[ Self ];
end;

class procedure TEstadoCivilHelper.FillCbb(ACbb: TcxImageComboBox);
var
  ec : TEstadoCivil;
begin
  try
    Cbb_Begin(ACbb);
    for ec := Low( TEstadoCivil ) to High( TEstadoCivil ) do
      Cbb_Add( ACbb, ec.AsString, ec.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

class function TEstadoCivilHelper.FromCbb(
  ACbb: TcxImageComboBox): TEstadoCivil;
var
  Est : TEstadoCivil;
begin
  for Est := Low( TEstadoCivil ) to High( TEstadoCivil ) do
    if Est.Value = ACbb.EditValue then
      Exit( Est );

//  Result := //TEstadoCivil( ACbb.EditValue );
end;

function TEstadoCivilHelper.Value: string;
begin
  Result := Values[ Self ];
end;

{ TUFHelper }

function TUFHelper.AsInteger: integer;
begin
  Result := Codigos[ Self ];
end;

function TUFHelper.AsShortString: string;
begin
  Result := Siglas[ Self ];
end;

function TUFHelper.AsString: string;
begin
  Result := Names[ Self ];
end;

class procedure TUFHelper.FillCbb(ACbb: TcxImageComboBox);
var
  uf : TUF;
begin
  try
    CBB_Begin( ACbb );
    for uf := Low( TUF ) to High( TUF ) do
      Cbb_Add( ACbb, uf.AsShortString, uf.AsShortString );
  finally
    Cbb_Terminate(ACbb);
  end;
end;

class function TUFHelper.FromCbb(ACbb: TcxImageComboBox): TUF;
var
  uf : TUF;
begin
  for uf := Low( TUF ) to High( TUF ) do begin
    if uf.AsShortString = ACbb.EditValue then
      Exit( uf );
  end;
end;

class function TUFHelper.FromCodigo(I: Integer): TUF;
var
  uf : TUF;
begin
  for uf := Low( TUF ) to High( TUF ) do begin
    if uf.AsInteger = I then
      Exit( uf );
  end;
end;

class function TUFHelper.FromString(S: string): TUF;
var
  uf : TUF;
begin
  for uf := Low( TUF ) to High( TUF ) do
    if uf.AsShortString = S then
      Exit( uf );
end;

{ TTipoSanguineoHelper }

function TTipoSanguineoHelper.AsInteger: integer;
begin
  Result := Ord( Self );
end;

function TTipoSanguineoHelper.AsShortString: string;
begin
  Result := Siglas[ Self ];
end;

function TTipoSanguineoHelper.AsString: string;
begin
  Result := Names[ Self ];
end;

class procedure TTipoSanguineoHelper.FillCbb(ACbb: TcxImageComboBox);
var
  ts : TTipoSanguineo;
begin
  try
    Cbb_Begin( ACbb );
    for ts := Low( TTipoSanguineo ) to High( TTipoSanguineo ) do
      Cbb_Add( ACbb, ts.AsString, ts.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

class function TTipoSanguineoHelper.FromCbb(
  ACbb: TcxImageComboBox): TTipoSanguineo;
var
  ts : TTipoSanguineo;
begin
  for ts := Low( TTipoSanguineo ) to High( TTipoSanguineo ) do begin
    if ts.Value = ACbb.EditValue then
      Exit( ts );
  end;
end;

function TTipoSanguineoHelper.Value: string;
begin
  Result := Values[ Self ];
end;

{ TTipoClasseHelper }

function TTipoClasseHelper.AsInteger: Integer;
begin
  Result := Ord( Self );
end;

function TTipoClasseHelper.Value: string;
begin
  Result := Values[ Self ];
end;

function TTipoClasseHelper.AsString: String;
begin
  Result := Names[ Self ];
end;

class procedure TTipoClasseHelper.FillCbb(ACbb: TcxImageComboBox);
var
  tc : TTipoClasse;
begin
  try
    Cbb_Begin( ACbb );
    for tc := Low( TTipoClasse ) to High( TTipoClasse ) do
      Cbb_Add( ACbb, tc.AsString, tc.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

class function TTipoClasseHelper.FromCbb(
  ACbb: TcxImageComboBox): TTipoClasse;
var
  tc : TTipoClasse;
begin
  for tc := Low( TTipoClasse ) to High( TTipoClasse ) do
    if tc.Value = ACbb.EditValue then
      Exit( tc );
end;

{ TDiaDaSemanaHelper }

function TDiaDaSemanaHelper.AsInteger: Integer;
begin
  Result := Ord( Self ) + 1;
end;

function TDiaDaSemanaHelper.AsString: string;
begin
  case Self of
    dsSegunda : Exit( 'Segunda-Feira' );
    dsTerca   : Exit( 'Ter�a-Feira' );
    dsQuarta  : Exit( 'Quarta-Feira' );
    dsQuinta  : Exit( 'Quinta-Feira' );
    dsSexta   : Exit( 'Sexta-Feira' );
    dsSabado  : Exit( 'S�bado' );
    dsDomingo : Exit( 'Domingo' );
  end;
end;

function TDiaDaSemanaHelper.Value: String;
begin
  case Self of
    dsSegunda : Exit( 'SEGUNDA_FEIRA' );
    dsTerca   : Exit( 'TERCA_FEIRA' );
    dsQuarta  : Exit( 'QUARTA_FEIRA' );
    dsQuinta  : Exit( 'QUINTA_FEIRA' );
    dsSexta   : Exit( 'SEXTA_FEIRA' );
    dsSabado  : Exit( 'SABADO' );
    dsDomingo : Exit( 'DOMINGO' );
  end;
end;

class function TDiaDaSemanaHelper.ValueOf(ADia: string): TDiaDaSemana;
var
  tDia : TDiaDaSemana;
begin
  for tDia := Low( TDiaDaSemana ) to High( TDiaDaSemana ) do
    if tDia.Value = ADia then
      Exit( tDia );
//  raise .Create('Error Message');
end;

class function TDiaDaSemanaHelper.ValueOf(ADia: Integer): TDiaDaSemana;
begin
  if ( ADia < 1 ) or ( ADia > 7 ) then
    raise Exception.Create('Valor inv�lido para o dia da semana(' + IntToStr( ADia ) + ')' );
  Result := TDiaDaSemana( ADia - 1 );
end;

{ TTipoLctoHelper }

class function TTipoLctoHelper.ByValue(Value: string): TTipoLcto;
begin
  Result := tlPagamento;
  if Value = tlRecebimento.Value then
    Result := tlRecebimento;
end;

class function TTipoLctoHelper.ByDescription(
  Description: string): TTipoLcto;
begin
  Result := tlPagamento;
  if Description = tlRecebimento.Description then
    Result := tlRecebimento;
end;

class function TTipoLctoHelper.ByValue(Value: variant;
  ValorDefault: TTipoLcto): TTipoLcto;
begin
  if VarToStrDef( Value, '' ) = '' then
    Exit( ValorDefault );
  Result := TTipoLcto.ByValue( VarToStr( Value ) );
end;

function TTipoLctoHelper.Description: string;
begin
  if Self = tlPagamento then
    Exit( 'Pagamento' )
  else
    Exit( 'Recebimento' );
end;

function TTipoLctoHelper.Value: string;
begin
  if Self = tlPagamento then
    Result := 'PAGAMENTO'
  else
    Result := 'RECEBIMENTO';
end;
{ TCrtISSQNHelper }

{ TCartaoTipoHelper }

class function TCartaoTipoHelper.ByValue(Value: string): TCartaoTipo;
begin
  for Result := Low( TCartaoTipo ) to High( TCartaoTipo ) do
    if Result.Value = Value then
      Break;
end;

function TCartaoTipoHelper.Descricao: string;
begin
  case Self of
    ctCredito: Exit( 'Cr�dito' );
    ctDebito: Exit( 'D�bito' );
  end;
end;

function TCartaoTipoHelper.Value: string;
begin
  case Self of
    ctCredito: Exit( 'CREDITO' );
    ctDebito: Exit( 'DEBITO' );
  end;
end;

{ TCartaoRedeHelper }

class function TCartaoRedeHelper.ByValue(Value: string): TCartaoRede;
begin
  for Result := Low( TCartaoRede ) to High( TCartaoRede ) do
    if Result.Value = Value then
      Break;
end;

function TCartaoRedeHelper.Descricao: string;
begin
  case Self of
    crPadrao    : Exit( 'Padr�o' );
    crTecBan    : Exit( 'TecBan' );
    crCrediShop : Exit( 'CrediShop' );
    crHiperCard : Exit( 'HiperCard' );
    crSiTef     : Exit( 'SiTEF' );
  else
    Raise EEnumNotImplementedError<TCartaoRede>.Create( Integer( Self ) );
  end;
end;

function TCartaoRedeHelper.Value: string;
begin
  case Self of
    crPadrao    : Exit( 'PADRAO' );
    crTecBan    : Exit( 'TECBAN' );
    crCrediShop : Exit( 'CREDISHOP' );
    crHiperCard : Exit( 'HIPERCARD' );
    crSiTef     : Exit( 'SITEF' );
  else
    Raise EEnumNotImplementedError<TCartaoRede>.Create( Integer( Self ) );
  end;
end;

function TCrtISSQNHelper.AsInteger: integer;
begin
  Result := Ord( Self ) + 1;
end;

function TCrtISSQNHelper.Descricao: string;
begin
  Result := Names[ Self ];
end;

class procedure TCrtISSQNHelper.FillCbb(ACbb: TcxImageComboBox);
var
  crt : TCrtISSQN;
begin
  try
    Cbb_Begin( ACbb );
    for crt := Low( TCrtISSQN ) to High( TCrtISSQN ) do
      Cbb_Add( ACbb, crt.Descricao, crt.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

class function TCrtISSQNHelper.FromCbb(ACbb: TcxImageComboBox): TCrtISSQN;
var
  crt : TCrtISSQN;
begin
  for crt := Low( TCrtISSQN ) to High( TCrtISSQN ) do
    if crt.Value = ACbb.EditValue then
      Exit( crt );
end;

function TCrtISSQNHelper.Value: string;
begin
  Result := Values[ Self ];
end;

{ TCfopTipoHelper }

function TCfopTipoHelper.Descricao: string;
begin
  Result := Names[ Self ];
end;

class procedure TCfopTipoHelper.FillCbb(ACbb: TcxImageComboBox);
var
  c : TTipoCfop;
begin
  try
    Cbb_Begin( ACbb );
    for c := Low( TTipoCfop ) to High( TTipoCfop ) do
      Cbb_Add( ACbb, c.Descricao, c.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

class function TCfopTipoHelper.FromCbb(ACbb: TcxImageComboBox): TTipoCfop;
var
  c : TTipoCfop;
begin
  for c := Low( TTipoCfop ) to High( TTipoCfop ) do
    if c.Value = ACbb.EditValue then
      Exit( c );
end;

function TCfopTipoHelper.Value: string;
begin
  Result := Values[ Self ];
end;

{ TPlanoPgtoTipoHelper }

function TPlanoPgtoTipoHelper.Descricao: string;
begin
  Result := Names[ Self ];
end;

class procedure TPlanoPgtoTipoHelper.FillCbb(ACbb: TcxImageComboBox);
var
  t : TPlanoPgtoTipo;
begin
  try
    Cbb_Begin( ACbb );
    for t := Low( TPlanoPgtoTipo ) to High( TPlanoPgtoTipo ) do
      Cbb_Add( ACbb, t.Descricao, t.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

class function TPlanoPgtoTipoHelper.FromCbb(
  ACbb: TcxImageComboBox): TPlanoPgtoTipo;
var
  t : TPlanoPgtoTipo;
begin
  for t := Low( TPlanoPgtoTipo ) to High( TPlanoPgtoTipo ) do
    if t.Value = ACbb.EditValue then
      Exit( t );
end;

class function TPlanoPgtoTipoHelper.FromString(
  AString: string): TPlanoPgtoTipo;
var
  t : TPlanoPgtoTipo;
begin
  for t := Low( TPlanoPgtoTipo ) to High( TPlanoPgtoTipo ) do
    if t.Value = AString then
      Exit( t );
end;

function TPlanoPgtoTipoHelper.Value: string;
begin
  Result := Values[ Self ];
end;

{ TJurosTipoHelper }

function TJurosTipoHelper.Descricao: string;
begin
  case Self of
    jurSimples  : Exit( 'Juros Simples' );
    jurComposto : Exit( 'Juros Composto' );
  end;
end;

class procedure TJurosTipoHelper.FillCbb(ACbb: TcxImageComboBox);
var
  j : TJurosTipo;
begin
  try
    Cbb_Begin( ACbb );
    for j := Low( TJurosTipo ) to High( TJurosTipo ) do
      Cbb_Add( ACbb, j.Descricao, j.Value );
  finally
    Cbb_Terminate( ACbb );
  end;

end;

class function TJurosTipoHelper.FromCbb(
  ACbb: TcxImageComboBox): TJurosTipo;
var
  j : TJurosTipo;
begin
  for j := Low( TJurosTipo ) to High( TJurosTipo ) do
    if j.Value = ACbb.EditValue then
      Exit( j );
end;

function TJurosTipoHelper.Value: string;
begin
  case Self of
    jurSimples  : Exit( 'SIMPLES' );
    jurComposto : Exit( 'COMPOSTO' );
  end;
end;



{ TIcmsCstHelper }

{

    icms_00, icms_10, icms_20, icms_30, icms_40, icms_41, icms_45, icms_50, icms_51, icms_60, icms_70,
    icms_80, icms_81, icms_90, icms_part10, icms_part90, icms_rep41, icms_icmsoutrasuf, icms_icmssn, icms_rep60,
    icms_101, icms_102, icms_103, icms_201, icms_202, icms_203, icms_300, icms_400, icms_500, icms_900  );

}


function TIcmsCstHelper.Descricao: string;
begin
  case Self of
    icms_00: Exit( '00 - Tributado integralmente' );
    icms_10: Exit( '10 - Tributado com substitui��o tribut�ria' );
    icms_20: Exit( '20 - Com redu��o de base de c�lculo' );
    icms_30: Exit( '30 - Isento com substitui��o tribut�ria' );
    icms_40: Exit( '40 - Isento' );
    icms_41: Exit( '41 - N�o tributado' );
    icms_50: Exit( '50 - Com suspens�o' );
    icms_51: Exit( '51 - Com diferimento' );
    icms_60: Exit( '60 - ICMS cobrado anteriormente' );
    icms_70: Exit( '70 - N�o incidente' );
    icms_90: Exit( '90 - Outras' );
    icms_101: Exit( '101 - Tributada com permiss�o de cr�dito' );
    icms_102: Exit( '102 - Tributada sem permiss�o de cr�dito' );
    icms_103: Exit( '103 - Isen��o do ICMS para faixa de receita bruta' );
    icms_201: Exit( '201 - Tributada com permiss�o de cr�dito e com cobran�a do ICMS por substitui��o tribut�ria' );
    icms_202: Exit( '202 - Tributada sem permiss�o de cr�dito e com cobran�a do ICMS por substitui��o tribut�ria' );
    icms_203: Exit( '203 - Isen��o do ICMS para faixa de receita bruta e com cobran�a do ICMS por substitui��o tribut�ria' );
    icms_300: Exit( '300 - Imune: Classificam-se neste c�digo as opera��es praticadas por optantes contempladas com imunidade do ICMS' );
    icms_400: Exit( '400 - N�o tributada' );
    icms_500: Exit( '500 - ICMS cobrado anteriormente por substitui��o tribut�ria (substitu�do) ou por antecipa��o' );
    icms_900: Exit( '900 - Outros' );
    //CHICO -- CST NOVOS
//    icms_45: Exit( '45 - Isento, N�o Tributado ou Diferido' ); //CHICO RED#2013
    //CHICO -- o 80 e 81 s�o somente para CT-e
//    icms_80: Exit( '80 - Responsabilidade do Recolhimento do ICMS Atribu�do ao Tomador ou 3� por Subst. Tribut�ria' ); //CHICO RED#2013
//    icms_81: Exit( '81 - ICMS Devido a outra UF' ); //CHICO RED#2013
//    icms_icmsoutrasuf : Exit( '90 - ICMS devido � UF de Origem da Presta��o, Quando diferente da UF do Emitente' ); //CHICO RED#2013
//    icms_icmssn       : Exit( '90 - Simples Nacional' ); //CHICO RED#2013
    {
      CHICO -- Comentei esses novos aaqui porque n sabemos a que se referem
      Precisamos ver pra que servem

    icms_part10: Exit( '10 - ICMS_PART10' );
    icms_part90: Exit( '90 - ICMS_PART90' );
    icms_rep41: Exit( '41 - ICMS_REP41' );
    icms_rep60: Exit( '60 - ICMS_REP60' );
    }
  else
    Exit( '' );
  end;
end;

class procedure TIcmsCstHelper.FillCbb(ACbb: TcxImageComboBox);
var
  t : TIcmsCst;
begin
  try
    Cbb_Begin( ACbb );
    for t := Low( TIcmsCst ) to High( TIcmsCst ) do
      Cbb_Add( ACbb, t.Descricao, t.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;


function TIcmsCstHelper.Numero: string;
begin
  case Self of
    icms_00 : Exit( '00' );
    icms_10 : Exit( '10' );
    icms_20 : Exit( '20' );
    icms_30 : Exit( '30' );
    icms_40 : Exit( '40' );
    icms_41 : Exit( '41' );
//    icms_45 : Exit( '45' ); //CHICO RED#2013
    icms_50 : Exit( '50' );
    icms_51 : Exit( '51' );
    icms_60 : Exit( '60' );
    icms_70 : Exit( '70' );
//    icms_80 : Exit( '80' ); //CHICO RED#2013
//    icms_81 : Exit( '81' ); //CHICO RED#2013
    icms_90 : Exit( '90' );
    icms_101: Exit( '101' );
    icms_102: Exit( '102' );
    icms_103: Exit( '103' );
    icms_201: Exit( '201' );
    icms_202: Exit( '202' );
    icms_203: Exit( '203' );
    icms_300: Exit( '300' );
    icms_400: Exit( '400' );
    icms_500: Exit( '500' );
    icms_900: Exit( '900' );
  end;
end;

function TIcmsCstHelper.Value: string;
begin
  Result := '';
  case Self of
    icms_00: Exit( 'ICMS_00' );
    icms_10: Exit( 'ICMS_10' );
    icms_20: Exit( 'ICMS_20' );
    icms_30: Exit( 'ICMS_30' );
    icms_40: Exit( 'ICMS_40' );
    icms_41: Exit( 'ICMS_41' );
    icms_50: Exit( 'ICMS_50' );
    icms_51: Exit( 'ICMS_51' );
    icms_60: Exit( 'ICMS_60' );
    icms_70: Exit( 'ICMS_70' );
    icms_90: Exit( 'ICMS_90' );
    icms_101: Exit( 'ICMS_101' );
    icms_102: Exit( 'ICMS_102' );
    icms_103: Exit( 'ICMS_103' );
    icms_201: Exit( 'ICMS_201' );
    icms_202: Exit( 'ICMS_202' );
    icms_203: Exit( 'ICMS_203' );
    icms_300: Exit( 'ICMS_300' );
    icms_400: Exit( 'ICMS_400' );
    icms_500: Exit( 'ICMS_500' );
    icms_900: Exit( 'ICMS_900' );

    //CHICO -- CST NOVOS
//    icms_45: Exit( 'ICMS_45' ); //CHICO RED#2013
    //CHICO -- o 80 e 81 s�o somente para CT-e
//    icms_80: Exit( 'ICMS_80' ); //CHICO RED#2013
//    icms_81: Exit( 'ICMS_81' ); //CHICO RED#2013
//    icms_icmsoutrasuf : Exit( 'ICMS_ICMSOUTRASUF' ); //CHICO RED#2013
//    icms_icmssn       : Exit( 'ICMS_ICMSSN' ); //CHICO RED#2013
    {
      CHICO -- Comentei esses novos aaqui porque n sabemos a que se referem
      Precisamos ver pra que servem

    icms_part10: Exit( 'ICMS_PART10' );
    icms_part90: Exit( 'ICMS_PART90' );
    icms_rep41: Exit( 'ICMS_REP41' );
    icms_rep60: Exit( 'ICMS_REP60' );
    }
  end;
end;

class function TIcmsCstHelper.ValueOf(AValue: string): TIcmsCst;
var
  cst : TIcmsCst;
begin
  for cst := Low( TicmsCst ) to High( TicmsCst ) do
    if cst.Value = AValue then
      Exit( cst );
end;

{ TIpiCstHelper }

function TIpiCstHelper.Descricao: string;
begin
  case Self of
    ipi_00: Exit( '00 - Entrada com Recupera��o de Cr�dito' );
    ipi_01: Exit( '01 - Entrada tributada com al�quota zero' );
    ipi_02: Exit( '02 - Entrada Isenta' );
    ipi_03: Exit( '03 - Entrada n�o-tributada' );
    ipi_04: Exit( '04 - Entrada Imune' );
    ipi_05: Exit( '05 - Entrada com suspens�o' );
    ipi_49: Exit( '49 - Outras Entradas' );
    ipi_50: Exit( '50 - Sa�da Tributada' );
    ipi_51: Exit( '51 - Sa�da Tribut�vel com Al�quota Zero' );
    ipi_52: Exit( '52 - Sa�da Isenta' );
    ipi_53: Exit( '53 - Sa�da N�o-Tributada' );
    ipi_54: Exit( '54 - Sa�da Imune' );
    ipi_55: Exit( '55 - Sa�da com Suspens�o' );
    ipi_99: Exit( '99 - Outras Sa�das' );
  end;
end;

class procedure TIpiCstHelper.FillCbb(ACbb: TcxImageComboBox);
var
  t : TIpiCst;
begin
  try
    Cbb_Begin( ACbb );
    for t := Low( TIpiCst ) to High( TIpiCst ) do
      Cbb_Add( ACbb, t.Descricao, t.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

function TIpiCstHelper.Value: string;
begin
  case Self of
    ipi_00: Exit( 'IPI_00' );
    ipi_01: Exit( 'IPI_01' );
    ipi_02: Exit( 'IPI_02' );
    ipi_03: Exit( 'IPI_03' );
    ipi_04: Exit( 'IPI_04' );
    ipi_05: Exit( 'IPI_05' );
    ipi_49: Exit( 'IPI_49' );
    ipi_50: Exit( 'IPI_50' );
    ipi_51: Exit( 'IPI_51' );
    ipi_52: Exit( 'IPI_52' );
    ipi_53: Exit( 'IPI_53' );
    ipi_54: Exit( 'IPI_54' );
    ipi_55: Exit( 'IPI_55' );
    ipi_99: Exit( 'IPI_99' );
  end;
end;

{ TIpiEnquadramentoHelper }

function TIpiEnquadramentoHelper.Descricao: string;
begin
  case Self of
    ipienq_001: Exit( '01 - Livros, jornais, peri�dicos e o papel destinado � sua impress�o - Art. 18 Inciso I do Decreto 7.212/2010' );
    ipienq_002: Exit( '02 - Produtos industrializados destinados ao exterior - Art. 18 Inciso II do Decreto 7.212/2010' );
    ipienq_003: Exit( '03 - Ouro, definido em lei como ativo financeiro ou instrumento cambial - Art. 18 Inciso III do Decreto 7.212/2010' );
    ipienq_004: Exit( '04 - Energia el�trica, derivados de petr�leo, combust�veis e minerais do Pa�s - Art. 18 Inciso IV do Decreto 7.212/2010' );
    ipienq_005: Exit( '05 - Exporta��o de produtos nacionais - sem sa�da do territ�rio brasileiro - venda para empresa sediada no exterior - atividades de pesquisa ou lavra de jazidas de petr�leo e de g�s natural - Art. 19 Inciso I do Decreto 7.212/2010' );
    ipienq_006: Exit( '06 - Exporta��o de produtos nacionais - sem sa�da do territ�rio brasileiro - venda para empresa sediada no exterior - incorporados a produto final exportado para o Brasil - Art. 19 Inciso II do Decreto 7.212/2010' );
    ipienq_007: Exit( '07 - Exporta��o de produtos nacionais - sem sa�da do territ�rio brasileiro - venda para �rg�o ou entidade de governo estrangeiro ou organismo internacional de que o Brasil seja membro, para ser entregue, no Pa�s, � ordem do comprador - Art. 19 Inciso III ' +
     'do Decreto 7.212/2010' );
    ipienq_101: Exit( '101 - �leo de menta em bruto, produzido por lavradores - Art. 43 Inciso I do Decreto 7.212/2010' );
    ipienq_102: Exit( '102 - Produtos remetidos � exposi��o em feiras de amostras e promo��es semelhantes - Art. 43 Inciso II do Decreto 7.212/2010' );
    ipienq_103: Exit( '103 - Produtos remetidos a dep�sitos fechados ou armaz�ns-gerais, bem assim aqueles devolvidos ao remetente - Art. 43 Inciso III do Decreto 7.212/2010' );
    ipienq_104: Exit( '104 - Produtos industrializados, que com mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) importados submetidos a regime aduaneiro especial (drawback - suspens�o/isen��o), remetidos diretamente a empresas industriais export' +
     'adoras - Art. 43 Inciso IV do Decreto 7.212/2010 ' );
    ipienq_105: Exit( '105 - Produtos, destinados � exporta��o, que saiam do estabelecimento industrial para empresas comerciais exportadoras, com o fim espec�fico de exporta��o - Art. 43, Inciso V, al�nea a do Decreto 7.212/2010' );
    ipienq_106: Exit( '106 - Produtos, destinados � exporta��o, que saiam do estabelecimento industrial para recintos alfandegados onde se processe o despacho aduaneiro de exporta��o - Art. 43, Inciso V, al�neas b do Decreto 7.212/2010' );
    ipienq_107: Exit( '107 - Produtos, destinados � exporta��o, que saiam do estabelecimento industrial para outros locais onde se processe o despacho aduaneiro de exporta��o - Art. 43, Inciso V, al�neas c do Decreto 7.212/2010' );
    ipienq_108: Exit( '108 - Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) destinados ao executor de industrializa��o por encomenda - Art. 43 Inciso VI do Decreto 7.212/2010' );
    ipienq_109: Exit( '109 - Produtos industrializados por encomenda remetidos ao estabelecimento de origem - Art. 43 Inciso VII do Decreto 7.212/2010' );
    ipienq_110: Exit( '110 - Mat�rias-primas ou produtos intermedi�rios remetidos para emprego em opera��o industrial realizada pelo remetente fora do estabelecimento - Art. 43 Inciso VIII do Decreto 7.212/2010' );
    ipienq_111: Exit( '111 - Ve�culo, aeronave ou embarca��o destinados a emprego em provas de engenharia pelo fabricante - Art. 43 Inciso IX do Decreto 7.212/2010' );
    ipienq_112: Exit( '112 - Produtos remetidos, para industrializa��o ou com�rcio, de um para outro estabelecimento da mesma firma - Art. 43 Inciso X do Decreto 7.212/2010' );
    ipienq_113: Exit( '113 - Bens do ativo permanente remetidos a outro estabelecimento da mesma firma, para serem utilizados no processo industrial do recebedor - Art. 43 Inciso XI do Decreto 7.212/2010' );
    ipienq_114: Exit( '114 - Bens do ativo permanente remetidos a outro estabelecimento, para serem utilizados no processo industrial de produtos encomendados pelo remetente - Art. 43 Inciso XII do Decreto 7.212/2010' );
    ipienq_115: Exit( '115 - Partes e pe�as destinadas ao reparo de produtos com defeito de fabrica��o, quando a opera��o for executada gratuitamente, em virtude de garantia - Art. 43 Inciso XIII do Decreto 7.212/2010' );
    ipienq_116: Exit( '116 - Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) de fabrica��o nacional, vendidos a estabelecimento industrial, para industrializa��o de produtos destinados � exporta��o ou a estabelecimento comercial, para industrial' +
     'iza��o em outro estabelecimento da mesma firma ou de terceiro, de produto destinado � exporta��o - Art. 43 Inciso XIV do Decreto 7.212/2010' );
    ipienq_117: Exit( '117 - Produtos para emprego ou consumo na industrializa��o ou elabora��o de produto a ser exportado, adquiridos no mercado interno ou importados - Art. 43 Inciso XV do Decreto 7.212/2010' );
    ipienq_118: Exit( '118 - Bebidas alc�olicas e demais produtos de produ��o nacional acondicionados em recipientes de capacidade superior ao limite m�ximo permitido para venda a varejo - Art. 44 do Decreto 7.212/2010' );
    ipienq_119: Exit( '119 - Produtos classificados NCM 21.06.90.10 Ex 02, 22.01, 22.02, exceto os Ex 01 e Ex 02 do C�digo 22.02.90.00 e 22.03 sa�dos de estabelecimento industrial destinado a comercial equiparado a industrial - Art. 45 Inciso I do Decreto7.212/2010' );
    ipienq_120: Exit( '120 - Produtos classificados NCM 21.06.90.10 Ex 02, 22.01, 22.02, exceto os Ex 01 e Ex 02 do C�digo 22.02.90.00 e 22.03 sa�dos de estabelecimento comercial equiparado a industrial destinado a equiparado a industrial - Art. 45 Inciso II do Decreto7.212/20' +
     '10' );
    ipienq_121: Exit( '121 - Produtos classificados NCM 21.06.90.10 Ex 02, 22.01, 22.02, exceto os Ex 01 e Ex 02 do C�digo 22.02.90.00 e 22.03 sa�dos de importador destinado a equiparado a industrial - Art. 45 Inciso III do Decreto7.212/2010' );
    ipienq_122: Exit( '122 - Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) destinados a estabelecimento que se dedique � elabora��o de produtos classificados nos c�digos previstos no art. 25 da Lei 10.684/2003 - Art. 46 Inciso I do Decreto 7.2' +
     '12/2010' );
    ipienq_123: Exit( '123 - Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) adquiridos por estabelecimentos industriais fabricantes de partes e pe�as destinadas a estabelecimento industrial fabricante de produto classificado no Cap�tulo 88 da T' +
     'ipi - Art. 46 Inciso II do Decreto 7.212/2010' );
    ipienq_124: Exit( '124 - Mat�rias-primas (MP), produtos intermedi�rios (PI) e material de embalagem (ME) adquiridos por pessoas jur�dicas preponderantemente exportadoras - Art. 46 Inciso III do Decreto 7.212/2010' );
    ipienq_125: Exit( '125 - Materiais e equipamentos destinados a embarca��es pr�-registradas ou registradas no Registro Especial Brasileira - REB quando adquiridos por estaleiros navais brasileiros - Art. 46 Inciso IV do Decreto 7.212/2010' );
    ipienq_126: Exit( '126 - Aquisi��o por benefici�rio de regime aduaneiro suspensivo do imposto, destinado a industrializa��o para exporta��o - Art. 47 do Decreto 7.212/2010' );
    ipienq_127: Exit( '127 - Desembara�o de produtos de proced�ncia estrangeira importados por lojas francas - Art. 48 Inciso I do Decreto 7.212/2010' );
    ipienq_128: Exit( '128 - Desembara�o de maquinas, equipamentos, ve�culos, aparelhos e instrumentos sem similar nacional importados por empresas nacionais de engenharia, destinados � execu��o de obras no exterior - Art. 48 Inciso II do Decreto 7.212/2010' );
    ipienq_129: Exit( '129 - Desembara�o de produtos de proced�ncia estrangeira com sa�da de reparti��es aduaneiras com suspens�o do Imposto de Importa��o - Art. 48 Inciso III do Decreto 7.212/2010' );
    ipienq_130: Exit( '130 - Desembara�o de mat�rias-primas, produtos intermedi�rios e materiais de embalagem, importados diretamente por estabelecimento de que tratam os incisos I a III do caput do Decreto 7.212/2010 - Art. 48 Inciso IV do Decreto 7.212/2010' );
    ipienq_131: Exit( '131 - Remessa de produtos para a ZFM destinados ao seu consumo interno, utiliza��o ou industrializa��o - Art. 84 do Decreto 7.212/2010' );
    ipienq_132: Exit( '132 - Remessa de produtos para a ZFM destinados � exporta��o - Art. 85 Inciso I do Decreto 7.212/2010' );
    ipienq_133: Exit( '133 - Produtos que, antes de sua remessa � ZFM, forem enviados pelo seu fabricante a outro estabelecimento, para industrializa��o adicional, por conta e ordem do destinat�rio - Art. 85 Inciso II do Decreto 7.212/2010' );
    ipienq_134: Exit( '134 - Desembara�o de produtos de proced�ncia estrangeira importados pela ZFM quando ali consumidos ou utilizados, exceto armas, muni��es, fumo, bebidas alco�licas e autom�veis de passageiros. - Art. 86 do Decreto 7.212/2010' );
    ipienq_135: Exit( '135 - Remessa de produtos para a Amaz�nia Ocidental destinados ao seu consumo interno ou utiliza��o - Art. 96 do Decreto 7.212/2010' );
    ipienq_136: Exit( '136 - Entrada de produtos estrangeiros na �rea de Livre Com�rcio de Tabatinga - ALCT destinados ao seu consumo interno ou utiliza��o - Art. 106 do Decreto 7.212/2010' );
    ipienq_137: Exit( '137 - Entrada de produtos estrangeiros na �rea de Livre Com�rcio de Guajar�-Mirim - ALCGM destinados ao seu consumo interno ou utiliza��o - Art. 109 do Decreto 7.212/2010' );
    ipienq_138: Exit( '138 - Entrada de produtos estrangeiros nas �reas de Livre Com�rcio de Boa Vista - ALCBV e Bomfim - ALCB destinados a seu consumo interno ou utiliza��o - Art. 112 do Decreto 7.212/2010' );
    ipienq_139: Exit( '139 - Entrada de produtos estrangeiros na �rea de Livre Com�rcio de Macap� e Santana - ALCMS destinados a seu consumo interno ou utiliza��o - Art. 116 do Decreto 7.212/2010' );
    ipienq_140: Exit( '140 - Entrada de produtos estrangeiros nas �reas de Livre Com�rcio de Brasil�ia - ALCB e de Cruzeiro do Sul - ALCCS destinados a seu consumo interno ou utiliza��o - Art. 119 do Decreto 7.212/2010' );
    ipienq_141: Exit( '141 - Remessa para Zona de Processamento de Exporta��o - ZPE - Art. 121 do Decreto 7.212/2010' );
    ipienq_142: Exit( '142 - Setor Automotivo - Desembara�o aduaneiro, chassis e outros - regime aduaneiro especial industrializa��o 87.01 a 87.05 - Art. 136, I do Decreto 7.212/2010' );
    ipienq_143: Exit( '143 - Setor Automotivo - Do estabelecimento industrial produtos 87.01 a 87.05 da TIPI - mercado interno - empresa comercial atacadista controlada por PJ encomendante do exterior. - Art. 136, II do Decreto 7.212/2010' );
    ipienq_144: Exit( '144 - Setor Automotivo - Do estabelecimento industrial - chassis e outros classificados nas posi��es 84.29, 84.32, 84.33, 87.01 a 87.06 e 87.11 da TIPI. - Art. 136, III do Decreto 7.212/2010' );
    ipienq_145: Exit( '145 - Setor Automotivo - Desembara�o aduaneiro, chassis e outros classificados nas posi��es 84.29, 84.32, 84.33, 87.01 a 87.06 e 87.11 da TIPI quando importados diretamente por estabelecimento industrial - Art. 136, IV do Decreto 7.212/2010' );
    ipienq_146: Exit( '146 - Setor Automotivo - do estabelecimento industrial mat�rias-primas, os produtos intermedi�rios e os materiais de embalagem, adquiridos por fabricantes, preponderantemente, de componentes, chassis e outros classificados nos C�digos 84.29, 8432.40.00, ' +
     '8432.80.00, 8433.20, 8433.30.00, 8433.40.00, 8433.5 e 87.01 a 87.06 da TIPI - Art. 136, V do Decreto 7.212/2010' );
    ipienq_147: Exit( '147 - Setor Automotivo - Desembara�o aduaneiro, as mat�rias-primas, os produtos intermedi�rios e os materiais de embalagem, importados diretamente por fabricantes, preponderantemente, de componentes, chassis e outros classificados nos C�digos 84.29, 8432' +
     '.40.00, 8432.80.00, 8433.20, 8433.30.00, 8433.40.00, 8433.5 e 87.01 a 87.06 da TIPI - Art. 136, VI do Decreto 7.212/2010' );
    ipienq_148: Exit( '148 - Bens de Inform�tica e Automa��o - mat�rias-primas, os produtos intermedi�rios e os materiais de embalagem, quando adquiridos por estabelecimentos industriais fabricantes dos referidos bens. - Art. 148 do Decreto 7.212/2010' );
    ipienq_149: Exit( '149 - Reporto - Sa�da de Estabelecimento de m�quinas e outros quando adquiridos por benefici�rios do REPORTO - Art. 166, I do Decreto 7.212/2010' );
    ipienq_150: Exit( '150 - Reporto - Desembara�o aduaneiro de m�quinas e outros quando adquiridos por benefici�rios do REPORTO - Art. 166, II do Decreto 7.212/2010' );
    ipienq_151: Exit( '151 - Repes - Desembara�o aduaneiro - bens sem similar nacional importados por benefici�rios do REPES - Art. 171 do Decreto 7.212/2010' );
    ipienq_152: Exit( '152 - Recine - Sa�da para benefici�rio do regime - Art. 14, III da Lei 12.599/2012' );
    ipienq_153: Exit( '153 - Recine - Desembara�o aduaneiro por benefici�rio do regime - Art. 14, IV da Lei 12.599/2012' );
    ipienq_154: Exit( '154 - Reif - Sa�da para benefici�rio do regime - Lei 12.794/1013, art. 8, III' );
    ipienq_155: Exit( '155 - Reif - Desembara�o aduaneiro por benefici�rio do regime - Lei 12.794/1013, art. 8, IV' );
    ipienq_156: Exit( '156 - Repnbl-Redes - Sa�da para benefici�rio do regime - Lei n� 12.715/2012, art. 30, II' );
    ipienq_157: Exit( '157 - Recompe - Sa�da de mat�rias-primas e produtos intermedi�rios para benefici�rios do regime - Decreto n� 7.243/2010, art. 5�, I' );
    ipienq_158: Exit( '158 - Recompe - Sa�da de mat�rias-primas e produtos intermedi�rios destinados a industrializa��o de equipamentos - Programa Est�mulo Universidade-Empresa - Apoio � Inova��o - Decreto n� 7.243/2010, art. 5�, III' );
    ipienq_159: Exit( '159 - Rio 2016 - Produtos nacionais, dur�veis, uso e consumo dos eventos, adquiridos pelas pessoas jur�dicas mencionadas no � 2o do art. 4o da Lei n� 12.780/2013 - Lei n� 12.780/2013, Art. 13' );
    ipienq_160: Exit( '160 - Regime Especial de Admiss�o Tempor�ria nos Termos do Art. 2o da IN 1361/2013' );
    ipienq_161: Exit( '161 - Regime Especial de  Admiss�o Tempor�ria nos termos do art. 5o da IN 1361/2013' );
    ipienq_162: Exit( '162 - Regime Especial de Admiss�o Tempor�ria nos termos do art. 7o da IN 1361/2013 (Suspens�o com pagamento de tributos diferidos at� a dura��o do regime, limitado a 100% do valor original)' );
    ipienq_301: Exit( '301 - Produtos industrializados por institui��es de educa��o ou de assist�ncia social, destinados a uso pr�prio ou a distribui��o gratuita a seus educandos ou assistidos - Art. 54 Inciso I do Decreto 7.212/2010' );
    ipienq_302: Exit( '302 - Produtos industrializados por estabelecimentos p�blicos e aut�rquicos da Uni�o, dos Estados, do Distrito Federal e dos Munic�pios, n�o destinados a com�rcio - Art. 54 Inciso II do Decreto 7.212/2010' );
    ipienq_303: Exit( '303 - Amostras de produtos para distribui��o gratuita, de diminuto ou nenhum valor comercial - Art. 54 Inciso III do Decreto 7.212/2010' );
    ipienq_304: Exit( '304 - Amostras de tecidos sem valor comercial - Art. 54 Inciso IV do Decreto 7.212/2010' );
    ipienq_305: Exit( '305 - P�s isolados de cal�ados - Art. 54 Inciso V do Decreto 7.212/2010' );
    ipienq_306: Exit( '306 - Aeronaves de uso militar e suas partes e pe�as, vendidas � Uni�o - Art. 54 Inciso VI do Decreto 7.212/2010' );
    ipienq_307: Exit( '307 - Caix�es funer�rios - Art. 54 Inciso VII do Decreto 7.212/2010' );
    ipienq_308: Exit( '308 - Papel destinado � impress�o de m�sicas - Art. 54 Inciso VIII do Decreto 7.212/2010' );
    ipienq_309: Exit( '309 - Panelas e outros artefatos semelhantes, de uso dom�stico, de fabrica��o r�stica, de pedra ou barro bruto - Art. 54 Inciso IX do Decreto 7.212/2010' );
    ipienq_310: Exit( '310 - Chap�us, roupas e prote��o, de couro, pr�prios para tropeiros - Art. 54 Inciso X do Decreto 7.212/2010' );
    ipienq_311: Exit( '311 - Material b�lico, de uso privativo das For�as Armadas, vendido � Uni�o - Art. 54 Inciso XI do Decreto 7.212/2010' );
    ipienq_312: Exit( '312 - Autom�vel adquirido diretamente a fabricante nacional, pelas miss�es diplom�ticas e reparti��es consulares de car�ter permanente, ou seus integrantes, bem assim pelas representa��es internacionais ou regionais de que o Brasil seja membro, e seus fu' +
     'ncion�rios, peritos, t�cnicos e consultores, de nacionalidade estrangeira, que exer�am fun��es de car�ter permanente - Art. 54 Inciso XII do Decreto 7.212/2010' );
    ipienq_313: Exit( '313 - Ve�culo de fabrica��o nacional adquirido por funcion�rio das miss�es diplom�ticas acreditadas junto ao Governo Brasileiro - Art. 54 Inciso XIII do Decreto 7.212/2010' );
    ipienq_314: Exit( '314 - Produtos nacionais sa�dos diretamente para Lojas Francas - Art. 54 Inciso XIV do Decreto 7.212/2010' );
    ipienq_315: Exit( '315 - Materiais e equipamentos destinados a Itaipu Binacional - Art. 54 Inciso XV do Decreto 7.212/2010' );
    ipienq_316: Exit( '316 - Produtos Importados por miss�es diplom�ticas, consulados ou organismo internacional - Art. 54 Inciso XVI do Decreto 7.212/2010' );
    ipienq_317: Exit( '317 - Bagagem de passageiros desembara�ada com isen��o do II. - Art. 54 Inciso XVII do Decreto 7.212/2010' );
    ipienq_318: Exit( '318 - Bagagem de passageiros desembara�ada com pagamento do II. - Art. 54 Inciso XVIII do Decreto 7.212/2010' );
    ipienq_319: Exit( '319 - Remessas postais internacionais sujeitas a tributa��o simplificada. - Art. 54 Inciso XIX do Decreto 7.212/2010' );
    ipienq_320: Exit( '320 - M�quinas e outros destinados � pesquisa cient�fica e tecnol�gica - Art. 54 Inciso XX do Decreto 7.212/2010' );
    ipienq_321: Exit( '321 - Produtos de proced�ncia estrangeira, isentos do II conforme Lei n� 8032/1990. - Art. 54 Inciso XXI do Decreto 7.212/2010' );
    ipienq_322: Exit( '322 - Produtos de proced�ncia estrangeira utilizados em eventos esportivos - Art. 54 Inciso XXII do Decreto 7.212/2010' );
    ipienq_323: Exit( '323 - Ve�culos automotores, m�quinas, equipamentos, bem assim suas partes e pe�as separadas, destinadas � utiliza��o nas atividades dos Corpos de Bombeiros - Art. 54 Inciso XXIII do Decreto 7.212/2010' );
    ipienq_324: Exit( '324 - Produtos importados para consumo em congressos, feiras e exposi��es - Art. 54 Inciso XXIV do Decreto 7.212/2010' );
    ipienq_325: Exit( '325 - Bens de inform�tica, Mat�ria Prima, produtos intermedi�rios e embalagem destinados a Urnas eletr�nicas - TSE - Art. 54 Inciso XXV do Decreto 7.212/2010' );
    ipienq_326: Exit( '326 - Materiais, equipamentos, m�quinas, aparelhos e instrumentos, bem assim os respectivos acess�rios, sobressalentes e ferramentas, que os acompanhem, destinados � constru��o do Gasoduto Brasil - Bol�via - Art. 54 Inciso XXVI do Decreto 7.212/2010' );
    ipienq_327: Exit( '327 - Partes, pe�as e componentes, adquiridos por estaleiros navais brasileiros, destinados ao emprego na conserva��o, moderniza��o, convers�o ou reparo de embarca��es registradas no Registro Especial Brasileiro - REB - Art. 54 Inciso XXVII do Decreto 7.' +
     '212/2010' );
    ipienq_328: Exit( '328 - Aparelhos transmissores e receptores de radiotelefonia e radiotelegrafia; ve�culos para patrulhamento policial; armas e muni��es, destinados a �rg�os de seguran�a p�blica da Uni�o, dos Estados e do Distrito Federal - Art. 54 Inciso XXVIII do Decret' +
     'o 7.212/2010' );
    ipienq_329: Exit( '329 - Autom�veis de passageiros de fabrica��o nacional destinados � utiliza��o como t�xi adquiridos por motoristas profissionais - Art. 55 Inciso I do Decreto 7.212/2010' );
    ipienq_330: Exit( '330 - Autom�veis de passageiros de fabrica��o nacional destinados � utiliza��o como t�xi por impedidos de exercer atividade por destrui��o, furto ou roubo do ve�culo adquiridos por motoristas profissionais. - Art. 55 Inciso II do Decreto 7.212/2010' );
    ipienq_331: Exit( '331 - Autom�veis de passageiros de fabrica��o nacional destinados � utiliza��o como t�xi adquiridos por cooperativas de trabalho. - Art. 55 Inciso II do Decreto 7.212/2010' );
    ipienq_332: Exit( '332 - Autom�veis de passageiros de fabrica��o nacional, destinados a pessoas portadoras de defici�ncia f�sica, visual, mental severa ou profunda, ou autistas - Art. 55 Inciso IV do Decreto 7.212/2010' );
    ipienq_333: Exit( '333 - Produtos estrangeiros, recebidos em doa��o de representa��es diplom�ticas estrangeiras sediadas no Pa�s, vendidos em feiras, bazares e eventos semelhantes por entidades beneficentes - Art. 67 do Decreto 7.212/2010' );
    ipienq_334: Exit( '334 - Produtos industrializados na Zona Franca de Manaus - ZFM, destinados ao seu consumo interno - Art. 81 Inciso I do Decreto 7.212/2010' );
    ipienq_335: Exit( '335 - Produtos industrializados na ZFM, por estabelecimentos com projetos aprovados pela SUFRAMA, destinados a comercializa��o em qualquer outro ponto do Territ�rio Nacional Art. 81 Inciso II do Decreto 7.212/2010' );
    ipienq_336: Exit( '336 - Produtos nacionais destinados � entrada na ZFM, para seu consumo interno, utiliza��o ou industrializa��o, ou ainda, para serem remetidos, por interm�dio de seus entrepostos, � Amaz�nia Ocidental - Art. 81 Inciso III do Decreto 7.212/2010' );
    ipienq_337: Exit( '337 - Produtos industrializados por estabelecimentos com projetos aprovados pela SUFRAMA, consumidos ou utilizados na Amaz�nia Ocidental, ou adquiridos atrav�s da ZFM ou de seus entrepostos na referida regi�o - Art. 95 Inciso I do Decreto 7.212/2010' );
    ipienq_338: Exit( '338 - Produtos de proced�ncia estrangeira, relacionados na legisla��o, oriundos da ZFM e que derem entrada na Amaz�nia Ocidental para ali serem consumidos ou utilizados: - Art. 95 Inciso II do Decreto 7.212/2010' );
    ipienq_339: Exit( '339 - Produtos elaborados com mat�rias-primas agr�colas e extrativas vegetais de produ��o regional, por estabelecimentos industriais localizados na Amaz�nia Ocidental, com projetos aprovados pela SUFRAMA - Art. 95 Inciso III do Decreto 7.212/2010' );
    ipienq_340: Exit( '340 - Produtos industrializados em �rea de Livre Com�rcio - Art. 105 do Decreto 7.212/2010' );
    ipienq_341: Exit( '341 - Produtos nacionais ou nacionalizados, destinados � entrada na �rea de Livre Com�rcio de Tabatinga - ALCT - Art. 107 do Decreto 7.212/2010' );
    ipienq_342: Exit( '342 - Produtos nacionais ou nacionalizados, destinados � entrada na �rea de Livre Com�rcio de Guajar�-Mirim - ALCGM - Art. 110 do Decreto 7.212/2010' );
    ipienq_343: Exit( '343 - Produtos nacionais ou nacionalizados, destinados � entrada nas �reas de Livre Com�rcio de Boa Vista - ALCBV e Bonfim - ALCB - Art. 113 do Decreto 7.212/2010' );
    ipienq_344: Exit( '344 - Produtos nacionais ou nacionalizados, destinados � entrada na �rea de Livre Com�rcio de Macap� e Santana - ALCMS - Art. 117 do Decreto 7.212/2010' );
    ipienq_345: Exit( '345 - Produtos nacionais ou nacionalizados, destinados � entrada nas �reas de Livre Com�rcio de Brasil�ia - ALCB e de Cruzeiro do Sul - ALCCS - Art. 120 do Decreto 7.212/2010' );
    ipienq_346: Exit( '346 - Recompe - equipamentos de inform�tica - de benefici�rio do regime para escolas das redes p�blicas de ensino federal, estadual, distrital, municipal ou nas escolas sem fins lucrativos de atendimento a pessoas com defici�ncia - Decreto n� 7.243/2010,' +
     ' art. 7�' );
    ipienq_347: Exit( '347 - Rio 2016 - Importa��o de materiais para os jogos (medalhas, trof�us, impressos, bens n�o dur�veis, etc.) - Lei n� 12.780/2013, Art. 4�, �1�, I' );
    ipienq_348: Exit( '348 - Rio 2016 - Suspens�o convertida em Isen��o - Lei n� 12.780/2013, Art. 6�, I' );
    ipienq_349: Exit( '349 - Rio 2016 - Empresas vinculadas ao CIO - Lei n� 12.780/2013, Art. 9�, I, d' );
    ipienq_350: Exit( '350 - Rio 2016 - Sa�da de produtos importados pelo RIO 2016 - Lei n� 12.780/2013, Art. 10, I, d' );
    ipienq_351: Exit( '351 - Rio 2016 - Produtos nacionais, n�o dur�veis, uso e consumo dos eventos, adquiridos pelas pessoas jur�dicas mencionadas no � 2o do art. 4o da Lei n� 12.780/2013, Art. 12' );
    ipienq_601: Exit( '601 - Equipamentos e outros destinados � pesquisa e ao desenvolvimento tecnol�gico - Art. 72 do Decreto 7.212/2010' );
    ipienq_602: Exit( '602 - Equipamentos e outros destinados � empresas habilitadas no PDTI e PDTA utilizados em pesquisa e ao desenvolvimento tecnol�gico - Art. 73 do Decreto 7.212/2010' );
    ipienq_603: Exit( '603 - Microcomputadores e outros de at� R$11.000,00, unidades de disco, circuitos, etc, destinados a bens de inform�tica ou automa��o. Centro-Oeste SUDAM SUDENE - Art. 142, I do Decreto 7.212/2010' );
    ipienq_604: Exit( '604 - Microcomputadores e outros de at� R$11.000,00, unidades de disco, circuitos, etc, destinados a bens de inform�tica ou automa��o. - Art. 142, I do Decreto 7.212/2010' );
    ipienq_605: Exit( '605 - Bens de inform�tica n�o inclu�dos no art. 142 do Decreto 7.212/2010 - Produzidos no CentroOeste, SUDAM, SUDENE - Art. 143, I do Decreto 7.212/2010' );
    ipienq_606: Exit( '606 - Bens de inform�tica n�o inclu�dos no art. 142 do Decreto 7.212/2010 - Art. 143, II do Decreto 7.212/2010' );
    ipienq_607: Exit( '607 - Padis - Art. 150 do Decreto 7.212/2010' );
    ipienq_608: Exit( '608 - Patvd - Art. 158 do Decreto 7.212/2010' );
    ipienq_999: Exit( '999 - Tributa��o normal IPI; Outros' );
  end;
end;

class procedure TIpiEnquadramentoHelper.FillCbb(ACbb: TcxImageComboBox);
var
  t : TIpiEnquadramento;
begin
  try
    Cbb_Begin( ACbb );
    for t := Low( TIpiEnquadramento ) to High( TIpiEnquadramento ) do
      Cbb_Add( ACbb, t.Descricao, t.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

function TIpiEnquadramentoHelper.Value: string;
begin
  case Self of
    ipienq_001: Exit( 'IPIENQ_001' );
    ipienq_002: Exit( 'IPIENQ_002' );
    ipienq_003: Exit( 'IPIENQ_003' );
    ipienq_004: Exit( 'IPIENQ_004' );
    ipienq_005: Exit( 'IPIENQ_005' );
    ipienq_006: Exit( 'IPIENQ_006' );
    ipienq_007: Exit( 'IPIENQ_007' );
    ipienq_101: Exit( 'IPIENQ_101' );
    ipienq_102: Exit( 'IPIENQ_102' );
    ipienq_103: Exit( 'IPIENQ_103' );
    ipienq_104: Exit( 'IPIENQ_104' );
    ipienq_105: Exit( 'IPIENQ_105' );
    ipienq_106: Exit( 'IPIENQ_106' );
    ipienq_107: Exit( 'IPIENQ_107' );
    ipienq_108: Exit( 'IPIENQ_108' );
    ipienq_109: Exit( 'IPIENQ_109' );
    ipienq_110: Exit( 'IPIENQ_110' );
    ipienq_111: Exit( 'IPIENQ_111' );
    ipienq_112: Exit( 'IPIENQ_112' );
    ipienq_113: Exit( 'IPIENQ_113' );
    ipienq_114: Exit( 'IPIENQ_114' );
    ipienq_115: Exit( 'IPIENQ_115' );
    ipienq_116: Exit( 'IPIENQ_116' );
    ipienq_117: Exit( 'IPIENQ_117' );
    ipienq_118: Exit( 'IPIENQ_118' );
    ipienq_119: Exit( 'IPIENQ_119' );
    ipienq_120: Exit( 'IPIENQ_120' );
    ipienq_121: Exit( 'IPIENQ_121' );
    ipienq_122: Exit( 'IPIENQ_122' );
    ipienq_123: Exit( 'IPIENQ_123' );
    ipienq_124: Exit( 'IPIENQ_124' );
    ipienq_125: Exit( 'IPIENQ_125' );
    ipienq_126: Exit( 'IPIENQ_126' );
    ipienq_127: Exit( 'IPIENQ_127' );
    ipienq_128: Exit( 'IPIENQ_128' );
    ipienq_129: Exit( 'IPIENQ_129' );
    ipienq_130: Exit( 'IPIENQ_130' );
    ipienq_131: Exit( 'IPIENQ_131' );
    ipienq_132: Exit( 'IPIENQ_132' );
    ipienq_133: Exit( 'IPIENQ_133' );
    ipienq_134: Exit( 'IPIENQ_134' );
    ipienq_135: Exit( 'IPIENQ_135' );
    ipienq_136: Exit( 'IPIENQ_136' );
    ipienq_137: Exit( 'IPIENQ_137' );
    ipienq_138: Exit( 'IPIENQ_138' );
    ipienq_139: Exit( 'IPIENQ_139' );
    ipienq_140: Exit( 'IPIENQ_140' );
    ipienq_141: Exit( 'IPIENQ_141' );
    ipienq_142: Exit( 'IPIENQ_142' );
    ipienq_143: Exit( 'IPIENQ_143' );
    ipienq_144: Exit( 'IPIENQ_144' );
    ipienq_145: Exit( 'IPIENQ_145' );
    ipienq_146: Exit( 'IPIENQ_146' );
    ipienq_147: Exit( 'IPIENQ_147' );
    ipienq_148: Exit( 'IPIENQ_148' );
    ipienq_149: Exit( 'IPIENQ_149' );
    ipienq_150: Exit( 'IPIENQ_150' );
    ipienq_151: Exit( 'IPIENQ_151' );
    ipienq_152: Exit( 'IPIENQ_152' );
    ipienq_153: Exit( 'IPIENQ_153' );
    ipienq_154: Exit( 'IPIENQ_154' );
    ipienq_155: Exit( 'IPIENQ_155' );
    ipienq_156: Exit( 'IPIENQ_156' );
    ipienq_157: Exit( 'IPIENQ_157' );
    ipienq_158: Exit( 'IPIENQ_158' );
    ipienq_159: Exit( 'IPIENQ_159' );
    ipienq_160: Exit( 'IPIENQ_160' );
    ipienq_161: Exit( 'IPIENQ_161' );
    ipienq_162: Exit( 'IPIENQ_162' );
    ipienq_301: Exit( 'IPIENQ_301' );
    ipienq_302: Exit( 'IPIENQ_302' );
    ipienq_303: Exit( 'IPIENQ_303' );
    ipienq_304: Exit( 'IPIENQ_304' );
    ipienq_305: Exit( 'IPIENQ_305' );
    ipienq_306: Exit( 'IPIENQ_306' );
    ipienq_307: Exit( 'IPIENQ_307' );
    ipienq_308: Exit( 'IPIENQ_308' );
    ipienq_309: Exit( 'IPIENQ_309' );
    ipienq_310: Exit( 'IPIENQ_310' );
    ipienq_311: Exit( 'IPIENQ_311' );
    ipienq_312: Exit( 'IPIENQ_312' );
    ipienq_313: Exit( 'IPIENQ_313' );
    ipienq_314: Exit( 'IPIENQ_314' );
    ipienq_315: Exit( 'IPIENQ_315' );
    ipienq_316: Exit( 'IPIENQ_316' );
    ipienq_317: Exit( 'IPIENQ_317' );
    ipienq_318: Exit( 'IPIENQ_318' );
    ipienq_319: Exit( 'IPIENQ_319' );
    ipienq_320: Exit( 'IPIENQ_320' );
    ipienq_321: Exit( 'IPIENQ_321' );
    ipienq_322: Exit( 'IPIENQ_322' );
    ipienq_323: Exit( 'IPIENQ_323' );
    ipienq_324: Exit( 'IPIENQ_324' );
    ipienq_325: Exit( 'IPIENQ_325' );
    ipienq_326: Exit( 'IPIENQ_326' );
    ipienq_327: Exit( 'IPIENQ_327' );
    ipienq_328: Exit( 'IPIENQ_328' );
    ipienq_329: Exit( 'IPIENQ_329' );
    ipienq_330: Exit( 'IPIENQ_330' );
    ipienq_331: Exit( 'IPIENQ_331' );
    ipienq_332: Exit( 'IPIENQ_332' );
    ipienq_333: Exit( 'IPIENQ_333' );
    ipienq_334: Exit( 'IPIENQ_334' );
    ipienq_335: Exit( 'IPIENQ_335' );
    ipienq_336: Exit( 'IPIENQ_336' );
    ipienq_337: Exit( 'IPIENQ_337' );
    ipienq_338: Exit( 'IPIENQ_338' );
    ipienq_339: Exit( 'IPIENQ_339' );
    ipienq_340: Exit( 'IPIENQ_340' );
    ipienq_341: Exit( 'IPIENQ_341' );
    ipienq_342: Exit( 'IPIENQ_342' );
    ipienq_343: Exit( 'IPIENQ_343' );
    ipienq_344: Exit( 'IPIENQ_344' );
    ipienq_345: Exit( 'IPIENQ_345' );
    ipienq_346: Exit( 'IPIENQ_346' );
    ipienq_347: Exit( 'IPIENQ_347' );
    ipienq_348: Exit( 'IPIENQ_348' );
    ipienq_349: Exit( 'IPIENQ_349' );
    ipienq_350: Exit( 'IPIENQ_350' );
    ipienq_351: Exit( 'IPIENQ_351' );
    ipienq_601: Exit( 'IPIENQ_601' );
    ipienq_602: Exit( 'IPIENQ_602' );
    ipienq_603: Exit( 'IPIENQ_603' );
    ipienq_604: Exit( 'IPIENQ_604' );
    ipienq_605: Exit( 'IPIENQ_605' );
    ipienq_606: Exit( 'IPIENQ_606' );
    ipienq_607: Exit( 'IPIENQ_607' );
    ipienq_608: Exit( 'IPIENQ_608' );
    ipienq_999: Exit( 'IPIENQ_999' );
  end;
end;

{ TPisCstHelper }

function TPisCstHelper.Descricao: string;
begin
  case Self of
    pis_01: Exit( '01 - Opera��o Tribut�vel com Al�quota B�sica' );
    pis_02: Exit( '02 - Opera��o Tribut�vel com Al�quota Diferenciada' );
    pis_03: Exit( '03 - Opera��o Tribut�vel com Al�quota por Unidade de Medida de Produto ' );
    pis_04: Exit( '04 - Opera��o Tribut�vel Monof�sica - Revenda a Al�quota Zero ' );
    pis_05: Exit( '05 - Opera��o Tribut�vel por Substitui��o Tribut�ria ' );
    pis_06: Exit( '06 - Opera��o Tribut�vel a Al�quota Zero ' );
    pis_07: Exit( '07 - Opera��o Isenta da Contribui��o ' );
    pis_08: Exit( '08 - Opera��o sem Incid�ncia da Contribui��o ' );
    pis_09: Exit( '09 - Opera��o com Suspens�o da Contribui��o' );
    pis_49: Exit( '49 - Outras Opera��es de Sa�da' );
    pis_50: Exit( '50 - Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno' );
    pis_51: Exit( '51 - Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita N�o Tributada no Mercado Interno ' );
    pis_52: Exit( '52 - Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita de Exporta��o' );
    pis_53: Exit( '53 - Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno ' );
    pis_54: Exit( '54 - Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o' );
    pis_55: Exit( '55 - Opera��o com Direito a Cr�dito - Vinculada a Receitas N�o-Tributadas no Mercado Interno e de Exporta��o ' );
    pis_56: Exit( '56 - Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno, e de Exporta��o ' );
    pis_60: Exit( '60 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita Tributada no Mercado Interno' );
    pis_61: Exit( '61 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita N�o-Tributada no Mercado Interno' );
    pis_62: Exit( '62 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita de Exporta��o' );
    pis_63: Exit( '63 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno' );
    pis_64: Exit( '64 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o' );
    pis_65: Exit( '65 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas N�o-Tributadas no Mercado Interno e de Exporta��o' );
    pis_66: Exit( '66 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno, e de Exporta��o' );
    pis_67: Exit( '67 - Cr�dito Presumido - Outras Opera��es' );
    pis_70: Exit( '70 - Opera��o de Aquisi��o sem Direito a Cr�dito' );
    pis_71: Exit( '71 - Opera��o de Aquisi��o com Isen��o' );
    pis_72: Exit( '72 - Opera��o de Aquisi��o com Suspens�o' );
    pis_73: Exit( '73 - Opera��o de Aquisi��o a Al�quota Zero' );
    pis_74: Exit( '74 - Opera��o de Aquisi��o sem Incid�ncia da Contribui��o' );
    pis_75: Exit( '75 - Opera��o de Aquisi��o por Substitui��o Tribut�ria' );
    pis_98: Exit( '98 - Outras Opera��es de Entrada' );
    pis_99: Exit( '99 - Outras Opera��es' );
  end;
end;

class procedure TPisCstHelper.FillCbb(ACbb: TcxImageComboBox);
var
  t : TPisCst;
begin
  try
    Cbb_Begin( ACbb );
    for t := Low( TPisCst ) to High( TPisCst ) do
      Cbb_Add( ACbb, t.Descricao, t.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

function TPisCstHelper.Value: string;
begin
  case Self of
    pis_01: Exit( 'PIS_01' );
    pis_02: Exit( 'PIS_02' );
    pis_03: Exit( 'PIS_03' );
    pis_04: Exit( 'PIS_04' );
    pis_05: Exit( 'PIS_05' );
    pis_06: Exit( 'PIS_06' );
    pis_07: Exit( 'PIS_07' );
    pis_08: Exit( 'PIS_08' );
    pis_09: Exit( 'PIS_09' );
    pis_49: Exit( 'PIS_49' );
    pis_50: Exit( 'PIS_50' );
    pis_51: Exit( 'PIS_51' );
    pis_52: Exit( 'PIS_52' );
    pis_53: Exit( 'PIS_53' );
    pis_54: Exit( 'PIS_54' );
    pis_55: Exit( 'PIS_55' );
    pis_56: Exit( 'PIS_56' );
    pis_60: Exit( 'PIS_60' );
    pis_61: Exit( 'PIS_61' );
    pis_62: Exit( 'PIS_62' );
    pis_63: Exit( 'PIS_63' );
    pis_64: Exit( 'PIS_64' );
    pis_65: Exit( 'PIS_65' );
    pis_66: Exit( 'PIS_66' );
    pis_67: Exit( 'PIS_67' );
    pis_70: Exit( 'PIS_70' );
    pis_71: Exit( 'PIS_71' );
    pis_72: Exit( 'PIS_72' );
    pis_73: Exit( 'PIS_73' );
    pis_74: Exit( 'PIS_74' );
    pis_75: Exit( 'PIS_75' );
    pis_98: Exit( 'PIS_98' );
    pis_99: Exit( 'PIS_99' );
  end;
end;

{ TCofinsCstHelper }

function TCofinsCstHelper.Descricao: string;
begin
  case Self of
    cofins_01: Exit( '01 - Opera��o Tribut�vel com Al�quota B�sica' );
    cofins_02: Exit( '02 - Opera��o Tribut�vel com Al�quota Diferenciada' );
    cofins_03: Exit( '03 - Opera��o Tribut�vel com Al�quota por Unidade de Medida de Produto' );
    cofins_04: Exit( '04 - Opera��o Tribut�vel Monof�sica - Revenda a Al�quota Zero' );
    cofins_05: Exit( '05 - Opera��o Tribut�vel por Substitui��o Tribut�ria' );
    cofins_06: Exit( '06 - Opera��o Tribut�vel a Al�quota Zero' );
    cofins_07: Exit( '07 - Opera��o Isenta da Contribui��o' );
    cofins_08: Exit( '08 - Opera��o sem Incid�ncia da Contribui��o' );
    cofins_09: Exit( '09 - Opera��o com Suspens�o da Contribui��o' );
    cofins_49: Exit( '49 - Outras Opera��es de Sa�da' );
    cofins_50: Exit( '50 - Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno' );
    cofins_51: Exit( '51 - Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita N�o-Tributada no Mercado Interno' );
    cofins_52: Exit( '52 - Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita de Exporta��o' );
    cofins_53: Exit( '53 - Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno' );
    cofins_54: Exit( '54 - Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o' );
    cofins_55: Exit( '55 - Opera��o com Direito a Cr�dito - Vinculada a Receitas N�o Tributadas no Mercado Interno e de Exporta��o' );
    cofins_56: Exit( '56 - Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno e de Exporta��o' );
    cofins_60: Exit( '60 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita Tributada no Mercado Interno' );
    cofins_61: Exit( '61 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita N�o-Tributada no Mercado Interno' );
    cofins_62: Exit( '62 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita de Exporta��o' );
    cofins_63: Exit( '63 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno' );
    cofins_64: Exit( '64 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o' );
    cofins_65: Exit( '65 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas N�o-Tributadas no Mercado Interno e de Exporta��o' );
    cofins_66: Exit( '66 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno e de Exporta��o' );
    cofins_67: Exit( '67 - Cr�dito Presumido - Outras Opera��es' );
    cofins_70: Exit( '70 - Opera��o de Aquisi��o sem Direito a Cr�dito' );
    cofins_71: Exit( '71 - Opera��o de Aquisi��o com Isen��o' );
    cofins_72: Exit( '72 - Opera��o de Aquisi��o com Suspens�o' );
    cofins_73: Exit( '73 - Opera��o de Aquisi��o a Al�quota Zero' );
    cofins_74: Exit( '74 - Opera��o de Aquisi��o sem Incid�ncia da Contribui��o' );
    cofins_75: Exit( '75 - Opera��o de Aquisi��o por Substitui��o Tribut�ria' );
    cofins_98: Exit( '98 - Outras Opera��es de Entrada' );
    cofins_99: Exit( '99 - Outras Opera��es' );
  end;
end;

class procedure TCofinsCstHelper.FillCbb(ACbb: TcxImageComboBox);
var
  t : TCofinsCst;
begin
  try
    Cbb_Begin( ACbb );
    for t := Low( TCofinsCst ) to High( TCofinsCst ) do
      Cbb_Add( Acbb, t.Descricao, t.Value );
  finally
    Cbb_Terminate( ACbb );
  end;
end;

function TCofinsCstHelper.Value: string;
begin
  case Self of
    cofins_01: Exit( 'COFINS_01' );
    cofins_02: Exit( 'COFINS_02' );
    cofins_03: Exit( 'COFINS_03' );
    cofins_04: Exit( 'COFINS_04' );
    cofins_05: Exit( 'COFINS_05' );
    cofins_06: Exit( 'COFINS_06' );
    cofins_07: Exit( 'COFINS_07' );
    cofins_08: Exit( 'COFINS_08' );
    cofins_09: Exit( 'COFINS_09' );
    cofins_49: Exit( 'COFINS_49' );
    cofins_50: Exit( 'COFINS_50' );
    cofins_51: Exit( 'COFINS_51' );
    cofins_52: Exit( 'COFINS_52' );
    cofins_53: Exit( 'COFINS_53' );
    cofins_54: Exit( 'COFINS_54' );
    cofins_55: Exit( 'COFINS_55' );
    cofins_56: Exit( 'COFINS_56' );
    cofins_60: Exit( 'COFINS_60' );
    cofins_61: Exit( 'COFINS_61' );
    cofins_62: Exit( 'COFINS_62' );
    cofins_63: Exit( 'COFINS_63' );
    cofins_64: Exit( 'COFINS_64' );
    cofins_65: Exit( 'COFINS_65' );
    cofins_66: Exit( 'COFINS_66' );
    cofins_67: Exit( 'COFINS_67' );
    cofins_70: Exit( 'COFINS_70' );
    cofins_71: Exit( 'COFINS_71' );
    cofins_72: Exit( 'COFINS_72' );
    cofins_73: Exit( 'COFINS_73' );
    cofins_74: Exit( 'COFINS_74' );
    cofins_75: Exit( 'COFINS_75' );
    cofins_98: Exit( 'COFINS_98' );
    cofins_99: Exit( 'COFINS_99' );
  end;
end;

{ TEnderecoEmissaoTipoHelper }

function TEnderecoEmissaoTipoHelper.Descricao: string;
begin
  case Self of
    enderPrincipal: Exit( 'Endere�o Principal' );
    enderCobranca : Exit( 'Endere�o de Cobran�a' );
    enderEntrega  : Exit( 'Endere�o de Entrega' );
  end;
end;

function TEnderecoEmissaoTipoHelper.Value: string;
begin
  case Self of
    enderPrincipal: Exit( 'ENDERECO_PRINCIPAL' );
    enderCobranca : Exit( 'ENDERECO_COBRANCA' );
    enderEntrega  : Exit( 'ENDERECO_ENREGA' );
  end;
end;

{ TClassificacaoDestinatarioHelper }

function TClassificacaoDestinatarioHelper.Descricao: string;
begin
  case Self of
    cdNenhum            : Exit( '(nenhum)' );
    cdContribuinte      : Exit( 'Contribuinte ICMS' );
    cdContribuinteIsento: Exit( 'Contribuinte isento de Inscri��o no cadastro de Contribuintes do ICMS' );
    cdNaoContribuinte   : Exit( 'N�o contribuinte, que pode ou n�o possuir Inscri��o Estadual no Cadastro de Contribuintes do ICMS' );
  end;
end;

function TClassificacaoDestinatarioHelper.Value: string;
begin
  case Self of
    cdNenhum            : Exit( 'NENHUM' );
    cdContribuinte      : Exit( 'CONTRIBUINTE' );
    cdContribuinteIsento: Exit( 'CONTRIBUINTE_ISENTO_CADASTRO' );
    cdNaoContribuinte   : Exit( 'NAO_CONTRIBUINTE' );
  end;
end;

{ TTipoMovimentacaoHelper }

class function TTipoMovimentacaoHelper.ByValue(ATipo: string): TTipoMovimentacao;
var
  tm: TTipoMovimentacao;
begin
  for tm := Low( TTipoMovimentacao ) to High( TTipoMovimentacao ) do
    if tm.Value = ATipo then
      Exit( tm );
end;

function TTipoMovimentacaoHelper.Description: string;
begin
  case Self of
    tmCompra      : Exit( 'Compra' );
    tmVenda       : Exit( 'Venda' );
    tmOrcamento   : Exit( 'Or�amento' );
    tmOrdemServico: Exit( 'Ordem de Servi�o' );
    tmOrdemCompra : Exit( 'Ordem de Compra' );
    tmCotacao     : Exit( 'Cota��o' );
  else
    raise ENotImplemented.Create( 'Tipo n�o implementado!' );
  end;
end;



function TTipoMovimentacaoHelper.Value: string;
begin
  case Self of
    tmCompra      : Exit( 'COMPRA' );
    tmVenda       : Exit( 'VENDA' );
    tmOrcamento   : Exit( 'ORCAMENTO' );
    tmOrdemServico: Exit( 'ORDEM_SERVICO' );
    tmOrdemCompra : Exit( 'PEDIDO_FORNECEDOR' );
    tmCotacao     : Exit( 'COTACAO' );
  else
    raise ENotImplemented.Create( 'Tipo n�o implementado!' );
  end;
end;


{ TCompraEVendaTipoHelper }

class function TCompraEVendaTipoHelper.ByValue(
  Value: string): TCompraEVendaTipo;
begin
  for Result := Low( TCompraEVendaTipo ) to High( TCompraEVendaTipo ) do
    if Result.Value = Value then
      Break;
//  case AnsiIndexStr( Value, [ 'COMPRA', 'VENDA', 'DEVOLUCAO_FORNECEDOR', 'RETORNO_MERCADORIAS', 'TRANSFERENCIA_ESTOQUE',
//    'REMESSA_FORNECEDOR', 'DEVOLUCAO_CLIENTE', 'COMPRA_ALMOXARIFADO' ] ) of
//    0: Result := cvtCompra;
//    1: Result := cvtVenda;
//    2: Result := cvtDevolucaoFornecedor;
//    3: Result := cvtRetornoMercadorias;
//    4: Result := cvtTransferenciaEstoque;
//    5: Result := cvtRemessaFornecedor;
//    6: Result := cvtDevolucaoCliente;
//    7: Result := cvtCompraAlmoxarifado;
//  else
//    raise ENotImplemented.Create('Tipo n�o implementado');
//  end;
end;

function TCompraEVendaTipoHelper.Description: string;
begin
  case Self of
    cvtCompra               : Result := 'Compra';
    cvtVenda                : Result := 'Venda';
    cvtDevolucaoFornecedor  : Result := 'Devolu��o ao Fornecedor';
    cvtRetornoMercadorias   : Result := 'Retorno de Mercadorias';
    cvtTransferenciaEstoque : Result := 'Transfer�ncia de Estoque';
    cvtRemessaFornecedor    : Result := 'Remessa ao Fornecedor';
    cvtDevolucaoCliente     : Result := 'Devolu��o de Cliente';
    cvtCompraAlmoxarifado   : Result := 'Compra para Almoxarifado';
    cvtComplementar         : Result := 'Complementar';
    cvtAjuste               : Result := 'Ajuste';
  else
    raise ENotImplemented.Create('Tipo n�o implementado');
  end;
end;

function TCompraEVendaTipoHelper.Value: string;
begin
  case Self of
    cvtCompra               : Result := 'COMPRA';
    cvtVenda                : Result := 'VENDA';
    cvtDevolucaoFornecedor  : Result := 'DEVOLUCAO_FORNECEDOR';
    cvtRetornoMercadorias   : Result := 'RETORNO_MERCADORIAS';
    cvtTransferenciaEstoque : Result := 'TRANSFERENCIA_ESTOQUE';
    cvtRemessaFornecedor    : Result := 'REMESSA_FORNECEDOR';
    cvtDevolucaoCliente     : Result := 'DEVOLUCAO_CLIENTE';
    cvtCompraAlmoxarifado   : Result := 'COMPRA_ALMOXARIFADO';
    cvtComplementar         : Result := 'COMPLEMENTAR';
    cvtAjuste               : Result := 'AJUSTE';
  else
    raise ENotImplemented.Create('Tipo n�o implementado');
  end;
end;

{ TCompraEVendaSituacaoHelper }

class function TCompraEVendaSituacaoHelper.ByValue(
  Value: string): TCompraEVendaSituacao;
begin
  for Result := Low( TCompraEVendaSituacao ) to High( TCompraEVendaSituacao ) do
    if Result.Value = Value then
      Break;
end;

function TCompraEVendaSituacaoHelper.Description: string;
begin
  case Self of
    cvsPendente       : Result := 'Pendente';
    cvsEstoqueBaixado : Result := 'Estoque baixado';
    cvsFaturado       : Result := 'Faturado';
  end;
end;

function TCompraEVendaSituacaoHelper.Value: String;
begin
  case Self of
    cvsPendente       : Result := 'PENDENTE';
    cvsEstoqueBaixado : Result := 'ESTOQUE_BAIXADO';
    cvsFaturado       : Result := 'FATURADO';
  end;
end;

{ TRelatorioAgendadoStatusHelper }

function TRelatorioAgendadoStatusHelper.Descricao: string;
begin
  case Self of
    agQueued    : Exit( 'Na Fila' );
    agRunning   : Exit( 'Em Execu��o' );
    agCompleted : Exit( 'Conclu�do' );
    agFailed    : Exit( 'Falha' );
  end;
end;

class function TRelatorioAgendadoStatusHelper.FromValue(
  Value: string): TRelatorioAgendadoStatus;
begin
  for Result := Low( TRelatorioAgendadoStatus )  to High( TRelatorioAgendadoStatus ) do
    if Result.Value = Value then
      Break;
end;

function TRelatorioAgendadoStatusHelper.Value: string;
begin
  case Self of
    agQueued    : Exit( 'QUEUED' );
    agRunning   : Exit( 'RUNNING' );
    agCompleted : Exit( 'COMPLETED' );
    agFailed    : Exit( 'FAILED' );
  end;
end;

{ TContentTypeHelper }

function TContentTypeHelper.Descricao: String;
begin
  Result := Names[ Self ];
end;

class function TContentTypeHelper.FromValue(
  AValue: string): TContentType;
var
  ct : TContentType;
begin
  for ct := Low( TContentType ) to High( TContentType ) do
    if ct.Value = AValue then
      Exit( ct );
  raise Exception.Create('Content Type not expected');
end;

class function TContentTypeHelper.FromDescricao(ADescricao: string): TContentType;
var
  ct : TContentType;
begin
  for ct := Low( TContentType ) to High( TContentType ) do
    if ct.Descricao = ADescricao then
      Exit( ct );
  raise Exception.Create('Content Type not expected');
end;

class function TContentTypeHelper.FromFileExt(AFileExt: string): TContentType;
begin
  case AnsiIndexStr( AnsiLowerCase( AFileExt ).Replace( '.', '', [ rfReplaceAll ] ),
                      [ 'png', 'jpg', 'jpeg', 'bmp', 'xml', 'pdf', 'rtf', 'gif', 'swf', 'html' ]  ) of
    0   : Exit( imgPng );
    1,2 : Exit( imgJpeg );
    3   : Exit( imgBmp );
    4   : Exit( appXml );
    5   : Exit( appPdf );
    6   : Exit( appRtf );
    7   : Exit( imgGif );
    8   : Exit( appSwf );
    9   : Exit( textHtml );
  else
    Exit( appOctetStream );
  end;
end;

function TContentTypeHelper.Value: string;
begin
  Result := Values[ Self ];
end;

{ TNFSePadraoHelper }

class function TNFSePadraoHelper.ByValue(Value: string): TNFSePadrao;
begin
  for Result := Low( TNFSePadrao ) to High( TNFSePadrao ) do
    if Result.Value = Value then
      Break;
end;

function TNFSePadraoHelper.Description: string;
begin
  case Self of
    nfsepOutro: Result := 'Outro';
    nfsepBoanf: Result := 'BOANF';
    nfsepIpm: Result := 'IPM';
    nfsepAssessorPublico: Result := 'Assessor P�blico';
    nfsepNFPSe: Result := 'NFPSe';
    nfsepBetha: Result := 'Betha';
    nfsepNfPaulistana: Result := 'NF-Paulistana';
    nfsepIssNet: Result := 'ISS.NET';
  else
    raise Exception.Create('NFSePadrao$1857 Tipo n�o implementado');
  end;
end;

function TNFSePadraoHelper.Value: String;
begin
  case Self of
    nfsepOutro: Result := 'OUTRO';
    nfsepBoanf: Result := 'BOANF';
    nfsepIpm: Result := 'IPM';
    nfsepAssessorPublico: Result := 'ASSESSOR_PUBLICO';
    nfsepNFPSe: Result := 'NFPSE';
    nfsepBetha: Result := 'BETHA';
    nfsepNfPaulistana: Result := 'NF_PAULISTANA';
    nfsepIssNet: Result := 'ISS_NET';
  else
    raise Exception.Create('NFSePadrao$1857 Tipo n�o implementado');
  end;
end;

{ TLctoAntecipadoOrigemHelper }

class function TLctoAntecipadoOrigemHelper.ByValue(
  Value: String): TLctoAntecipadoOrigem;
begin
  for Result := Low( TLctoAntecipadoOrigem ) to High( TLctoAntecipadoOrigem ) do
    if Result.Value = Value then
      Break;
end;

function TLctoAntecipadoOrigemHelper.Description: string;
begin
  case Self of
    laoManual       : Result := 'Manual';
    laoOrdemCompra  : Result := 'Ordem de Compra';
    laoOrcamento    : Result := 'Or�amento';
    laoOrdemServico : Result := 'Ordem de Servi�o';
    laoCompra       : Result := 'Compra';
    laoVenda        : Result := 'Venda';
    laoPDV          : Result := 'PDV';
    laoDevolucao    : Result := 'Devolu��o';
    laoRctoConta    : Result := 'Recebimento de Contas';
    laoPgtoConta    : Result := 'Pagamento de Contas';
  end;
end;

function TLctoAntecipadoOrigemHelper.Value: string;
begin
  case Self of
    laoManual       : Result := 'MANUAL';
    laoOrdemCompra  : Result := 'ORDEM_COMPRA';
    laoOrcamento    : Result := 'ORCAMENTO';
    laoOrdemServico : Result := 'ORDEM_SERVICO';
    laoCompra       : Result := 'COMPRA';
    laoVenda        : Result := 'VENDA';
    laoPDV          : Result := 'PDV';
    laoDevolucao    : Result := 'DEVOLUCAO';
    laoRctoConta    : Result := 'RECEBIMENTO';
    laoPgtoConta    : REsult := 'PAGAMENTO';
  end;
end;

{ TOrdemCompraStatusHelper }

class function TOrdemCompraStatusHelper.ByValue(
  Value: string): TOrdemCompraStatus;
begin
  for Result := Low( TOrdemCompraStatus ) to High( TOrdemCompraStatus ) do
    if Result.Value = Value then
      Break;
end;


function TOrdemCompraStatusHelper.Description: string;
begin
  case Self of
    ocsPendente   : Exit( 'Pendente' );
    ocsLiberado   : Exit( 'Liberado' );
    ocsRejeitado  : Exit( 'Rejeitado' );
    ocsCancelado  : Exit( 'Cancelado' );
    ocsFinalizado : Exit( 'Finalizado' );
  end;
end;

function TOrdemCompraStatusHelper.Value: string;
begin
  case Self of
    ocsPendente   : Exit( 'PENDENTE' );
    ocsLiberado   : Exit( 'LIBERADO' );
    ocsRejeitado  : Exit( 'REJEITADO' );
    ocsCancelado  : Exit( 'CANCELADO' );
    ocsFinalizado : Exit( 'FINALIZADO' );
  end;
end;

{TModFreteHelper}

function TModFreteHelper.Description: string;
begin
  case Self of
    mfContaEmitente       : Exit( 'Emitente' );
    mfContaDestinatario   : Exit( 'Destinat�rio' );
    mfContaTerceiros      : Exit( 'Por conta de terceiros' );
    mfSemFrete            : Exit( 'Sem frete' );
    mfProprioRemetente    : Exit( 'Transporte Pr�prio por conta do Remetente' );
    mfProprioDestinatario : Exit( 'Transporte Pr�prio por conta do Destinat�rio' );
  end;
end;

function TModFreteHelper.Value: string;
begin
  case Self of
    mfContaEmitente       : Exit( 'CONTA_EMITENTE' );
    mfContaDestinatario   : Exit( 'CONTA_DESTINATARIO' ) ;
    mfContaTerceiros      : Exit( 'CONTA_TERCEIROS' ) ;
    mfSemFrete            : Exit( 'SEM_FRETE' );
    mfProprioRemetente    : Exit( 'PROPRIO_REMETENTE' );
    mfProprioDestinatario : Exit( 'PROPRIO_DESTINATARIO' );
  end;
end;

class function TModFreteHelper.ByValue(
  Value: string): TModFrete;
begin
  for Result := Low( TModFrete ) to High( TModFrete ) do
    if Result.Value = Value then
      Break;
end;

{ TAutorizadoTipoHelper }

class function TAutorizadoTipoHelper.ByValue(AValue: string): TAutorizadoTipo;
begin
  for Result := Low( TAutorizadoTipo ) to High( TAutorizadoTipo ) do
    if Result.Value = AValue then
      Exit;
end;

function TAutorizadoTipoHelper.Descricao: string;
begin
  case Self of
    atTodasCompras      : Exit( 'Todas as Compras' );
    atSomenteEstaCompra : Exit( 'Somente esta Compra' );
  end;
end;

function TAutorizadoTipoHelper.Value: string;
begin
  case Self of
    atTodasCompras      : Exit( 'TODAS_COMPRAS' );
    atSomenteEstaCompra : Exit( 'SOMENTE_ESTA_COMPRA' );
  end;
end;

{ TModeloNotaHelper }

class function TModeloNotaHelper.ByValue(Value: String): TModeloNota;
begin
  for Result := Low( TmodeloNota ) to High( TModeloNota ) do
    if Result.Value = Value then
      Break;
end;

function TModeloNotaHelper.Description: string;
begin
  case Self of
    mn1  : Exit( '1' );
    mn1A : Exit( '1A' );
    mn2  : Exit( '2' );
    mn3  : Exit( '3' );
    mn3A : Exit( '3A' );
    mn6  : Exit( '6' );
    mn21 : Exit( '21' );
    mn22 : Exit( '22' );
    mn55 : Exit( '55' );
    mn65 : Exit( '65' );
  else
    raise ENotSupportedException.Create( 'ME$2101 Tipo n�o implementado' );
  end;
end;

function TModeloNotaHelper.Value: string;
begin
  case Self of
    mn1 : Exit( 'M_1' );
    mn1A: Exit( 'M_1A' );
    mn2 : Exit( 'M_2' );
    mn3 : Exit( 'M_3' );
    mn3A: Exit( 'M_3A' );
    mn6 : Exit( 'M_6' );
    mn21: Exit( 'M_21' );
    mn22: Exit( 'M_22' );
    mn55: Exit( 'M_55' );
    mn65: Exit( 'M_65' );
  else
    raise ENotSupportedException.Create( 'ME$2101 Tipo n�o implementado' );
  end;
end;

{ TConsumidorFinalHelper }

class function TConsumidorFinalHelper.ByValue(
  Value: string): TConsumidorFinal;
begin
  for Result := Low( TConsumidorFinal ) to High( TConsumidorFinal ) do
    if Result.Value = Value then
      Break;
end;

function TConsumidorFinalHelper.Description: string;
begin
  case Self of
    cfNaoDefinido: Exit( 'N�o definido' );
    cfNao: Exit( 'N�o' );
    cfSim: Exit( 'Sim' );
  end;
end;

function TConsumidorFinalHelper.Value: string;
begin
  case Self of
    cfNaoDefinido: Exit( 'NAO_DEFINIDO' );
    cfNao: Exit( 'NAO' );
    cfSim: Exit( 'SIM' );
  end;
end;

{ TTipoEmpresaHelper }

class function TTipoEmpresaHelper.ByValue(Value: String): TTipoEmpresa;
begin
   for Result := Low( TTipoEmpresa ) to High( TTipoEmpresa ) do
    if Result.Value = Value then
      Break;
end;

function TTipoEmpresaHelper.Description: string;
begin
  case Self of
    teMatriz        : Exit( 'Matriz' );
    teFilial        : Exit( 'Filial' );
    teDeposito      : Exit( 'Dep�sito' );
    teEscritorio    : Exit( 'Escrit�rio' );
    teProdutorRural : Exit( 'Produtor Rural' );
  end;
end;


function TTipoEmpresaHelper.Value: string;
begin
  case Self of
    teMatriz        : Exit( 'MATRIZ' );
    teFilial        : Exit( 'FILIAL' );
    teDeposito      : Exit( 'DEPOSITO' );
    teEscritorio    : Exit( 'ESCRITORIO' );
    teProdutorRural : Exit( 'PRODUTOR_RURAL' );
  end;
end;

{ TNewsTipoHelper }

class function TNewsTipoHelper.ByValue(AValue: string): TNewsTipo;
var
  n : TNewsTipo;
begin
  for n := Low( TNewsTipo ) to High( TNewsTipo ) do
    if n.Value.Equals( AValue ) then
      Exit( n );
end;

function TNewsTipoHelper.Descricao: String;
begin
  case Self of
    newsNews    : Exit( 'News' );
    newsBanner  : Exit( 'Banner' );
    newsMensagem: Exit( 'Mensagem' );
  end;
end;

function TNewsTipoHelper.Value: String;
begin
  case Self of
    newsNews    : Exit( 'NEWS' );
    newsBanner  : Exit( 'BANNER' );
    newsMensagem: Exit( 'MENSAGEM' );
  end;
end;

{ TNewsFiltroEmissaoHelper }

class function TNewsFiltroEmissaoHelper.ByValue(
  AValue: string): TNewsFiltroEmissao;
var
  n : TNewsFiltroEmissao;
begin
  for n := Low( TNewsFiltroEmissao ) to High( TNewsFiltroEmissao ) do
    if n.Value.Equals( AValue ) then
      Exit( n );
end;

function TNewsFiltroEmissaoHelper.Descricao: String;
begin
  case Self of
    nfeAmbos    : Exit( 'Ambos' );
    nfeEmite    : Exit( 'Emite' );
    nfeNaoEmite : Exit( 'N�o Emite' );
  end;
end;

function TNewsFiltroEmissaoHelper.Value: String;
begin
  case Self of
    nfeAmbos    : Exit( 'AMBOS' );
    nfeEmite    : Exit( 'EMITE' );
    nfeNaoEmite : Exit( 'NAO_EMITE' );
  end;
end;

{ TNewsBannerPrioridadeHelper }

class function TNewsBannerPrioridadeHelper.ByValue(
  AValue: string): TNewsBannerPrioridade;
var
  n : TNewsBannerPrioridade;
begin
  for n := Low( TNewsBannerPrioridade ) to High( TNewsBannerPrioridade ) do
    if n.Value.Equals( AValue ) then
      Exit( n );
end;

function TNewsBannerPrioridadeHelper.Descricao: String;
begin
  case Self of
    nbpBaixa: Exit( 'Baixa' );
    nbpMedia: Exit( 'M�dia' );
    nbpAlta : Exit( 'Alta' );
  end;
end;

function TNewsBannerPrioridadeHelper.Value: String;
begin
  case Self of
    nbpBaixa: Exit( 'BAIXA' );
    nbpMedia: Exit( 'MEDIA' );
    nbpAlta : Exit( 'ALTA' );
  end;
end;

{ TTarefaPrioridadeHelper }

class function TTarefaPrioridadeHelper.ByValue(
  Value: String): TTarefaPrioridade;
var
  p : TTarefaPrioridade;
begin
  for p := Low( TTarefaPrioridade ) to High( TTarefaPrioridade ) do
    if p.Value.Equals( Value ) then
      Exit ( p );
end;

function TTarefaPrioridadeHelper.Description: string;
begin
   case Self of
     tpUrgente: Exit( 'Urgente' ) ;
     tpAlta: Exit( 'Alta' ) ;
     tpMedia: Exit( 'M�dia' ) ;
     tpBaixa: Exit( 'Baixa' ) ;
     tpInsignificante: Exit( 'Insignificante' ) ;
   end;
end;

function TTarefaPrioridadeHelper.Value: string;
begin
  case Self of
    tpUrgente:  Exit( 'URGENTE' ) ;
    tpAlta: Exit( 'ALTA' ) ;
    tpMedia: Exit( 'MEDIA' ) ;
    tpBaixa: Exit( 'BAIXA' ) ;
    tpInsignificante: Exit( 'INSIGNIFICANTE' ) ;
  end;
end;

{ TBoletoTipoCarteiraHelper }

class function TBoletoTipoCarteiraHelper.ByValue(
  AValue: string): TBoletoTipoCarteira;
var
  tc : TBoletoTipoCarteira;
begin
  for tc := Low( TBoletoTipoCarteira ) to High( TBoletoTipoCarteira ) do
    if tc.Value = AValue then
      Exit( tc );
end;

function TBoletoTipoCarteiraHelper.Description: String;
begin
  case Self of
    btcSimples    : Exit( 'Carteira Simples' );
    btcRegistrada : Exit( 'Carteira Registrada' );
    btcEletronica : Exit( 'Carteira Eletr�nica' );
  end;
end;

function TBoletoTipoCarteiraHelper.Value: String;
begin
  case Self of
    btcSimples    : Exit( 'SIMPLES' );
    btcRegistrada : Exit( 'REGISTRADA' );
    btcEletronica : Exit( 'ELETRONICA' );
  end;
end;

{ TBoletoTipoEmissaoHelper }

class function TBoletoTipoEmissaoHelper.ByValue(
  AValue: string): TBoletoTipoEmissao;
var
  te : TBoletoTipoEmissao;
begin
  for te := Low(TBoletoTipoEmissao) to High(TBoletoTipoEmissao) do
    if te.Value.Equals( AValue ) then
      Exit( te );
end;

function TBoletoTipoEmissaoHelper.Description: String;
begin
  case Self of
    bteClienteEmite   : Exit( 'Cliente Emite' );
    bteBancoEmite     : Exit( 'Banco Emite' );
    bteBancoReemite   : Exit( 'Banco Reemite' );
    bteBancoNaoReemite: Exit( 'Banco N�o Reemite' );
  end;
end;

function TBoletoTipoEmissaoHelper.Value: String;
begin
  case Self of
    bteClienteEmite   : Exit( 'CLIENTE_EMITE' );
    bteBancoEmite     : Exit( 'BANCO_EMITE' );
    bteBancoReemite   : Exit( 'BANCO_REEMITE' );
    bteBancoNaoReemite: Exit( 'BANCO_NAO_REEMITE' );
  end;
end;

{ TBoletoTipoDocumentoHelper }

class function TBoletoTipoDocumentoHelper.ByValue(
  AValue: string): TBoletoTipoDocumento;
var
  td : TBoletoTipoDocumento;
begin
  for td := Low( TBoletoTipoDocumento ) to High( TBoletoTipoDocumento ) do
    if td.Value.Equals( AValue ) then
      Exit( td );
end;

function TBoletoTipoDocumentoHelper.Description: String;
begin
  case Self of
    btdTradicional: Exit( 'Documento Tradicional' );
    btdEscritural : Exit( 'Documento Escritural' );
  end;
end;

function TBoletoTipoDocumentoHelper.Value: String;
begin
  case Self of
    btdTradicional: Exit( 'TRADICIONAL' );
    btdEscritural : Exit( 'ESCRITURAL' );
  end;
end;

{ TBoletoCaracTituloHelper }

class function TBoletoCaracTituloHelper.ByValue(
  AValue: string): TBoletoCaracTitulo;
var
  tic : TBoletoCaracTitulo;
begin
  for tic := Low( TBoletoCaracTitulo ) to High( TBoletoCaracTitulo ) do
    if tic.Value.Equals( AValue ) then
      Exit( tic );
end;

function TBoletoCaracTituloHelper.Description: String;
begin
  case Self of
    ticSimples    : Exit( 'Cobran�a Simples' );
    ticVinculado  : Exit( 'Cobran�a Vinculada' );
    ticCaucionado : Exit( 'Cobran�a Caucionada' );
    ticDescontado : Exit( 'Cobran�a Descontada' );
    ticVendor     : Exit( 'Cobran�a Vendor' );
  end;
end;

function TBoletoCaracTituloHelper.Value: String;
begin
  case Self of
    ticSimples    : Exit( 'SIMPLES' );
    ticVinculado  : Exit( 'VINCULADO' );
    ticCaucionado : Exit( 'CAUCIONADO' );
    ticDescontado : Exit( 'DESCONTADO' );
    ticVendor     : Exit( 'VENDOR' );
  end;
end;

{ TBoletoTipoInscricaoHelper }

class function TBoletoTipoInscricaoHelper.ByValue(
  AValue: string): TBoletoTipoInscricao;
var
  ti : TBoletoTipoInscricao;
begin
  for ti := Low( TBoletoTipoInscricao ) to High( TBoletoTipoInscricao ) do
    if ti.Value.Equals( AValue ) then
      Exit( ti );
end;

function TBoletoTipoInscricaoHelper.Description: String;
begin
  case Self of
    btiFisica   : Exit( 'Pessoa F�sica' );
    btiJuridica : Exit( 'Pessoa Jur�dica' );
    btiOutros   : Exit( 'Outros' );
  end;
end;

function TBoletoTipoInscricaoHelper.Value: String;
begin
   case Self of
     btiFisica  : Exit( 'P_FISICA' );
     btiJuridica: Exit( 'P_JURIDICA' );
     btiOutros  : Exit( 'P_OUTRO' );
   end;
end;

{ TNFRefTipoHelper }

class function TNFRefTipoHelper.ByValue(AValue: string): TNFRefTipo;
var
  nf : TNFRefTipo;
begin
  for nf := Low( TNFRefTipo ) to High( TNFRefTipo ) do
    if nf.Value.Equals( AValue ) then
      Exit( nf );
end;

function TNFRefTipoHelper.Description: string;
begin
  case Self of
    nfrNFE_NFCE     : Exit( 'NF-e/NFC-e' );
    nfr1_1A         : Exit( 'Modelo 1/1A' );
    nfrProdutorRural: Exit( 'NF-e de Produtor Rural' );
    nfrCupomFiscal  : Exit( 'Cupom Fiscal' );
    nfrCte          : Exit( 'CT-e' );
  end;
end;

function TNFRefTipoHelper.Value: string;
begin
  case Self of
    nfrNFE_NFCE     : Exit( 'NFE_NFCE' );
    nfr1_1A         : Exit( 'MODELO_1_1A' );
    nfrProdutorRural: Exit( 'NFE_PRODUTOR_RURAL' );
    nfrCupomFiscal  : Exit( 'CUPOM_FISCAL' );
    nfrCte          : Exit( 'CTE' );
  end;
end;

{ TModRefNFeHelper }

class function TModRefNFeHelper.ByValue(AValue: string): TModRefNFE;
var
  nf : TModRefNFE;
begin
  for nf := Low( TModRefNFE ) to High( TModRefNFE ) do
    if nf.Value.Equals( AValue ) then
      Exit( nf );
end;

function TModRefNFeHelper.Description: string;
begin
  case Self of
    rnProdutor: Exit( 'Nota de Produtor' );
    rnAvulsa  : Exit( 'Nota Avulsa' );
    rnModelo2 : Exit( 'Nota Modelo 2' );
  end;
end;

function TModRefNFeHelper.Value: string;
begin
  case Self of
    rnProdutor: Exit( 'NF_DE_PRODUTOR' );
    rnAvulsa  : Exit( 'NF_AVULSA' );
    rnModelo2 : Exit( 'MODELO_2' );
  end;
end;

{ TModRefECFHelper }

class function TModRefECFHelper.ByValue(AValue: string): TModRefECF;
var
  nf : TModRefECF;
begin
  for nf := Low( TModRefECF ) to High( TModRefECF ) do
    if nf.Value.Equals( AValue ) then
      Exit( nf );
end;

function TModRefECFHelper.Description: string;
begin
  case Self of
    reM2B: Exit( 'Modelo 2B' );
    reM2C: Exit( 'Modelo 2C' );
    reM2D: Exit( 'Modelo 2D' );
  end;
end;

function TModRefECFHelper.Value: string;
begin
  case Self of
    reM2B: Exit( 'M_2B' );
    reM2C: Exit( 'M_2C' );
    reM2D: Exit( 'M_2D' );
  end;
end;

{ TTipoOrcamentoHelper }

class function TTipoOrcamentoHelper.ByValue(
  AValue: string): TTipoOrcamento;
var
  ATipo : TTipoOrcamento;
begin
  for ATipo := Low( TTipoOrcamento ) to High( TTipoOrcamento ) do begin
    if ATipo.Value.Equals( AValue )  then
      Exit( ATipo );
  end;
end;

function TTipoOrcamentoHelper.Description: String;
begin
  case Self of
    toOrcamento   : Exit( 'Or�amento' );
    toCondicional : Exit( 'Condicional' );
  end;
end;

function TTipoOrcamentoHelper.Value: string;
begin
  case Self of
    toOrcamento   : Exit( 'ORCAMENTO' );
    toCondicional : Exit( 'CONDICIONAL' );
  end;
end;

{ TTipoPapelImpressaoHelper }

class function TTipoPapelImpressaoHelper.ByValue(
  AValue: string): TTipoPapelImpressao;
var
  t : TTipoPapelImpressao;
begin
  for t := Low( TTipoPapelImpressao ) to High( TTipoPapelImpressao ) do
    if t.Value = AValue then
      Exit( t );
end;

function TTipoPapelImpressaoHelper.Description: string;
begin
  case Self of
    piBobina  : Exit( 'Bobina T�rmica (80mm)' );
    piA4      : Exit( 'A4' );
    piCarta   : Exit( 'Carta' );
    piOficio  : Exit( 'Of�cio' );
    piOficio2 : Exit( 'Of�cio 2' );
    piOficio9 : Exit( 'Of�cio 9' );
    piTabloide: Exit( 'Tabloide' );
  end;
end;

function TTipoPapelImpressaoHelper.Value: string;
begin
  case Self of
    piBobina  : Exit( 'BOBINA_TERMICA' );
    piA4      : Exit( 'A4' );
    piCarta   : Exit( 'CARTA' );
    piOficio  : Exit( 'OFICIO' );
    piOficio2 : Exit( 'OFICIO_2' );
    piOficio9 : Exit( 'OFICIO_9' );
    piTabloide: Exit( 'TABLOIDE' );
  end;
end;

function TTipoPapelImpressaoHelper.X: Integer;
begin
  case Self of
    piBobina  : Exit( 80 );
    piA4      : Exit( 210 );
    piCarta,
    piOficio,
    piOficio2 : Exit( 216 );
    piOficio9 : Exit( 215 );
    piTabloide: Exit( 279 );
  end;
end;

function TTipoPapelImpressaoHelper.Y: Integer;
begin
  case Self of
    piBobina  : Exit( 297 );
    piA4      : Exit( 297 );
    piCarta   : Exit( 279 );
    piOficio  : Exit( 356 );
    piOficio2 : Exit( 330 );
    piOficio9 : Exit( 315 );
    piTabloide: Exit( 432 );
  end;
end;

{ TPrioridadeOrdemServicoHelper }

class function TPrioridadeOrdemServicoHelper.ByValue(
  Value: String): TPrioridadeOrdemServico;
var
  t : TPrioridadeOrdemServico;
begin
  for t := Low( TPrioridadeOrdemServico ) to High( TPrioridadeOrdemServico ) do
    if t.Value = Value then
      Exit( t );
end;

function TPrioridadeOrdemServicoHelper.Description: string;
begin
  case Self of
    tposUrgente  : Exit( 'Urgente' );
    tposAlta     : Exit( 'Alta' );
    tposMedia    : Exit( 'M�dia' );
    tposBaixa    : Exit( 'Baixa' );
  end;
end;

class procedure TPrioridadeOrdemServicoHelper.FillCbb(ACbb: TcxImageComboBox);
var
  t : TPrioridadeOrdemServico;
begin
  try
    Cbb_Begin( ACbb );
    for t := Low( TPrioridadeOrdemServico ) to High( TPrioridadeOrdemServico ) do begin
      Cbb_Add( ACbb, t.Description, t.Value );
    end;
  finally
    Cbb_Terminate( ACbb );
  end;
end;

function TPrioridadeOrdemServicoHelper.Value: string;
begin
   case Self of
    tposUrgente  : Exit( 'URGENTE' );
    tposAlta     : Exit( 'ALTA' );
    tposMedia    : Exit( 'MEDIA' );
    tposBaixa    : Exit( 'BAIXA' );
  end;
end;


{ TOrigemOrcamentoHelper }

class function TOrigemOrcamentoHelper.ByValue(AValue: string): TOrigemOrcamento;
var
  t : TOrigemOrcamento;
begin
  for t := Low( TOrigemOrcamento ) to High( TOrigemOrcamento ) do
    if t.Value = AValue then
      Exit( t );
end;

function TOrigemOrcamentoHelper.Description: String;
begin
  case Self of
    ooManual     : Exit( 'Manual' );
    ooForcaVenda : Exit( 'For�a de Venda' );
    ooNuvemShop  : Exit( 'NuvemShop' );
  end;

end;

function TOrigemOrcamentoHelper.Value: string;
begin
  case Self of
    ooManual     : Exit( 'MANUAL' );
    ooForcaVenda : Exit( 'FORCAVENDA' );
    ooNuvemShop  : Exit( 'NUVEMSHOP' );
  end;

end;

{ TOrdemProducaoTipoHelper }

class function TOrdemProducaoTipoHelper.ByValue(Value: String): TOrdemProducaoTipo;
begin
  for Result := Low( TOrdemProducaoTipo ) to High( TOrdemProducaoTipo ) do
    if Result.Value = Value then
      Break;
end;

function TOrdemProducaoTipoHelper.Description: string;
begin
  case Self of
    optImediata: Exit( 'Imediata' );
    optEmProducao: Exit( 'Em Produ��o' );
  end;
end;

function TOrdemProducaoTipoHelper.Value: string;
begin
  case Self of
    optImediata: Exit( 'IMEDIATA' );
    optEmProducao: Exit( 'EM_PRODUCAO' );
  end;
end;

{ TOrdemProducaoStatusHelper }

class function TOrdemProducaoStatusHelper.ByValue(Value: String): TOrdemProducaoStatus;
begin
  for Result := Low( TOrdemProducaoStatus ) to High( TOrdemProducaoStatus ) do
    if Result.Value = Value then
      Break;
end;

function TOrdemProducaoStatusHelper.Description: string;
begin
  case Self of
    opsPendente: Exit( 'Pendente' );
    opsEmAndamento: Exit( 'Em andamento' );
    opsFinalizado: Exit( 'Finalizado' );
    opsCancelado: Exit( 'Cancelado' );
  end;
end;

function TOrdemProducaoStatusHelper.Value: string;
begin
  case Self of
    opsPendente: Exit( 'PENDENTE' );
    opsEmAndamento: Exit( 'EM_ANDAMENTO' );
    opsFinalizado: Exit( 'FINALIZADO' );
    opsCancelado: Exit( 'CANCELADO' );
  end;
end;

{ TImportacaoViaTranspHelper }

function TImportacaoViaTranspHelper.Description: string;
begin
  case Self of
    vtMaritima          : Exit( 'Mar�tima' );
    vtFluvial           : Exit( 'Fluvial' );
    vtLacustre          : Exit( 'Lacustre' );
    vtAerea             : Exit( 'A�rea' );
    vtPostal            : Exit( 'Postal' );
    vtFerroviaria       : Exit( 'Ferrovi�ria' );
    vtRodoviaria        : Exit( 'Rodovi�ria' );
    vtConduto           : Exit( 'Conduto' );
    vtMeiosProprios     : Exit( 'Meios Pr�prios' );
    vtEntradaSaidaFicta : Exit( 'Entrada/Sa�da Ficta' );
  end;
end;

function TImportacaoViaTranspHelper.Value: string;
begin
  case Self of
    vtMaritima          : Exit( 'MARITIMA' );
    vtFluvial           : Exit( 'FLUVIAL' );
    vtLacustre          : Exit( 'LACUSTRE' );
    vtAerea             : Exit( 'AEREA' );
    vtPostal            : Exit( 'POSTAL' );
    vtFerroviaria       : Exit( 'FERROVIARIA' );
    vtRodoviaria        : Exit( 'RODOVIARIA' );
    vtConduto           : Exit( 'CONDUTO' );
    vtMeiosProprios     : Exit( 'MEIOS_PROPRIOS' );
    vtEntradaSaidaFicta : Exit( 'ENTRADA_SAIDA_FICTA' );
  end;
end;

class function TImportacaoViaTranspHelper.ValueOf(
  AValue: string): TImportacaoViaTransp;
begin
  for Result := Low( TImportacaoViaTransp ) to High( TImportacaoViaTransp ) do
    if Result.Value = AValue then
      Break;
end;

{ TImportacaoIntermedioHelper }

function TImportacaoIntermedioHelper.Description: string;
begin
  case Self of
    intContaPropria : Exit( 'Importa��o por conta pr�pria' );
    intContaEOrdem  : Exit( 'Importa��o por conta e ordem' );
    intEncomenda    : Exit( 'Importa��o por encomenda' );
  end;
end;

function TImportacaoIntermedioHelper.Value: string;
begin
  case Self of
    intContaPropria : Exit( 'CONTA_PROPRIA' );
    intContaEOrdem  : Exit( 'CONTA_E_ORDEM' );
    intEncomenda    : Exit( 'ENCOMENDA' );
  end;
end;

class function TImportacaoIntermedioHelper.ValueOf(
  AValue: string): TImportacaoIntermedio;
begin
    for Result := Low( TImportacaoIntermedio ) to High( TImportacaoIntermedio ) do
    if Result.Value = AValue then
      Break;
end;

end.
