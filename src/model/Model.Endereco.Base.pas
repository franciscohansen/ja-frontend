unit Model.Endereco.Base;

interface

uses
  Model.Endereco.Interfaces, Json.Interfaces, Model.Enums;

type
  TEndereco = class( TInterfacedObject, IEndereco )
  private
    FLogradouro: string;
    FNumero: string;
    FComplemento: string;
    FBairro: string;
    FCep: string;
    FPais: string;
    FUF: string;
    FMunicipio: string;
    FJson : IJson;
  public
    constructor Create( Json: IJson );
    class function New( Json: IJson ): IEndereco; overload;
    class function New: IEndereco; overload;
    function Logradouro: string;
    function Numero: string;
    function Complemento: string;
    function Bairro: string;
    function Cep: string;
    function Pais: string;
    function UF: string;
    function Municipio: string;
    function Vazio : boolean;
    function Json : IJson;
  end;

  TMunicipioIBGE = class( TInterfacedObject, IMunicipioIBGE )
  protected
    constructor Create( Json : IJson );
  private
    FNome : string;
    FCodigo : Integer;
    FUF : TUF;
  public
    class function New( Json : IJson ) : IMunicipioIBGE;
    function Nome : string;
    function Codigo : integer;
    function Uf : TUF;
  end;

  TPaisIBGE = class( TInterfacedObject, IPaisIBGE )
  private
    constructor Create( Json : IJson );
  private
    FNome : String;
    FCodigo : Integer;
  public
    class function New( Json : IJson ) : IPaisIBGE;
    function Nome : string;
    function Codigo : integer;
  end;

implementation

{ TEndereco }

uses Util.Format, Json.Builder;

function TEndereco.Bairro: string;
begin
  Result := FBairro;
end;

function TEndereco.Cep: string;
begin
  Result := FCep;
end;

function TEndereco.Complemento: string;
begin
  Result := FComplemento;
end;

constructor TEndereco.Create(Json: IJson);
begin
  inherited Create;
  if ( Json <> nil ) and ( not Json.IsEmpty ) then begin
    FJson        := Json;
    FLogradouro  := Json.Item( 'logradouro' ).AsString;
    FNumero      := Json.Item( 'numero' ).AsString;
    FComplemento := Json.Item( 'complemento' ).AsString;
    FBairro      := Json.Item( 'bairro' ).AsString;
    FCep         := TFormat.Cep( Json.Item( 'cep' ).AsString );
    FPais        := Json.Item( 'pais' ).AsString;
    FUF          := Json.Item( 'uf' ).AsString;
    FMunicipio   := Json.Item( 'municipio' ).AsString;
  end
  else
    FJson := TJsonBuilder.Endereco;
end;

function TEndereco.Json: IJson;
begin
  Result := FJson;
end;

function TEndereco.Logradouro: string;
begin
  Result := FLogradouro;
end;

function TEndereco.Municipio: string;
begin
  Result := FMunicipio;
end;

class function TEndereco.New(Json: IJson): IEndereco;
begin
  Result := TEndereco.Create( Json );
end;

class function TEndereco.New: IEndereco;
begin
  Result := TEndereco.Create( nil );
end;

function TEndereco.Numero: string;
begin
  Result := FNumero;
end;

function TEndereco.Pais: string;
begin
  Result := FPais;
end;

function TEndereco.UF: string;
begin
  Result := FUf;
end;

function TEndereco.Vazio: boolean;
begin
  Result := ( FLogradouro = '' ) and ( FNumero = '' ) and ( FComplemento = '' ) and
            ( FBairro = '' ) and ( FCep = '' ) and ( FPais = '' ) and ( ( FUf = '' ) or ( FUf = 'NENHUM' ) ) and ( FMunicipio = '' );
end;

{ TMunicipioIBGE }

function TMunicipioIBGE.Codigo: integer;
begin
  Result := FCodigo;
end;

constructor TMunicipioIBGE.Create(Json: IJson);
begin
  inherited Create;
  if ( Json <> nil ) and ( not Json.IsEmpty ) then begin
    FCodigo := Json.Item( 'codigo' ).AsInteger;
    FNome := Json.Item( 'nome' ).AsString;
    FUF := TUF.FromString( Json.Item( 'uf' ).AsString );
  end;

end;

class function TMunicipioIBGE.New(Json: IJson): IMunicipioIBGE;
begin
  Result := Create( Json );
end;

function TMunicipioIBGE.Nome: string;
begin
  Result := FNome;
end;

function TMunicipioIBGE.Uf: TUF;
begin
  Result := FUF;
end;

{ TPaisIBGE }

function TPaisIBGE.Codigo: integer;
begin
  Result := FCodigo;
end;

constructor TPaisIBGE.Create(Json: IJson);
begin
  inherited Create;
  if ( Json <> nil ) and ( not Json.IsEmpty ) then begin
    FCodigo := Json.Item( 'codigo' ).AsInteger;
    FNome := Json.Item( 'nome' ).AsString;
  end;

end;

class function TPaisIBGE.New(Json: IJson): IPaisIBGE;
begin
  Result := Create( Json );
end;

function TPaisIBGE.Nome: string;
begin
  Result := FNome;
end;

end.
