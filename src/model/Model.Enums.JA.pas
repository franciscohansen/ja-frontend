unit Model.Enums.JA;

interface

uses
  System.SysUtils, System.Variants, System.Classes;

type
  TTipoDocumento = ( docVeiculo, docCartao, docEmprestimo );
  TIndicacao = ( indCliente, indNaoCliente, indFacebook, indTwitter, indInstagram,
     indGoogle, indJornal, indTV, indSBT, indRedeTV, indGlobo, indRecord, indRevista, indOutro );
  TTipoContrato = ( tcVeiculo, tcCartaoCredito, tcEmprestimos, tcOutros, tcGeral );
  TJuridicoSituacao = ( jsSemBusca, jsComBusca, jsMandadoExpedido, jsContratoCancelado, jsCarroApreendido, jsRenajud, jsEmExecucao, jsFinalizado, jsArquivado );
  TIndicacaoHelper = record helper for TIndicacao
  public
    function Description : string;
    function Value : string;
    class function ValueOf( AValue : string ) : TIndicacao; static;
  end;
  TTipoContratoHelper = record helper for TTipoContrato
  public
    function Description : String;
    function Value : string;
    class function ValueOf( AValue : string ) : TTipoContrato; static;
  end;
  TTipoDocumentoHelper = record helper for TTipoDocumento
  public
    function Description : string;
    function Value : string;
    class function ValueOf( AValue : string ) : TTipoDocumento; static;
  end;
  TJuridicoSituacaoHelper = record helper for TJuridicoSituacao
  public
    function Description : string;
    function Value : string;
    class function ValueOf( AValue : string ) : TJuridicoSituacao; static;
  end;

implementation

{ TTipoDocumentoHelper }

function TTipoDocumentoHelper.Description: string;
begin
  case Self of
    docVeiculo    : Exit( 'Ve�culo' );
    docCartao     : Exit( 'Cart�o de Cr�dito' );
    docEmprestimo : Exit( 'Empr�stimo' );
  end;
end;

function TTipoDocumentoHelper.Value: string;
begin
  case Self of
    docVeiculo    : Exit( 'VEICULO' );
    docCartao     : Exit( 'CARTAO_CREDITO' );
    docEmprestimo : Exit( 'EMPRESTIMOS' );
  end;
end;

class function TTipoDocumentoHelper.ValueOf(AValue: string): TTipoDocumento;
var
  d : TTipoDocumento;
begin
  for d := Low( TTipoDocumento ) to High( TTipoDocumento ) do
    if d.Value = AValue then
      Exit( d );
end;

{ TIndicacaoHelper }

function TIndicacaoHelper.Description: string;
begin
  case Self of
    indCliente    : Exit( 'Indica��o de Cliente' );
    indNaoCliente : Exit( 'Indica��o N�o Cliente' );
    indFacebook   : Exit( 'Facebook' );
    indTwitter    : Exit( 'Twitter' );
    indInstagram  : Exit( 'Instagram' );
    indGoogle     : Exit( 'Google' );
    indJornal     : Exit( 'Jornal' );
    indTV         : Exit( 'TV' );
    indSBT        : Exit( 'SBT' );
    indRedeTV     : Exit( 'RedeTV' );
    indGlobo      : Exit( 'Globo' );
    indRecord     : Exit( 'Record' );
    indRevista    : Exit( 'Revista' );
    indOutro      : Exit( 'Outro' );
  end;
end;

function TIndicacaoHelper.Value: string;
begin
  case Self of
    indCliente    : Exit( 'CLIENTE' );
    indNaoCliente : Exit( 'NAO_CLIENTE' );
    indFacebook   : Exit( 'FACEBOOK' );
    indTwitter    : Exit( 'TWITTER' );
    indInstagram  : Exit( 'INSTAGRAM' );
    indGoogle     : Exit( 'GOOGLE' );
    indJornal     : Exit( 'JORNAL' );
    indTV         : Exit( 'TV' );
    indSBT        : Exit( 'SBT' );
    indRedeTV     : Exit( 'REDETV' );
    indGlobo      : Exit( 'GLOBO' );
    indRecord     : Exit( 'RECORD' );
    indRevista    : Exit( 'REVISTA' );
    indOutro      : Exit( 'OUTRO' );
  end;
end;

class function TIndicacaoHelper.ValueOf(AValue: string): TIndicacao;
var
  i : TIndicacao;
begin
  for i := Low( TIndicacao ) to High( TIndicacao ) do
    if i.Value = AValue then
      Exit( i );
end;

{ TTipoContratoHelper }

function TTipoContratoHelper.Description: String;
begin
  case Self of
    tcVeiculo       : Exit( 'Ve�culos' );
    tcCartaoCredito : Exit( 'Cart�o de Cr�dito' );
    tcEmprestimos   : Exit( 'Empr�stimos' );
    tcOutros        : Exit( 'Outros' );
    tcGeral         : Exit( 'Geral' );
  end;
end;

function TTipoContratoHelper.Value: string;
begin
  case Self of
    tcVeiculo       : Exit( 'VEICULO' );
    tcCartaoCredito : Exit( 'CARTAO_CREDITO' );
    tcEmprestimos   : Exit( 'EMPRESTIMOS' );
    tcOutros        : Exit( 'OUTROS' );
    tcGeral         : Exit( 'GERAL' );
  end;
end;

class function TTipoContratoHelper.ValueOf(AValue: string): TTipoContrato;
var
  t : TTipoContrato;
begin
  for t := Low( TTipoContrato ) to High( TTipoContrato ) do
    if t.Value = AValue then
      Exit( t );
end;

{ TJuridicoSituacaoHelper }

function TJuridicoSituacaoHelper.Description: string;
begin
  case Self of
    jsSemBusca          : Exit( 'Sem Busca' );
    jsComBusca          : Exit( 'Com Busca' );
    jsMandadoExpedido   : Exit( 'Mandado Expedido' );
    jsContratoCancelado : Exit( 'Contrato Cancelado' );
    jsCarroApreendido   : Exit( 'Carro Apreendido' );
    jsRenajud           : Exit( 'RENAJUD' );
    jsEmExecucao        : Exit( 'Em Execu��o' );
    jsFinalizado        : Exit( 'Finalizado' );
    jsArquivado         : Exit( 'Arquivado' );
  end;
end;

function TJuridicoSituacaoHelper.Value: string;
begin
  case Self of
    jsSemBusca          : Exit( 'SEM_BUSCA' );
    jsComBusca          : Exit( 'COM_BUSCA' );
    jsMandadoExpedido   : Exit( 'MANDADO_EXPEDIDO' );
    jsContratoCancelado : Exit( 'CONTRATO_CANCELADO' );
    jsCarroApreendido   : Exit( 'CARRO_APREENDIDO' );
    jsRenajud           : Exit( 'RENAJUD' );
    jsEmExecucao        : Exit( 'EM_EXECUCAO' );
    jsFinalizado        : Exit( 'FINALIZADO' );
    jsArquivado         : Exit( 'ARQUIVADO' );
  end;
end;

class function TJuridicoSituacaoHelper.ValueOf(
  AValue: string): TJuridicoSituacao;
var
  i: TJuridicoSituacao;
begin
  for i := Low( TJuridicoSituacao ) to High( TJuridicoSituacao ) do
    if i.Value = AValue then
      Exit( i );
end;

end.
