unit View.MappingControls.Interfaces;

interface

uses
  Json.Interfaces, Vcl.Controls;

type
  TMapCtrlTypeOfData = ( mcdString, mcdInteger, mcdFloat, mcdDate, mcdDateTime, mcdBoolean, mcdJsonArray, mcdJson, mcdVarArray );

  IMapCtrl = interface
    ['{2F27DBD8-5081-41F6-8F3C-10EB2B091FCB}']
    function GetTypeOfData: TMapCtrlTypeOfData;
    function GetValue: variant;
    procedure SetValue( Value: Variant );
    procedure ReadOnly( b: boolean );
  end;

  IMapCtrls = interface
    ['{3651DDE7-52A4-43CD-862C-B86233484C57}']
    function Add( JsonProperty: string; MapCtrl: IMapCtrl ): IMapCtrls; overload;
    function Add( JsonProperty: string; MapCtrls: IMapCtrls ): IMapCtrls; overload;
    function MapCtrl( JsonProperty: string ): IMapCtrl;
    function MapCtrls( JsonProperty: string ): IMapCtrls;
    function IsMapCtrl( JsonProperty: string ): boolean;
    function Properties: TArray<String>;
  end;

  IManipulateCtrls = interface
    ['{2E005B79-7D90-4821-AEE5-635A2C86F353}']
    function ToJson: IJson;
    procedure ToCtrl( ReadOnly: boolean = false );
  end;

  IManipulateCamposUsuario = interface
    ['{2183428D-38FD-4AA9-828F-87B4440CB3F2}']
    function ToJson: TArray<IJson>;
    procedure ToCtrl;
  end;

implementation

end.
