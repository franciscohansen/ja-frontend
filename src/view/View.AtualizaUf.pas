unit View.AtualizaUf;

interface

uses
  dxLayoutContainer, dxLayoutControl, cxClasses, dxLayoutLookAndFeels, System.SysUtils, cxSpinEdit, cxTL,
  cxCalc, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit, cxDropDownEdit, Vcl.Forms, cxCheckListBox, cxImageComboBox, Model.Enums, cxMemo, cxButtons,
  cxProgressBar, cxCheckBox, cxLabel, System.Classes, cxDateUtils, cxCalendar, cxCheckComboBox,
  Util.ConsultaLocalizacao.Interfaces, Model.Endereco.Interfaces, Windows, ExtCtrls, Variants, StdCtrls, VCL.Controls, VCL.Dialogs;


type
  ThreadAtualizaUF = class( TThread )
  protected
    procedure Execute; override;
  private
    FCbbUF : TcxImageComboBox;
    FLciUF : TdxLayoutItem;
    FCbbMun : TcxImageComboBox;
    FLciMun : TdxLayoutItem;
    Flc : TdxLayoutControl;
    FFoco: TWinControl;
    procedure PopulaComboMunicipio(Cbb: TcxImageComboBox; UF: TUF);
  public
    constructor Create( CreateSuspended: boolean );
    property CbbUF : TcxImageComboBox write FCbbUF;
    property LciUF : TdxLayoutItem write FLciUF;
    property CbbMun: TcxImageComboBox write FCbbMun;
    property LciMun: TdxLayoutItem write FLciMun;
    property Foco: TWinControl read FFoco write FFoco;
    property LC : TdxLayoutControl write FLC;
  end;

implementation

{ ThreadAtualizaUF }

uses Util.ConsultaLocalizacao.Base, Util.Strings;

procedure ThreadAtualizaUF.PopulaComboMunicipio(Cbb: TcxImageComboBox; UF: TUF);
var
  municipios : TArray<IMunicipioIBGE>;
  busca : IConsultaUF;
  i, l : Integer;
  m : IMunicipioIBGE;
  value : String;
  MunicipioEncontrado: string;
begin
  Cbb.Properties.BeginUpdate;
  try
    MunicipioEncontrado := cbb.HelpKeyword;
    cbb.HelpKeyword := '';
    value := VarToStrDef( cbb.EditValue, '' );
    Cbb.Properties.Items.Clear;
    busca := TConsultaUF.New( UF.AsShortString );
    if busca.Achou then begin

      municipios := busca.Municipios;
      l := Length( municipios );
      for i := 0 to l - 1 do begin
        m := municipios[ i ];
        with cbb.Properties.Items.Add do begin
          Description := m.Nome;
          Value := m.Nome;
        end;

        if MunicipioEncontrado <> '' then begin
          if TStringUtils.TiraAcentos( m.Nome ).ToUpper = MunicipioEncontrado then begin
            Value := m.Nome;
          end;
        end;
      end;

    end;
  finally
    //O chamado ao EndUpdate dos controles tem que ser dentro de um Synchronize, sen�o trava a UI
    Synchronize(
      procedure
      begin
        try
          if Assigned( Cbb ) and Assigned( Cbb.Properties ) and Assigned( cbb.Parent ) then begin
            Cbb.Properties.EndUpdate;
            Cbb.EditValue := value;
          end;
        except
          on e: EAccessViolation do begin
            { red#2676, o Ryan disse que se abrir e fechar a tela rapidamente o problema ocorre
            eu acho que � porque a tela fechou mas a thread ainda t� executando, ent�o quando finaliza aqui
            e vai atualizar na tela, ta tudo fechado a� d� pau
            ent�o filtra o AV e segue a vida }
          end;
        end;
      end
    );
  end;
end;

constructor ThreadAtualizaUF.Create( CreateSuspended: boolean );
begin
  inherited Create( CreateSuspended );
end;

procedure ThreadAtualizaUF.Execute;
begin
  inherited;
  FreeOnTerminate := True;
  try
    try
      while FLC.Container.IsUpdateLocked do
        Application.ProcessMessages;
      //Se o form for fechado antes de terminar de rodar a Thread, da AV dentro dos SYNCHRONIZE, por isso os try...except...
      Synchronize(
        procedure
        begin
          try
            FLC.BeginUpdate
          except
          end;
        end );
//      FLC.BeginUpdate;
      FLciUF.Enabled  := False;
      FLciMun.Enabled := False;
      FCbbUF.Cursor   := crHourGlass;
      FCbbMun.Cursor  := crHourGlass;
    finally
      //O chamado ao EndUpdate dos controles tem que ser dentro de um Synchronize, sen�o trava a UI
      //Se o form for fechado antes de terminar de rodar a Thread, da AV dentro dos SYNCHRONIZE, por isso os try...except...
      Synchronize(
        procedure
        begin
          try
            FLC.EndUpdate;
          except

          end;
        end
      );
    end;

    try
      PopulaComboMunicipio( FCbbMun, TUF.FromString( FCbbUF.EditValue ) );
    finally
      try
        while Flc.Container.IsUpdateLocked do
          Application.ProcessMessages;
        //Se o form for fechado antes de terminar de rodar a Thread, da AV dentro dos SYNCHRONIZE, por isso os try...except...
        Synchronize( procedure begin
          try
            FLC.BeginUpdate
          except
          end;
        end
        );
//        FLC.BeginUpdate;
        FLciUF.Enabled  := True;
        FLciMun.Enabled := True;
        FCbbUF.Cursor   := crDefault;
        FCbbMun.Cursor  := crDefault;
      finally
        //O chamado ao EndUpdate dos controles tem que ser dentro de um Synchronize, sen�o trava a UI
        //Se o form for fechado antes de terminar de rodar a Thread, da AV dentro dos SYNCHRONIZE, por isso os try...except...
        Synchronize(
          procedure
          begin
            try
              if Assigned( FLC ) then
                FLC.EndUpdate;

              if Assigned( FFoco ) then
                FFoco.SetFocus; // pra n�o perder o foco;
            except
            end;
          end
        );
      end;
    end;
    FCbbUF.Tag := 0;
  except
    on E:Exception do
      OutputDebugString( PWideChar( E.Message ) );
  end;
end;



end.
