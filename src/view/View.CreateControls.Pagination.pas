unit View.CreateControls.Pagination;

interface

uses
  dxLayoutContainer, dxLayoutControl, cxClasses, dxLayoutLookAndFeels, View.CreateControls.Interfaces, System.SysUtils, cxSpinEdit, cxTL,
  cxCalc, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit, cxDropDownEdit, Vcl.Forms, cxCheckListBox, cxImageComboBox, Model.Enums, cxMemo, cxButtons,
  Cfg.Constantes, Model.CampoUsuario.Interfaces, cxProgressBar, Dao.Interfaces, cxLabel, Vcl.Controls, Variants, System.Classes,
  Util.Lists;
type
  TCreatePaginator = class( TInterfacedObject, ICreatePaginator )
  procedure OnClick( Sender : TObject );
  procedure OnLoadAllClick( Sender : TObject );
  procedure OnCbbPropertiesChange( Sender : TObject );
  protected
    constructor Create( AOwner : TForm );
  private
    FOwner          : TForm;
    FPaginatorProc  : TPaginatorProc;
    FPagination     : IPagination;
    FRefresh        : IPaginationRefresh;
    FSearch         : ISearchParams;
    FSearchString   : string;
    FCustomNavFunc  : TCustomFindAllFunction;
    FCustomRecFunc  : TCustomRecordCountFunction;
    FDao            : IDao;
    FPag            : Integer;
    FPaginaAtual    : Integer;
    FMaxReg         : Integer;
    cbb             : TcxImageComboBox;
    pb              : TcxProgressBar;
    lb              : TcxLabel;
    FUseIdx,
    FAbortLoadAll   : boolean;
    btnAnterior,
    btnProximo,
    btnLoadMore,
    btnLoadAll      : TcxButton;
    bLoadMore       : boolean;
    FThreadList     : IList<TThread>;
    procedure FormataLayoutItem( Item : TdxLayoutItem; Caption : String; AlignHorz: TdxLayoutAlignHorz; AlignVert : TdxLayoutAlignVert; ShowCaption: Boolean = True );
    function PegaMaxRegistros : Integer;
    function CriaCxButton( Name, Caption : string ) : TcxButton;
    function CriaCxImageComboBox( Name : string; Width : Integer ) : TcxImageComboBox;
    function CriaCxProgressBar( Name : string ) : TcxProgressBar;
    function CriaCxLabel( Name : string ) : TcxLabel;
    procedure Navega( PageNo : Integer );
  public
    class function New( Owner : TForm ) : ICreatePaginator;
    function MultiplePagesPaginator( Group : TdxLayoutGroup ) : ICreatePaginator;
    function LoadMorePaginator( Group : TdxLayoutGroup ) : ICreatePaginator;
    function SetOnPaginatorRefresh( Proc : TPaginatorProc ) : ICreatePaginator;
    function SetCustomNavigateFunction( CustomNav : TCustomFindAllFunction ) : ICreatePaginator;
    function SetCustomRecordCountFunction( CustomRec : TCustomRecordCountFunction ) : ICreatePaginator;
    function SetUseIndexedSearch( AUseIdx : boolean ) : ICreatePaginator;
    function DoAbortLoadAll : ICreatePaginator;
    procedure RedoSearch;
  end;

implementation

uses
  Dao.Pagination, View.Patterns, View.MessageBox, Util.View, Cfg.Factory;

{ TCreatePaginator }

constructor TCreatePaginator.Create(AOwner: TForm);
begin
  inherited Create;
  FOwner := AOwner;
  FUseIdx := False;
end;

function TCreatePaginator.CriaCxButton(Name, Caption: string): TcxButton;
begin
  Result          := TcxButton.Create( FOwner );
  Result.Parent   := FOwner;
  Result.Height   := 39;
  Result.Width    := 87;
  Result.Name     := Name;
  Result.Caption  := Caption;
end;

function TCreatePaginator.CriaCxImageComboBox(
  Name: string; Width : integer ): TcxImageComboBox;
begin
  Result        := TcxImageComboBox.Create( FOwner );
  Result.Parent := FOwner;
  Result.Name   := Name;
  Result.Width  := Width;
end;

function TCreatePaginator.CriaCxLabel(Name: string): TcxLabel;
begin
  Result              := TcxLabel.Create( FOwner );
  Result.Parent       := FOwner;
  Result.Transparent  := True;
  Result.AutoSize     := True;
end;

function TCreatePaginator.CriaCxProgressBar(Name: string): TcxProgressBar;
begin
  Result := TcxProgressBar.Create( FOwner );
  Result.Parent := FOwner;
  Result.Name := Name;
end;

function TCreatePaginator.DoAbortLoadAll: ICreatePaginator;
begin
  Result        := Self;
  FAbortLoadAll := True;
end;

procedure TCreatePaginator.FormataLayoutItem(Item: TdxLayoutItem;
  Caption: String; AlignHorz: TdxLayoutAlignHorz; AlignVert : TdxLayoutAlignVert; ShowCaption: Boolean);
begin
  Item.CaptionOptions.Text    := Caption;
  Item.CaptionOptions.Layout  := clTop;
  Item.CaptionOptions.Visible := ShowCaption;
  Item.AlignHorz              := AlignHorz;
  Item.AlignVert              := AlignVert;
end;

function TCreatePaginator.LoadMorePaginator(
  Group: TdxLayoutGroup): ICreatePaginator;
var
  g, gPb : TdxLayoutGroup;
begin
  Result := Self;
  bLoadMore := True;
  btnLoadMore := CriaCxButton( 'btnLoadMore', 'Carregar Mais...' );
  btnLoadMore.OnClick := OnClick;
  btnLoadMore.Width := 130;

  btnLoadAll := CriaCxButton( 'btnLoadAll', 'Carregar Todos...' );
  btnLoadAll.OnClick  := OnLoadAllClick;
  btnLoadAll.Width := 130;

  g := Group.CreateGroup;
  g.AlignHorz := ahClient;
  g.LayoutDirection := ldHorizontal;
  g.ShowBorder := False;
  pb := CriaCxProgressBar( 'pbPaginacao' );
  FormataLayoutItem( g.CreateItemForControl( btnLoadMore ), '', ahParentManaged, avParentManaged, False );
  FormataLayoutItem( g.CreateItemForControl( btnLoadAll ), '', ahParentManaged, avParentManaged, False );

  gPb := g.CreateGroup;
  gPb.ShowBorder := false;
  gPb.LayoutDirection := ldVertical;
  gPb.AlignHorz := ahClient;

  lb := CriaCxLabel( 'lbNroRegistros' );
  lb.Properties.Alignment.Vert := taBottomJustify;

  FormataLayoutItem( gPb.CreateItemForControl( lb ), '', ahClient, avParentManaged, false );
  FormataLayoutItem( gPb.CreateItemForControl( pb ), '', ahClient, avParentManaged, False );

  TFormPattern.New
    .SetStandard( pb )
    .SetStandard( btnLoadMore )
    .SetStandard( btnLoadAll );
end;

function TCreatePaginator.MultiplePagesPaginator(
  Group: TdxLayoutGroup): ICreatePaginator;
var
  g : TdxLayoutGroup;
begin
  Result := Self;
  bLoadMore := False;

  btnAnterior         := CriaCxButton( 'btnAnterior', 'Anterior' );
  btnAnterior.OnClick := OnClick;
  btnProximo          := CriaCxButton( 'btnProximo', 'Pr�ximo' );
  btnProximo.OnClick  := OnClick;

  cbb := CriaCxImageComboBox( 'cbbPagina', 100 );
  pb  := CriaCxProgressBar( 'pbPaginacao' );
  lb  := CriaCxLabel( 'lbRegistros' );
  FormataLayoutItem( Group.CreateItemForControl( pb ), '', ahClient, avParentManaged, False );
  FormataLayoutItem( Group.CreateItemForControl( lb ), '', ahClient, avParentManaged, False );

  g             := Group.CreateGroup;
  g.AlignHorz   := ahClient;
  g.LayoutDirection := ldHorizontal;
  g.AlignVert   := avBottom;
  g.ShowBorder  := False;
  FormataLayoutItem( g.CreateItemForControl( btnAnterior ), '', ahParentManaged, avBottom, False );
  FormataLayoutItem( g.CreateItemForControl( btnProximo ), '', ahParentManaged, avBottom, False );
  FormataLayoutItem( g.CreateItemForControl( cbb ), 'Ir para p�gina', ahParentManaged, avParentManaged );
end;

procedure TCreatePaginator.Navega(PageNo: Integer);
var
  s : String;
  iRegAtual, iLastRegPage : integer;
begin
  if FPagination = nil then
    Exit;
  FPagination.GoToPage( PageNo, FSearchString );
  FPaginaAtual := PageNo;
  s := '';
  if not FSearchString.IsEmpty then
    s :=  ' contendo a palavra "' + FSearchString + '"';

  pb.Position := FPagination.Page -1;
  iRegAtual := ( ( FPagination.Page -1 ) * FPagination.PageSize ) + 1;

  if FMaxReg < FPagination.PageSize then
    pb.Position := 1;

  if bLoadMore then begin
    iLastRegPage := iRegAtual + FPagination.PageSize -1;
    iRegAtual := 1;
  end;


  if iLastRegPage > FMaxReg then
    iLastRegPage := FMaxReg;
  lb.Caption := Format( 'Registros%s: %s at� %s de %s', [ s, iRegAtual.ToString, iLastRegPage.ToString, FMaxReg.ToString ] );
  if bLoadMore then
    lb.Caption := Format( 'Registros: %s de %s', [ iLastRegPage.ToString, FMaxReg.ToString ] );

  if FMaxReg = 0 then
    lb.Caption := '';
end;

class function TCreatePaginator.New(Owner: TForm): ICreatePaginator;
begin
  Result := Create( Owner );
end;

procedure TCreatePaginator.OnCbbPropertiesChange(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  try
    cbb.Enabled := False;
    try
      Navega( StrToInt( VarToStrDef( cbb.EditValue, '0' ) ) );
    finally
      cbb.Enabled := True;
    end;
    btnAnterior.Enabled := cbb.ItemIndex > 0;
    btnProximo.Enabled  := cbb.ItemIndex < cbb.Properties.Items.Count -1;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TCreatePaginator.OnClick(Sender: TObject);
begin
  inherited;
  if bLoadMore then begin
    TView.PoeCursor( Self.FOwner,
      procedure
      begin
        Inc( FPaginaAtual );
        Navega( FPaginaAtual );
      end
    );
    btnLoadMore.Enabled := not ( FPaginaAtual = FPag );
    btnLoadAll.Enabled  := not ( FPaginaAtual = FPag );
  end
  else begin
    if Sender = btnAnterior then begin
      if cbb.ItemIndex > 0 then
        cbb.ItemIndex := cbb.ItemIndex - 1
    end
    else begin
      if cbb.ItemIndex < cbb.Properties.Items.Count -1 then
        cbb.ItemIndex := cbb.ItemIndex + 1;
    end;
  end;
end;

procedure TCreatePaginator.OnLoadAllClick(Sender: TObject);
var
  thd : TThread;
begin
  if TMsgQuestion.New( 'Deseja carregar TODOS os registros?' + #13#10 +'A opera��o pode demorar alguns instantes...' ).Show.Yes then begin
    FAbortLoadAll := False;
    if not Assigned( FThreadList ) then
      FThreadList := TList<TThread>.New;
    thd := TThread.CreateAnonymousThread(
      procedure
      begin
        TThread.Synchronize( TThread.CurrentThread,
          procedure
          begin
            Screen.Cursor := crHourGlass;
            btnLoadAll.Enabled  := False;
            btnLoadMore.Enabled := False;
          end
        );
        try
          while ( FPaginaAtual < FPag ) and (not FAbortLoadAll) do begin
            Inc( FPaginaAtual );

            TThread.Synchronize( TThread.CurrentThread,
              procedure
              begin
                Navega( FPaginaAtual );
                Application.ProcessMessages;

              end
            );
          end;
          if FAbortLoadAll then
            TThread.CurrentThread.Terminate;
        finally
          TThread.Synchronize( TThread.CurrentThread,
          procedure
          begin
            Screen.Cursor := crDefault;
          end );
        end;
      end
    );
    thd.Start;
    FThreadList.Add( thd );
  end;
end;

function TCreatePaginator.PegaMaxRegistros: Integer;
begin
  if Assigned( FCustomRecFunc ) then
    Result := FCustomRecFunc( FSearchString, FSearch )
  else if FUseIdx then
    Result := FDao.IndexedRecordCount( FSearchString, FSearch )
  else
    Result := FDao.RecordCount( FSearchString, FSearch );
end;

procedure TCreatePaginator.RedoSearch;
var
  i : Integer;
  r : Integer;
begin

  if not Assigned( FPaginatorProc ) then
    raise Exception.Create('TCreatePaginator@L106 - Paginator procedure not set!');
  FAbortLoadAll := True;
  //Pega os dados de busca...
  FPaginatorProc( FRefresh, FSearch, FSearchString, FDao );
  //Cria o IPagination
  r := TCfgFactory.GetInstance.ConfiguracaoSistema.ResultadosPorPesquisa;
  if r <= 0 then
    r := 20;
  FPagination := TPagination.New( FDao, r, FRefresh, FSearch );
  if Assigned( FCustomNavFunc ) then
    FPagination.UseCustomPaginationNavigateFunction( FCustomNavFunc );
  if Assigned( FCustomRecFunc ) then
    FPagination.UseCustomRecordCountFunction( FCustomRecFunc );
  FPagination.UseIndexedSearch( FUseIdx );

  FPag := FPagination.Count( FSearchString );
  FMaxReg := PegaMaxRegistros;
  pb.Properties.Max := FPag -1;
  if not bLoadMore then begin
    cbb.Properties.BeginUpdate;
    try
      cbb.Properties.OnChange := nil;
      cbb.ItemIndex := -1;
      cbb.Properties.Items.Clear;
      for i := 1 to FPag do begin
        with cbb.Properties.Items.Add do begin
          Description := i.ToString;
          Value       := i;
        end;
      end;

      cbb.ItemIndex := 0;
    finally
      cbb.Properties.EndUpdate;
      cbb.Properties.OnChange := OnCbbPropertiesChange;
    end;
    OnCbbPropertiesChange( nil );
  end
  else begin
    if {FPag > 0 } true then begin // quando pesquisa e n�o tem resultados, precisa limpar os registros, por isso comentei a condi��o
      FPaginaAtual := 0;
      btnLoadMore.Click;
    end;
  end;
end;

function TCreatePaginator.SetCustomNavigateFunction(
  CustomNav: TCustomFindAllFunction): ICreatePaginator;
begin
  Result := Self;
  FCustomNavFunc := CustomNav;
end;

function TCreatePaginator.SetCustomRecordCountFunction(
  CustomRec: TCustomRecordCountFunction): ICreatePaginator;
begin
  Result := Self;
  FCustomRecFunc := CustomRec;
end;

function TCreatePaginator.SetOnPaginatorRefresh(
  Proc: TPaginatorProc): ICreatePaginator;
begin
  Result := Self;
  FPaginatorProc := Proc;
end;

function TCreatePaginator.SetUseIndexedSearch(
  AUseIdx: boolean): ICreatePaginator;
begin
  Result := Self;
  FUseIdx := AUseIdx;
end;

end.
