unit View.CreateControls.Base;

interface

uses
  dxLayoutContainer, dxLayoutControl, cxClasses, dxLayoutLookAndFeels, View.CreateControls.Interfaces, System.SysUtils, cxSpinEdit, cxTL, cxLookAndFeelPainters,
  cxCalc, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit, cxDropDownEdit, Vcl.Forms, cxCheckListBox, cxImageComboBox, Model.Enums, cxMemo, cxButtons,
  Cfg.Constantes, cxProgressBar, cxCheckBox, cxLabel, System.Classes, cxDateUtils, cxCalendar, cxCheckComboBox,
  Util.ConsultaLocalizacao.Interfaces, Model.Endereco.Interfaces, Windows, ExtCtrls, Variants, StdCtrls, VCL.Controls, VCL.Dialogs;

type
  TCreateControls = class( TInterfacedObject, ICreateControls )
    procedure CbbUFPropertiesChange( Sender : TObject );
  private
    FOwner: TForm;
    function CriaCxButtonEdit( Name: string; Width: integer ): TcxButtonEdit;
    function CriaCxTextEdit( Name: string; Width: integer ): TcxTextEdit;
    function CriaCxComboBox( Name: string; Width: integer ): TcxComboBox;
    function CriaCxCheckListBox( Name : string; Width : integer ) : TcxCheckListBox;
    function CriaCxSpinEdit( Name: string; Width: integer ): TcxSpinEdit;
    function CriaCxCalcEdit( Name: string; Width: integer ): TcxCalcEdit;
    function CriaCxMemo( Name: string; Width, Height: integer ): TcxMemo;
    function CriaCxButton( Name, Caption: string; Width: integer = 87; Height: integer = 39 ): TcxButton;
    function CriaCxCheckBox( Name, Caption : string; Width : Integer ) : TcxCheckBox;
    function CriaCxLabel( Name, Caption : string ) : TcxLabel;
    function CriaCxDate( Name: string ): TcxDateEdit;
    function CriaCxCheckComboBox( Name: String; Width: integer ): TcxCheckComboBox;
    procedure FormatLayoutItem( Item: TdxLayoutItem; Caption: string; CaptionLayout: TdxCaptionLayout; Align: TdxLayoutAlignHorz; ItemName : string = '' ); overload;
    procedure FormatLayoutItem( Item: TdxLayoutItem; AlignHorz: TdxLayoutALignHorz; AlignVert: TdxLayoutAlignVert; ItemName : string = '' ); overload;
    procedure FormatLayoutItem( Item: TdxLayoutItem; Caption : string; CaptionLayout : TdxCaptionLayout; AlignHorz : TdxLayoutAlignHorz; AlignVert : TdxLayoutAlignVert; ItemName : string = '' ); overload;
    function CriaCxImageComboBox(Name: string; Width: integer): TcxImageComboBox;
    procedure PopulaComboPais( Cbb : TcxImageComboBox );
  public
    constructor Create( AOwner: TForm );
    destructor Destroy;
    class function New( AOwner: TForm ): ICreateControls;
    function Endereco( Group: TdxLayoutGroup ): ICreateControls; overload;
    function Endereco( Group: TdxLayoutGroup; Prefixo: string ): ICreateControls; overload;
    function Endereco( Group: TdxLayoutGroup; Prefixo: string; Criar: TCreateControlsEnderecoItems ): ICreateControls; overload;
    function Empresas( Group: TdxLayoutGroup ) : ICreateControls; overload;
    function LctoFinanceiroDetalhes( Group: TdxLayoutGroup ): ICreateControls;
    function ConfiguracoesJuros( Group : TdxLayoutGroup; OnChange : TNotifyEvent = nil ) : ICreateControls; overload;
    function ConfiguracoesJuros( Group : TdxLayoutGroup; Prefixo: string; OnChange : TNotifyEvent = nil ) : ICreateControls; overload;
  end;

implementation

{ TCreateControls }

uses Exceptions, Util.ConsultaLocalizacao.Base, Json.Interfaces,
  View.AtualizaUf;


constructor TCreateControls.Create(AOwner: TForm);
begin
  inherited Create;
  FOwner := AOwner;
end;

function TCreateControls.CriaCxButton(Name, Caption: string; Width,
  Height: integer): TcxButton;
begin
  Result         := TcxButton.Create( FOwner );
  Result.Parent  := FOwner;
  REsult.Width   := Width;
  Result.Height  := Height;
  Result.Name    := Name;
  Result.Caption := Caption;
end;

function TCreateControls.CriaCxButtonEdit(Name: string;
  Width: integer): TcxButtonEdit;
var
  i: Integer;
begin
  Result := TcxButtonEdit.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width := Width;
  Result.Name := Name;
  Result.Text := '';
  for i := 0 to Result.Properties.Buttons.Count - 1 do
    Result.Properties.Buttons.Items[ i ].Kind := bkDown;
end;

function TCreateControls.CriaCxCalcEdit(Name: string;
  Width: integer): TcxCalcEdit;
begin
  Result := TcxCalcEdit.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width := Width;
  Result.Name := Name;
end;

function TCreateControls.CriaCxCheckBox(Name, Caption: string;
  Width: Integer): TcxCheckBox;
begin
  Result := TcxCheckBox.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width := Width;
  Result.Name := Name;
  Result.Caption := Caption;
  Result.Transparent := True;
end;

function TCreateControls.CriaCxCheckComboBox(Name: String;
  Width: integer): TcxCheckComboBox;
begin
  Result := TcxCheckComboBox.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width := Width;
  Result.Name := Name;
end;

function TCreateControls.CriaCxCheckListBox(Name: string;
  Width: integer): TcxCheckListBox;
begin
  Result := TcxCheckListBox.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width := Width;
  Result.Name := Name;
end;

function TCreateControls.CriaCxComboBox(Name: string;
  Width: integer): TcxComboBox;
begin
  Result := TcxComboBox.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width := Width;
  Result.Name := Name;
end;

function TCreateControls.CriaCxDate(Name: string): TcxDateEdit;
begin
  Result        := TcxDateEdit.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width  := 100;
  Result.Name   := Name;
end;

function TCreateControls.CriaCxImageComboBox( Name: string; Width: integer ) : TcxImageComboBox;
begin
  Result        := TcxImageComboBox.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width  := Width;
  Result.Name   := Name;
end;

function TCreateControls.CriaCxLabel(Name, Caption: string): TcxLabel;
begin
  Result := TcxLabel.Create( FOwner );
  Result.Parent := FOwner;
  Result.Caption := Caption;
  Result.Name := Name;
  Result.Transparent := True;
end;

function TCreateControls.CriaCxMemo(Name: string; Width,
  Height: integer): TcxMemo;
begin
  Result := TcxMemo.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width := Width;
  Result.Height := Height;
  Result.Name := Name;
  Result.Lines.Clear;
end;

function TCreateControls.CriaCxSpinEdit(Name: string;
  Width: integer): TcxSpinEdit;
begin
  Result := TcxSpinEdit.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width := Width;
  Result.Name := Name;
  Result.Value := 0;
end;

function TCreateControls.CriaCxTextEdit(Name: string;
  Width: integer): TcxTextEdit;
begin
  Result        := TcxTextEdit.Create( FOwner );
  Result.Parent := FOwner;
  Result.Width  := Width;
  Result.Name   := Name;
  Result.Text   := '';
  Result.Clear;
end;

destructor TCreateControls.Destroy;
begin
  FOwner := nil;
  inherited;
end;

function TCreateControls.Empresas(Group: TdxLayoutGroup): ICreateControls;
var
  Item: TdxLayoutItem;
begin
  Result := Self;
  Item := Group.CreateItemForControl( CriaCxCheckListBox( 'lstEmpresas', 100 ) );
  FormatLayoutItem( Item, '', clTop, ahClient );
  Item.AlignVert := avClient;
end;

function TCreateControls.Endereco(Group: TdxLayoutGroup; Prefixo: string;
  Criar: TCreateControlsEnderecoItems): ICreateControls;
var
  g: TdxLayoutGroup;
  cbbUF, cbbPaises, cbbCidade : TcxImageComboBox;
  edtCEP : TcxButtonEdit;
  edtLogradouro, edtNumero, edtComplemento, edtBairro : TcxTextEdit;


  function CriaGrupo: TdxLayoutGroup;
  begin
    Result := Group.CreateGroup;
    Result.AlignHorz := ahClient;
    Result.AlignVert := avParentManaged;
    Result.ShowBorder := false;
    Result.LayoutDirection := ldHorizontal;
  end;

begin
  Result := Self;
  if Criar = [ cceLogradouro, cceCidade, cceUF ] then begin
    { Logradouro | UF | Cidade | Pais }
    g := CriaGrupo;
    FormatLayoutItem( g.CreateItemForControl( CriaCxTextEdit( 'edt' + Prefixo + 'Logradouro', 100 ) ), 'Endere�o', clTop, ahClient );
    cbbUF := CriaCxImageComboBox( 'cbb' + Prefixo + 'UF', 60 );
    TUF.FillCbb( cbbUF );
    cbbUF.Hint := 'Selecione a UF';
    cbbUF.Properties.OnChange := CbbUFPropertiesChange;
    FormatLayoutItem( g.CreateItemForControl( cbbUf ), 'UF', clTop, ahLeft, Prefixo + 'UF' );
    cbbCidade := CriaCxImageComboBox( 'cbb' + Prefixo + 'Cidade', 100 );
    cbbCidade.Hint := 'Selecione a cidade';
    FormatLayoutItem( g.CreateItemForControl( cbbCidade ), 'Cidade', clTop, ahClient, Prefixo + 'Cidade' );
//    cbbPaises := CriaCxImageComboBox( 'cbb' + Prefixo + 'Pais', 100 );
//    PopulaComboPais( cbbPaises );
//    FormatLayoutItem( g.CreateItemForControl( cbbPaises ), 'Pa�s', clTop, ahClient, Prefixo + 'Pais' );
  end
  else begin
    {
      CEP | Endereco | Numero | Bairro
      Complemento | UF | Cidade | Pais
    }
    g := CriaGrupo;
    edtCep := CriaCxButtonEdit( 'edt' + Prefixo + 'CEP', 100 );
    edtCep.Hint := 'Informe o CEP para realizar a busca autom�tica do endere�o';
    edtCep.Properties.MaskKind := emkRegExprEx;
    edtCep.Properties.EditMask := TAppConstantes.MascaraCEP;
    edtLogradouro := CriaCxTextEdit( 'edt' + Prefixo + 'Logradouro', 100 );
    edtLogradouro.Hint := 'Informe o Logradouro (Endere�o)';
    edtNumero := CriaCxTextEdit( 'edt' + Prefixo + 'Numero', 80 );
    edtNumero.Hint := 'Informe o N�mero';
    edtBairro := CriaCxTextEdit( 'edt' + Prefixo + 'Bairro', 80 );
    edtBairro.Hint := 'Informe o Bairro';
    FormatLayoutItem( g.CreateItemForControl( edtCEP ), 'CEP', clTop, ahLeft );
    FormatLayoutItem( g.CreateItemForControl( edtLogradouro  ), 'Endere�o', clTop, ahClient );
    FormatLayoutItem( g.CreateItemForControl( edtNumero ), 'N�mero', clTop, ahLeft );
    FormatLayoutItem( g.CreateItemForControl( edtBairro ), 'Bairro', clTop, ahClient );

    //g := CriaGrupo;

    g := CriaGrupo;
    edtComplemento := CriaCxTextEdit( 'edt' + Prefixo + 'Complemento', 100 );
    edtComplemento.Hint := 'Informe o Complemento';
    cbbUF := CriaCxImageComboBox( 'cbb' + Prefixo + 'UF', 60 );
    TUF.FillCbb( cbbUF );
    cbbUF.Properties.OnChange := CbbUFPropertiesChange;
    cbbUF.Hint := 'Selecione a UF';
    cbbCidade := CriaCxImageComboBox( 'cbb' + Prefixo + 'Cidade', 100 );
    cbbCidade.Hint := 'Selecione a cidade';
    FormatLayoutItem( g.CreateItemForControl( cbbUf ), 'UF', clTop, ahLeft, Prefixo + 'UF' );
    FormatLayoutItem( g.CreateItemForControl( cbbCidade ), 'Cidade', clTop, ahClient, Prefixo + 'Cidade' );
    FormatLayoutItem( g.CreateItemForControl( edtComplemento ), 'Complemento', clTop, ahClient );


    cbbPaises := CriaCxImageComboBox( 'cbb' + Prefixo + 'Pais', 100 );
    PopulaComboPais( cbbPaises );
    FormatLayoutItem( g.CreateItemForControl( cbbPaises ), 'Pa�s', clTop, ahClient, Prefixo + 'Pais' );
    cbbPaises.Hint := 'Selecione o Pa�s';
  end;
end;

function TCreateControls.ConfiguracoesJuros( Group: TdxLayoutGroup; OnChange : TNotifyEvent): ICreateControls;
begin
  Result := Self.ConfiguracoesJuros( Group, '', OnChange );
end;

function TCreateControls.Endereco(Group: TdxLayoutGroup;
  Prefixo: string): ICreateControls;
begin
  Result := Self.Endereco( Group, Prefixo, [] );
end;

procedure TCreateControls.CbbUFPropertiesChange(Sender: TObject);
var
  cbbCidade, cbbPais, cbbUF : TcxImageComboBox;
  iPais, iCidade, iUF : TdxLayoutItem;
  LC : TdxLayoutControl;
  sPrefixo : string;
  thd : ThreadAtualizaUF;
begin
  inherited;
  if not Assigned( FOwner ) then
    raise Exception.Create('$CreateEndereco#OwnerIsNull - Set Global Variable of ICreateControls to resolve');

  cbbUF := TcxImageComboBox( Sender );

  sPrefixo := cbbUF.Name;
  Delete( sPrefixo, 1, 3 );
  sPrefixo := Copy( sPrefixo, 1, Pos( 'UF', sPrefixo ) -1 );
  cbbPais   := TcxImageComboBox( FOwner.FindComponent( 'cbb' + sPrefixo + 'Pais' ) );
  cbbCidade := TcxImageComboBox( FOwner.FindComponent( 'cbb' + sPrefixo + 'Cidade' ) );


  if ( not Assigned( cbbCidade ) ) then
    Exit;
  iUF     := TdxLayoutItem( Fowner.FindComponent( 'lci' + sPrefixo + 'UF' ) );
  iCidade := TdxLayoutItem( FOwner.FindComponent( 'lci' + sPrefixo + 'Cidade' ) );
  LC := TdxLayoutControl( FOwner.FindComponent( 'LCBaseGeral' ) );
  if LC = nil then
    LC := TdxLayoutControl( FOwner.FindComponent( 'LCBaseModal' ) );
  if LC = nil then
    LC := TdxLayoutControl( FOwner.FindComponent( 'LCBaseTelaCheia' ) );

  iCidade.Visible := cbbUF.EditValue <> 'EX';
//  if cbbUF.EditValue = '' then
//    Exit;

  if Assigned( cbbPais ) then begin
    iPais := TdxLayoutItem( FOwner.FindComponent( 'lci' + sPrefixo + 'Pais' ) );
    iPais.Visible := cbbUF.EditValue = 'EX';
  end;


  if iCidade.Visible then begin
    cbbUF.Tag := 1;
    thd := ThreadAtualizaUF.Create( True );
    thd.CbbUF   := cbbUF;
    thd.LciUF   := iUF;
    thd.CbbMun  := cbbCidade;
    thd.LciMun  := iCidade;
    thd.LC      := LC;
    thd.Foco    := FOwner.ActiveControl;

    thd.Start;
    //PopulaComboMunicipio( cbbCidade, TUF.FromCbb( cbbUF ) );
  end;
end;

function TCreateControls.ConfiguracoesJuros(Group: TdxLayoutGroup;
  Prefixo: string; OnChange: TNotifyEvent): ICreateControls;
var
  g : TdxLayoutGroup;
  cbb : TcxImageComboBox;
  cbox, cboxHabilitado : TcxCheckBox;
  calc : TcxCalcEdit;
  sp : TcxSpinEdit;
  function CriaGrupo : TdxLayoutGroup;
  begin
    Result := Group.CreateGroup;
    Result.AlignHorz := ahClient;
    Result.AlignVert := avParentManaged;
    Result.ShowBorder := False;
    Result.LayoutDirection := ldHorizontal;
  end;
begin
  Result := Self;
  g := CriaGrupo;
  cboxHabilitado := CriaCxCheckBox( 'cbox' + Prefixo + 'Habilitado', 'Habilitar esta Configura��o', 100 );
  cboxHabilitado.Properties.Multiline := False;
  cboxHabilitado.Autosize := True;
  {cboxHabilitado.Properties.OnChange := TAnonymousNotifyEvent.New( cboxHabilitado,
    procedure( Sender : TObject )
      procedure DisableChildren( lcg : TdxLayoutGroup );
      var
        i : Integer;
      begin
        for i := 0 to lcg.Count -1 do begin
          if lcg.Items[ i ].ClassType = TdxLayoutGroup then
            DisableChildren( TdxLayoutGroup( lcg.Items[ i ] ) )
          else begin
            if TdxLayoutItem( lcg.Items[ i ] ).Control <> Sender then
              TdxLayoutItem( lcg.Items[ i ] ).Enabled := TcxCheckBox( Sender ).Checked;
          end;
        end;
      end;
    begin
      DisableChildren( Group );
    end
  );    }
  FormatLayoutItem( g.CreateItemForControl( cboxHabilitado ), ahParentManaged, avBottom );
  cboxHabilitado.Hint := 'Habilite as configura��es de Juros e Multas para esse Cliente';
  g := CriaGrupo;
  cbb := CriaCxImageComboBox( 'cbb' + Prefixo + 'TipoJuros', 150 );
  TJurosTipo.FillCbb( cbb );
  cbb.Properties.OnChange := OnChange;
  cbb.Hint := 'Selecione o tipo de Juros utilizado para este Cliente. Podendo ser Juros Simples ou Juros Composto';


  cbox := CriaCxCheckBox( 'cbox' + Prefixo + 'IncluiCarenciaJuros', 'Incluir car�ncia no c�lculo de Juros', 100 );
  cbox.Properties.MultiLine := false;
  cbox.AutoSize := true;
  cbox.Properties.OnChange := OnChange;


  calc := CriaCxCalcedit( 'edt' + Prefixo + 'JuroMensal', 100 );
  calc.Properties.DisplayFormat := TAppConstantes.Percentual;
  calc.Properties.OnChange := OnChange;
  calc.Hint := 'Preencha a porcentagem de Juro Mensal para este Cliente';

  sp := CriaCxSpinEdit( 'edt' + Prefixo + 'DiasCarenciaJuros', 100 );
  sp.Properties.OnChange := OnChange;
  sp.Hint := 'Selecione os dias de car�ncia para a cobran�a de Juros';

  FormatLayoutItem( g.CreateItemForControl( calc ), 'Juro Mensal (%)', clTop, ahParentManaged );
  FormatLayoutItem( g.CreateItemForControl( cbb ), 'Tipo de Juros', clTop, ahParentManaged );
  FormatLayoutItem( g.CreateItemForControl( sp ), 'Dias de car�ncia na cobran�a de Juros', clTop, ahParentManaged );
  FormatLayoutItem( g.CreateItemForControl( cbox ), ahParentManaged, avBottom );

  g := CriaGrupo;
  calc := CriaCxCalcEdit( 'edt' + Prefixo + 'Multa', 100 );
  calc.Properties.DisplayFormat := TAppConstantes.Percentual;
  calc.Properties.OnChange := OnChange;
  calc.Hint := 'Preencha a porcentagem de Multa para este Cliente';

  sp := CriaCxSpinEdit( 'edt' + Prefixo + 'DiasCarenciaMulta', 100 );
  sp.Properties.OnChange := OnChange;
  sp.Hint := 'Selecione os dias de car�ncia para a cobran�a de Multa';

  FormatLayoutItem( g.CreateItemForControl( calc ), 'Multa (%)', clTop, ahParentManaged );
  FormatLayoutItem( g.CreateItemForControl( sp ), 'Cobrar multa ap�s (dias)', clTop, ahParentManaged );

  //g := CriaGrupo;
  calc := CriaCxCalcEdit( 'edt' + Prefixo + 'Desconto', 100 );
  calc.Properties.DisplayFormat := TAppConstantes.Percentual;
  calc.Properties.OnChange := OnChange;
  calc.Hint := 'Preencha qual a Porcentagem de Desconto deste Cliente';
  sp := CriaCxSpinEdit( 'edt' + Prefixo + 'DescontoAte', 100 );
  sp.Properties.OnChange := OnChange;
  sp.Hint := 'Selecione at� quantos dias antes do vencimento o desconto ser� aplicado';

  FormatLayoutItem( g.CreateItemForControl( calc ), 'Desconto (%)', clTop, ahParentManaged );
  FormatLayoutItem( g.CreateItemForControl( sp ), 'Aplicar desconto at�', clLeft, ahParentManaged, avBottom );
  FormatLayoutItem( g.CreateItemForControl( CriaCxLabel( 'lb' + Prefixo + 'DescontoAte', 'dias antes do vencimento' ) ), ahParentManaged, avBottom );

end;

procedure TCreateControls.FormatLayoutItem(Item: TdxLayoutItem;
  AlignHorz: TdxLayoutALignHorz; AlignVert: TdxLayoutAlignVert; ItemName : string = '');
begin
  Item.CaptionOptions.Visible := false;
  Item.AlignHorz := AlignHorz;
  Item.AlignVert := AlignVert;
  if ItemName <> '' then
    Item.Name := 'lci' + ItemName;
end;

procedure TCreateControls.FormatLayoutItem(Item: TdxLayoutItem;
  Caption: string; CaptionLayout: TdxCaptionLayout;
  AlignHorz: TdxLayoutAlignHorz; AlignVert: TdxLayoutAlignVert; ItemName : string = '');
begin
  with Item.CaptionOptions do begin
    Text := Caption;
    Layout := CaptionLayout;
  end;
  Item.AlignHorz := AlignHorz;
  Item.AlignVert := AlignVert;
  if ItemName <> '' then
    Item.Name := 'lci' + ItemName;
end;

procedure TCreateControls.FormatLayoutItem(Item: TdxLayoutItem;
  Caption: string; CaptionLayout: TdxCaptionLayout;
  Align: TdxLayoutAlignHorz; ItemName : string = '');
begin
  Item.Caption := Caption;
  Item.CaptionOptions.Layout := CaptionLayout;
  Item.AlignHorz := Align;
  if ItemName <> '' then
    Item.Name := 'lci' + ItemName;
end;

function TCreateControls.LctoFinanceiroDetalhes(Group: TdxLayoutGroup): ICreateControls;
var
  g: TdxLayoutGroup;
  Grid: TcxTreeList;
  Total: TcxCalcEdit;
  Item: TdxLayoutItem;
begin
  Result := Self;
  Group.CaptionOptions.Text := 'Detalhamento do lan�amento';
  g := Group.CreateGroup;
  g.ShowBorder := false;
  g.AlignHorz := ahClient;
  g.LayoutDirection := ldHorizontal;
  FormatLayoutItem(
    g.CreateItemForControl(
      CriaCxButtonEdit( 'edtContaCaixa', 100 )
    ),
    'Conta Caixa',
    clTop,
    ahClient
  );
  FormatLayoutItem(
    g.CreateItemForControl(
      CriaCxButtonEdit( 'edtCentroCusto', 100 )
    ),
    'Centro de Custo',
    clTop,
    ahClient
  );
  FormatLayoutItem(
    g.CreateItemForControl(
      CriaCxCalcEdit( 'edtValor', 100 )
    ),
    'Valor',
    clTop,
    ahParentManaged
  );
  FormatLayoutItem(
    g.CreateItemForControl( CriaCxButton( 'btnIncluir', 'Incluir' ) ),
    ahParentManaged,
    avBottom
  );

  Grid := TcxTreeList.Create( FOwner );
  Grid.Parent := FOwner;
  Grid.Name := 'GridDetalhes';
  Item := Group.CreateItemForControl( Grid );
  FormatLayoutItem(
    Item,
    'Detalhes',
    clTop,
    ahClient
  );
  Item.AlignVert := avClient;

  g := Group.CreateGroup;
  g.ShowBorder := False;
  g.AlignHorz := ahClient;
  g.LayoutDirection := ldHorizontal;
  FormatLayoutItem(
    g.CreateItemForControl( CriaCxButton( 'btnEditar', 'Editar' ) ),
    ahLeft,
    avBottom
  );
  FormatLayoutItem(
    g.CreateItemForControl( CriaCxButton( 'btnExcluir', 'Excluir' ) ),
    ahLeft,
    avBottom
  );
  Total := CriaCxCalcEdit( 'edtValorTotal', 100 );
  Total.TabStop := false;
  Total.Properties.ReadOnly := true;
  FormatLayoutItem(
    g.CreateItemForControl( Total ),
    'Valor Total',
    clTop,
    ahLeft
  );
end;

function TCreateControls.Endereco(Group: TdxLayoutGroup): ICreateControls;
begin
  Result := Self.Endereco( Group, '' );
end;

class function TCreateControls.New(AOwner: TForm): ICreateControls;
begin
  Result := TCreateControls.Create( AOwner );
end;

procedure TCreateControls.PopulaComboPais(Cbb: TcxImageComboBox);
var
  busca : IConsultaPaises;
  paises : TArray<IPaisIBGE>;
  i, l : Integer;
  p : IPaisIBGE;
begin
  busca   := TConsultaPaises.New;
  paises  := busca.Paises;
  cbb.Properties.BeginUpdate;
  try
    cbb.Properties.Items.Clear;
    l := Length( Paises );
    for i := 0 to l -1 do begin
      p := paises[ i ];
      with cbb.Properties.Items.Add do begin
        Description := p.Nome;
        Value       := p.Nome;
      end;
    end;
  finally
    cbb.Properties.EndUpdate;
  end;
end;

end.
