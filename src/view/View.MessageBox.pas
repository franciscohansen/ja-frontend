unit View.MessageBox;

interface

uses
  Winapi.Windows, Vcl.Forms, Vcl.Dialogs, Vcl.Controls, Vcl.Graphics, Vcl.StdCtrls, StrUtils, SysUtils;

type
  IUserMessage = interface
    ['{5872AEF2-5510-41A9-B811-8F4149E69D12}']
    function Show: IUserMessage;
    function Yes: boolean;
    function No: boolean;
    function Cancel: boolean;
  end;
  ICustomMessage = interface ( IUserMessage )
    function YesCaption( Caption : String ) : ICustomMessage;
    function NoCaption( Caption : string ) : ICustomMessage;
    function CancelCaption( Caption : string ) : ICustomMessage;
  end;

  TMessageBox = class( TAggregatedObject, IUserMessage )
  private
    FMsg: PWideChar;
    FBotoes: integer;
    FResult : integer;
  public
    constructor Create( Controller: IInterface; Msg: string; Botoes: integer );
    destructor Destroy; override;
    function Show: IUserMessage;
    function Yes: boolean;
    function No: boolean;
    function Cancel: boolean;
  end;

  TMsgQuestion = class( TInterfacedObject, IUserMessage )
  private
    FUserMessage: TMessageBox;
    constructor Create( Msg: String );
  published
    class function New( Msg: string ): IUserMessage;
    destructor Destroy; override;
    property UserMessage: TMessageBox read FUserMessage implements IUserMessage;
  end;

  TMsgWarning = class( TInterfacedObject, IUserMessage )
  private
    FUserMessage: TMessageBox;
    constructor Create( Msg: String; ApenasBotaoDeOk : boolean );
  published
    class function New( Msg: string; ApenasBotaoDeOk : boolean = True ): IUserMessage;
    destructor Destroy; override;
    property UserMessage: TMessageBox read FUserMessage implements IUserMessage;
  end;

  TMsgInformation = class( TInterfacedObject, IUserMessage )
  private
    FUserMessage: TMessageBox;
    constructor Create( Msg: String );
  published
    class function New( Msg: string ): IUserMessage;
    destructor Destroy; override;
    property UserMessage: TMessageBox read FUserMessage implements IUserMessage;
  end;

  TMsgError = class( TInterfacedObject, IUserMessage )
  private
    FUserMessage: TMessageBox;
    constructor Create( Msg: String );
  published
    class function New( Msg: string ): IUserMessage;
    destructor Destroy; override;
    property UserMessage: TMessageBox read FUserMessage implements IUserMessage;
  end;
  TCustomMessage = class ( TInterfacedObject, ICustomMessage, IUserMessage )
  private
    FMsg : string;
    FType : TMsgDlgType;
    FButtons : TMsgDlgButtons;
    FYesCaption, FNoCaption, FCancelCaption : string;
    FYesWidth, FNoWidth, FCancelWidth : Integer;
    FResult : Integer;
    constructor Create( Msg : string; MsgType : TMsgDlgType; Buttons : TMsgDlgButtons  );

  public
    class function New( Msg : String; MsgType : TMsgDlgType; Buttons : TMsgDlgButtons ) : ICustomMessage;
    function Show: IUserMessage;
    function Yes: boolean;
    function No: boolean;
    function Cancel: boolean;
    function YesCaption( Caption : String ) : ICustomMessage;
    function NoCaption( Caption : string ) : ICustomMessage;
    function CancelCaption( Caption : string ) : ICustomMessage;
    
  end;

implementation

{ TMessageBox }

uses untMessageBox;

function TMessageBox.Cancel: boolean;
begin
  Result := FResult = IDCANCEL;
end;

constructor TMessageBox.Create(Controller: IInterface; Msg: string; Botoes: integer);
begin
  inherited Create( Controller );
  FMsg := PWideChar( Msg );
  FBotoes := Botoes;
end;

destructor TMessageBox.Destroy;
begin
  inherited Destroy;
end;

function TMessageBox.No: boolean;
begin
  Result := FResult = IDNO;
end;

function TMessageBox.Show: IUserMessage;
var
  frm: TfrmMessageBox;
begin
  Result := Self;
  frm := TfrmMessageBox.Create( Application, FMsg, FBotoes );
  try
    frm.ShowModal;
    FResult := frm.Retorno; //Application.MessageBox( FMsg, 'H�bil', FBotoes );
  finally
    frm.Free;
  end;
end;

function TMessageBox.Yes: boolean;
begin
  Result := FResult = IDYES;
end;

{ TUserQuestion }

constructor TMsgQuestion.Create(Msg: String);
begin
  inherited Create;
  FUserMessage := TMessageBox.Create( Self, Msg, MB_ICONQUESTION + MB_YESNO );
end;

destructor TMsgQuestion.Destroy;
begin
  FUserMessage.Free;
  inherited Destroy;
end;

class function TMsgQuestion.New(Msg: string): IUserMessage;
begin
  Result := TMsgQuestion.Create( Msg );
end;

{ TUserInformation }

constructor TMsgInformation.Create(Msg: String);
begin
  inherited Create;
  FUserMessage := TMessageBox.Create( Self, Msg, MB_ICONINFORMATION + MB_OK );
end;

destructor TMsgInformation.Destroy;
begin
  FUserMessage.Free;
  inherited Destroy;
end;

class function TMsgInformation.New(Msg: string): IUserMessage;
begin
  Result := TMsgInformation.Create( Msg );
end;

{ TUserError }

constructor TMsgError.Create(Msg: String);
begin
  inherited Create;
  FUserMessage := TMessageBox.Create( Self, Msg, MB_ICONERROR + MB_OK );
end;

destructor TMsgError.Destroy;
begin
  FUserMessage.Free;
  inherited Destroy;
end;

class function TMsgError.New(Msg: string): IUserMessage;
begin
  Result := TMsgError.Create( Msg );
end;

{ TUserWarning }

constructor TMsgWarning.Create(Msg: String; ApenasBotaoDeOk : boolean );
begin
  inherited Create;
  if not ApenasBotaoDeOk then
    FUserMessage := TMessageBox.Create( Self, Msg, MB_ICONWARNING + MB_YESNO + MB_DEFBUTTON2 )
  else
    FUserMessage := TMessageBox.Create( Self, Msg, MB_ICONWARNING + MB_OK );
end;

destructor TMsgWarning.Destroy;
begin
  FUserMessage.Free;
  inherited;
end;

class function TMsgWarning.New(Msg: string; ApenasBotaoDeOk : boolean ): IUserMessage;
begin
  Result := TMsgWarning.Create( Msg, ApenasBotaoDeOk );
end;

{ TCustomMessage }

function TCustomMessage.Cancel: boolean;
begin
  Result := FResult = ID_Cancel;
end;

function TCustomMessage.CancelCaption(Caption: string): ICustomMessage;
begin
  Result := Self;
  FCancelCaption := Caption;
end;

constructor TCustomMessage.Create(Msg: string; MsgType : TMsgDlgType; Buttons : TMsgDlgButtons );
begin
  inherited Create;
  FMsg := Msg;
  FType := MsgType;
  FButtons := Buttons;
  FCancelCaption := 'Cancelar';
  FYesCaption    := 'Sim';
  FNoCaption     := 'N�o';
end;

class function TCustomMessage.New(Msg: String; MsgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): ICustomMessage;
begin
  Result := Create( Msg, MsgType, Buttons );
end;

function TCustomMessage.No: boolean;
begin
  Result := FResult = ID_NO;
end;

function TCustomMessage.NoCaption(Caption: string): ICustomMessage;
begin
  Result := Self;
  FNoCaption := Caption;
end;


function TCustomMessage.Show: IUserMessage;
  function GetTextWidth( AFrm : TForm; AText : string ) : Integer;
  var
    s : String;

  begin
    if AText.Contains( #13#10 ) then begin
      Result := 0;
      for s in AText.Split( [ #13#10 ] ) do begin
        if AFrm.Canvas.TextWidth( s ) > Result then
          Result := AFrm.Canvas.TextWidth( s );
      end;
    end
    else
      Result := AFrm.Canvas.TextWidth( AText );
  end;

var
  frm : TForm;
  I : Integer;
  iYes, iNo, iCancel, iLeft, iWidth : Integer;
begin
  Result := Self;
  try
    iYes := 0; iNo := 0; iLeft := 0; iWidth := 0; iCancel := 0;
    frm := CreateMessageDialog( FMsg, FType, FButtons );
    frm.Caption := 'H�bil';
    for I := 0 to frm.ControlCount -1 do begin
      if frm.Controls[ I ] is TButton then begin
        with TButton( frm.Controls[ i ] ) do begin
          case AnsiIndexStr( Name, [ 'Yes', 'No', 'Cancel' ] ) of
            0: begin
              Caption := FYesCaption;
              iYes := frm.Canvas.TextWidth( FYesCaption ) + 20 ;
              if iYes < Width then
                iYes := Width
              else
                Width := iYes;
              iLeft := Left;
            end;
            1: begin
              Caption := FNoCaption;
              iNo := frm.Canvas.TextWidth( FNoCaption ) + 20 ;
              if iNo > Width then
                Width := iNo
              else
                iNo := Width;
            end;
            2: begin
              Caption := FCancelCaption;
              iCancel := frm.Canvas.TextWidth( FCancelCaption ) + 20;
              if iCancel > Width then
                Width := iCancel
              else
                iCancel := Width;
            end;
          end;
          WordWrap := True;
        end;
      end;
    end;
    TButton( frm.FindComponent( 'No' ) ).Left := iLeft + iYes + 5;
    if iCancel > 0 then
      TButton( frm.FindComponent( 'Cancel' ) ).Left := iLeft + iYes + 5 + iNo + 5;
    iWidth := 0;
    iWidth := iLeft + iYes + 5 + iNo;
    if iCancel > 0 then begin
      iWidth := iWidth + 5 + iCancel;
    end;
    iWidth := iWidth + iLeft;
    if iWidth < ( GetTextWidth( frm, FMsg ) + 10 ) then
      iWidth := GetTextWidth( frm, FMsg ) + 10;
    frm.Width := iWidth;
    FResult := frm.ShowModal
  finally
//    FreeAndNil( frm );
  end;
end;

function TCustomMessage.Yes: boolean;
begin
  Result := FResult = ID_YES;
end;

function TCustomMessage.YesCaption(Caption: String): ICustomMessage;
begin
  Result := Self;
  FYesCaption := Caption;
end;



end.
