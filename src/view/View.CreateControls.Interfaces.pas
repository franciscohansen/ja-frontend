unit View.CreateControls.Interfaces;

interface

uses
  dxLayoutContainer, dxLayoutControl, cxClasses, dxLayoutLookAndFeels, Vcl.Graphics, Dao.Interfaces, System.Classes;

type
  TCreateControlsEnderecoItem = ( cceCEP, cceLogradouro, cceNumero, cceComplemento, cceBairro, cceUF, cceCidade, ccePais );
  TCreateControlsEnderecoItems = set of TCreateControlsEnderecoItem;


  ICreateControls = interface
    ['{1CD640E3-2E93-4268-8865-29E6DC49B567}']
    function Endereco( Group: TdxLayoutGroup ): ICreateControls; overload;
    function Endereco( Group: TdxLayoutGroup; Prefixo: string ): ICreateControls; overload;
    function Endereco( Group: TdxLayoutGroup; Prefixo: string; Criar: TCreateControlsEnderecoItems ): ICreateControls; overload;
    function Empresas( Group: TdxLayoutGroup ) : ICreateControls;
    function LctoFinanceiroDetalhes( Group: TdxLayoutGroup ): ICreateControls;
    function ConfiguracoesJuros( Group : TdxLayoutGroup; OnChange : TNotifyEvent = nil ) : ICreateControls; overload;
    function ConfiguracoesJuros( Group : TdxLayoutGroup; Prefixo: string; OnChange : TNotifyEvent = nil ) : ICreateControls; overload;
  end;

implementation

end.
