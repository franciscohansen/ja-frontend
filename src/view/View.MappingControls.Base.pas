unit View.MappingControls.Base;

interface

uses View.MappingControls.Interfaces, Generics.Collections, System.SysUtils, Vcl.Forms, System.Classes, Vcl.Controls, cxTextEdit, cxEdit, cxButtonEdit,
  cxSpinEdit, cxCalc, cxMemo, Json.Interfaces, Variants, cxDropDownEdit;

type
  TMapCtrls = class( TInterfacedObject, IMapCtrls )
  private
    FCtrl: TDictionary<String, IMapCtrl>;
    FCtrls: TDictionary<String, IMapCtrls>;
  public
    constructor Create;
    destructor Destroy; override;
    class function New: IMapCtrls;
    function Add( JsonProperty: string; MapCtrl: IMapCtrl ): IMapCtrls; overload;
    function Add( JsonProperty: string; MapCtrls: IMapCtrls ): IMapCtrls; overload;
    function MapCtrl( JsonProperty: string ): IMapCtrl;
    function MapCtrls( JsonProperty: string ): IMapCtrls;
    function IsMapCtrl( JsonProperty: string ): boolean;
    function Properties: TArray<String>;
  end;

  TManipulateCtrls = class( TInterfacedObject, IManipulateCtrls )
  private
    FJson: IJson;
    FMap: IMapCtrls;
  public
    constructor Create( MapCtrls: IMapCtrls; Json: IJson );
    class function New( MapCtrls: IMapCtrls; Json: IJson ): IManipulateCtrls;
    function ToJson: IJson;
    procedure ToCtrl( ReadOnly: boolean = false );
  end;



implementation

{ TMapCtrls }

uses Exceptions, Json.Base,  View.MappingControls.Edits, Dao.Interfaces, Dao.Base;

function TMapCtrls.Add(JsonProperty: string;
  MapCtrls: IMapCtrls): IMapCtrls;
begin
  Result := Self;
  FCtrls.Add( JsonProperty, MapCtrls );
end;

function TMapCtrls.Add(JsonProperty: string; MapCtrl: IMapCtrl): IMapCtrls;
begin
  Result := Self;
  FCtrl.Add( JsonProperty, MapCtrl );
end;

constructor TMapCtrls.Create;
begin
  inherited Create;
  FCtrl := TDictionary<String, IMapCtrl>.Create;
  FCtrls := TDictionary<String, IMapCtrls>.Create;
end;

destructor TMapCtrls.Destroy;
begin
  FCtrl.TrimExcess;
  FCtrl.Free;
  FCtrls.TrimExcess;
  FCtrls.Free;
  inherited;
end;

function TMapCtrls.IsMapCtrl(JsonProperty: string): boolean;
begin
  Result := FCtrl.ContainsKey( JsonProperty );
end;

function TMapCtrls.MapCtrl(JsonProperty: string): IMapCtrl;
begin
  Result := FCtrl.Items[ JsonProperty ];
end;

function TMapCtrls.MapCtrls(JsonProperty: string): IMapCtrls;
begin
  Result := FCtrls.Items[ JsonProperty ];
end;

class function TMapCtrls.New: IMapCtrls;
begin
  Result := TMapCtrls.Create;
end;

function TMapCtrls.Properties: TArray<String>;
var
  i, nA, nB: integer;
  A, B: TArray<String>;
begin
  A := FCtrl.Keys.ToArray;
  B := FCtrls.Keys.ToArray;
  nA := length( A );
  nB := length( B );

  SetLength( Result, nA + nB );
  for i := 0 to nA - 1 do
    Result[ i ] := A[ i ];
  for i := 0 to nB - 1 do
    Result[ i + nA ] := B[ i ];
end;

{ TManipulateCtrls }

constructor TManipulateCtrls.Create(MapCtrls: IMapCtrls; Json: IJson);
begin
  inherited Create;
  FMap := MapCtrls;
  FJson := Json;
end;

class function TManipulateCtrls.New(MapCtrls: IMapCtrls; Json: IJson): IManipulateCtrls;
begin
  Result := TManipulateCtrls.Create( MapCtrls, Json );
end;

procedure TManipulateCtrls.ToCtrl( ReadOnly: boolean = false );
var
  s: string;
  Ctrl: IMapCtrl;
begin
  for s in FMap.Properties do begin
    if FMap.IsMapCtrl( s ) then begin
      Ctrl := FMap.MapCtrl( s );
      case Ctrl.GetTypeOfData of
        mcdString   : Ctrl.SetValue( FJson.Item( s ).AsString );
        mcdInteger  : Ctrl.SetValue( FJson.Item( s ).AsInteger );
        mcdFloat    : Ctrl.SetValue( FJson.Item( s ).AsFloat );
        mcdDate     : Ctrl.SetValue( FJson.Item( s ).AsDate );
        mcdDateTime : Ctrl.SetValue( FJson.Item( s ).AsDateTime );
        mcdBoolean  : Ctrl.SetValue( FJson.Item( s ).AsBoolean );
        mcdJsonArray: Ctrl.SetValue( FJson.Item( s ).AsList );
        mcdJson     : Ctrl.SetValue( FJson.Item( s ).AsJson );
        mcdVarArray : Ctrl.SetValue( FJson.Item( s ).AsArray );
      else
        raise EEnumNotImplementedError<TMapCtrlTypeOfData>.Create( Integer( Ctrl.GetTypeOfData ) );
      end;
      Ctrl.ReadOnly( ReadOnly );
    end
    else begin
      if ( FJson.Item( s ) <> nil ) and ( not FJson.Item( s ).AsJson.IsEmpty )  then //Se fosse um mapping novo para um registro ja existente, dava pau dizendo que nao achava...
        TManipulateCtrls.New( FMap.MapCtrls( s ), FJson.Item( s ).AsJson ).ToCtrl( ReadOnly ); //RED#421 - tava sem o ( ReadOnly )
    end;
  end;
end;

function TManipulateCtrls.ToJson: IJson;
var
  s: string;
begin
  // red#2326, pra resolver de vez
  // algumas vezes o java n�o cria o objeto e manda sem o json
  // ent�o aqui eu dou um AddOrSet pra criar se n�o existir;
  for s in FMap.Properties do begin
    if FMap.IsMapCtrl( s ) then begin
      case FMap.MapCtrl( s ).GetTypeOfData of
        mcdString   : FJson.AddOrSetString( s, FMap.MapCtrl( s ).GetValue );
        mcdInteger  : FJson.AddOrSetInt( s, FMap.MapCtrl( s ).GetValue );
        mcdFloat    : FJson.AddOrSetFloat( s, FMap.MapCtrl( s ).GetValue );
        mcdDate     : FJson.AddOrSetDate( s, FMap.MapCtrl( s ).GetValue );
        mcdDateTime : FJson.AddOrSetDateTime( s, FMap.MapCtrl( s ).GetValue );
        mcdBoolean  : FJson.AddOrSetBoolean( s, FMap.MapCtrl( s ).GetValue );
        mcdJsonArray: FJson.AddOrSetJsonArray( s, TJsonArray.New( FMap.MapCtrl( s ).GetValue ) );
        mcdJson     : begin
          if TJson.New( FMap.MapCtrl( s ).GetValue ).IsEmpty then
            FJson.AddOrSetNull( s )
          else
            FJson.AddOrSetJson( s, TJson.New( FMap.MapCtrl( s ).GetValue ) );
        end;
        mcdVarArray : FJson.AddOrSetVarArray( s, FMap.MapCtrl( s ).GetValue );
      else
        raise EEnumNotImplementedError<TMapCtrlTypeOfData>.Create( Integer( FMap.MapCtrl( s ).GetTypeOfData ) );
      end;
    end
    else begin
      if not FJson.Item( s ).AsJson.IsEmpty then //Esse if foi inserido na rev 676, n�o sei por que... Ficar de olho se o else linha abaixo n�o gerar� um problema...
        FJson.SetJson( s, TManipulateCtrls.New( FMap.MapCtrls( s ), FJson.Item( s ).AsJson ).ToJson )
      else
        FJson.SetJson( s, TManipulateCtrls.New( FMap.MapCtrls( s ), FJson.Item( s ).AsJson ).ToJson );
    end;
  end;
  Result := FJson;
end;


end.
