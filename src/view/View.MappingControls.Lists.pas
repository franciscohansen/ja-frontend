
unit View.MappingControls.Lists;

interface

uses View.MappingControls.Interfaces, cxTL, Generics.Collections, System.SysUtils, cxCheckListBox, cxLookAndFeelPainters, StrUtils, Variants,
  cxEdit, cxDropDownEdit, cxCalc, cxButtons, System.Classes, cxTextEdit, cxButtonEdit, Json.Interfaces, cxImageComboBox,
  Util.Format, cxEditRepositoryItems, cxMaskEdit,
  Util.Lists;

type
  TMapTreeList = class( TInterfacedObject, IMapCtrl )
  private
    FGrid: TcxTreeList;
    FMap: TDictionary<String, TcxTreeListColumn>;
    FSearch: TDictionary<String, ISearchControl>;
  public
    constructor Create( Grid: TcxTreeList; JsonProperties: TArray<String>; Columns: TArray<TcxTreeListColumn>;
      JsonSearch: TArray<String>; ColumnsSearch: TArray<ISearchControl> );
    destructor Destroy; override;
    class function New( Grid: TcxTreeList; Map: TArray<String>; Columns : TArray<TcxTreeListColumn> ) : IMapCtrl; overload;
    class function New( Grid: TcxTreeList; Map: TArray<String> ): IMapCtrl; overload;
    class function New( Grid: TcxTreeList; JsonProperties: TArray<String>; Columns: TArray<TcxTreeListColumn>;
      JsonSearch: TArray<String>; ColumnsSearch: TArray<ISearchControl> ): IMapCtrl; overload;
    function GetTypeOfData: TMapCtrlTypeOfData;
    function GetValue: Variant;
    procedure SetValue( Value: Variant );
    procedure ReadOnly( b: boolean );
  end;

  TMapCheckListBox = class( TInterfacedObject, IMapCtrl )
  private
    FCheck: TcxCheckListBox;
    FList: TArray<IJson>;
    FDescricao: string;
    procedure PreencheCheckList;
  public
    constructor Create( CheckListBox: TcxCheckListBox; CompleteList: TArray<IJson>; JsonProperty: string );
    class function New( CheckListBox: TcxCheckListBox; CompleteList: TArray<IJson>; JsonProperty: string ): IMapCtrl; overload;
    class function New( CheckListBox: TcxCheckListBox; CompleteList: TArray<IJson> ): IMapCtrl; overload;
    function GetTypeOfData: TMapCtrlTypeOfData;
    function GetValue: Variant;
    procedure SetValue( Value: Variant );
    procedure ReadOnly( b: boolean );
  end;

  TMapTreeListSelection = class( TInterfacedObject, IMapCtrl )
  private
    FSelection: ISelection;
  public
    constructor Create( Selection: ISelection );
    class function New( Selection: ISelection ): IMapCtrl;
    function GetTypeOfData: TMapCtrlTypeOfData;
    function GetValue: variant;
    procedure SetValue( Value: Variant );
    procedure ReadOnly( b: boolean );
  end;

  TMapPrimitiveValueTreeList = class( TInterfacedObject, IMapCtrl )
  protected
    constructor Create( Grid : TcxTreeList; Column : TcxTreeListColumn );
  private
    FGrid   : TcxTreeList;
    FColumn : TcxTreeListColumn;
  public
    class function New( Grid : TcxTreeList; Column : TcxTreeListColumn ) : IMapCtrl;
    function GetTypeOfData: TMapCtrlTypeOfData;
    function GetValue: variant;
    procedure SetValue( Value: Variant );
    procedure ReadOnly( b: boolean );
  end;

  TMapComplexObjectTreeList = class( TInterfacedObject, IMapCtrl )
  private
    FGrid: TcxTreeList;
    FMap: TDictionary<String, TcxTreeListColumn>;
    FSearch: TDictionary<String, ISearchControl>;
    procedure AddComplexValue( var Json : IJson; const Path : string; const DataType : string; Value : Variant );
    function GetComplexValue( const Json : IJson; const Path : string; C : TcxTreeListColumn ) : Variant;
    function LinhaEstaVazia( n: TcxTreeListNode ): boolean;
  public
    constructor Create( Grid: TcxTreeList; JsonProperties: TArray<String>; Columns: TArray<TcxTreeListColumn>;
      JsonSearch: TArray<String>; ColumnsSearch: TArray<ISearchControl> );
    destructor Destroy; override;
    class function New( Grid: TcxTreeList; Map: TArray<String>; Columns : TArray<TcxTreeListColumn> ) : IMapCtrl; overload;
    class function New( Grid: TcxTreeList; Map: TArray<String> ): IMapCtrl; overload;
    class function New( Grid: TcxTreeList; JsonProperties: TArray<String>; Columns: TArray<TcxTreeListColumn>;
      JsonSearch: TArray<String>; ColumnsSearch: TArray<ISearchControl> ): IMapCtrl; overload;
    function GetTypeOfData: TMapCtrlTypeOfData;
    function GetValue: Variant;
    procedure SetValue( Value: Variant );
    procedure ReadOnly( b: boolean );
  end;

  TOnSetValueProc<T> = procedure( Item : T ) of object;
  TOnReadValueProc<T> = procedure( ANode : TcxTreeListNode; var Item : T ) of object;
  TOnParseValueArrayFunc<T> = function( AVarArray : Variant ) : TArray<T> of object;

  TMapGenericTreeList<T> = class( TInterfacedObject, IMapCtrl )
  private
    FGrid : TcxTreeList;
    FValues : IList<T>;
    FOnSetValueProc   : TOnSetValueProc<T>;
    FOnReadValueProc  : TOnReadValueProc<T>;
    FOnParseValueArrayFunc : TOnParseValueArrayFunc<T>;
    constructor Create( AGrid : TcxTreeList;
                        AOnSetValue : TOnSetValueProc<T>;
                        AOnReadValue : TOnReadValueProc<T>;
                        AOnParseValueArray : TOnParseValueArrayFunc<T> );
  public
    class function New( AGrid : TcxTreeList;
                        AOnSetValue : TOnSetValueProc<T>;
                        AOnReadValue : TOnReadValueProc<T>;
                        AOnParseValueArray : TOnParseValueArrayFunc<T> ) : IMapCtrl;
    function GetTypeOfData: TMapCtrlTypeOfData;
    function GetValue: variant;
    procedure SetValue( Value: Variant );
    procedure ReadOnly( b: boolean );
  end;

implementation

{ TMapTreeList }

uses Json.Base, {View.Search.Base,} Dao.Interfaces, Server.Interfaces,
  View.MessageBox, Util.Constraints.Base, Util.Constraints.Interfaces,
  Model.LctoFinanceiro.Interfaces, Log, Exceptions, Cfg.Constantes, View.Search.Lists;


constructor TMapTreeList.Create(Grid: TcxTreeList; JsonProperties: TArray<String>; Columns: TArray<TcxTreeListColumn>; JsonSearch: TArray<String>; ColumnsSearch: TArray<ISearchControl>);
var
  i: integer;
begin
  inherited Create;
  FGrid := Grid;
  FSearch := nil;
  FMap  := TDictionary<String,TcxTreeListColumn>.Create;
  for i := Low( JsonProperties ) to High( JsonProperties ) do begin
    FMap.Add( JsonProperties[ i ], Columns[ i ] );
  end;
  if JsonSearch <> nil then begin
    FSearch := TDictionary<String, ISearchControl>.Create;
    for i := Low( JsonSearch ) to High( JsonSearch ) do begin
      FSearch.Add( JsonSearch[ i ], ColumnsSearch[ i ] );
    end;
  end;
end;

destructor TMapTreeList.Destroy;
begin
  FMap.Free;
  if FSearch <> nil then
    FSearch.Free;
  inherited;
end;

function TMapTreeList.GetTypeOfData: TMapCtrlTypeOfData;
begin
  Result := mcdJsonArray;
end;

function TMapTreeList.GetValue: Variant;
var
  Json: IJson;
  i, iid : integer;
  Coluna: TcxTreeListColumn;
  n: TcxTreeListNode;
  Retorno: string;
  s: string;
  Value: variant;
begin
  Retorno := '';
  FGrid.FocusedNode := nil;
  for i := 0 to FGrid.Count - 1 do begin
    n := FGrid.Items[ i ];
    FGrid.FocusedNode := n;

    Json := TJson.New;

    for s in FMap.Keys do begin
      Coluna := FMap.Items[ s ];

      Value  := n.Values[ Coluna.ItemIndex ];
      if not ( VarType( Value ) in [ varEmpty, varNull ] ) then begin
        if ( Coluna.PropertiesClass = TcxImageComboBoxProperties ) and ( Coluna.RepositoryItem <> nil ) then begin
          Json.AddJson( s, TJson.New( Value ) );
        end
        else if Coluna.PropertiesClass = TcxImageComboBoxProperties then begin
          Json.AddString( s, Value );
        end
        else begin
          case AnsiIndexStr( Coluna.DataBinding.ValueType, [ 'String', 'DateTime', 'Integer', 'Boolean', 'Float' ] ) of
            0: Json.AddString( s, n.Texts[ Coluna.ItemIndex ] );
            1: Json.AddDateTime( s, Value );
            2: Json.AddInt( s, n.Values[ Coluna.ItemIndex ] );
            3: Json.AddBoolean( s, n.Values[ Coluna.ItemIndex ] = True );
            4: Json.AddFloat( s, n.Values[ Coluna.ItemIndex ] );
          else
            Raise Exception.Create( 'Tipo n�o implementado: ' + Coluna.DataBinding.ValueType );
          end;
        end;
      end
      else begin
        if ( s ) = 'ID' then
          Json.AddInt( s, 0 )
        else
          Json.AddNull( s );
      end;
    end;
    if FSearch <> nil then begin
      for s in FSearch.Keys do begin
        if Supports( FSearch.Items[ s ], ITreeListColumnSearch ) then begin
          if Assigned( ( FSearch.Items[ s ] as ITreeListColumnSearch ).GetJson(n)) then  begin
            iid := ( FSearch.Items[ s ] as ITreeListColumnSearch ).GetID(n);
            Json.AddJson( s, TJson.New.AddInt( 'id', iid ) );
          end
          else
            Json.AddNull( s );
        end
        else begin
          if Assigned(FSearch.Items[ s ].GetJson) then  begin
            iid := FSearch.Items[ s ].GetID;
            Json.AddJson( s, TJson.New.AddInt( 'id', iid ) );
          end
          else
            Json.AddNull( s );
  //        Json.AddInt( s, FSearch.Items[ s ].GetID );
        end;
      end;
    end;
    if not Json.IsEmpty then
      Retorno := Retorno + Json.AsJsonString + ',';
  end;
  Delete( Retorno, Retorno.Length, 1 );
  Result := '[' + Retorno + ']';
end;

class function TMapTreeList.New(Grid: TcxTreeList;
  JsonProperties: TArray<String>; Columns: TArray<TcxTreeListColumn>;
  JsonSearch: TArray<String>;
  ColumnsSearch: TArray<ISearchControl>): IMapCtrl;
begin
  Result := TMapTreeList.Create( Grid, JSonProperties, Columns, JsonSearch, ColumnsSearch );
end;

class function TMapTreeList.New(Grid: TcxTreeList; Map: TArray<String>;
  Columns: TArray<TcxTreeListColumn>): IMapCtrl;
begin
  Result := TMapTreeList.Create( Grid, Map, Columns, nil, nil );
end;

procedure TMapTreeList.ReadOnly(b: boolean);
begin
  if b then
    TFormat.FormatGrid( FGrid ).Editable( false );
end;

class function TMapTreeList.New(Grid: TcxTreeList; Map: TArray<String>): IMapCtrl;
var
  Colunas: TArray<TcxTreeListColumn>;
  i: integer;
begin
  SetLength( Colunas, Length( Map ) );
  for i := Low( Map ) to High( Map ) do begin
    Colunas[ i ] := Grid.Columns[ i ];
  end;
  Result := TMapTreeList.Create( Grid, Map, Colunas, nil, nil );
end;

procedure TMapTreeList.SetValue(Value: Variant);
var
  JsonArray: TArray<IJson>;
  Json: IJson;
  n: TcxTreeListNode;
  s: string;
  c: TcxTreeListColumn;
  p: TcxImageComboBoxProperties;
  i: Integer;
begin
  JsonArray := Value;
  FGrid.BeginUpdate;
  try
    FGrid.Clear;
    for Json in JsonArray do begin
      n := FGrid.Add;
      for s in FMap.Keys do begin
        c := FMap.Items[ s ];
        if ( c.PropertiesClass = TcxImageComboBoxProperties ) and ( c.RepositoryItem <> nil ) then begin
          p := TcxImageComboBoxProperties( c.RepositoryItem.Properties );
          for i := 0 to p.Items.Count -1 do begin
            if Json.Item( s ).AsJson.AsJsonString = p.Items.Items[ i ].Value then begin
              n.Values[ c.ItemIndex ] := p.Items.Items[ i ].Value;
              break;
            end;
          end;
        end
        else if c.PropertiesClass = TcxMaskEditProperties then begin // red#1769
          if ( TcxMaskEditProperties( c.Properties ).EditMask = TAppConstantes.MascaraCPF ) or
             ( TcxMaskEditProperties( c.Properties ).EditMask = TAppConstantes.MascaraCNPJ ) then
            n.Values[ c.ItemIndex ] := TFormat.CpfCnpj( Json.Item( s ).AsString );
        end
        else begin
          case AnsiIndexStr( c.DataBinding.ValueType, [ 'String', 'DateTime', 'Integer', 'Boolean', 'Float' ] ) of
            1: begin
              if Json.Item( s ).AsDateTime > 0 then
                n.Values[ c.ItemIndex ] := Json.Item( s ).AsDateTime
              else
                n.Values[ c.ItemIndex ] := NULL;
            end;
            3: n.Values[ c.ItemIndex ] := StrToBoolDef( Json.Item( s ).AsString, False );
            4: n.Values[ c.ItemIndex ] := StrToFloatDef( VarToStr( Json.Item( s ).AsString ).Replace( '.', ',', [ rfReplaceAll ] ), 0 )
          else
            n.Values[ c.ItemIndex ] := Json.Item( s ).AsString;
          end;
        end;
      end;
      if FSearch <> nil then begin
        for s in FSearch.Keys do begin
          FGrid.FocusedNode := n;
          if Assigned( Json.Item( s ).AsJson ) and ( not Json.Item( s ).AsJson.IsEmpty ) then
            FSearch.Items[ s ].SetID( Json.Item( s ).AsJson.Item( 'id' ).AsInteger );
        end;
      end;
    end;
  finally
    FGrid.EndUpdate;
  end;
end;

{ TMapCheckListBox }

constructor TMapCheckListBox.Create(CheckListBox: TcxCheckListBox;
  CompleteList: TArray<IJson>; JsonProperty: string );
begin
  inherited Create;
  FCheck := CheckListBox;
  FList  := CompleteLIst;
  FDescricao := JsonProperty;
  PreencheCheckList;
end;

function TMapCheckListBox.GetTypeOfData: TMapCtrlTypeOfData;
begin
  Result := mcdJsonArray
end;

function TMapCheckListBox.GetValue: Variant;
var
  Retorno: string;
  i: integer;
begin
  Retorno := '';
  for i := 0 to FCheck.Items.Count - 1 do begin
    if ( FCheck.Items.Items[ i ].State = cbsChecked ) then begin
      Retorno := Retorno + TJson.New.AddInt( 'id', FCheck.Items.Items[ i ].Tag ).AsJsonString + ','
    end;
  end;
  Delete( Retorno, Retorno.Length, 1 );
  Result := '[' + Retorno + ']';
end;

class function TMapCheckListBox.New(CheckListBox: TcxCheckListBox;
  CompleteList: TArray<IJson>): IMapCtrl;
begin
  Result := TMapCheckListBox.Create( CheckListBox, COmpleteList, 'nome' );
end;

class function TMapCheckListBox.New(CheckListBox: TcxCheckListBox; CompleteList: TArray<IJson>; JsonProperty: string): IMapCtrl;
begin
  Result := TMapCheckListBox.Create( CheckListBox, COmpleteList, JsonProperty );
end;

procedure TMapCheckListBox.PreencheCheckList;
var
  Json: IJson;
  n: TcxCheckListBoxItem;
begin
  FCheck.Items.BeginUpdate;
  try
    FCheck.Items.Clear;
    for Json in FList do begin
      n := FCheck.Items.Add;
      n.Text := Json.Item( FDescricao ).AsString;
      n.Tag  := Json.Item( 'id' ).AsInteger;
    end;
  finally
    FCheck.Items.EndUpdate;
  end;
end;

procedure TMapCheckListBox.ReadOnly(b: boolean);
begin
  FCheck.ReadOnly := b;
end;

procedure TMapCheckListBox.SetValue(Value: Variant);
var
  JsonArray: TArray<IJson>;
  Json: IJson;
  i: integer;
begin
  // primeiro zera a lista
  for i := 0 to FCheck.Items.Count - 1 do
    FCheck.Items.Items[ i ].State := cbsUnchecked;

  JsonArray := Value;
  for Json in JsonArray do begin
    for i := 0 to FCheck.Items.Count - 1 do begin
      if FCheck.Items.Items[ i ].Tag = Json.Item( 'id' ).AsInteger then begin
        FCheck.Items.Items[ i ].State := cbsChecked;
        Break;
      end;
    end;
  end;
end;

{ TMapPrimitiveValueTreeList }

constructor TMapPrimitiveValueTreeList.Create(Grid: TcxTreeList;
  Column: TcxTreeListColumn);
begin
  inherited Create;
  FGrid := Grid;
  FColumn := Column;
end;

function TMapPrimitiveValueTreeList.GetTypeOfData: TMapCtrlTypeOfData;
begin
  Result := mcdVarArray;
end;

function TMapPrimitiveValueTreeList.GetValue: variant;
var
  i : Integer;
  Arr : TArray<Variant>;
begin
  SetLength( Arr, FGrid.Count );
  for i := 0 to FGrid.Count -1 do
    Arr[i] := FGrid.Items[ i ].Values[ FColumn.ItemIndex ];

  Result := Arr;
end;

class function TMapPrimitiveValueTreeList.New(Grid: TcxTreeList;
  Column: TcxTreeListColumn): IMapCtrl;
begin
  Result := Create( Grid, Column );
end;

procedure TMapPrimitiveValueTreeList.ReadOnly(b: boolean);
begin
  if b then
    TFormat.FormatGrid( FGrid ).Editable( False );
end;

procedure TMapPrimitiveValueTreeList.SetValue(Value: Variant);
var
  Arr : TArray<Variant>;
  i, l : Integer;
begin
  Arr := Value;
  try
    FGrid.BeginUpdate;
    FGrid.Clear;
    l := Length( Arr );
    for i := 0 to l -1 do begin
      with FGrid.Add do begin
        Values[ FColumn.ItemIndex ] := Arr[i];
      end;
    end;
  finally
    FGrid.EndUpdate;
  end;
end;

{ TMapComplexObjectTreeList }

function TMapComplexObjectTreeList.GetComplexValue(const Json: IJson;
  const Path: string; C : TcxTreeListColumn): Variant;
var
  sl : TStringList;
  i : Integer;
  js : IJson;
begin
  sl := TStringList.Create;
  try
    sl := TStringList.Create;
    sl.Delimiter := '.';
    sl.DelimitedText := Path;
    if sl.Count > 0 then begin
      if not Json.Contains( sl.Strings[ 0 ] ) then
        Exit( null );
      js := Json.Item( sl.Strings[ 0 ] ).AsJson;
      if sl.Count > 2 then begin
        for i := 0 to sl.Count -2 do begin
          if not js.Contains( sl.Strings[ i ] ) then
            Exit( NULL );
          js := js.Item( sl.Strings[ i ] ).AsJson;
        end;
      end;
      case AnsiIndexStr( AnsiLowerCase( c.DataBinding.ValueType ) , [ 'string', 'datetime', 'integer', 'boolean', 'float', 'json'  ] ) of
        0,
        5: Result := js.Item( sl.Strings[ sl.Count -1 ] ).AsString;
        1: Result := js.Item( sl.Strings[ sl.Count -1 ] ).AsDateTime;
        2: Result := js.Item( sl.Strings[ sl.Count -1 ] ).AsInteger;
        3: Result := js.Item( sl.Strings[ sl.Count -1 ] ).AsBoolean;
        4: Result := js.Item( sl.Strings[ sl.Count -1 ] ).AsFloat;
      end;

//      Result := js.Item( sl.Strings[ sl.Count -1 ] ).AsString;
    end
    else
      Exit( NULL );
  finally
    sl.Free;
  end;
end;

procedure TMapComplexObjectTreeList.AddComplexValue(var Json: IJson;
  const Path, DataType: string; Value: Variant);
var
  sl : TStringList;
  i : Integer;
  js : IJson;
  s : string;
begin
  sl := TStringList.Create;
  try
//    TLog.Logger.Log( Path + '-' + VarToStrDef( Value, 'NULL' ) );
    sl.Delimiter := '.';
    sl.DelimitedText := Path;
    if sl.Count > 0 then begin
      if not Json.Contains( sl.Strings[ 0 ] ) then
        Json.AddJson( sl.Strings[ 0 ], TJson.New );
      js := Json.Item( sl.Strings[ 0 ] ).AsJson;
      if sl.Count > 2 then begin
        for i := 1 to sl.Count -2 do begin
          if not js.Contains( sl.Strings[ i ] ) then
            js.AddJson( sl.Strings[ i ] , TJson.New );
          js := js.Item( sl.Strings[ i ] ).AsJson;
        end;
      end;
      s := sl.Strings[ sl.Count -1 ];
      if not ( VarType(Value) in [ varEmpty, varNull ] ) then begin
        case AnsiIndexStr( AnsiLowerCase( DataType ), [ 'string', 'datetime', 'integer', 'boolean', 'float', 'json' ] ) of
          0: js.AddString( s, Value );
          1: js.AddDateTime( s, Value );
          2: js.AddInt( s, Value );
          3: js.AddBoolean( s, Value = True );
          4: js.AddFloat( s, Value );
          5: js.AddJson( s, TJson.New( Value ) );
        else
          raise Exception.Create('Tipo n�o implementado: ' + DataType );
        end;
      end
      else begin
        js.AddNull( s );
      end;
      Json.SetJson( sl.Strings[ 0 ], js );
    end;
  finally
    sl.Free;
  end;
end;

constructor TMapComplexObjectTreeList.Create(Grid: TcxTreeList;
  JsonProperties: TArray<String>; Columns: TArray<TcxTreeListColumn>;
  JsonSearch: TArray<String>; ColumnsSearch: TArray<ISearchControl>);
var
  i: integer;
begin
  inherited Create;
  FGrid := Grid;
  FSearch := nil;
  FMap  := TDictionary<String,TcxTreeListColumn>.Create;
  for i := Low( JsonProperties ) to High( JsonProperties ) do begin
    FMap.Add( JsonProperties[ i ], Columns[ i ] );
  end;
  if JsonSearch <> nil then begin
    FSearch := TDictionary<String, ISearchControl>.Create;
    for i := Low( JsonSearch ) to High( JsonSearch ) do begin
      FSearch.Add( JsonSearch[ i ], ColumnsSearch[ i ] );
    end;
  end;
end;

destructor TMapComplexObjectTreeList.Destroy;
begin
  FMap.Free;
  if FSearch <> nil then
    FSearch.Free;
  inherited;
end;

function TMapComplexObjectTreeList.GetTypeOfData: TMapCtrlTypeOfData;
begin
  Result := mcdJsonArray;
end;

function TMapComplexObjectTreeList.GetValue: Variant;
var
  Json: IJson;
  i : integer;
  Coluna: TcxTreeListColumn;
  n: TcxTreeListNode;
  Retorno: string;
  s: string;
  Value: variant;
begin
  Retorno := '';
  for i := 0 to FGrid.Count - 1 do begin
    n := FGrid.Items[ i ];
    // red#1590, verifica se a linha est� vazia;
    if LinhaEstaVazia( n ) then
      Continue;

    Json := TJson.New;

    for s in FMap.Keys do begin
      Coluna := FMap.Items[ s ];
      Value  := n.Values[ Coluna.ItemIndex ];

      if s.ToUpper.Equals( 'ASJSON' ) then begin
        Continue;
      end;

      if Pos( '.', s ) > 0 then begin
        if ( Coluna.PropertiesClass = TcxImageComboBoxProperties ) and ( Coluna.RepositoryItem <> nil ) then
          AddComplexValue( Json, s, 'json', Value )
        else
          AddComplexValue( Json, s, Coluna.DataBinding.ValueType, Value );
      end
      else begin
        if not ( VarType( Value ) in [ varEmpty, varNull ] ) then begin
          if ( Coluna.PropertiesClass = TcxImageComboBoxProperties ) and ( Coluna.RepositoryItem <> nil ) then begin
            Json.AddJson( s, TJson.New( Value ) );
          end
          else if Coluna.PropertiesClass = TcxImageComboBoxProperties then begin
            Json.AddString(  s, Value );
          end
          else begin
            case AnsiIndexStr( Coluna.DataBinding.ValueType, [ 'String', 'DateTime', 'Integer', 'Boolean', 'Float' ] ) of
              0: Json.AddString( s, n.Texts[ Coluna.ItemIndex ] );
              1: Json.AddDateTime( s, Value );
              2: Json.AddInt( s, n.Values[ Coluna.ItemIndex ] );
              3: Json.AddBoolean( s, n.Values[ Coluna.ItemIndex ] = True );
              4: Json.AddFloat( s, n.Values[ Coluna.ItemIndex ] );
            else
              Raise Exception.Create( 'Tipo n�o implementado: ' + Coluna.DataBinding.ValueType );
            end;
          end;
        end
        else
          Json.AddNull( s );
      end;
    end;
    if FSearch <> nil then begin
      for s in FSearch.Keys do begin
        if Supports( FSearch.Items[ s ], ITreeListColumnSearch ) then begin
          if ( FSearch.Items[ s ] as ITreeListColumnSearch ).GetID( n ) = 0 then
            Json.AddNull( s )
          else begin
            if Pos( '.', s ) > 0 then begin
              AddComplexValue( Json, s, 'json',
                  TJson.New.AddInt( 'id', TTreeListColumnSearch( FSearch.Items[ s ] ).GetID( n ) )
              );
            end
            else begin
              Json.AddJson( s, TJson.New.AddInt( 'id', TTreeListColumnSearch( FSearch.Items[ s ] ).GetID( n ) ) )
            end;
          end;
        end
        else begin
          if FSearch.Items[ s ].GetID = 0 then // red#1916
            Json.AddNull( s )
          else begin
            if Pos( '.', s ) > 0 then
              AddComplexValue( Json, s, 'json', TJson.New.AddInt( 'id', FSearch.Items[ s ].GetID ) )
            else begin
              Json.AddJson( s, TJson.New.AddInt( 'id', FSearch.Items[ s ].GetID ) );
              //Json.AddInt( s, FSearch.Items[ s ].GetID );
            end;
          end;
        end;
      end;
    end;
    if not Json.IsEmpty then
      Retorno := Retorno + Json.AsJsonString + ',';
  end;
  Delete( Retorno, Retorno.Length, 1 );
  Result := '[' + Retorno + ']';
end;

function TMapComplexObjectTreeList.LinhaEstaVazia(n: TcxTreeListNode): boolean;
var
  i: integer;
begin
  Result := true;
  for i := 0 to FGrid.ColumnCount - 1 do begin
    case AnsiIndexStr( FGrid.Columns[ i ].DataBinding.ValueType, [ 'String', 'Boolean', 'DateTime', 'Integer', 'Float' ] ) of
      0: begin
        if not n.Texts[ i ].IsEmpty then
          Exit( false );
      end;
      1: begin
        if n.Values[ i ] = true then
          Exit( false );
      end;
      2, 3, 4 : begin
        if n.Values[ i ] > 0 then
          Exit( false );
      end;
    else
      if n.Values[ i ] <> null then
        Exit( false );
    end;
  end;
end;

class function TMapComplexObjectTreeList.New(Grid: TcxTreeList;
  Map: TArray<String>; Columns: TArray<TcxTreeListColumn>): IMapCtrl;
begin
  Result := TMapComplexObjectTreeList.Create( Grid, Map, Columns, nil, nil );
end;

class function TMapComplexObjectTreeList.New(Grid: TcxTreeList;
  Map: TArray<String>): IMapCtrl;
var
  Colunas: TArray<TcxTreeListColumn>;
  i: integer;
begin
  SetLength( Colunas, Length( Map ) );
  for i := Low( Map ) to High( Map ) do begin
    Colunas[ i ] := Grid.Columns[ i ];
  end;
  Result := TMapComplexObjectTreeList.Create( Grid, Map, Colunas, nil, nil );
end;

class function TMapComplexObjectTreeList.New(Grid: TcxTreeList;
  JsonProperties: TArray<String>; Columns: TArray<TcxTreeListColumn>;
  JsonSearch: TArray<String>;
  ColumnsSearch: TArray<ISearchControl>): IMapCtrl;
begin
   Result := TMapComplexObjectTreeList.Create( Grid, JSonProperties, Columns, JsonSearch, ColumnsSearch );
end;

procedure TMapComplexObjectTreeList.ReadOnly(b: boolean);
begin
  if b then
    TFormat.FormatGrid( FGrid ).Editable( false );
end;

procedure TMapComplexObjectTreeList.SetValue(Value: Variant);
var
  JsonArray: TArray<IJson>;
  Json: IJson;
  n: TcxTreeListNode;
  s: string;
  c: TcxTreeListColumn;
  p: TcxImageComboBoxProperties;
  i: Integer;
  sVal : string;
begin
  JsonArray := Value;
  FGrid.BeginUpdate;
  try
    FGrid.Clear;
    for Json in JsonArray do begin
//      TLog.Logger.Log( Json.AsJsonString );
      n := FGrid.Add;
      for s in FMap.Keys do begin
        c := FMap.Items[ s ];

        if s.ToUpper.Equals( 'ASJSON' ) then begin
          n.Values[ c.ItemIndex ] := Json.AsJsonString;
          Continue;
        end;
//        TLog.Logger.Log( s );
        if Pos( '.', s ) > 0 then
          sVal := GetComplexValue( Json, s, c )
        else begin
          sVal := Json.Item( s ).AsString;
        end;

        if ( c <> nil ) and (c.PropertiesClass <> nil ) and ( c.PropertiesClass = TcxImageComboBoxProperties ) and ( c.RepositoryItem <> nil ) then begin
          p := TcxImageComboBoxProperties( c.RepositoryItem.Properties );
          for i := 0 to p.Items.Count -1 do begin
            if TJson.New( sVal ).AsJsonString = p.Items.Items[ i ].Value then begin
              n.Values[ c.ItemIndex ] := p.Items.Items[ i ].Value;
              break;
            end;
          end;
        end
        else if c.PropertiesClass = TcxImageComboBoxProperties then
          n.Values[ c.ItemIndex ] := sVal
        else if AnsiLowerCase( c.DataBinding.ValueType ) = 'float' then
          n.Values[ c.ItemIndex ] := StrToFloatDef( VarToStr( sVal ).Replace( '.', ',', [ rfReplaceAll ] ), 0 )
        else if AnsiLowerCase( c.DataBinding.ValueType ) = 'boolean' then
          n.Values[ c.ItemIndex ] := sVal.ToUpper = 'TRUE'
        else begin
          n.Values[ c.ItemIndex ] := sval;
        end;
      end;
      if FSearch <> nil then begin
        for s in FSearch.Keys do begin
          FGrid.FocusedNode := n;
          if Pos( '.', s ) > 0 then
            FSearch.Items[ s ].SetID( TJson.New( GetComplexValue( Json, s, c ) ).Item( 'id' ).AsInteger )
          else begin
            if not Json.Item( s ).AsJson.IsEmpty then
              FSearch.Items[ s ].SetID( Json.Item( s ).AsJson.Item( 'id' ).AsInteger );
          end;
        end;
      end;
    end;
  finally
    FGrid.EndUpdate;
  end;
end;

{ TMapGenericTreeList<TInput, TOutput> }

constructor TMapGenericTreeList<T>.Create(AGrid: TcxTreeList; AOnSetValue: TOnSetValueProc<T>;
  AOnReadValue: TOnReadValueProc<T>; AOnParseValueArray : TOnParseValueArrayFunc<T>);
begin
  inherited Create;
  FGrid             := AGrid;
  FOnSetValueProc   := AOnSetValue;
  FOnReadValueProc  := AOnReadValue;
  FOnParseValueArrayFunc := AOnParseValueArray;
end;

function TMapGenericTreeList<T>.GetTypeOfData: TMapCtrlTypeOfData;
begin
  Result := mcdVarArray;
end;

function TMapGenericTreeList<T>.GetValue: variant;
var
  FItem : T;
  i: Integer;
begin
  if not Assigned( FValues ) then
    FValues := TList<T>.New;
  for i := 0 to FGrid.Count -1 do begin
    try
      FOnReadValueProc( FGrid.Items[ i ], FItem );
      FValues.Add( FItem );
    except
      on E:Exception do begin
        //Se for EVarNotAssignedError, � pq n setou o valor na baga�a la...
        //Pq se fizer null check no FItem n funfa,
        //Generics bugado kk
        if E.ClassType <> EVarNotAssignedError then
          raise E;
      end;
    end;
  end;
  Result := FValues.ToArray;
end;

class function TMapGenericTreeList<T>.New(AGrid: TcxTreeList; AOnSetValue: TOnSetValueProc<T>;
  AOnReadValue: TOnReadValueProc<T>; AOnParseValueArray : TOnParseValueArrayFunc<T>): IMapCtrl;
begin
  Result := Create( AGrid, AOnSetValue, AOnReadValue, AOnParseValueArray );
end;

procedure TMapGenericTreeList<T>.ReadOnly(b: boolean);
begin
  if b then
    TGridFormat.New( FGrid ).Editable( False );
end;

procedure TMapGenericTreeList<T>.SetValue(Value: Variant);
var
  Values : TArray<T>;
  Item : T;
begin
  Values := FOnParseValueArrayFunc( Value );
  for Item in Values do begin
    FOnSetValueProc( Item );
  end;
end;

{ TMapTreeListSelection }

constructor TMapTreeListSelection.Create(Selection: ISelection);
begin
  inherited Create;
  FSelection := Selection;
end;

function TMapTreeListSelection.GetTypeOfData: TMapCtrlTypeOfData;
begin
  Result := TMapCtrlTypeOfData.mcdJsonArray;
end;

function TMapTreeListSelection.GetValue: variant;
begin
  Result := TJson.ArrayToString( FSelection.JsonList ); // tem que mandar como string sen�o d� paaaaaaau
end;

class function TMapTreeListSelection.New(Selection: ISelection): IMapCtrl;
begin
  Result := TMapTreeListSelection.Create( Selection );
end;

procedure TMapTreeListSelection.ReadOnly(b: boolean);
begin

end;

procedure TMapTreeListSelection.SetValue(Value: Variant);
var
  List: TArray<IJson>;
  j: IJson;
begin
  List := Value;
  for j in List do
    FSelection.Select( j.ID );
end;

end.
