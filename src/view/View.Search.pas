unit View.Search;

interface

uses
  System.SysUtils, System.Classes, Dao.Interfaces, Json.Interfaces, untPesquisa,
  cxButtonEdit, Vcl.Forms, Vcl.Dialogs, Vcl.Controls, cxImageComboBox,
  cxEdit, cxTextEdit, Http.Interfaces;
type
   IPesquisa = interface
    ['{67B81261-DB81-4BA3-9FCE-F16FA0A84928}']
    function SetJson( AJson : IJson ) : IPesquisa;
    function GetJson : IJson;
  end;
  TOnChange = reference to procedure( AJson : IJson );

  TPesquisa = class( TInterfacedObject, IPesquisa )
  procedure OnChange( Sender : TObject );
  procedure OnButtonClick(Sender: TObject; AButtonIndex: Integer);
  procedure OnExit( Sender : TObject );
  private
    FEdit : TcxButtonEdit;
    FJson : IJson;
    FDao : IDao;
    FField : string;
    FParams : IUrlParams;
    FOnChange : TOnChange;
    FSettingJson : Boolean;
    constructor Create( AEdit : TcxButtonEdit; ADao : IDao; AField : string; AParams : IUrlParams; AOnChange : TOnChange );
    procedure Initialize;
    procedure ChamaPesquisa;
  public
    class function New( AEdit : TcxButtonEdit; ADao : IDao; AField : string; AParams : IUrlParams = nil; AOnChange : TOnChange = nil ) : IPesquisa;
    function SetJson( AJson : IJson ) : IPesquisa;
    function GetJson : IJson;
  end;

  TPesquisaEndereco = class( TInterfacedObject, IPesquisa )
  procedure OnExit( Sender : TObject );
  private
    FOwner : TForm;
    FPrefixo : string;
    FJson : IJson;
    FEdit : TcxButtonEdit;
    procedure ChamaPesquisa;
    procedure Configura;
    function Find( APrefix : string; ASuffix : string ) : TComponent;
    function Exists( APrefix, ASuffix : string ) : Boolean;
    procedure SetValor( APrefix, ASuffix : string; AValue : Variant );
    constructor Create( AOwner : TForm; APrefixo : string = '' );
  public
    class function New( AOwner : TForm; APrefixo : string = '' ) : IPesquisa;
    function SetJson( AJson : IJson ) : IPesquisa;
    function GetJson : IJson;
  end;

implementation

uses
  Json.Base, Dao.Base, Model.Endereco.Interfaces, Model.Endereco.Base,
  Http.Base, Dao.Localizacao.H10;

{ TPesquisa }

procedure TPesquisa.ChamaPesquisa;
var
  iID : Integer;
  j : IJson;
  frm : TfrmPesquisa;
begin
  iID := StrToIntDef( FEdit.Text, -1 );
  if iID > 0 then
    j := FDao.FindById( iID );

  if not Assigned( j ) then begin
    if FDao.RecordCount( Trim( FEdit.Text ), FParams ) = 1 then begin
      j := FDao.FindAll( Trim( FEdit.Text ), 0, 10, FParams )[ 0 ];
    end
    else begin
      frm := TfrmPesquisa.Create( nil );
      try
        frm.Field   := FField;
        frm.Dao     := FDao;
        frm.Params  := FParams;
        if frm.ShowModal = mrOk then begin
          j := frm.Json;
        end;
      finally
        frm.Release;
      end;
    end;
  end;
  if Assigned( j ) then
    SetJson( j );
end;

constructor TPesquisa.Create(AEdit : TcxButtonEdit; ADao: IDao; AField: string; AParams: IUrlParams; AOnChange : TOnChange);
begin
  inherited Create;
  FDao    := ADao;
  FField  := AField;
  FParams := AParams;
  FEdit   := AEdit;
  FOnChange := AOnChange;
  Initialize;
end;

function TPesquisa.GetJson: IJson;
begin
  if Assigned( FJson ) and ( not FJson.IsEmpty ) then
    Result := FJson
  else
    Result := nil;
end;

procedure TPesquisa.Initialize;
begin
  FEdit.Properties.OnChange       := OnChange;
  FEdit.OnExit                    := OnExit;
  FEdit.Properties.OnButtonClick  := OnButtonClick;
  FEdit.Clear;
end;

class function TPesquisa.New(AEdit : TcxButtonEdit; ADao: IDao; AField: string;
  AParams: IUrlParams; AOnChange : TOnChange): IPesquisa;
begin
  Result := Create( AEdit, ADao, AField, AParams, AOnChange );
end;

procedure TPesquisa.OnButtonClick(Sender: TObject; AButtonIndex: Integer);
begin
  ChamaPesquisa;
end;

procedure TPesquisa.OnChange(Sender: TObject);
begin
  if not FSettingJson then
    SetJson( nil );
end;

procedure TPesquisa.OnExit(Sender: TObject);
begin
  if ( not Assigned( FJson ) ) and ( FEdit.ModifiedAfterEnter ) then
    ChamaPesquisa;
end;

function TPesquisa.SetJson(AJson: IJson): IPesquisa;
begin
  FSettingJson := True;
  try
    Result := Self;
    FJson := nil;
    if Assigned( AJson ) and ( not AJson.IsEmpty ) then begin
      FJson := FDao.FindById( AJson.ID );
      FEdit.Text := FJson.Item( FField ).AsString;
      if Assigned( FOnChange ) then
        FOnChange( FJson );
    end;
  finally
    FSettingJson := False;
  end;
end;

{ TPesquisaEndereco }

procedure TPesquisaEndereco.ChamaPesquisa;
var
  j : IJson;
begin
  j := TLocalizacaoDao.New.PesquisaCEP( FEdit.Text );
  SetJson( j );
end;

procedure TPesquisaEndereco.Configura;
begin
  FEdit := TcxButtonEdit( Find( 'edt', 'CEP' ) );
  FEdit.OnExit := OnExit;
end;


constructor TPesquisaEndereco.Create(AOwner: TForm; APrefixo: string);
begin
  inherited Create;
  FOwner := AOwner;
  FPrefixo := APrefixo;
  Configura;
end;

function TPesquisaEndereco.Exists(APrefix, ASuffix: string): Boolean;
begin
  Result := Assigned( Find( APrefix, ASuffix ) );
end;

function TPesquisaEndereco.Find(APrefix, ASuffix: string): TComponent;
begin
  Result := FOwner.FindComponent( APrefix + FPrefixo + ASuffix );
end;

function TPesquisaEndereco.GetJson: IJson;
begin
  Result := FJson;
end;

class function TPesquisaEndereco.New(AOwner: TForm;
  APrefixo: string): IPesquisa;
begin
  Result := Create( AOwner, APrefixo );
end;

procedure TPesquisaEndereco.OnExit(Sender: TObject);
begin
  if FEdit.ModifiedAfterEnter then begin
    ChamaPesquisa;
  end;
end;

function TPesquisaEndereco.SetJson(AJson: IJson): IPesquisa;
var
  FEndereco : IEndereco;
begin
  FJson := AJson;

  FEndereco := TEndereco.New( AJson );
  FEdit.Text := FEndereco.Cep;
  SetValor( 'edt', 'Logradouro', FEndereco.Logradouro );
  SetValor( 'edt', 'Numero', FEndereco.Numero );
  SetValor( 'edt', 'Complemento', FEndereco.Complemento );
  SetValor( 'edt', 'Bairro', FEndereco.Bairro );
  SetValor( 'cbb', 'UF', FEndereco.UF );
  if FEndereco.UF <> 'NENHUM' then
    Application.ProcessMessages;
end;

procedure TPesquisaEndereco.SetValor(APrefix, ASuffix: string; AValue: Variant);
begin
  if Exists( APrefix, ASuffix ) then begin
    if APrefix = 'cbb' then
      TcxImageComboBox( Find( APrefix, ASuffix ) ).EditValue := AValue
    else
      TcxTextEdit( Find( APrefix, ASuffix ) ).Text := AValue;
  end;
end;

end.
