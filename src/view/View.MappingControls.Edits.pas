unit View.MappingControls.Edits;

interface

uses
  cxEdit, cxImageComboBox, cxTextEdit, cxMaskEdit, Vcl.Controls, cxDropDownEdit, cxCalendar, cxCheckBox, cxCalc, cxMemo, cxTimeEdit, cxSpinEdit, cxButtonEdit,
  System.SysUtils, Variants, cxLookAndFeelPainters, View.MappingControls.Interfaces, System.Classes, cxTrackbar, dxColorEdit;

type
  TMapEdit = class( TInterfacedObject, IMapCtrl )
  private
    FEhId: boolean;
    FEdit: TControl;
    FDontClearMask : Boolean;
    function GetTimeForCxTimeEdit: TTime;
  public
    constructor Create( Edit: TControl; Id: boolean = false; ADontClearMask : Boolean = false );
    destructor Destroy;
    class function New( Edit: TcxTextEdit ): IMapCtrl; overload;
    class function New( Edit: TcxTextEdit; Id: boolean ): IMapCtrl; overload;
    class function New( Edit: TcxButtonEdit ): IMapCtrl; overload;
    class function New( Edit: TcxMaskEdit ): IMapCtrl; overload;
    class function New( Edit : TcxMaskEdit; ADontClearMask : Boolean  ) : IMapCtrl; overload;
    class function New( Edit: TcxImageComboBox ): IMapCtrl; overload;
    class function New( Edit: TcxComboBox ): IMapCtrl; overload;
    class function New( Edit: TcxDateEdit ): IMapCtrl; overload;
    class function New( Edit: TcxTimeEdit ): IMapCtrl; overload;
    class function New( Edit: TcxCheckBox ): IMapCtrl; overload;
    class function New( Edit: TcxCalcEdit ): IMapCtrl; overload;
    class function New( Edit: TcxSpinEdit ): IMapCtrl; overload;
    class function New( Edit: TcxMemo ): IMapCtrl; overload;
    class function New( Edit: TcxTrackBar ): IMapCtrl; overload;
    class function New( Edit: TdxColorEdit ) : IMapCtrl; overload;
    function GetTypeOfData: TMapCtrlTypeOfData;
    function GetValue: Variant;
    procedure SetValue( Value: Variant );
    procedure ReadOnly( b: boolean );
  end;

  TMapEndereco = class
  public
    class function New( Parent: TComponent; Prefixo: string ): IMapCtrls; overload;
    class function New( Parent: TComponent ): IMapCtrls; overload;
  end;

  TMapCfgJuros = class
  public
    class function New( Parent : TComponent ) : IMapCtrls; overload;
    class function New( Parent: TComponent; Prefixo: string ): IMapCtrls; overload;
  end;

implementation

{ TMapEdit }

uses Exceptions, Json.Base, Cfg.Constantes,
  View.MappingControls.Base, Json.Interfaces, Util.Format;

constructor TMapEdit.Create(Edit: TControl; Id: boolean; ADontClearMask : Boolean);
begin
  inherited Create;
  FEdit := Edit;
  FEhId := Id;
  FDontClearMask := ADontClearMask;
  if Id then
    with TcxTextEdit( FEdit ) do begin
      Properties.ReadOnly := true;
      TabStop := false;
      Hint := 'C�digo gerado automaticamente pelo sistema, n�o sendo poss�vel alterar.'; // red#1929
    end;
end;

destructor TMapEdit.Destroy;
begin
  if Assigned( FEdit ) then
    FEdit := nil;
  inherited;
end;

function TMapEdit.GetTimeForCxTimeEdit: TTime;
var
  ar: TArray<String>;
begin
  ar := String( TcxTimeEdit( FEdit ).Text ).Split( [ ':' ] );
  case Length( ar ) of
    2: Result := EncodeTime( ar[ 0 ].ToInteger, ar[ 1 ].ToInteger, 0, 0 );
    3: Result := EncodeTime( ar[ 0 ].ToInteger, ar[ 1 ].ToInteger, ar[ 2 ].ToInteger, 0 );
  else
    Result := 0;
  end;
end;

function TMapEdit.GetTypeOfData: TMapCtrlTypeOfData;
begin
  if FEhId then
    Exit( mcdInteger );

  Result := mcdString;
  if FEdit is TcxDateEdit then begin
    Result := mcdDate;
    if TcxDateEdit( FEdit ).Properties.Kind = ckDateTime then
      Result := mcdDateTime;
  end
  else if FEdit is TcxCalcEdit then
    Result := mcdFloat
  else if FEdit is TcxSpinEdit then
    Result := mcdInteger
  else if FEdit is TcxCheckBox then
    Result := mcdBoolean
  else if FEdit is TcxTimeEdit then
    Result := mcdString;

end;

function TMapEdit.GetValue: Variant;
begin
  if ( FEdit is TcxTextEdit ) or ( FEdit is TcxButtonEdit ) then begin
    if ( FEdit is TcxButtonEdit ) and ( TcxButtonEdit( FEdit ).Properties.MaskKind <> emkStandard ) then
      Result := TFormat.ClearFormat( TcxButtonEdit( FEdit ).Text, './-' )
    else
      Result := TcxTextEdit( FEdit ).Text;
  end
  else if ( FEdit is TcxMaskEdit ) then begin
    if FDontClearMask then
      Result := TcxMaskEdit( FEdit ).Text
    else
      Result := TFormat.ClearFormat( TcxMaskEdit( FEdit ).Text, '.-/' );
  end
  else if FEdit is TcxImageComboBox then
    Result := VarToStr( TcxImageComboBox( FEdit ).EditValue )
  else if FEdit is TcxComboBox then
    Result := TcxComboBox( FEdit ).Text
  else if FEdit is TcxDateEdit then
    Result := TcxDateEdit( FEdit ).Date
  else if FEdit is TcxTimeEdit then begin
    Result := GetTimeForCxTimeEdit
  end
  else if FEdit is TcxCalcEdit then begin
    if TcxCalcEdit( FEdit ).Text = '' then
      Result := 0
    else
      Result := TcxCalcEdit( FEdit ).EditValue;
  end
  else if FEdit is TcxSpinEdit then
    Result := TcxSpinEdit( FEdit ).Value
  else if FEdit is TcxCheckBox then
    Result := TcxCheckBox( FEdit ).Checked
  else if FEdit is TcxMemo then
    Result := StringReplace(TcxMemo( FEdit ).Lines.Text, '\', '\\', [rfReplaceAll] )
  else if FEdit is TcxTrackBar then
    Result := TcxTrackBar( FEdit ).Position
  else if FEdit is TdxColorEdit then
    Result := TdxColorEdit( FEdit ).ColorValue
  else
    Raise Exception.Create( FEdit.ClassName + ' n�o implementada' );
end;

class function TMapEdit.New(Edit: TcxComboBox): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxDateEdit): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxImageComboBox): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxButtonEdit): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxMaskEdit): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxSpinEdit): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxMemo): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxTextEdit; Id: boolean): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit, Id );
end;

procedure TMapEdit.ReadOnly(b: boolean);
begin
  if FEhId then
    b := true;

  if ( FEdit is TcxTextEdit ) or ( ( FEdit is TcxButtonEdit ) and ( TcxButtonEdit( FEdit ).Properties.MaskKind = emkStandard ) ) then
    TcxTextEdit( FEdit ).Properties.ReadOnly := b
  else if ( FEdit is TcxMaskEdit ) or ( FEdit is TcxButtonEdit ) then begin
    TcxMaskEdit( FEdit ).Properties.ReadOnly := b;
  end
  else if FEdit is TcxImageComboBox then
    TcxImageComboBox( FEdit ).Properties.ReadOnly := b
  else if FEdit is TcxComboBox then
    TcxComboBox( FEdit ).Properties.ReadOnly := b
  else if FEdit is TcxDateEdit then begin
    TcxDateEdit( FEdit ).Properties.ReadOnly := b
  end
  else if FEdit is TcxTimeEdit then begin
    TcxTimeEdit( FEdit ).Properties.ReadOnly := b
  end
  else if FEdit is TcxCheckBox then
    TcxCheckBox( FEdit ).Properties.ReadOnly := b
  else if FEdit is TcxCalcEdit then
    TcxCalcEdit( FEdit ).Properties.ReadOnly := b
  else if FEdit is TcxSpinEdit then
    TcxSpinEdit( FEdit ).Properties.ReadOnly := b
  else if FEdit is TcxMemo then
    TcxMemo( FEdit ).Properties.ReadOnly := b
  else if FEdit is TcxTrackBar then
    TcxTrackBar( FEdit ).Properties.ReadOnly := b
  else if FEdit is TdxColorEdit then
    TdxColorEdit( FEdit ).Properties.ReadOnly := b
  else
    Raise Exception.Create( FEdit.ClassName + ' n�o implementada' );
end;

class function TMapEdit.New(Edit: TcxCalcEdit): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxTimeEdit): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxCheckBox): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxTextEdit): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

procedure TMapEdit.SetValue(Value: Variant);
var
  Mask: TcxMaskEdit;
begin
  if ( FEdit is TcxTextEdit ) or ( ( FEdit is TcxButtonEdit ) and ( TcxButtonEdit( FEdit ).Properties.MaskKind = emkStandard ) ) then
    TcxTextEdit( FEdit ).Text := Value //Json.Item( Name ).AsString
  else if ( FEdit is TcxMaskEdit ) or ( FEdit is TcxButtonEdit ) then begin
    Mask := TcxMaskEdit( FEdit );
    if ( Mask.Properties.EditMask = '' ) or ( FDontClearMask ) then
      Mask.Text := Value
    else if ( Mask.Properties.EditMask = TAppConstantes.MascaraCPF ) or ( Mask.Properties.EditMask = TAppConstantes.MascaraCNPJ ) then
      Mask.Text := TFormat.CpfCnpj( Value )
    else if Mask.Properties.EditMask = TAppConstantes.MascaraCEP then
      Mask.Text := TFormat.Cep( Value )
    else if Mask.Properties.EditMask = TAppConstantes.MascaraCNAE then
      Mask.Text := TFormat.Cnae( Value )
    else if Mask.Properties.EditMask = TAppConstantes.MascaraIP then
      Mask.Text := TFormat.IP( Value )
    else
      Raise Exception.Create( 'Mascara n�o prevista!' );
  end
  else if FEdit is TcxImageComboBox then begin
    TcxImageComboBox( FEdit ).EditValue := VarToStrDef( Value, '' ).ToUpper; // red#1769, pra encontrar cidades com min�sculas;
  end
  else if FEdit is TcxComboBox then
    TcxComboBox( FEdit ).Text := Value
  else if FEdit is TcxDateEdit then begin
    if ( Value > 0 ) then begin
      TcxDateEdit( FEdit ).Date := Value;
    end
    else begin
      TcxDateEdit( FEdit ).Clear; // red#2112, se a data t� vazia, limpa o campo
    end;
  end
  else if FEdit is TcxTimeEdit then begin
    if  ( Value <> '01/01/1970') and ( VarToStr( Value ) <> '' ) then  // RED#2258, tirei a valida��o do <> de 0, na convers�o ia pras treva.
      TcxTimeEdit( FEdit ).EditValue := TFormat.FormattedStrToDateTime( VarToStr( Value ) );
  end
  else if FEdit is TcxCheckBox then
    TcxCheckBox( FEdit ).Checked := Value = True
  else if FEdit is TcxCalcEdit then
    TcxCalcEdit( FEdit ).EditValue := StrToFloatDef( VarToStr( Value ).Replace( '.', ',', [ rfReplaceAll ] ), 0 )
  else if FEdit is TcxSpinEdit then
    TcxSpinEdit( FEdit ).EditValue := Value
  else if FEdit is TcxMemo then begin
    TcxMemo( FEdit ).Lines.Text := Value;
    //CHICO RED#733
    TcxMemo( FEdit ).Lines.Text := TcxMemo( FEdit ).Lines.Text.Replace( '\r\n', #13#10, [ rfReplaceAll ] );
    TcxMemo( FEdit ).Lines.Text := TcxMemo( FEdit ).Lines.Text.Replace( '\\', '\', [rfReplaceAll] );
  end
  else if FEdit is TcxTrackBar then
    TcxTrackBar( FEdit ).Position := Value
  else if FEdit is TdxColorEdit then
    TdxColorEdit( FEdit ).ColorValue := Value
  else
    Raise Exception.Create( FEdit.ClassName + ' n�o implementada' );
end;

class function TMapEdit.New(Edit: TcxTrackBar): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TdxColorEdit): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit );
end;

class function TMapEdit.New(Edit: TcxMaskEdit;
  ADontClearMask: Boolean): IMapCtrl;
begin
  Result := TMapEdit.Create( Edit, False, ADontClearMask );
end;


{ TMapEndereco }

class function TMapEndereco.New(Parent: TComponent; Prefixo: string): IMapCtrls;
begin
  Result := TMapCtrls.New
    .Add( 'cep', TMapEdit.New( TcxButtonEdit( Parent.FindComponent( 'edt' + Prefixo + 'CEP' ) ) ) )
    .Add( 'logradouro', TMapEdit.New( TcxTextEdit( Parent.FindComponent( 'edt' + Prefixo + 'Logradouro' ) ) ) )
    .Add( 'numero', TMapEdit.New( TcxTextEdit( Parent.FindComponent( 'edt' + Prefixo + 'Numero' ) ) ) )
    .Add( 'complemento', TMapEdit.New( TcxTextEdit( Parent.FindComponent( 'edt' + Prefixo + 'Complemento' ) ) ) )
    .Add( 'bairro', TMapEdit.New( TcxTextEdit( Parent.FindComponent( 'edt' + Prefixo + 'Bairro' ) ) ) )
    .Add( 'uf', TMapEdit.New( TcxComboBox( Parent.FindComponent( 'cbb' + Prefixo + 'UF' ) ) ) )
    .Add( 'municipio', TMapEdit.New( TcxComboBox( Parent.FindComponent( 'cbb' + Prefixo + 'Cidade' ) ) ) )
    .Add( 'pais', TMapEdit.New( TcxComboBox( Parent.FindComponent( 'cbb' + Prefixo + 'Pais' ) ) ) )
end;

class function TMapEndereco.New( Parent: TComponent ): IMapCtrls;
begin
  Result := TMapEndereco.New( Parent, '' );
end;

{ TMapJuros }

class function TMapCfgJuros.New(Parent: TComponent): IMapCtrls;
begin
  Result := TMapCfgJuros.New( Parent, '' );
end;

class function TMapCfgJuros.New(Parent: TComponent;
  Prefixo: string): IMapCtrls;
begin
  Result := TMapCtrls.New
    .Add( 'juroMensal', TMapEdit.New( TcxCalcEdit( Parent.FindComponent( 'edt' + Prefixo + 'JuroMensal' ) ) ) )
    .Add( 'tipoJuros', TMapEdit.New( TcxImageComboBox( Parent.FindComponent('cbb' + Prefixo + 'TipoJuros' ) ) ) )
    .Add( 'diasCarenciaJuros', TMapEdit.New( TcxSpinEdit( Parent.FindComponent( 'edt' + Prefixo + 'DiasCarenciaJuros' ) ) ) )
    .Add( 'incluiCarenciaJuros', TMapEdit.New( TcxCheckBox( Parent.FindComponent( 'cbox' + Prefixo + 'IncluiCarenciaJuros' ) ) ) )
    .Add( 'multa', TMapEdit.New( TcxCalcEdit( Parent.FindComponent( 'edt' + Prefixo + 'Multa' ) ) ) )
    .Add( 'cobrarMultaApos', TMapEdit.New( TcxSpinEdit( Parent.FindComponent( 'edt' + Prefixo + 'DiasCarenciaMulta' ) ) ) )
    .Add( 'desconto', TMapEdit.New( TcxCalcEdit( Parent.FindComponent( 'edt' + Prefixo + 'Desconto' ) ) ) )
    .Add( 'descontoDiasAntesVencimento', TMapEdit.New( TcxSpinEdit( Parent.FindComponent( 'edt' + Prefixo + 'DescontoAte' ) ) ) )
    .Add( 'habilitado', TMapEdit.New( TcxCheckBox( Parent.FindComponent( 'cbox' + Prefixo + 'Habilitado' ) ) ) );
end;

end.
