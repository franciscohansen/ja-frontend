inherited frmConsultaFollowUp: TfrmConsultaFollowUp
  Caption = 'Consultar Follow-Up'
  ClientHeight = 379
  OnCreate = FormCreate
  ExplicitWidth = 651
  ExplicitHeight = 418
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseModal: TdxLayoutControl
    Height = 379
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 635
    ExplicitHeight = 379
    object edtDataInicial: TcxDateEdit [0]
      Left = 10
      Top = 28
      Properties.DisplayFormat = 'dd/mm/yyyy'
      Properties.EditFormat = 'dd/mm/yyyy'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 0
      Width = 121
    end
    object edtDataFinal: TcxDateEdit [1]
      Left = 137
      Top = 28
      AutoSize = False
      Properties.DisplayFormat = 'dd/mm/yyyy'
      Properties.EditFormat = 'dd/mm/yyyy'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 1
      Height = 21
      Width = 121
    end
    object Grid: TcxTreeList [2]
      Left = 10
      Top = 55
      Width = 615
      Height = 201
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 3
      OnFocusedNodeChanged = GridFocusedNodeChanged
      object GridData: TcxTreeListColumn
        Caption.Text = 'Data'
        DataBinding.ValueType = 'DateTime'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridMotivo: TcxTreeListColumn
        Caption.Text = 'Motivo'
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridAssunto: TcxTreeListColumn
        Caption.Text = 'Assunto'
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridResumo: TcxTreeListColumn
        PropertiesClassName = 'TcxRichEditProperties'
        Properties.PlainText = True
        Caption.Text = 'Resumo'
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object btnBuscar: TcxButton [3]
      Left = 550
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 2
      OnClick = btnBuscarClick
    end
    object edtResumo: TcxRichEdit [4]
      Left = 10
      Top = 280
      Properties.PlainText = True
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 4
      Height = 89
      Width = 615
    end
    object lciDataInicial: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'Data Inicial'
      CaptionOptions.Layout = clTop
      Control = edtDataInicial
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciDataFinal: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'Data Final'
      CaptionOptions.Layout = clTop
      Control = edtDataFinal
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciGrid: TdxLayoutItem
      Parent = lcgBaseModal
      AlignVert = avClient
      CaptionOptions.Layout = clTop
      Control = Grid
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lciBuscar: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      AlignVert = avBottom
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnBuscar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciResumo: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Resumo'
      CaptionOptions.Layout = clTop
      Control = edtResumo
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 2
    end
  end
end
