unit untBaseCadastro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseGeral, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutControlAdapters, dxLayoutcxEditAdapters, Vcl.Menus,
  cxContainer, cxEdit, cxTextEdit, Vcl.StdCtrls, cxButtons, Json.Interfaces,
  Dao.Interfaces, Forms.Interfaces, View.MappingControls.Interfaces;

type
  TfrmBaseCadastroClass = class of TfrmBaseCadastro;
  TfrmBaseCadastro = class(TfrmBaseGeral, IFormCadastro)
    btnSalvar: TcxButton;
    lciSalvar: TdxLayoutItem;
    btnFechar: TcxButton;
    lciFechar: TdxLayoutItem;
    edtID: TcxTextEdit;
    lciID: TdxLayoutItem;
    lcgContent: TdxLayoutGroup;
    lcgRodape: TdxLayoutGroup;
    procedure btnSalvarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

    FCallback : IListCallback;
    FOpening : Boolean;
    FLoading: Boolean;
    { Private declarations }

    function DoSave : IJson;

    procedure SetOpening(const Value: boolean);
    procedure SetLoading(const Value: Boolean);

  protected
    FJson : IJson;
    procedure SetJson(const Value: IJson); virtual;
    function GetJson : IJson; virtual;
    function Dao : IDao; virtual; abstract;
    function GetMap : IMapCtrls; virtual; abstract;


    function DoValidate : Boolean; virtual;
    function DoBeforeSave( AJson : IJson ) : IJson; virtual;
    function DoAfterSave( AJson : IJson ) : IJson; virtual;

    property Json : IJson read FJson write SetJson;
    property Opening: boolean read FOpening write SetOpening;
    property Loading: Boolean read FLoading write SetLoading;
  public
    { Public declarations }
    function Insert : IFormCadastro;
    function Update( AJson : IJson ) : IFormCadastro;
    function Open( AJson : IJson ) : IFormCadastro;
    function Callback( ACallback : IListCallback ) : IFormCadastro;
  end;


implementation

uses
  View.MappingControls.Base;

{$R *.dfm}

{ TfrmBaseCadastro }

procedure TfrmBaseCadastro.btnFecharClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmBaseCadastro.btnSalvarClick(Sender: TObject);
begin
  inherited;
  DoSave;
  if Assigned( FCallback ) then
    FCallback.DoCallback( FJson );
  Close;
end;

function TfrmBaseCadastro.Callback(ACallback: IListCallback): IFormCadastro;
begin
  Result := Self;
  FCallback := ACallback;
end;

function TfrmBaseCadastro.DoAfterSave(AJson: IJson): IJson;
begin
  Result := AJson;
end;

function TfrmBaseCadastro.DoBeforeSave(AJson: IJson): IJson;
begin
  Result := AJson;
end;

function TfrmBaseCadastro.DoSave: IJson;
begin
  if DoValidate then begin
    FJson := GetJson;
    FJson := DoBeforeSave( FJson );
    FJson := Dao.Save( FJson );
    FJson := DoAfterSave( FJson );
    Result := FJson;
  end;
end;

function TfrmBaseCadastro.DoValidate: Boolean;
begin
  Result := True;
end;

procedure TfrmBaseCadastro.FormCreate(Sender: TObject);
begin
  inherited;
  FOpening := False;
end;

function TfrmBaseCadastro.GetJson: IJson;
begin
  Result := TManipulateCtrls.New( GetMap, FJson ).ToJson;
end;

function TfrmBaseCadastro.Insert: IFormCadastro;
begin
  Loading := True;
  Result := Self;
  Json := Dao.NewRecord;
end;

function TfrmBaseCadastro.Open(AJson: IJson): IFormCadastro;
begin
  Loading := True;
  Result := Self;
  FOpening := True;
  Json := Dao.FindById( AJson.ID );
end;

procedure TfrmBaseCadastro.SetJson(const Value: IJson);
begin
  FJson := Value;
  TManipulateCtrls.New( GetMap, FJson ).ToCtrl( FOpening );
  Loading := False;
end;

procedure TfrmBaseCadastro.SetLoading(const Value: Boolean);
begin
  FLoading := Value;
end;

procedure TfrmBaseCadastro.SetOpening(const Value: boolean);
begin
  FOpening := Value;
  lciSalvar.Visible := not FOpening;
end;

function TfrmBaseCadastro.Update(AJson: IJson): IFormCadastro;
begin
  Loading := True;
  Result  := Self;
  Json   := Dao.FindById( AJson.ID );
end;

end.
