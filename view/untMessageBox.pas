unit untMessageBox;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxGroupBox, dxGDIPlusClasses, Vcl.ExtCtrls, cxTextEdit, cxMemo,
  cxRichEdit, Vcl.ComCtrls;

type
  TfrmMessageBox = class(TForm)
    pnlMsg: TcxGroupBox;
    pnlBotoes: TcxGroupBox;
    btnCancelar: TcxButton;
    btnNao: TcxButton;
    btnSim: TcxButton;
    btnOk: TcxButton;
    mmoMsg: TRichEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mmoMsgEnter(Sender: TObject);
    procedure BotoesClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FClicouBotao: boolean;
    FTexto: string;
    FBotoes: longint;
    FRetorno: longint;
    FFoco: TWinControl;
    procedure SetaTamanhoDaTela;
    procedure CreateParams(var Params: TCreateParams); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent; AMsg: string; ABotoes: longint );
    property Retorno: longint read FRetorno write FRetorno;
  end;

implementation

{$R *.dfm}


{ TfrmMessageBox }

procedure TfrmMessageBox.BotoesClick(Sender: TObject);
begin
  FClicouBotao := true;
  if Sender = btnOk then
    FRetorno := ID_OK
  else if Sender = btnCancelar then
    FRetorno := ID_CANCEL
  else if Sender = btnSim then
    FRetorno := ID_YES
  else if Sender = btnNao then
    FREtorno := ID_NO;
  ModalResult := mrCancel;
end;

constructor TfrmMessageBox.Create(AOwner: TComponent; AMsg: string;
  ABotoes: longint);
begin
  FTexto := AMsg;
  FBotoes := ABotoes;
  inherited Create( AOwner );
end;

procedure TfrmMessageBox.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if Screen.ActiveForm <> nil then
    params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
  PopupMode := pmExplicit;
  PopupParent := self;

end;

procedure TfrmMessageBox.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Botao: TObject;
begin
  if not FClicouBotao then begin
    Botao := nil;
    if btnCancelar.Visible then
      Botao := btnCancelar
    else if btnNao.Visible then
      Botao := btnNao
    else if btnSim.Visible then
      Botao := btnSim
    else if btnOk.Visible then
      Botao := btnOk;

    if Botao <> nil then
      BotoesClick( Botao );
  end;
end;

procedure TfrmMessageBox.FormCreate(Sender: TObject);
var
  iFocoBotao: integer;
  iAux: integer;
begin

  FClicouBotao := false;

  mmoMsg.Color           := pnlMsg.Style.Color;


  Self.Caption := 'H�bil';
  mmoMsg.Text := FTexto;

  if FBotoes >= 528 then begin  // foco no terceiro bot�o
    FBotoes := FBotoes - MB_DEFBUTTON3;
    iFocoBotao := 3;
  end
  else if FBotoes >= 272 then begin // foco no segundo bot�o
    FBotoes := FBotoes - MB_DEFBUTTON2;
    iFocoBotao := 2;
  end;


  if FBotoes <= 20 then begin  // mostra as imagens na tela
    iAux := FBotoes - MB_ICONERROR;
  end
  else if FBotoes <= 36 then begin
    iAux := FBotoes - MB_ICONQUESTION;
  end
  else if FBotoes <= 52 then begin
    iAux := FBotoes - MB_ICONEXCLAMATION;
  end
  else if FBotoes <= 68 then begin
    iAux := FBotoes - MB_ICONINFORMATION;
  end;

  { -------------- mostra os bot�es -------------- }
  case iAux of
    0 : begin  // OK
          btnOk.Left := btnCancelar.Left;
          BtnOk.Visible := true;
        end;
    1 : begin  // OKCANCEL
          btnOk.Left := btnNao.Left;
          BtnOk.Visible := true;
          BtnCancelar.Visible := true;

          BtnOk.TabOrder := 0;
          BtnCancelar.TabOrder := 1;

          if iFocoBotao = 2 then
            FFoco := btnCancelar;
        end;
    3 : begin  // YESNOCANCEL
          BtnSim.Visible := true;
          BtnNao.Visible := true;
          BtnCancelar.Visible := true;

          BtnSim.TabOrder := 0;
          BtnNao.TabOrder := 1;
          BtnCancelar.TabOrder := 2;

          if iFocoBotao = 2 then
            FFoco := btnNao
          else if iFocoBotao = 3 then
            FFoco := btnCancelar;
        end;
    4 : begin   // YESNO
          btnSim.Left := btnNao.Left;
          btnNao.Left := btnCancelar.Left;

          BtnSim.Visible := true;
          BtnNao.Visible := true;

          BtnSim.TabOrder := 0;
          BtnNao.TabOrder := 1;

          if iFocoBotao = 2 then
            FFoco := btnNao;
        end;
  else // padr�o
    btnOk.Left := btnCancelar.Left;
    BtnOk.Visible := true;
  end;
  SetaTamanhoDaTela;
end;

procedure TfrmMessageBox.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then begin
    Close;

  end;
end;

procedure TfrmMessageBox.FormShow(Sender: TObject);
begin
  Application.BringToFront;
  Self.BringToFront;
  if FFoco <> nil then
    FFoco.SetFocus;
  SetWindowPos(self.handle, HWND_TOPMOST, self.Left, self.Top,self.Width, self.Height, 0);
end;

procedure TfrmMessageBox.mmoMsgEnter(Sender: TObject);
var
  OldFocus : TWinControl;
begin
  OldFocus := Self.ActiveControl;
  try
    if TWinControl( OldFocus ).Enabled then begin
      TWinControl( OldFocus ).SetFocus;
      Self.SetFocusedControl( OldFocus );
    end;
  except
  end;

end;

procedure TfrmMessageBox.SetaTamanhoDaTela;
var
  TextWidth,
  TextHeight,
  HorzMargin,
  VertMargin,
  HorzSpacing,
  VertSpacing,
  ButtonSpacing: Integer;
  TextRect: TRect;
  DialogUnits: TPoint;
  ii: Integer;
  iAux: integer;

  function GetAveCharSize( Canvas: TCanvas ): TPoint;
  var
    I: Integer;
    Buffer: array[ 0..51 ] of Char;
  begin
    for I := 0 to 25 do
      Buffer[ I ] := Chr( I + Ord( 'A' ) );
    for I := 0 to 25 do
      Buffer[ I + 26 ] := Chr( I + Ord( 'a' ) );
    GetTextExtentPoint( Canvas.Handle, Buffer, 52, TSize( Result ) );
    Result.X := Result.X div 52;
  end;

begin
  try
    DialogUnits := GetAveCharSize( Canvas );
    HorzMargin := MulDiv( 8, DialogUnits.X, 4 );
    VertMargin := MulDiv( 8, DialogUnits.Y, 8 );
    HorzSpacing := MulDiv( 10, DialogUnits.X, 4 );
    VertSpacing := MulDiv( 10, DialogUnits.Y, 8 );
    ButtonSpacing := MulDiv( 4, DialogUnits.X, 4 );
    iAux := Screen.Width;
    repeat
      SetRect( TextRect, 0, 0, iAux div 2, 0 );
      DrawText( Canvas.Handle, PChar( FTexto ), Length( FTexto ), TextRect, DT_EXPANDTABS or DT_CALCRECT or DT_WORDBREAK );
      TextWidth := TextRect.Right;
      TextHeight := TextRect.Bottom;
      Dec( iAux, 100 );
    until ( TextWidth + 40 + HorzSpacing + ( HorzMargin * 2 ) ) / 1.3 < TextHeight + 28 + VertSpacing + ( VertMargin * 2 );
    ClientWidth := TextWidth + 40 + HorzSpacing + ( HorzMargin * 2 );
    if ClientWidth < 200 then
       ClientWidth := 200;
    if TextHeight < 40 then
      TextHeight := 40;
    ClientHeight := ( mmoMsg.Lines.Count * DialogUnits.Y + 10 ) + 28 + VertSpacing + ( VertMargin * 2 );
    //ClientHeight := ClientHeight + 100;
    ClientWidth := ClientWidth + 50;

    with mmoMsg do begin
      {Properties.}WordWrap := True;
      Top := Top + 10;
      Lines.Text := FTexto;
      Height := Lines.Count * DialogUnits.Y + 10;
    end; { with }
    while true do begin
      for ii := 0 to mmoMSg.Lines.Count do
        if ( Copy( mmoMSg.lines.Strings[ ii ], 1, 1 ) = '!' ) or
           ( Copy( mmoMSg.lines.Strings[ ii ], 1, 1 ) = '?' ) or
           ( Copy( mmoMSg.lines.Strings[ ii ], 1, 1 ) = ' ' ) then begin
          ClientWidth := ClientWidth + DialogUnits.X;
          Continue;
        end;
      Break;
    end;
  except
  end;
end;

end.
