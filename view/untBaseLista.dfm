inherited frmBaseLista: TfrmBaseLista
  Caption = 'frmBaseLista'
  ClientHeight = 360
  ClientWidth = 797
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 813
  ExplicitHeight = 399
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Width = 797
    Height = 360
    ExplicitWidth = 797
    ExplicitHeight = 360
    object GridBase: TcxTreeList [0]
      Left = 10
      Top = 41
      Width = 777
      Height = 278
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.ShowRoot = False
      ScrollbarAnnotations.CustomAnnotations = <>
      Styles.ContentEven = stlEven
      Styles.ContentOdd = stlOdd
      Styles.UseOddEvenStyles = bTrue
      TabOrder = 2
      OnDblClick = GridBaseDblClick
      object GridBase_Json: TcxTreeListColumn
        Visible = False
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_ID: TcxTreeListColumn
        Caption.Text = 'C'#243'digo'
        Options.Sizing = False
        Width = 75
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object btnNovo: TcxButton [1]
      Left = 10
      Top = 325
      Width = 75
      Height = 25
      Caption = 'Novo'
      TabOrder = 3
      OnClick = btnNovoClick
    end
    object btnEditar: TcxButton [2]
      Left = 91
      Top = 325
      Width = 75
      Height = 25
      Caption = 'Editar'
      TabOrder = 4
      OnClick = btnEditarClick
    end
    object btnExcluir: TcxButton [3]
      Left = 253
      Top = 325
      Width = 75
      Height = 25
      Caption = 'Excluir'
      TabOrder = 6
      OnClick = btnExcluirClick
    end
    object edtBusca: TcxTextEdit [4]
      Left = 10
      Top = 10
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
      TextHint = 'Digite a informa'#231#227'o para buscar'
      Width = 696
    end
    object btnBuscar: TcxButton [5]
      Left = 712
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 1
      OnClick = btnBuscarClick
    end
    object btnAnterior: TcxButton [6]
      Left = 631
      Top = 325
      Width = 75
      Height = 25
      Caption = 'Anterior'
      TabOrder = 10
      OnClick = btnAnteriorClick
    end
    object btnProxima: TcxButton [7]
      Left = 712
      Top = 325
      Width = 75
      Height = 25
      Caption = 'Pr'#243'xima'
      TabOrder = 11
      OnClick = btnProximaClick
    end
    object cbbPagina: TcxComboBox [8]
      Left = 536
      Top = 325
      Properties.OnChange = cbbPaginaPropertiesChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 8
      Width = 50
    end
    object btnAbrir: TcxButton [9]
      Left = 172
      Top = 325
      Width = 75
      Height = 25
      Caption = 'Abrir'
      TabOrder = 5
      OnClick = btnAbrirClick
    end
    object btnImprimir: TcxButton [10]
      Left = 334
      Top = 325
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 7
      OnClick = btnImprimirClick
    end
    object lbTotalPaginas: TcxLabel [11]
      Left = 592
      Top = 331
      Caption = 'de 100'
      Style.HotTrack = False
      Style.TransparentBorder = False
      Transparent = True
    end
    inherited lcgRoot: TdxLayoutGroup
      ItemIndex = 2
    end
    object lciBase: TdxLayoutItem
      Parent = lcgRoot
      AlignVert = avClient
      CaptionOptions.Text = 'cxTreeList1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = GridBase
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciNovo: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnNovo
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciEditar: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnEditar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciExcluir: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnExcluir
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgRoot
      LayoutDirection = ldHorizontal
      Index = 2
    end
    object lciBusca: TdxLayoutItem
      Parent = lcg2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Buscar'
      CaptionOptions.Visible = False
      Control = edtBusca
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciBuscar: TdxLayoutItem
      Parent = lcg2
      CaptionOptions.Text = 'Buscar'
      CaptionOptions.Visible = False
      Control = btnBuscar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg2: TdxLayoutAutoCreatedGroup
      Parent = lcgRoot
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lciAnterior: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnAnterior
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 7
    end
    object lciProxima: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnProxima
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 8
    end
    object lciPagina: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'Ir Para P'#225'gina'
      Control = cbbPagina
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object lciAbrir: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnAbrir
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciImprimir: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnImprimir
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciTotalPaginas: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      AlignVert = avCenter
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lbTotalPaginas
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 33
      ControlOptions.ShowBorder = False
      Index = 6
    end
  end
  object stRepoLista: TcxStyleRepository
    PixelsPerInch = 96
    object stlOdd: TcxStyle
    end
    object stlEven: TcxStyle
      AssignedValues = [svColor]
      Color = cl3DLight
    end
  end
end
