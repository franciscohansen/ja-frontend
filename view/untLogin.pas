unit untLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseModal, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutcxEditAdapters, dxLayoutControlAdapters, cxContainer,
  cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxTextEdit, dxGDIPlusClasses,
  Vcl.ExtCtrls, cxMaskEdit, cxDropDownEdit, cxImageComboBox, Json.Interfaces;

type
  TStep = ( stLogin, stEmpresa );
  TfrmLogin = class(TfrmBaseModal)
    edtCnpj: TcxTextEdit;
    lciCnpj: TdxLayoutItem;
    edtUsuario: TcxTextEdit;
    lciUsuario: TdxLayoutItem;
    edtSenha: TcxTextEdit;
    lciSenha: TdxLayoutItem;
    btnOk: TcxButton;
    lciOk: TdxLayoutItem;
    btnFechar: TcxButton;
    lciFechar: TdxLayoutItem;
    btnConfigurar: TcxButton;
    lciConfigurar: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    lcgLogin: TdxLayoutGroup;
    imgLogo: TImage;
    lciLogo: TdxLayoutItem;
    cbbEmpresa: TcxImageComboBox;
    lciEmpresa: TdxLayoutItem;
    procedure btnConfigurarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FStep: TStep;
    procedure SetStep(const Value: TStep);
    { Private declarations }
    property Step : TStep read FStep write SetStep default stLogin;
  public
    class function DoLogin : Boolean;
    { Public declarations }
  end;


implementation

uses
  untConnectionConfig, View.MessageBox, Dao.Login, Cfg, Util.Format,
  Dao.Configuracao, Dao.Base, Util.Lists, Dao.Base.H10, Json.Base;

{$R *.dfm}

{ TfrmLogin }

procedure TfrmLogin.btnConfigurarClick(Sender: TObject);
var
  frm : TfrmConnectionConfig;
begin
  inherited;
  frm := TfrmConnectionConfig.Create( Self );
  try
    frm.ShowModal;
  finally
    frm.Release;
  end;
end;

procedure TfrmLogin.btnFecharClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmLogin.btnOkClick(Sender: TObject);
var
  dao : ILoginDao;
  sCnpj : string;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  try
    dao := TLoginDao.New;
    case Step of
      stLogin: begin
        if ( edtCnpj.Text = '' ) or ( edtUsuario.Text = '' ) or ( edtSenha.Text = '' ) then begin
          TMsgWarning.New( 'Informe todos os dados para o login!' ).Show;
          Abort;
        end;
        sCnpj := TFormat.ClearFormat( Trim( edtCnpj.Text ) );
        try
          if dao.ConectaCNPJ( sCnpj ) and dao.ConectaUsuarioSenha( sCnpj, edtUsuario.Text, edtSenha.Text ) then begin
            Step := stEmpresa;
          end;
        except
          on E:Exception do begin
            TMsgError.New( E.Message ).Show;
          end;
        end;
      end;
      stEmpresa: begin
        if cbbEmpresa.ItemIndex < 0 then begin
          TMsgError.New( 'Selecione a empresa para continuar!' ).Show;
          Abort;
        end;
        TCfg.GetInstance.SetEmpresa( TJson.New( cbbEmpresa.EditValue ) );
        ModalResult := mrOk;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

class function TfrmLogin.DoLogin: Boolean;
var
  frm : TfrmLogin;
begin
  frm := TfrmLogin.Create( nil );
  try
    Result := frm.ShowModal = mrOk;
    if not Result then begin
      TerminateProcess( GetCurrentProcess, 1 );
    end;
  finally
    frm.Release;
  end;
end;

procedure TfrmLogin.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    btnOk.Click;
end;

procedure TfrmLogin.FormShow(Sender: TObject);
var
  cfg : ICfg;
begin
  inherited;
  cfg := TCfg.GetInstance;
  edtCnpj.Text    := cfg.GetIniValueH10( 'Abertura', 'Documento' );
  edtUsuario.Text := cfg.GetIniValueH10( 'Abertura', 'Usuario' );
end;

procedure TfrmLogin.SetStep(const Value: TStep);
var
  l : IList<IJson>;
  j : IJson;
  i : Integer;
begin
  FStep := Value;
  lciCnpj.Visible     := FStep = stLogin;
  lciUsuario.Visible  := FStep = stLogin;
  lciSenha.Visible    := FStep = stLogin;
  lciEmpresa.Visible  := FStep = stEmpresa;
  if Step = stEmpresa then begin
    l := TList<IJson>.New( TDao.New( '/empresa' ).FindAll( 0, 0 ) );
    cbbEmpresa.Properties.BeginUpdate;
    try
      cbbEmpresa.Properties.Items.Clear;
      for i := 0 to l.Count -1 do begin
        j := l.Item( i );
        with cbbEmpresa.Properties.Items.Add do begin
          Description := j.Item( 'nome' ).AsString;
          Value := j.AsJsonString;
        end;
      end;
    finally
      cbbEmpresa.Properties.EndUpdate;
      cbbEmpresa.ItemIndex := 0;
    end;

  end;
end;

end.
