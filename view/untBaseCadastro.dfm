inherited frmBaseCadastro: TfrmBaseCadastro
  Caption = 'frmBaseCadastro'
  ClientHeight = 578
  ClientWidth = 801
  OnCreate = FormCreate
  ExplicitWidth = 817
  ExplicitHeight = 617
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Width = 801
    Height = 578
    ExplicitWidth = 801
    ExplicitHeight = 578
    object btnSalvar: TcxButton [0]
      Left = 635
      Top = 543
      Width = 75
      Height = 25
      Caption = 'Salvar'
      TabOrder = 1
      OnClick = btnSalvarClick
    end
    object btnFechar: TcxButton [1]
      Left = 716
      Top = 543
      Width = 75
      Height = 25
      Caption = 'Fechar'
      TabOrder = 2
      OnClick = btnFecharClick
    end
    object edtID: TcxTextEdit [2]
      Left = 10
      Top = 28
      Properties.ReadOnly = True
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
      Text = 'edtID'
      Width = 121
    end
    object lciSalvar: TdxLayoutItem
      Parent = lcgRodape
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnSalvar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciFechar: TdxLayoutItem
      Parent = lcgRodape
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnFechar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciID: TdxLayoutItem
      Parent = lcgContent
      AlignHorz = ahLeft
      CaptionOptions.Text = 'C'#243'digo'
      CaptionOptions.Layout = clTop
      Control = edtID
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lcgContent: TdxLayoutGroup
      Parent = lcgRoot
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 0
    end
    object lcgRodape: TdxLayoutGroup
      Parent = lcgRoot
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
  end
end
