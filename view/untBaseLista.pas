unit untBaseLista;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseGeral, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutControlAdapters, cxCustomData, cxStyles,
  dxScrollbarAnnotations, cxTL, cxTLdxBarBuiltInMenu, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxInplaceContainer, dxLayoutcxEditAdapters, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, Json.Interfaces, Dao.Interfaces, Util.Lists,
  Http.Interfaces, System.Math, untBaseCadastro, Forms.Interfaces, cxLabel,
  Dao.Report;

type
  TfrmBaseLista = class(TfrmBaseGeral, IListCallback)
    GridBase: TcxTreeList;
    lciBase: TdxLayoutItem;
    btnNovo: TcxButton;
    lciNovo: TdxLayoutItem;
    btnEditar: TcxButton;
    lciEditar: TdxLayoutItem;
    btnExcluir: TcxButton;
    lciExcluir: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    edtBusca: TcxTextEdit;
    lciBusca: TdxLayoutItem;
    btnBuscar: TcxButton;
    lciBuscar: TdxLayoutItem;
    lcg2: TdxLayoutAutoCreatedGroup;
    btnAnterior: TcxButton;
    lciAnterior: TdxLayoutItem;
    btnProxima: TcxButton;
    lciProxima: TdxLayoutItem;
    cbbPagina: TcxComboBox;
    lciPagina: TdxLayoutItem;
    btnAbrir: TcxButton;
    lciAbrir: TdxLayoutItem;
    btnImprimir: TcxButton;
    lciImprimir: TdxLayoutItem;
    GridBase_ID: TcxTreeListColumn;
    GridBase_Json: TcxTreeListColumn;
    lbTotalPaginas: TcxLabel;
    lciTotalPaginas: TdxLayoutItem;
    stRepoLista: TcxStyleRepository;
    stlOdd: TcxStyle;
    stlEven: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure cbbPaginaPropertiesChange(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnProximaClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnAbrirClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure GridBaseDblClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
    _count,
    _pages,
    _current : Integer;
    _content : IMap<Integer, TArray<IJson>>;
    FCurrentPage: TArray<IJson>;
    FTotalPages: Integer;
    procedure SetCurrentPage(const Value: TArray<IJson>);
    procedure SetTotalPages(const Value: Integer);
    property CurrentPage : TArray<IJson> read FCurrentPage write SetCurrentPage;
    property TotalPages : Integer read FTotalPages write SetTotalPages;
    procedure Next;
    procedure Previous;
    procedure GoToPage( APage : Integer; AReload : Boolean =False  );
    procedure IniciaBusca;
    procedure MontaRodape;

  protected
    procedure JsonToNode( AJson : IJson; ANode : TcxTreeListNode ); virtual;
    function Dao : IDao; virtual; abstract;
    function GetParams : IUrlParams; virtual;
    function FormInstance : IFormCadastro; virtual; abstract;
    function FindAll( ASearch : string; AStart, ALength : Integer; AParams : IURlParams ) :TArray<IJson>; virtual;
    function RecordCount( ASearch : string; AParams : IUrlParams ) : Integer; virtual;
    function ReportID : Integer; virtual;
    function ContextoImpressao : TContextoImpressao; virtual;
  public
    { Public declarations }
    function DoCallback( AJson : IJson ) : IListCallBack;
  end;


implementation
uses
  Cfg.Constantes, Json.Base, Http.Base, untSenhaAdmin, View.MessageBox,
  untImprimir;

{$R *.dfm}

{ TfrmBaseLista }

procedure TfrmBaseLista.btnAbrirClick(Sender: TObject);
begin
  inherited;
  if Assigned( GridBase.FocusedNode ) then begin
    FormInstance.Callback(Self).Open( TJson.New( GridBase.FocusedNode.Values[ GridBase_JSon.ItemIndex] ) );
  end;
end;

procedure TfrmBaseLista.btnAnteriorClick(Sender: TObject);
begin
  inherited;
  Previous;
end;

procedure TfrmBaseLista.btnBuscarClick(Sender: TObject);
begin
  inherited;
  _content.Clear;
  IniciaBusca;
end;

procedure TfrmBaseLista.btnEditarClick(Sender: TObject);
begin
  inherited;
  if Assigned( GridBase.FocusedNode ) then begin
    FormInstance.Callback(Self)
    .Update(
        TJson.New( GridBase.FocusedNode.Values[ GridBase_JSon.ItemIndex] )
    );
  end;
end;

procedure TfrmBaseLista.btnExcluirClick(Sender: TObject);
begin
  inherited;

  if not TfrmSenhaAdmin.PedeAdmin then begin
    TMsgWarning.New( 'Voc� n�o tem permiss�o para efetuar essa opera��o!' ).Show;
    Abort;
  end;
  if not Assigned( GridBase.FocusedNode ) then begin
    TMsgWarning.New( 'Selecione um registro para continuar!' ).Show;
    Abort;
  end;
  if Dao.Delete( GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ] ) then
    GoToPage( _current, True );
end;

procedure TfrmBaseLista.btnImprimirClick(Sender: TObject);
begin
  inherited;
  if not Assigned( GridBase.FocusedNode ) then begin
    TMsgError.New( 'Selecione um item para continuar!' ).Show;
    Abort;
  end;
  TfrmImprimir.Imprimir( GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ], ContextoImpressao );
end;

procedure TfrmBaseLista.btnNovoClick(Sender: TObject);
begin
  inherited;
  FormInstance.Callback( Self ).Insert;
end;

procedure TfrmBaseLista.btnProximaClick(Sender: TObject);
begin
  inherited;
  Next;
end;

procedure TfrmBaseLista.cbbPaginaPropertiesChange(Sender: TObject);
var
  iPagina : Integer;
begin
  inherited;
  iPagina := StrToIntDef( cbbPagina.EditValue, 0 );
  if iPagina > 0 then
    iPagina := iPagina -1;
  _current := iPagina;
  GoToPage( _current );
end;

function TfrmBaseLista.ContextoImpressao: TContextoImpressao;
begin
  Result := cxtVendas;
end;

function TfrmBaseLista.DoCallback(AJson: IJson): IListCallBack;
begin
  Result := Self;
end;

function TfrmBaseLista.FindAll(ASearch: string; AStart, ALength: Integer;
  AParams: IURlParams): TArray<IJson>;
begin
  Result := Dao.FindAll( ASearch, AStart, ALength, AParams );
end;

procedure TfrmBaseLista.FormCreate(Sender: TObject);
begin
  inherited;
  _content := TMap<Integer, TArray<IJson>>.New;
end;

procedure TfrmBaseLista.FormShow(Sender: TObject);
begin
  inherited;
  IniciaBusca;
end;

function TfrmBaseLista.GetParams: IUrlParams;
begin
  Result := TUrlParams.New;
end;

procedure TfrmBaseLista.GoToPage(APage: Integer; AReload : Boolean);
begin
  Screen.Cursor := crHourGlass;
  try
    if _content.ContainsKey( APage ) and ( not AReload ) then begin
      CurrentPage := _content.Item( APage );
    end
    else begin
      CurrentPage := FindAll( edtBusca.Text, APage, TAppConstantes.P_SIZE, GetParams );
      if _content.ContainsKey( APage ) then
        _content.Delete( APage );
      _content.Add( APage, CurrentPage );
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmBaseLista.GridBaseDblClick(Sender: TObject);
begin
  inherited;
  if Assigned( GridBase.FocusedNode ) then
    btnEditar.Click;
end;

procedure TfrmBaseLista.IniciaBusca;
var
  dPages : Real;
begin
  Screen.Cursor := crHourGlass;
  try
    _count := RecordCount( edtBusca.Text, GetParams );
    if _count > 0 then begin
      dPages := _count / TAppConstantes.P_SIZE;
      _pages := Floor( dPages );

      if ( dPages - _pages ) > 0 then
        Inc( _pages );
      _current := 0;
      TotalPages := _pages;
      GoToPage( 0 );
    end;
    MontaRodape;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmBaseLista.JsonToNode(AJson: IJson; ANode : TcxTreeListNode);
begin
  ANode.Values[ GridBase_Json.ItemIndex ] := AJson.AsJsonString;
  ANode.Values[ GridBase_ID.ItemIndex   ] := AJson.ID;
end;

procedure TfrmBaseLista.MontaRodape;
var
  i : Integer;
begin
  cbbPagina.Properties.BeginUpdate;
  try
    cbbPagina.Properties.Items.Clear;
    for i := 1 to _pages do begin
      cbbPagina.Properties.Items.Add( IntToStr( i ) );
    end;
  finally
    cbbPagina.Properties.EndUpdate;
    cbbPagina.EditValue := IntToStr( _current + 1 );
  end;
end;

procedure TfrmBaseLista.Next;
begin
  if _pages <= 0 then
    Exit;

  if ( _current + 1 ) < _pages then begin
    Inc( _current );
    cbbPagina.EditValue := _current + 1;
    //GoToPage( _current );
  end;
end;

procedure TfrmBaseLista.Previous;
begin
  if _pages <= 0 then
    Exit;
  if ( _current - 1 ) >= 0 then begin
    Inc( _current, -1 );
    cbbPagina.EditValue := _current + 1;
    //GoToPage( _current );
  end;
end;

function TfrmBaseLista.RecordCount(ASearch: string;
  AParams: IUrlParams): Integer;
begin
  Result := Dao.RecordCount( ASearch, AParams );
end;

function TfrmBaseLista.ReportID: Integer;
begin
  Result := 0;
end;

procedure TfrmBaseLista.SetCurrentPage(const Value: TArray<IJson>);
var
  j : IJson;
  n : TcxTreeListNode;
begin
  FCurrentPage := Value;
  try
    GridBase.BeginUpdate;
    GridBase.Clear;
    for j in FCurrentPage do begin
      n := GridBase.Add;
      JsonToNode(j, n);
    end;
  finally
    GridBase.EndUpdate;
  end;
end;

procedure TfrmBaseLista.SetTotalPages(const Value: Integer);
begin
  FTotalPages := Value;
  lbTotalPaginas.Caption := ' de ' + IntToStr( Value );
end;

end.
