inherited frmCad_OficialJustica: TfrmCad_OficialJustica
  Caption = 'Oficial de Justi'#231'a'
  ClientHeight = 181
  ExplicitHeight = 220
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Height = 181
    ExplicitHeight = 181
    inherited btnSalvar: TcxButton
      Top = 146
      TabOrder = 5
      ExplicitTop = 146
    end
    inherited btnFechar: TcxButton
      Top = 146
      TabOrder = 6
      ExplicitTop = 146
    end
    object edtNome: TcxTextEdit [3]
      Left = 137
      Top = 28
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Text = 'edtNome'
      Width = 654
    end
    object edtFone: TcxTextEdit [4]
      Left = 10
      Top = 73
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 2
      Text = 'edtFone'
      Width = 388
    end
    object edtEmail: TcxTextEdit [5]
      Left = 404
      Top = 73
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 3
      Text = 'edtEmail'
      Width = 387
    end
    object edtForum: TcxButtonEdit [6]
      Left = 10
      Top = 118
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 4
      Text = 'edtForum'
      Width = 781
    end
    inherited lciID: TdxLayoutItem
      Parent = lcg1
    end
    inherited lcgContent: TdxLayoutGroup
      ItemIndex = 1
    end
    object lciNome: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Nome'
      CaptionOptions.Layout = clTop
      Control = edtNome
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciFone: TdxLayoutItem
      Parent = lcg2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Telefone'
      CaptionOptions.Layout = clTop
      Control = edtFone
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciEmail: TdxLayoutItem
      Parent = lcg2
      AlignHorz = ahClient
      CaptionOptions.Text = 'E-mail'
      CaptionOptions.Layout = clTop
      Control = edtEmail
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciForum: TdxLayoutItem
      Parent = lcgContent
      CaptionOptions.Text = 'F'#243'rum'
      CaptionOptions.Layout = clTop
      Control = edtForum
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgContent
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg2: TdxLayoutAutoCreatedGroup
      Parent = lcgContent
      LayoutDirection = ldHorizontal
      Index = 1
    end
  end
end
