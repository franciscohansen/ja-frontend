inherited frmNewAcompanhamento: TfrmNewAcompanhamento
  Caption = 'Lan'#231'ar Acompanhamento'
  OnCreate = FormCreate
  ExplicitWidth = 651
  ExplicitHeight = 338
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseModal: TdxLayoutControl
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 635
    ExplicitHeight = 299
    object cbbMotivo: TcxComboBox [0]
      Left = 10
      Top = 28
      Properties.CharCase = ecUpperCase
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 0
      Width = 615
    end
    object mmoResumo: TcxMemo [1]
      Left = 10
      Top = 73
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Height = 185
      Width = 615
    end
    object btnOk: TcxButton [2]
      Left = 469
      Top = 264
      Width = 75
      Height = 25
      Caption = 'OK'
      TabOrder = 2
      OnClick = btnOkClick
    end
    object btnFechar: TcxButton [3]
      Left = 550
      Top = 264
      Width = 75
      Height = 25
      Caption = 'Fechar'
      TabOrder = 3
      OnClick = btnFecharClick
    end
    inherited lcgBaseModal: TdxLayoutGroup
      ItemIndex = 2
    end
    object lciMotivo: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Motivo'
      CaptionOptions.Layout = clTop
      Control = cbbMotivo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciResumo: TdxLayoutItem
      Parent = lcgBaseModal
      AlignVert = avClient
      CaptionOptions.Text = 'Resumo'
      CaptionOptions.Layout = clTop
      Control = mmoResumo
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciOk: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnOk
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciFechar: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnFechar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      LayoutDirection = ldHorizontal
      Index = 2
    end
  end
end
