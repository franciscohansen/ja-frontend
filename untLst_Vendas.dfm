inherited frmLst_Vendas: TfrmLst_Vendas
  Caption = 'Vendas'
  ClientWidth = 832
  ExplicitWidth = 848
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Width = 832
    ExplicitWidth = 832
    inherited GridBase: TcxTreeList
      Width = 891
      Height = 261
      TabOrder = 3
      ExplicitWidth = 891
      ExplicitHeight = 261
      object GridBase_Cliente: TcxTreeListColumn
        Caption.Text = 'Cliente'
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Data: TcxTreeListColumn
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mm/yyyy'
        Properties.EditFormat = 'dd/mm/yyyy'
        Caption.Text = 'Data'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Inativo: TcxTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Visible = False
        Caption.Text = 'Inativo'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    inherited btnNovo: TcxButton
      Top = 308
      TabOrder = 4
      ExplicitTop = 308
    end
    inherited btnEditar: TcxButton
      Top = 308
      TabOrder = 5
      ExplicitTop = 308
    end
    inherited btnExcluir: TcxButton
      Top = 308
      TabOrder = 7
      ExplicitTop = 308
    end
    inherited edtBusca: TcxTextEdit
      Left = 137
      Top = 14
      TabOrder = 1
      ExplicitLeft = 137
      ExplicitTop = 14
      ExplicitWidth = 683
      Width = 683
    end
    inherited btnBuscar: TcxButton
      Left = 826
      TabOrder = 2
      ExplicitLeft = 826
    end
    inherited btnAnterior: TcxButton
      Left = 745
      Top = 308
      TabOrder = 13
      ExplicitLeft = 745
      ExplicitTop = 308
    end
    inherited btnProxima: TcxButton
      Left = 826
      Top = 308
      TabOrder = 14
      ExplicitLeft = 826
      ExplicitTop = 308
    end
    inherited cbbPagina: TcxComboBox
      Left = 650
      Top = 308
      TabOrder = 11
      ExplicitLeft = 650
      ExplicitTop = 308
    end
    inherited btnAbrir: TcxButton
      Top = 308
      TabOrder = 6
      ExplicitTop = 308
    end
    inherited btnImprimir: TcxButton
      Top = 308
      TabOrder = 8
      ExplicitTop = 308
    end
    inherited lbTotalPaginas: TcxLabel
      Left = 706
      Top = 314
      ExplicitLeft = 706
      ExplicitTop = 314
    end
    object cbbFiltro: TcxImageComboBox [12]
      Left = 10
      Top = 10
      EditValue = 'TODOS'
      Properties.Items = <
        item
          Description = 'Todas'
          ImageIndex = 0
          Value = 'TODOS'
        end
        item
          Description = 'Simula'#231#245'es'
          Value = 'PENDENTE'
        end
        item
          Description = 'Vendas'
          Value = 'VENDIDO'
        end
        item
          Description = 'Arquivo'
          Value = 'ARQUIVADO'
        end
        item
          Description = 'Canceladas'
          Value = 'CANCELADO'
        end>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 0
      Width = 121
    end
    object btnInativar: TcxButton [13]
      Left = 415
      Top = 308
      Width = 75
      Height = 25
      Caption = 'Inativar'
      TabOrder = 9
      OnClick = btnInativarClick
    end
    object btnCancelar: TcxButton [14]
      Left = 496
      Top = 308
      Width = 75
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 10
    end
    inherited lciBusca: TdxLayoutItem
      AlignVert = avBottom
      Index = 1
    end
    inherited lciBuscar: TdxLayoutItem
      AlignVert = avBottom
      Index = 2
    end
    inherited lciAnterior: TdxLayoutItem
      Index = 9
    end
    inherited lciProxima: TdxLayoutItem
      Index = 10
    end
    inherited lciPagina: TdxLayoutItem
      Index = 7
    end
    inherited lciTotalPaginas: TdxLayoutItem
      Index = 8
    end
    object lciFiltro: TdxLayoutItem
      Parent = lcg2
      CaptionOptions.Text = 'cxImageComboBox1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = cbbFiltro
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciInativar: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnInativar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCancelar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 6
    end
  end
  inherited stRepoLista: TcxStyleRepository
    PixelsPerInch = 96
  end
end
