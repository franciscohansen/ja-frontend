unit untCad_Vendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseCadastro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxContainer, cxEdit,
  dxLayoutControlAdapters, dxLayoutcxEditAdapters, dxLayoutContainer, cxClasses,
  cxTextEdit, Vcl.StdCtrls, cxButtons, dxLayoutControl, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxButtonEdit, cxCheckBox, cxDropDownEdit, cxCalendar, cxMaskEdit,
  cxImageComboBox, cxCustomData, cxStyles, dxScrollbarAnnotations, cxTL,
  cxTLdxBarBuiltInMenu, View.CreateControls.Interfaces, cxInplaceContainer,
  View.MappingControls.Edits,
  View.MappingControls.Interfaces, Dao.Interfaces, Json.Interfaces, cxMemo,
  cxCalc, cxSpinEdit, System.DateUtils, View.Search, cxLabel, System.StrUtils,
  Cfg.Constantes;

type
  TfrmCad_Vendas = class(TfrmBaseCadastro)
    cbbIndicacao: TcxImageComboBox;
    lciIndicacao: TdxLayoutItem;
    edtDataLcto: TcxDateEdit;
    lciDataLcto: TdxLayoutItem;
    cboxComProcurador: TcxCheckBox;
    lciComProcurador: TdxLayoutItem;
    edtIndicador: TcxButtonEdit;
    lciIndicador: TdxLayoutItem;
    edtIndicadorNome: TcxTextEdit;
    lciIndicadorNome: TdxLayoutItem;
    edtIndicadorRg: TcxTextEdit;
    lciIndicadoRg: TdxLayoutItem;
    cbbTipoContrato: TcxImageComboBox;
    lciTipoContrato: TdxLayoutItem;
    edtVendedor: TcxButtonEdit;
    lciVendedor: TdxLayoutItem;
    cboxNovoCliente: TcxCheckBox;
    lciNovoCliente: TdxLayoutItem;
    edtCliente: TcxButtonEdit;
    lciCliente: TdxLayoutItem;
    edtNomeCliente: TcxTextEdit;
    lciNomeCliente: TdxLayoutItem;
    edtClienteRazao: TcxTextEdit;
    lciClienteRazao: TdxLayoutItem;
    edtClienteRg: TcxTextEdit;
    lciClienteRg: TdxLayoutItem;
    cboxClienteJuridico: TcxCheckBox;
    lciClienteJuridico: TdxLayoutItem;
    edtClienteNascimento: TcxDateEdit;
    lciClienteNascimento: TdxLayoutItem;
    edtClienteEmail: TcxTextEdit;
    lciClienteEmail: TdxLayoutItem;
    cbbClienteEstadoCivil: TcxImageComboBox;
    lciClienteEstadoCivil: TdxLayoutItem;
    edtClienteNaturalidade: TcxTextEdit;
    lciClienteNaturalidade: TdxLayoutItem;
    cbbClienteSexo: TcxImageComboBox;
    lciClienteSexo: TdxLayoutItem;
    edtClienteProfissao: TcxButtonEdit;
    lciClienteProfissao: TdxLayoutItem;
    edtClienteNomePai: TcxTextEdit;
    lciClienteNomePai: TdxLayoutItem;
    edtClienteNomeMae: TcxTextEdit;
    lciClienteNomeMae: TdxLayoutItem;
    GridC_Fones: TcxTreeList;
    lciC_Fones: TdxLayoutItem;
    lcgTabs: TdxLayoutGroup;
    lcgDadosGerais: TdxLayoutGroup;
    lcg1: TdxLayoutAutoCreatedGroup;
    lcg2: TdxLayoutAutoCreatedGroup;
    lcg4: TdxLayoutAutoCreatedGroup;
    lcgCliente: TdxLayoutGroup;
    lcg5: TdxLayoutAutoCreatedGroup;
    lcg6: TdxLayoutAutoCreatedGroup;
    lcgEnderecos: TdxLayoutGroup;
    btnCopiarEndereco: TcxButton;
    lciCopiarEndereco: TdxLayoutItem;
    edtBanco: TcxButtonEdit;
    lciBanco: TdxLayoutItem;
    edtBancoPerc: TcxCalcEdit;
    lciBancoPerc: TdxLayoutItem;
    edtBancoPrazo: TcxTextEdit;
    lciBancoPrazo: TdxLayoutItem;
    edtValorParcela: TcxCalcEdit;
    lciValorParcela: TdxLayoutItem;
    edtTotalParcelas: TcxCalcEdit;
    lciTotalParcelas: TdxLayoutItem;
    edtValorFinanciado: TcxCalcEdit;
    lciValorFinanciado: TdxLayoutItem;
    edtParcelasPagas: TcxCalcEdit;
    lciParcelasPagas: TdxLayoutItem;
    edtParcelasAtraso: TcxCalcEdit;
    lciParcelasAtraso: TdxLayoutItem;
    edtParcelasRestantes: TcxCalcEdit;
    lciParcelasRestantes: TdxLayoutItem;
    edtSaldoPagar: TcxCalcEdit;
    lciSaldoPagar: TdxLayoutItem;
    edtR_ParcelaReduzida: TcxCalcEdit;
    lciR_ParcelaReduzida: TdxLayoutItem;
    edtR_Saldo: TcxCalcEdit;
    lciR_Saldo: TdxLayoutItem;
    edtR_Economia: TcxCalcEdit;
    lciR_Economia: TdxLayoutItem;
    edtR_EconomiaTotal: TcxCalcEdit;
    lciR_EconomiaTotal: TdxLayoutItem;
    edtP_Nome: TcxTextEdit;
    lciP_Nome: TdxLayoutItem;
    edtP_Nacionalidade: TcxTextEdit;
    lciP_Nacionalidade: TdxLayoutItem;
    edtP_Naturalidade: TcxTextEdit;
    lciP_Naturalidade: TdxLayoutItem;
    edtP_Profissao: TcxTextEdit;
    lciP_Profissao: TdxLayoutItem;
    edtP_RG: TcxTextEdit;
    lciP_RG: TdxLayoutItem;
    edtP_CPF: TcxTextEdit;
    lciP_CPF: TdxLayoutItem;
    edtP_Filiacao: TcxTextEdit;
    lciP_Filiacao: TdxLayoutItem;
    edtP_Email: TcxTextEdit;
    lciP_Email: TdxLayoutItem;
    edtP_Telefone: TcxTextEdit;
    lciP_Telefone: TdxLayoutItem;
    btnDA_Buscar: TcxButton;
    lciDA_Buscar: TdxLayoutItem;
    edtCond_Nome: TcxTextEdit;
    lciCond_Nome: TdxLayoutItem;
    edtCond_Cpf: TcxTextEdit;
    lciCond_Cpf: TdxLayoutItem;
    edtCond_Cnh: TcxTextEdit;
    lciCond_Cnh: TdxLayoutItem;
    cbbVeicMarca: TcxComboBox;
    lciVeicMarca: TdxLayoutItem;
    cbbVeicModelo: TcxComboBox;
    lciVeicModelo: TdxLayoutItem;
    edtVeicCor: TcxTextEdit;
    lciVeicCor: TdxLayoutItem;
    edtVeicAno: TcxTextEdit;
    lciVeicAno: TdxLayoutItem;
    edtVeicAnoModelo: TcxTextEdit;
    lciVeicAnoModelo: TdxLayoutItem;
    edtVeicPlaca: TcxTextEdit;
    lciVeicPlaca: TdxLayoutItem;
    edtVeicRenavam: TcxTextEdit;
    lciVeicRenavam: TdxLayoutItem;
    edtVeicChassi: TcxTextEdit;
    lciVeicChassi: TdxLayoutItem;
    edtVeicCrlv: TcxTextEdit;
    lciVeicCrlv: TdxLayoutItem;
    edtCartUltimaFatura: TcxDateEdit;
    lciCartUltimaFatura: TdxLayoutItem;
    edtCartBandeira: TcxTextEdit;
    lciCartBandeira: TdxLayoutItem;
    edtCartContrato: TcxTextEdit;
    lciCartContrato: TdxLayoutItem;
    edtEmpContrato: TcxTextEdit;
    lciEmpContrato: TdxLayoutItem;
    mmoDA_Obs: TcxMemo;
    lciDA_Obs: TdxLayoutItem;
    edtDataPrimeiraPrestacao: TcxDateEdit;
    lciDataPrimeiraPrestacao: TdxLayoutItem;
    edtDiasEntreParcelas: TcxSpinEdit;
    lciDiasEntreParcelas: TdxLayoutItem;
    btnGerarParcelas: TcxButton;
    lciGerarParcelas: TdxLayoutItem;
    edtMesesQuitacao: TcxSpinEdit;
    lciMesesQuitacao: TdxLayoutItem;
    edtPrevisaoQuitacao: TcxDateEdit;
    lciPrevisaoQuitacao: TdxLayoutItem;
    GridParcelas: TcxTreeList;
    lciParcelas: TdxLayoutItem;
    GridP_Documento: TcxTreeListColumn;
    GridP_Vencimento: TcxTreeListColumn;
    GridP_Valor: TcxTreeListColumn;
    edtValorComissao: TcxCalcEdit;
    lciValorComissao: TdxLayoutItem;
    edtIBanco: TcxTextEdit;
    lciIBanco: TdxLayoutItem;
    edtIAgencia: TcxTextEdit;
    lciIAgencia: TdxLayoutItem;
    edtIConta: TcxTextEdit;
    lciIconta: TdxLayoutItem;
    GridDocumentos: TcxTreeList;
    lciDocumentos: TdxLayoutItem;
    GridD_Entregue: TcxTreeListColumn;
    GridD_Descricao: TcxTreeListColumn;
    edtAdesaoValor: TcxCalcEdit;
    lciAdesaoValor: TdxLayoutItem;
    edtAdesaoDiasParcelas: TcxSpinEdit;
    lciAdesaoDiasParcelas: TdxLayoutItem;
    edtAdesaoNroParcelas: TcxSpinEdit;
    lciAdesaoNroParcelas: TdxLayoutItem;
    btnAdesaoGerar: TcxButton;
    lciAdesaoGerar: TdxLayoutItem;
    GridAdesao: TcxTreeList;
    lciAdesao: TdxLayoutItem;
    GridA_Vencimento: TcxTreeListColumn;
    GridA_Documento: TcxTreeListColumn;
    GridA_Valor: TcxTreeListColumn;
    lcgEndereco: TdxLayoutGroup;
    lcgEnderecoCarne: TdxLayoutGroup;
    lcgSimulacao: TdxLayoutGroup;
    lcg7: TdxLayoutAutoCreatedGroup;
    lcg10: TdxLayoutAutoCreatedGroup;
    lcg11: TdxLayoutAutoCreatedGroup;
    lcgValoresFinanciamento: TdxLayoutGroup;
    lcg8: TdxLayoutAutoCreatedGroup;
    lcg9: TdxLayoutAutoCreatedGroup;
    lcgProcurador: TdxLayoutGroup;
    lcgP_Endereco: TdxLayoutGroup;
    lcgDadosAdicionais: TdxLayoutGroup;
    lcgParcelas: TdxLayoutGroup;
    lcgIndicacao: TdxLayoutGroup;
    lcgDocumentos: TdxLayoutGroup;
    lcgAdesao: TdxLayoutGroup;
    dxLayoutAutoCreatedGroup1: TdxLayoutAutoCreatedGroup;
    lcg13: TdxLayoutAutoCreatedGroup;
    lcg12: TdxLayoutAutoCreatedGroup;
    lcg14: TdxLayoutAutoCreatedGroup;
    lcgVeiculo: TdxLayoutGroup;
    lcgCartao: TdxLayoutGroup;
    lcgEmprestimo: TdxLayoutGroup;
    lcgCondutor: TdxLayoutGroup;
    lcg15: TdxLayoutAutoCreatedGroup;
    GridP_Json: TcxTreeListColumn;
    GridA_Json: TcxTreeListColumn;
    lcgDadosAdesao: TdxLayoutGroup;
    edtAdesaoPrimeiroVcto: TcxDateEdit;
    lciAdesaoPrimeiroVcto: TdxLayoutItem;
    edtR_NroParcelas: TcxSpinEdit;
    lciR_NroParcelas: TdxLayoutItem;
    GridD_Json: TcxTreeListColumn;
    btnGerarVenda: TcxButton;
    lciGerarVenda: TdxLayoutItem;
    btnGerarVendaSemAdesao: TcxButton;
    lciGerarVendaSemAdesao: TdxLayoutItem;
    lcg16: TdxLayoutAutoCreatedGroup;
    lbSituacao: TcxLabel;
    lciSituacao: TdxLayoutItem;
    GridF_Json: TcxTreeListColumn;
    GridF_Padrao: TcxTreeListColumn;
    GridF_Tipo: TcxTreeListColumn;
    GridF_DDD: TcxTreeListColumn;
    GridF_Fone: TcxTreeListColumn;
    GridF_Ramal: TcxTreeListColumn;
    GridF_Observacoes: TcxTreeListColumn;
    btnGerarVendaAgora: TcxButton;
    lciGerarVendaAgora: TdxLayoutItem;
    lcgIndicador: TdxLayoutGroup;
    edtIndicadorCpf: TcxMaskEdit;
    lciIndicadorCpf: TdxLayoutItem;
    edtClienteCpf: TcxMaskEdit;
    lciClienteCpf: TdxLayoutItem;
    edtPix: TcxTextEdit;
    lciPix: TdxLayoutItem;
    procedure FormCreate(Sender: TObject);
    procedure btnCopiarEnderecoClick(Sender: TObject);
    procedure btnGerarParcelasClick(Sender: TObject);
    procedure btnAdesaoGerarClick(Sender: TObject);
    procedure FinanciamentoChange(Sender: TObject);
    procedure RENChange(Sender: TObject);
    procedure btnGerarVendaClick(Sender: TObject);
    procedure btnGerarVendaAgoraClick(Sender: TObject);
    procedure edtR_NroParcelasPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cboxComProcuradorClick(Sender: TObject);
    procedure cbbIndicacaoPropertiesChange(Sender: TObject);
    procedure cboxClienteJuridicoClick(Sender: TObject);
    procedure cboxNovoClienteClick(Sender: TObject);
    procedure cbbTipoContratoPropertiesChange(Sender: TObject);
    procedure btnDA_BuscarClick(Sender: TObject);
    procedure edtMesesQuitacaoExit(Sender: TObject);
    procedure edtClienteCpfPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure edtIndicadorCpfPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
  private
    { Private declarations }
    FCtrls : ICreateControls;
    FMap : IMapCtrls;
    FChangingREN : Boolean;
    FPCliente,
    FPIndicador,
    FPVendedor,
    FPProfissao,
    FPBanco,
    FPEndereco,
    FPEnderecoCarne,
    FPEnderecoProcurador : IPesquisa;
    FSimulacaoOnly,
    FCalculando,
    FSettingJson: Boolean;
    function GetMapCliente : IMapCtrls;
    function GetMapIndicacao : IMapCtrls;
    function GetMapAdesao : IMapCtrls;
    function GetMapDadosAdicionais : IMapCtrls;
    function GetMapSimulacao: IMapCtrls;
    function GetMapProcurador : IMapCtrls;

    procedure MontaParcelas;
    procedure MontaAdesao;
    procedure MontaDocumentos;
    procedure MontaTelefones( AList : TArray<IJson> = []; ClienteChange: boolean = false );
    procedure SimCalculaEconomia;


    function GetPesquisaCliente   : IPesquisa;
    function GetPesquisaIndicador : IPesquisa;
    function GetPesquisaVendedor  : IPesquisa;
    function GetPesquisaProfissao : IPesquisa;
    function GetPesquisaBanco     : IPesquisa;
    function GetPesquisaEndereco : IPesquisa;
    function GetPesquisaEnderecoCarne : IPesquisa;
    function GetPesquisaEnderecoProcurador : IPesquisa;
    procedure InicializaPesquisas;
    procedure PreencheCombos;

    procedure AfterSearchCliente( AJson : IJson );
    procedure AfterSearchIndicador( AJson : IJson );

    function ValidateForVenda : Boolean;
    procedure SetSimulacaoOnly(const Value: Boolean);
  protected
    function GetJson : IJson; override;
    function Dao : IDao; override;
    function GetMap : IMapCtrls; override;
    procedure SetJson(const Value: IJson); override;
  public
    property SimulacaoOnly : Boolean read FSimulacaoOnly write SetSimulacaoOnly;
    { Public declarations }
  end;


implementation

uses
   View.CreateControls.Base, Json.Base, Dao.Base, View.MappingControls.Base,
  Util.Lists, Dao.Simulacao, Model.Enums.JA, Util.Format, View.MessageBox,
  untSenhaAdmin, Model.Enums, Dao.Cliente;

{$R *.dfm}

procedure TfrmCad_Vendas.AfterSearchCliente(AJson: IJson);
var
  j : IJson;
  fones : TArray<IJson>;
begin
  if FSettingJson then
    Exit;
  if Assigned( AJson ) then begin
    if FSettingJson then
      j := FJson.Item( 'dadosCliente' ).AsJson
    else
      j := GetJson.Item( 'dadosCliente' ).AsJson;
    j .AddOrSetJson( 'cliente', AJson )
      .AddOrSetString( 'nome', AJson.Item( 'nome' ).AsString )
      .AddOrSetString( 'razaoSocial', AJson.Item( 'razaoSocial' ).AsString )
      .AddOrSetString( 'cpfCnpj', AJson.Item( 'cpfCnpj' ).AsString )
      .AddOrSetString( 'rgIe', AJson.Item( 'rgIe' ).AsString )
      .AddOrSetString( 'email', AJson.Item( 'email' ).AsString )
      .AddOrSetString( 'estadoCivil', AJson.Item( 'estadoCivil' ).AsString )
      .AddOrSetString( 'naturalidade', AJson.Item( 'naturalidade' ).AsString )
      .AddOrSetString( 'nomePai', AJson.Item( 'nomePai' ).AsString )
      .AddOrSetString( 'nomeMae', AJson.Item( 'nomeMae' ).AsString )
      .AddOrSetDate( 'dataNascimento', AJson.Item( 'dataNascimento' ).AsDateTime )
      .AddOrSetJson( 'endereco', AJson.Item( 'endereco' ).AsJson )
      .AddOrSetJson( 'enderecoCarne', AJson.Item( 'enderecoCarne' ).AsJson )
      .AddOrSetString( 'sexo', AJson.Item( 'sexo' ).AsString );
    FJson.SetJson( 'dadosCliente', j );
    TManipulateCtrls.New( GetMapCliente, j ).ToCtrl( Opening );


    fones := TClienteDao.New.Telefones( Ajson.ID );//AJson.Item( 'telefones' ).AsList;
    MontaTelefones( fones, True );

    GetPesquisaEndereco.SetJson( j.Item( 'endereco' ).AsJson );
    GetPesquisaEnderecoCarne.SetJson( j.Item( 'enderecoCarne' ).AsJson );
  end;
end;

procedure TfrmCad_Vendas.AfterSearchIndicador(AJson: IJson);
var
  j : IJson;
begin
  if Assigned( AJson ) then begin
    if FSettingJson then
      j := FJson.Item( 'dadosIndicacao' ).AsJson
    else
      j := GetJson.Item( 'dadosIndicacao' ).AsJson;
    j.AddOrSetString( 'nome', AJson.Item( 'nome' ).AsString )
      .AddOrSetString( 'cpfCnpj', AJson.Item( 'cpfCnpj' ).AsString )
      .AddOrSetString( 'rgIe', AJson.Item( 'rgIe' ).AsString );
    FJson.SetJson( 'dadosIndicacao', j );
    TManipulateCtrls.New( GetMapIndicacao, j).ToCtrl( Opening );
  end;
end;

procedure TfrmCad_Vendas.btnAdesaoGerarClick(Sender: TObject);
var
  dt : TDateTime;
  j : IJson;
  l : IList<IJson>;
  i : Integer;
begin
  inherited;
  dt := edtAdesaoPrimeiroVcto.Date;
  if ( dt <= 0 ) or ( edtAdesaoNroParcelas.Value <= 0 ) or ( edtAdesaoDiasParcelas.Value <= 0 ) then begin
    TMsgWarning.New( 'H� dados faltantes para o preenchimento das parcelas.' ).Show;
    Abort;
  end;

  l := TList<IJson>.New( GetJson.Item( 'parcelas' ).AsList );
  l := l.Filter(
    function( AObj : IJson ) : Boolean
    begin
      Result := AObj.Item( 'tipo' ).AsString <> 'ADESAO'
    end
  );

  for i := 1 to edtAdesaoNroParcelas.Value do begin
    j := TJson.New
      .Add( 'id' )
      .AddDate( 'vencimento', dt )
      .AddString( 'tipo', 'ADESAO' )
      .AddString( 'documento', FormatFloat( '#,##0', i ) + '/' + FormatFloat( '#,##0', edtAdesaoNroParcelas.Value ) )
      .AddFloat( 'valor', edtAdesaoValor.EditValue / edtAdesaoNroParcelas.Value );
    l.Add( j );
    dt := IncDay( dt, edtAdesaoDiasParcelas.Value );
  end;
  FJson.AddOrSetJsonArray( 'parcelas', l.ToArray );
  MontaAdesao;
end;

procedure TfrmCad_Vendas.btnCopiarEnderecoClick(Sender: TObject);
var
  jEndereco, jDadosCliente : IJson;
begin
  inherited;
  jDadosCliente := FJson.Item( 'dadosCliente' ).AsJson;
  jDadosCliente := TManipulateCtrls.New( GetMapCliente, jDadosCliente ).ToJson;
  jEndereco := jDadosCliente.Item( 'endereco' ).AsJson;
  jDadosCliente.SetJson( 'enderecoCarne', TJson.New( jEndereco.AsJsonString ) );
  FJson.SetJson( 'dadosCliente', jDadosCliente );
  TManipulateCtrls.New( GetMapCliente, jDadosCliente ).ToCtrl( Opening );
end;

procedure TfrmCad_Vendas.btnDA_BuscarClick(Sender: TObject);
begin
  inherited;
  edtCond_Nome.Text := edtNomeCliente.Text;
  edtCond_Cpf.Text  := edtClienteCpf.Text;
end;

procedure TfrmCad_Vendas.btnGerarParcelasClick(Sender: TObject);
var
  dtPrimeira : TDateTime;
  j : IJson;
  l : IList<IJson>;
  i, iParcelas : Integer;
  rParcela : Real;
begin
  inherited;
  dtPrimeira := 0;
  if ( not VarIsNull( edtDataPrimeiraPrestacao.Date ) ) and ( not VarIsEmpty( edtDataPrimeiraPrestacao.Date ) ) then
    dtPrimeira := edtDataPrimeiraPrestacao.Date;


  iParcelas := 0;
  if ( not VarIsNull( edtR_NroParcelas.Value ) ) and ( not  VarIsEmpty( edtR_NroParcelas.Value ) ) then
    iParcelas := edtR_NroParcelas.Value;

  rParcela := 0;
  if ( not VarIsNull( edtR_ParcelaReduzida.Value ) ) and ( not VarIsEmpty( edtR_ParcelaReduzida.Value ) ) then
    rParcela := edtR_ParcelaReduzida.Value;

  if ( dtPrimeira <= 0 ) or ( iParcelas <= 0 ) or ( rParcela <= 0 ) then begin
    TMsgWarning.New( 'H� dados faltantes para o preenchimento das parcelas.' ).Show;
    Abort;
  end;

  l := TList<IJson>.New( GetJson.Item( 'parcelas' ).AsList );
  l := l.Filter(
    function( AObj : IJson ) : Boolean
    begin
      Result := AObj.Item( 'tipo' ).AsString <> 'PARCELA'
    end
  );
  for i := 1 to iParcelas do begin
    j := TJson.New
      .Add( 'id' )
      .AddString( 'tipo', 'PARCELA' )
      .AddDate( 'vencimento', dtPrimeira )
      .AddString( 'documento', FormatFloat( '#,##0', i ) + '/' + FormatFloat( '#,##0', iParcelas ) )
      .AddFloat( 'valor', rParcela );
    l.Add( j );
    dtPrimeira := IncMonth( dtPrimeira );
  end;
  FJson.AddOrSetJsonArray( 'parcelas', l.ToArray );
  MontaParcelas;
end;

procedure TfrmCad_Vendas.btnGerarVendaAgoraClick(Sender: TObject);
begin
  inherited;
  SimulacaoOnly := False;
end;

procedure TfrmCad_Vendas.btnGerarVendaClick(Sender: TObject);
var
  j : IJson;
  d: ISimulacaoDao;
begin
  inherited;
  FJson := GetJson;
  if not ValidateForVenda then
    Abort;

  d := TSimulacaoDao.New;
  j := d.Dao.Save( FJson );
  j := d.GeraVenda( j.ID, Sender = btnGerarVendaSemAdesao );
  if Assigned( j ) then begin
    d.CriaJuridico( j.ID );
    SetJson( d.Dao.FindById( j.ID ) );
    TMsgInformation.New( 'Opera��o efetuada com sucesso!' ).Show;
  end
  else begin
    TMsgError.New( 'Houve um erro ao processar a opera��o!' ).Show;
  end;
end;

procedure TfrmCad_Vendas.cbbIndicacaoPropertiesChange(Sender: TObject);
var
  ind : TIndicacao;
begin
  inherited;
  if VarIsNull( cbbIndicacao.EditValue ) then
    ind := indCliente
  else
    ind := TIndicacao.ValueOf( cbbIndicacao.EditValue );
  lcgIndicacao.Visible    := ind in [ indCliente, indNaoCliente ];
  lcgIndicacao.Visible    := ( ind in [ indCliente, indNaoCliente ] ) and ( not FSimulacaoOnly );
  lcgIndicador.Visible    := ( ind in [ indCliente, indNaoCliente ] );
  lciIndicador.Visible    := ind = indCliente;
  lciIndicadorNome.Visible:= ind = indNaoCliente;
  lciIBanco.Visible       := ind = indNaoCliente;
  lciIAgencia.Visible     := ind = indNaoCliente;
  lciIconta.Visible       := ind = indNaoCliente;
  lciPix.Visible          := ind = indNaoCliente;
end;

procedure TfrmCad_Vendas.cbbTipoContratoPropertiesChange(Sender: TObject);
var
  tc : TTipoContrato;
begin
  inherited;
  if VarIsNull( cbbTipoContrato.EditValue ) then
    tc := tcVeiculo
  else
    tc := TTipoContrato.ValueOf( cbbTipoContrato.EditValue );
  lcgCondutor.Visible   := tc = tcVeiculo;
  lcgVeiculo.Visible    := tc = tcVeiculo;
  lcgCartao.Visible     := tc = tcCartaoCredito;
  lcgEmprestimo.Visible := tc = tcEmprestimos;
  MontaDocumentos;
end;

procedure TfrmCad_Vendas.cboxClienteJuridicoClick(Sender: TObject);
begin
  inherited;
  lciClienteRazao.Visible := cboxClienteJuridico.Checked;
  if cboxClienteJuridico.Checked then
    edtClienteCpf.Properties.EditMask := TAppConstantes.MascaraCNPJ
  else
    edtClienteCpf.Properties.EditMask := TAppConstantes.MascaraCPF;
end;

procedure TfrmCad_Vendas.cboxComProcuradorClick(Sender: TObject);
begin
  inherited;
  lcgProcurador.Visible := cboxComProcurador.Checked;
end;

procedure TfrmCad_Vendas.cboxNovoClienteClick(Sender: TObject);
begin
  inherited;
  lciCliente.Visible := not cboxNovoCliente.Checked;
  lciNomeCliente.Visible := cboxNovoCliente.Checked;
end;

function TfrmCad_Vendas.Dao: IDao;
begin
  Result := TSimulacaoDao.New.Dao;
end;

procedure TfrmCad_Vendas.edtClienteCpfPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  j : IJson;
  sDoc : string;
begin
  inherited;
  sDoc := TFormat.ClearFormat( Trim( edtClienteCpf.Text ) );
  if sDoc <> '' then begin
    try
      Error := not TClienteDao.New.DocumentValidation( sDoc );
      ErrorText := 'CPF/CNPJ Inv�lido';
    except
      on E:EDocumentConflictException do begin
        if cboxNovoCliente.Checked and TMsgQuestion.New( 'J� existe um cliente cadastrado com este documento. Deseja carreg�-lo?' ).Show.Yes then begin
          j := TClienteDao.New.FindByDocumento( sDoc );
          GetPesquisaCliente.SetJson( j );
          cboxNovoCliente.Checked := False;
          cboxNovoClienteClick( cboxNovoCliente );
        end;
      end;
      on E:Exception do
        raise E;
    end;
  end;
end;

procedure TfrmCad_Vendas.edtIndicadorCpfPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  sDoc : string;
  j : IJson;
begin
  inherited;
  sDoc := TFormat.ClearFormat( Trim( edtIndicadorCpf.Text ) );
  if cbbIndicacao.EditValue = TIndicacao.indNaoCliente.Value then begin
    try
      Error := not TClienteDao.New.DocumentValidation( sDoc );
      ErrorText := 'CPF/CNPJ Inv�lido';
    except
      on E:EDocumentConflictException do begin
        if cboxNovoCliente.Checked and TMsgQuestion.New( 'J� existe um cliente cadastrado com este documento. Deseja carreg�-lo?' ).Show.Yes then begin
          j := TClienteDao.New.FindByDocumento( sDoc );
          GetPesquisaIndicador.SetJson( j );
          cbbIndicacao.EditValue := TIndicacao.indCliente.Value;
        end;
      end;
      on E:Exception do
        raise E;
    end;
  end;
end;

procedure TfrmCad_Vendas.edtMesesQuitacaoExit(Sender: TObject);
begin
  inherited;
  if TFormat.NullCheck( edtMesesQuitacao.Value, 0 ) = 0 then
    Exit;

  if ( edtDataPrimeiraPrestacao.Date > 0 ) then begin
    edtPrevisaoQuitacao.Date := IncMonth( edtDataPrimeiraPrestacao.Date, edtMesesQuitacao.Value );
  end;
end;

procedure TfrmCad_Vendas.edtR_NroParcelasPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  if DisplayValue > 60 then begin
    Error := not TfrmSenhaAdmin.PedeAdmin;
    ErrorText := 'Voc� n�o possui permiss�o para efetuar essa opera��o!';
  end;
end;

procedure TfrmCad_Vendas.FinanciamentoChange(Sender: TObject);
var
  rSaldo : Real;
begin
  inherited;
  if Loading or FCalculando then
    Exit;
  FCalculando := True;
  try
    if not Opening then begin
      edtValorFinanciado.Value    := edtTotalParcelas.Value * edtValorParcela.Value;
      edtParcelasRestantes.Value  := edtTotalParcelas.Value - edtParcelasPagas.Value;
      edtSaldoPagar.Value         := edtValorParcela.Value * edtParcelasRestantes.Value;
      rSaldo := 0;
      rSaldo := edtSaldoPagar.Value - ( edtSaldoPagar.Value * ( edtBancoPerc.Value / 100 ) );
      edtR_Saldo.EditValue  := rSaldo;
        edtR_NroParcelas.Value := edtParcelasRestantes.Value;
      if edtR_NroParcelas.Value > 0 then
        edtR_ParcelaReduzida.Value := edtR_Saldo.Value / edtR_NroParcelas.Value;
    end;
  finally
    FCalculando := False;
  end;
end;

procedure TfrmCad_Vendas.FormCreate(Sender: TObject);
begin
  inherited;
  SimulacaoOnly := True;
  LCBaseGeral.BeginUpdate;
  try
    FCtrls := TCreateControls.New( Self )
      .Endereco( lcgEndereco )
      .Endereco( lcgEnderecoCarne, 'carne' )
      .Endereco( lcgP_Endereco, 'proc' );
    TGridFormat.New( GridC_Fones )
      .ColumnsAutoWidth( True )
      .Editable( True )
      .Deletable( True )
      .ShowNavigationBar( True )
      .ShowRoot( False )
      .Insertable( True );
    InicializaPesquisas;
    PreencheCombos;
    edtClienteCpf.Properties.EditMask   := TAppConstantes.MascaraCPF;
    edtIndicadorCpf.Properties.EditMask := TAppConstantes.MascaraCPF;
  finally
    LCBaseGeral.EndUpdate;
  end;
end;

function TfrmCad_Vendas.GetJson: IJson;
var
  i : Integer;
  n : TcxTreeListNode;
  list : IList<IJson>;
  j, dadosCliente, dadosIndicaco, dadosSimulacao, vendedor, profissao : IJson;
  bPadrao, bEntregue : Boolean;
begin
  Result := inherited;
  list := TList<IJson>.New;
  for i := 0 to GridParcelas.Count -1 do begin
    n := GridParcelas.Items[ i ];
    list.Add(
      TJson.New( n.Values[ GridP_Json.ItemIndex ] )
    );
  end;
  for i := 0 to GridAdesao.Count -1 do begin
    n := GridAdesao.Items[ i ];
    list.Add(
      TJson.New( n.Values[ GridA_Json.ItemIndex ] )
    );
  end;
  Result.AddOrSetJsonArray( 'parcelas', list.ToArray );
  list.Clear;
  for i := 0 to GridDocumentos.Count -1 do begin
    n := GridDocumentos.Items[ i ];
    bEntregue := TFormat.NullCheck( n.Values[ GridD_Entregue.ItemIndex ], False );
    list.Add(
      TJson.New( n.Values[ GridD_Json.ItemIndex ] )
        .AddOrSetBoolean( 'entregue', bEntregue )
    );
  end;
  Result.AddOrSetJsonArray( 'documentos', list.ToArray );
  list.Clear;
  for i := 0 to GridC_Fones.Count -1 do begin
    n := GridC_Fones.Items[ i ];
    j := TJson.New;
    if ( not VarIsNull( n.Values[ GridF_Json.ItemIndex ] ) ) and ( not VarIsEmpty( n.Values[ GridF_Json.ItemIndex ] ) ) then
      j := TJson.New( n.Values[ GridF_Json.ItemIndex ] );

    bPadrao := TFormat.NullCheck( n.Values[ GridF_Padrao.ItemIndex ], False );
    j.AddOrSetBoolean( 'padrao', bPadrao )
      .AddOrSetString( 'tipo', n.Texts[ GridF_Tipo.ItemIndex ] )
      .AddOrSetString( 'ddd', n.Texts[ GridF_DDD.ItemIndex ] )
      .AddOrSetString( 'fone', n.Texts[ GridF_Fone.ItemIndex ] )
      .AddOrSetString( 'ramal', n.Texts[ GridF_Ramal.ItemIndex ] )
      .AddOrSetString( 'observacoes', n.Texts[ GridF_Observacoes.ItemIndex ] );
    list.Add( j );
  end;

  dadosCliente := Result.Item( 'dadosCliente' ).AsJson
    .AddOrSetJsonArray( 'telefones', list.ToArray );

  profissao := GetPesquisaProfissao.GetJson;
  if Assigned( profissao ) and ( not profissao.IsEmpty ) then
    dadosCliente.AddOrSetJson( 'profissao', profissao );
  if Assigned( GetPesquisaCliente.GetJson ) then
    dadosCliente.AddOrSetJson( 'cliente', GetPesquisaCliente.GetJson );
  Result.AddOrSetJson( 'dadosCliente', dadosCliente );

  dadosIndicaco := Result.Item( 'dadosIndicacao' ).AsJson;
  if Assigned( GetPesquisaIndicador.GetJson ) then
    dadosIndicaco.AddOrSetJson( 'cliente', GetPesquisaIndicador.GetJson );
  Result.AddOrSetJson( 'dadosIndicacao', dadosIndicaco );

  dadosSimulacao := Result.Item( 'simulacao' ).AsJson;
  if Assigned( GetPesquisaBanco.GetJson ) then
    dadosSimulacao.AddOrSetJson( 'banco', GetPesquisaBanco.GetJson );
  Result.AddOrSetJson( 'simulacao', dadosSimulacao );

  if Assigned( GetPesquisaVendedor.GetJson ) then
    Result.AddOrSetJson( 'vendedor', GetPesquisaVendedor.GetJson );
end;

function TfrmCad_Vendas.GetMap: IMapCtrls;
begin
  if not Assigned( FMap ) then begin
    FMap := TMapCtrls.New
      .Add( 'id', TMapEdit.New( edtID, true ) )
      .Add( 'dataLcto', TMapEdit.New( edtDataLcto ) )
      .Add( 'tipoContrato', TMapEdit.New( cbbTipoContrato ) )
      .Add( 'dadosIndicacao', GetMapIndicacao )
      .Add( 'dadosCliente', GetMapCliente )
      .Add( 'adesao', GetMapAdesao )
      .Add( 'comissao', TMapEdit.New( edtValorComissao ) )
      .Add( 'dadosAdicionais', GetMapDadosAdicionais )
      .Add( 'simulacao', GetMapSimulacao )
      .Add( 'procurador', GetMapProcurador )
  end;
  Result := FMap;
end;

function TfrmCad_Vendas.GetMapAdesao: IMapCtrls;
begin
  Result := TMapCtrls.New
    .Add( 'valor', TMapEdit.New( edtAdesaoValor ) )
    .Add( 'primeiroVencimento', TMapEdit.New( edtAdesaoPrimeiroVcto ) )
    .Add( 'diasEntre', TMapEdit.New( edtAdesaoDiasParcelas ) )
    .Add( 'nroParcelas', TMapEdit.New( edtAdesaoNroParcelas ) )
end;

function TfrmCad_Vendas.GetMapCliente: IMapCtrls;
begin
  Result := TMapCtrls.New
        .Add( 'novo', TMapEdit.New( cboxNovoCliente ) )
        .Add( 'pessoaJuridica', TMapEdit.New( cboxClienteJuridico ) )
        .Add( 'nome', TMapEdit.New( edtNomeCliente ) )
        .Add( 'razaoSocial', TMapEdit.New( edtClienteRazao ) )
        .Add( 'cpfCnpj', TMapEdit.New( edtClienteCpf ) )
        .Add( 'rgIe', TMapEdit.New( edtClienteRg ) )
        .Add( 'dataNascimento', TMapEdit.New( edtClienteNascimento ) )
        .Add( 'email', TMapEdit.New( edtClienteEmail ) )
        .Add( 'estadoCivil', TMapEdit.New( cbbClienteEstadoCivil ) )
        .Add( 'naturalidade', TMapEdit.New( edtClienteNaturalidade ) )
        .Add( 'sexo', TMapEdit.New( cbbClienteSexo ) )
        .Add( 'nomePai', TMapEdit.New( edtClienteNomePai ) )
        .Add( 'nomeMae', TMapEdit.New( edtClienteNomeMae ) )
        .Add( 'endereco', TMapEndereco.New( Self, '' ) )
        .Add( 'enderecoCarne', TMapEndereco.New( Self, 'carne' ) )
end;

function TfrmCad_Vendas.GetMapDadosAdicionais: IMapCtrls;
begin
  Result := TMapCtrls.New
    .Add( 'veiculo', TMapCtrls.New
        .Add( 'nomeCondutor', TMapEdit.New( edtCond_Nome ) )
        .Add( 'cpfCondutor', TMapEdit.New( edtCond_Cpf ) )
        .Add( 'cnh', TMapEdit.New( edtCond_Cnh ) )
        .Add( 'marca', TMapEdit.New( cbbVeicMarca ) )
        .Add( 'modelo', TMapEdit.New( cbbVeicModelo ) )
        .Add( 'cor', TMapEdit.New( edtVeicCor ) )
        .Add( 'anoModelo', TMapEdit.New( edtVeicAnoModelo ) )
        .Add( 'ano', TMapEdit.New( edtVeicAno ) )
        .Add( 'placa', TMapEdit.New( edtVeicPlaca ) )
        .Add( 'renavam', TMapEdit.New( edtVeicRenavam ) )
        .Add( 'chassi', TMapEdit.New( edtVeicChassi ) )
        .Add( 'crlvAno', TMapEdit.New( edtVeicCrlv ) )
    )
    .Add( 'cartao', TMapCtrls.New
        .Add( 'ultimaFatura', TMapEdit.New( edtCartUltimaFatura ) )
        .Add( 'bandeira', TMapEdit.New( edtCartBandeira ) )
        .Add( 'nroContrato', TMapEdit.New( edtCartContrato ) )
    )
    .Add( 'emprestimo', TMapCtrls.New.Add( 'nroContrato', TMapEdit.New( edtEmpContrato ) ) )
    .Add( 'observacoes', TMapEdit.New( mmoDA_Obs ) )
end;

function TfrmCad_Vendas.GetMapIndicacao: IMapCtrls;
begin
  Result := TMapCtrls.New
        .Add( 'indicacao', TMapEdit.New( cbbIndicacao ) )
        .Add( 'nome', TMapEdit.New( edtIndicadorNome ) )
        .Add( 'cpfCnpj', TMapEdit.New( edtIndicadorCpf ) )
        .Add( 'rgIe', TMapEdit.New( edtIndicadorRg ) )
        .Add( 'banco', TMapEdit.New( edtIBanco ) )
        .Add( 'agencia', TMapEdit.New( edtIAgencia ) )
        .Add( 'contaBancaria', TMapEdit.New( edtIConta ) )
        .Add( 'pix', TMapEdit.New( edtPix ) );
end;

function TfrmCad_Vendas.GetMapProcurador: IMapCtrls;
begin
  Result := TMapCtrls.New
    .Add( 'nome', TMapEdit.New( edtP_Nome ) )
    .Add( 'nacionalidade', TMapEdit.New( edtP_Nacionalidade ) )
    .Add( 'naturalidade', TMapEdit.New( edtP_Naturalidade ) )
    .Add( 'profissao', TMapEdit.New( edtP_Profissao ) )
    .Add( 'rg', TMapEdit.New( edtP_RG ) )
    .Add( 'cpf', TMapEdit.New( edtP_CPF ) )
    .Add( 'filiacao', TMapEdit.New( edtP_Filiacao ) )
    .Add( 'endereco', TMapEndereco.New( Self, 'proc' ) )
    .Add( 'email', TMapEdit.New( edtP_Email ) )
    .Add( 'fone', TMapEdit.New( edtP_Telefone ) );
end;

function TfrmCad_Vendas.GetMapSimulacao: IMapCtrls;
begin
  Result := TMapCtrls.New
    .Add( 'perc', TMapEdit.New( edtBancoPerc ) )
    .Add( 'bancoPrazo', TMapEdit.New( edtBancoPrazo ) )
    .Add( 'valorFinanciado', TMapEdit.New( edtValorFinanciado ) )
    .Add( 'totalParcelas', TMapEdit.New( edtTotalParcelas ) )
    .Add( 'valorParcela', TMapEdit.New( edtValorParcela ) )
    .Add( 'parcelasPagas', TMapEdit.New( edtParcelasPagas ) )
    .Add( 'parcelasRestantes', TMapEdit.New( edtParcelasRestantes ) )
    .Add( 'saldoPagar', TMapEdit.New( edtSaldoPagar) )
    .Add( 'renValorParcela', TMapEdit.New( edtR_ParcelaReduzida ) )
    .Add( 'renSaldo', TMapEdit.New( edtR_Saldo ) )
    .Add( 'renNroParcelas', TMapEdit.New( edtR_NroParcelas ) )
    .Add( 'dataVencimento', TMapEdit.New( edtDataPrimeiraPrestacao ) )
    .Add( 'diasEntreParcelas', TMapEdit.New( edtDiasEntreParcelas ) )
    .Add( 'previsaoQuitacao', TMapEdit.New( edtPrevisaoQuitacao ) )
    .Add( 'parcelasAtrasadas', TMapEdit.New( edtParcelasAtraso ) )
    .Add( 'mesesQuitacao', TMapEdit.New( edtMesesQuitacao ) )
end;

function TfrmCad_Vendas.GetPesquisaBanco: IPesquisa;
begin
  if not Assigned( FPBanco ) then begin
    FPBanco := TPesquisa.New(
      edtBanco,
      TDao.New( '/bancos'),
      'descricao',
      nil,
      procedure( AJson : IJson )
      begin
        if Assigned( AJson ) then begin
          edtBancoPerc.Value := AJson.Item( 'perc' ).AsFloat;
          edtBancoPrazo.Text := AJson.Item( 'prazo' ).AsString;
        end;
      end
    )
  end;
  Result := FPBanco;
end;

function TfrmCad_Vendas.GetPesquisaCliente: IPesquisa;
begin
  if not Assigned( FPCliente ) then begin
    FPCliente := TPesquisa.New( edtCliente, TDao.New( '/cliente' ), 'nome', nil, AfterSearchCliente );
  end;
  Result := FPCliente;
end;

function TfrmCad_Vendas.GetPesquisaEndereco: IPesquisa;
begin
  if not Assigned( FPEndereco ) then begin
    FPEndereco := TPesquisaEndereco.New( Self, '' );
  end;
  Result := FPEndereco;
end;

function TfrmCad_Vendas.GetPesquisaEnderecoCarne: IPesquisa;
begin
  if not Assigned( FPenderecoCarne ) then begin
    FPEnderecoCarne := TPesquisaEndereco.New( Self, 'carne' );
  end;
  Result := FPEnderecoCarne;
end;

function TfrmCad_Vendas.GetPesquisaEnderecoProcurador: IPesquisa;
begin
  if not Assigned( FPEnderecoProcurador ) then begin
    FPEnderecoProcurador := TPesquisaEndereco.New( Self, 'proc' );
  end;
  Result := FPEnderecoProcurador;
end;

function TfrmCad_Vendas.GetPesquisaIndicador: IPesquisa;
begin
  if not Assigned( FPIndicador ) then begin
    FPIndicador := TPesquisa.New( edtIndicador, TDao.New( '/cliente' ), 'nome', nil, AfterSearchIndicador );
  end;
  Result := FPIndicador;
end;

function TfrmCad_Vendas.GetPesquisaProfissao: IPesquisa;
begin
  if not Assigned( FPProfissao ) then begin
    FPProfissao := TPesquisa.New( edtClienteProfissao, TDao.New( '/profissao' ), 'descricao' );
  end;
  Result := FPProfissao;
end;

function TfrmCad_Vendas.GetPesquisaVendedor: IPesquisa;
begin
  if not Assigned( FPVendedor ) then begin
    FPVendedor := TPesquisa.New( edtVendedor, TDao.New( '/funcionario' ), 'nome' );
  end;
  Result := FPVendedor;
end;

procedure TfrmCad_Vendas.InicializaPesquisas;
begin
  GetPesquisaBanco;
  GetPesquisaCliente;
  GetPesquisaIndicador;
  GetPesquisaProfissao;
  GetPesquisaVendedor;
  GetPesquisaEndereco;
  GetPesquisaEnderecoCarne;
  GetPesquisaEnderecoProcurador;
end;

procedure TfrmCad_Vendas.MontaAdesao;
var
  l : IList<IJson>;
  i: Integer;
  j : IJson;
begin
  l := TList<IJson>.New( FJson.Item( 'parcelas' ).AsList );
  l := l.Filter(
    function( AObj : IJson ) : Boolean
    begin
      Result := AObj.Item( 'tipo' ).AsString = 'ADESAO'
    end
  );
  GridAdesao.BeginUpdate;
  try
    GridAdesao.Clear;
    for i := 0 to l.Count -1 do begin
      j := l.Item( i );
      with GridAdesao.Add do begin
        Values[ GridA_Documento.ItemIndex ] := j.Item( 'documento' ).AsString;
        Values[ GridA_Valor.ItemIndex     ] := j.Item( 'valor' ).AsFloat;
        Values[ GridA_Vencimento.ItemIndex  ] := j.Item( 'vencimento' ).AsDateTime;
        Values[ GridA_Json.ItemIndex      ] := j.AsJsonString;
      end;
    end;
  finally
    GridAdesao.EndUpdate;
  end;
end;

procedure TfrmCad_Vendas.MontaDocumentos;
var
  j : IJson;
  l, list : IList<IJson>;
  i: Integer;
begin
  l := TList<IJson>.New( TDao.New( '/documentos' ).FindAll(0, 0) )
    .Filter(
      function( AObj : IJson ): Boolean
      begin
        Result := AObj.Item( 'tipo' ).AsString = cbbTipoContrato.EditValue;
      end
    );
  list := TList<IJson>.New( FJson.Item( 'documentos' ).AsList );
  for i := 0 to l.Count -1 do begin
    j := nil;
    j := list.Find(
      function( AObj : IJson ) : Boolean
      begin
        Result := AObj.Item( 'documento.id' ).AsInteger = l.Item( i ).ID;
      end
    );
    if not Assigned( j ) then begin
      j := l.Item( i );
      list.Add(
        TJson.New
          .AddOrSetString( 'descDocumento', j.Item( 'descricao' ).AsString )
          .AddOrSetJson( 'documento', j )
          .AddOrSetBoolean( 'entregue', False )
      );
    end;
  end;
  GridDocumentos.BeginUpdate;
  try
    GridDocumentos.Clear;
    for i := 0 to list.Count -1 do begin
      j := list.Item( i );
      with GridDocumentos.Add do begin
        Values[ GridD_Descricao.ItemIndex ] := j.Item( 'descDocumento').AsString;
        Values[ GridD_Entregue.ItemIndex  ] := j.Item( 'entregue' ).AsBoolean;
        Values[ GridD_Json.ItemIndex      ] := j.AsJsonString;
      end;
    end;
  finally
    GridDocumentos.EndUpdate;
  end;
end;

procedure TfrmCad_Vendas.MontaParcelas;
var
  l : IList<IJson>;
  i: Integer;
  j : IJson;
begin
  l := TList<IJson>.New( FJson.Item( 'parcelas' ).AsList );
  l := l.Filter(
    function( AObj : IJson ) : Boolean
    begin
      Result := AObj.Item( 'tipo' ).AsString = 'PARCELA'
    end
  );
  GridParcelas.BeginUpdate;
  try
    GridParcelas.Clear;
    for i := 0 to l.Count -1 do begin
      j := l.Item( i );
      with GridParcelas.Add do begin
        Values[ GridP_Documento.ItemIndex   ] := j.Item( 'documento' ).AsString;
        Values[ GridP_Valor.ItemIndex       ] := j.Item( 'valor' ).AsFloat;
        Values[ GridP_Vencimento.ItemIndex  ] := j.Item( 'vencimento' ).AsDateTime;
        Values[ GridP_Json.ItemIndex        ] := j.AsJsonString;
      end;
    end;
  finally
    GridParcelas.EndUpdate;
  end;

end;

procedure TfrmCad_Vendas.MontaTelefones( AList : TArray<IJson>; ClienteChange: boolean );
var
  l : IList<IJson>;
  i: Integer;
  j : IJson;
begin
  if ( FJson.ID > 0 ) and ( not ClienteChange ) then begin
    l := TList<IJson>.New( TSimulacaoDao.New.Telefones( FJson.ID ) );
  end
  else begin
    l := TList<IJson>.New( AList );
  end;
  GridC_Fones.BeginUpdate;
  try
    for i := 0 to l.Count -1 do begin
      j := l.Item( i );
      with GridC_Fones.Add do begin
        Values[ GridF_Json.ItemIndex        ] := j.AsJsonString;
        Values[ GridF_Padrao.ItemIndex      ] := j.Item( 'padrao' ).AsBoolean;
        Values[ GridF_Tipo.ItemIndex        ] := j.Item( 'tipo' ).AsString;
        Values[ GridF_DDD.ItemIndex         ] := j.Item( 'ddd' ).AsString;
        Values[ GridF_Fone.ItemIndex        ] := j.Item( 'fone' ).AsString;
        Values[ GridF_Ramal.ItemIndex       ] := j.Item( 'ramal' ).AsString;
        Values[ GridF_Observacoes.ItemIndex ] := j.Item( 'observacoes' ).AsString;
      end;
    end;
  finally
    GridC_Fones.EndUpdate;
  end;
end;

procedure TfrmCad_Vendas.PreencheCombos;
var
  ti: TIndicacao;
  tc: TTipoContrato;
  sx: TSexoPessoa;
  ec: TEstadoCivil;
begin
  cbbIndicacao.Properties.BeginUpdate;
  try
    cbbIndicacao.Properties.Items.Clear;
    for ti := Low( TIndicacao ) to High( TIndicacao ) do begin
      with cbbIndicacao.Properties.Items.Add do begin
        Description := ti.Description;
        Value       := ti.Value;
      end;
    end;
  finally
    cbbIndicacao.Properties.EndUpdate;
  end;

  cbbTipoContrato.Properties.BeginUpdate;
  try
    cbbTipoContrato.Properties.Items.Clear;
    for tc := Low( TTipoContrato ) to High( TTipoContrato ) do begin
      with cbbTipoContrato.Properties.Items.Add do begin
        Description := tc.Description;
        Value       := tc.Value;
      end;
    end;
  finally
    cbbTipoContrato.Properties.EndUpdate;
  end;
  cbbClienteEstadoCivil.Properties.BeginUpdate;
  try
    cbbClienteEstadoCivil.Properties.Items.Clear;
    for ec := Low( TEstadoCivil ) to High( TEstadoCivil ) do
      with cbbClienteEstadoCivil.Properties.Items.Add do begin
        Description := ec.AsString;
        Value := ec.Value;
      end;
  finally
    cbbClienteEstadoCivil.Properties.EndUpdate;
  end;

  cbbClienteSexo.Properties.BeginUpdate;
  try
    cbbClienteSexo.Properties.Items.Clear;
    for sx := Low( TSexoPessoa ) to High( TSexoPessoa ) do
      with cbbClienteSexo.Properties.Items.Add do begin
        Description := sx.AsString;
        Value       := sx.Value;
      end;
  finally
    cbbClienteSexo.Properties.EndUpdate;
  end;


end;

procedure TfrmCad_Vendas.RENChange(Sender: TObject);
begin
  inherited;
  if ( not FChangingREN ) and ( not Opening ) and ( not FCalculando ) then try
    FChangingREN := True;
    if Sender = edtR_NroParcelas then begin
      edtR_ParcelaReduzida.Value := edtR_Saldo.Value;
      if edtR_NroParcelas.Value > 0 then
        edtR_ParcelaReduzida.Value := edtR_Saldo.Value / edtR_NroParcelas.Value;
    end
    else begin
      edtR_Saldo.Value := edtR_NroParcelas.Value * edtR_ParcelaReduzida.Value;
      SimCalculaEconomia;
    end;
  finally
    FChangingREN := False;
  end;
end;

procedure TfrmCad_Vendas.SetJson(const Value: IJson);
begin
  inherited;
  FSettingJson := True;
  try
    FJson.AddOrSetJsonArray( 'parcelas', TSimulacaoDao.New.Parcelas( FJson.ID ) )
      .AddOrSetJsonArray( 'documentos', TSimulacaoDao.New.Documentos( FJson.ID ) );

    if Value.ID > 0 then begin
      try
        if FJson.Contains( 'simulacao.banco' ) and ( not FJson.Item( 'simulacao.banco' ).AsJson.IsEmpty ) then
          GetPesquisaBanco.SetJson( FJson.Item( 'simulacao.banco' ).AsJson );
        if FJson.Contains( 'dadosCliente.cliente' ) and ( not FJson.Item( 'dadosCliente.cliente' ).AsJson.IsEmpty ) then
          GetPesquisaCliente.SetJson( FJson.Item( 'dadosCliente.cliente' ).AsJson );
        if FJson.Contains( 'dadosCliente.profissao' ) and ( not FJson.Item( 'dadosCliente.profissao' ).AsJson.IsEmpty ) then
          GetPesquisaProfissao.SetJson( FJson.Item( 'dadosCliente.profissao' ).AsJson );
        if FJson.Contains( 'dadosIndicacao.cliente' ) and ( not FJson.Item( 'dadosIndicacao.cliente' ).AsJson.IsEmpty ) then
          GetPesquisaIndicador.SetJson( FJson.Item( 'dadosIndicacao.cliente' ).AsJson );
        if FJson.Contains( 'vendedor' ) and ( not FJson.Item( 'vendedor' ).AsJson.IsEmpty ) then
          GetPesquisaVendedor.SetJson( FJson.Item( 'vendedor' ).AsJson );

        with FJson.Item( 'dadosCliente' ).AsJson do begin
          GetPesquisaEndereco.SetJson( Item( 'endereco' ).AsJson );
          GetPesquisaEnderecoCarne.SetJson( Item( 'enderecoCarne' ).AsJson );
        end;
      except

      end;
    end;


    MontaParcelas;
    MontaAdesao;
    if FJson.ID <= 0 then begin
      cbbTipoContrato.Properties.OnChange := nil;
      cbbTipoContrato.EditValue := FJson.Item('tipoContrato').AsString;
      cbbTipoContrato.Properties.OnChange := cbbTipoContratoPropertiesChange;
    end;
    MontaDocumentos;
    MontaTelefones;
    if FJson.Item( 'status' ).AsString = 'PENDENTE' then
      lbSituacao.Caption := 'Pendente'
    else begin
      lbSituacao.Caption := 'Vendido';
      btnGerarVendaAgora.Click;
    end;
    if FJson.Item( 'cancelado' ).AsBoolean then
      lbSituacao.Caption := 'Cancelado';

    cboxComProcuradorClick(nil);
    cbbIndicacaoPropertiesChange(nil);
    cboxClienteJuridicoClick(nil);
    cboxNovoClienteClick( nil );

    FinanciamentoChange( nil );
    RENChange( nil );
    SimCalculaEconomia;
    cboxClienteJuridicoClick(nil);
    lciGerarVenda.Enabled           := lbSituacao.Caption = 'Pendente';
    lciGerarVendaSemAdesao.Enabled  := lbSituacao.Caption = 'Pendente';
  finally
    FSettingJson := False;
  end;
end;

procedure TfrmCad_Vendas.SetSimulacaoOnly(const Value: Boolean);
begin
  FSimulacaoOnly := Value;
  lcgDadosAdicionais.Visible      := not Value;
  lcgParcelas.Visible             := not Value;
  lcgDocumentos.Visible           := not Value;
  lciGerarVenda.Visible           := not Value;
  lciGerarVendaSemAdesao.Visible  := not Value;
  lciGerarVendaAgora.Visible      := Value;
  if ( not VarIsEmpty( cbbIndicacao.EditValue ) ) and ( not VarIsNull( cbbIndicacao.EditValue ) ) then
    lcgIndicacao.Visible    := ( TIndicacao.ValueOf( cbbIndicacao.EditValue ) in [ indCliente, indNaoCliente ] ) and ( not FSimulacaoOnly );
end;

procedure TfrmCad_Vendas.SimCalculaEconomia;
begin
  edtR_Economia.Value       := edtValorParcela.Value - edtR_ParcelaReduzida.Value;
  edtR_EconomiaTotal.Value  := edtSaldoPagar.Value - edtR_Saldo.Value;
end;

function TfrmCad_Vendas.ValidateForVenda: Boolean;
var
  sb : TStringBuilder;
  j : IJson;
procedure Add( s : string );
begin
  sb.Append( s ).Append( #13#10 );
end;
begin
  Result := True;
  if FJson.ID <= 0 then begin
    TMsgWarning.New( '� necess�rio salvar o registro antes de continuar!' ).Show;
    Result := False;
  end;

  if ( AnsiIndexStr( FJson.Item( 'dadosIndicacao.indicacao' ).AsString, ['CLIENTE', 'NAO_CLIENTE'] ) >= 0 ) and
    ( FJson.Item( 'comissao' ).AsFloat <= 0 ) then begin
    if TMsgWarning.New( 'A comiss�o n�o foi informada. Deseja continuar?', False ).Show.No then
      Abort;
  end;
  sb := TStringBuilder.Create;
  try
    if FJson.Item( 'dataLcto' ).AsDate <= 0 then
      Add( 'Data de Lan�amento' );
//    if ( not FJson.Contains( 'vendedor' ) ) or
//       ( FJson.Item( 'vendedor' ).AsJson.IsEmpty ) or
//       ( FJson.Item( 'vendedor.id' ).AsInteger <= 0 ) then
//      Add( 'Vendedor' );
    j := FJson.Item( 'dadosCliente' ).AsJson;
    if Assigned( j ) and ( not j.IsEmpty ) then begin
      if j.Item( 'nome' ).AsString = '' then
        Add( 'Nome do Cliente' );
      if j.Item( 'cpfCnpj' ).AsString = '' then
        Add( 'CPF/CNPJ do Cliente' );
      if j.Item( 'rgIe' ).AsString = '' then
        Add( 'RG/IE do Cliente' );
      if j.Item( 'dataNascimento' ).AsDate <= 0 then
        Add( 'Data de Nascimento do Cliente' );
      if j.Item( 'estadoCivil' ).AsString = '' then
        Add( 'Estado Civil do Cliente' );
      if j.Item( 'naturalidade' ).AsString = '' then
        Add( 'Naturalidade do Cliente' );
      if j.Item( 'nomePai' ).AsString = '' then
        Add( 'Nome do Pai do Cliente' );
      if j.Item( 'nomeMae' ).AsString = '' then
        Add( 'Nome da M�e do Cliente' );
    end
    else begin
      Add( 'Dados do Cliente' );
    end;
    j := FJson.Item( 'dadosIndicacao' ).AsJson;
    if Assigned( j ) and ( not j.IsEmpty ) then begin
      if j.Item( 'indicacao' ).AsString = 'NAO_CLIENTE' then begin
        if j.Item( 'nome' ).AsString = '' then
          Add( 'Nome do Cliente Indicador' );
        if j.Item( 'cpfCnpj' ).AsString = '' then
          Add( 'CPF/CNPJ do Cliente Indicador' );
        if j.Item( 'rgIe' ).AsString = '' then
          Add( 'RG/IE do Cliente Indicador' );
      end;
    end;
    if not sb.ToString.IsEmpty then begin
      TMsgError.New( 'Os seguintes campos s�o de preenchimento obrigat�rio:' + #13#10 +
                     sb.ToString + #13#10 +
                     'Corrija-os antes de continuar!' ).Show;
      Abort;
    end;
  finally
    sb.Free;
  end;
end;

end.
