inherited frmCad_Bancos: TfrmCad_Bancos
  Caption = 'Bancos'
  ClientHeight = 152
  ClientWidth = 414
  ExplicitWidth = 430
  ExplicitHeight = 191
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Width = 414
    Height = 152
    inherited btnSalvar: TcxButton
      Left = 248
      Top = 117
      TabOrder = 4
      ExplicitLeft = 248
      ExplicitTop = 117
    end
    inherited btnFechar: TcxButton
      Left = 329
      Top = 117
      TabOrder = 5
      ExplicitLeft = 329
      ExplicitTop = 117
    end
    object edtDescricao: TcxTextEdit [3]
      Left = 137
      Top = 28
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Text = 'edtDescricao'
      Width = 267
    end
    object edtPercentual: TcxCalcEdit [4]
      Left = 10
      Top = 73
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '#,##0.00'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 2
      Width = 194
    end
    object edtPrazo: TcxTextEdit [5]
      Left = 210
      Top = 73
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 3
      Text = 'edtPrazo'
      Height = 21
      Width = 194
    end
    inherited lciID: TdxLayoutItem
      Parent = lcg2
    end
    object lciDescricao: TdxLayoutItem
      Parent = lcg2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Descri'#231#227'o'
      CaptionOptions.Layout = clTop
      Control = edtDescricao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciPercentual: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Percentual'
      CaptionOptions.Layout = clTop
      Control = edtPercentual
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciPrazo: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Prazo'
      CaptionOptions.Layout = clTop
      Control = edtPrazo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgContent
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object lcg2: TdxLayoutAutoCreatedGroup
      Parent = lcgContent
      LayoutDirection = ldHorizontal
      Index = 0
    end
  end
end
