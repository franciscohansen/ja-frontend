unit untCad_Bancos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseCadastro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxContainer, cxEdit,
  dxLayoutControlAdapters, dxLayoutcxEditAdapters, dxLayoutContainer, cxClasses,
  cxTextEdit, Vcl.StdCtrls, cxButtons, dxLayoutControl, Dao.Interfaces, Json.Interfaces,
  cxMaskEdit, cxDropDownEdit, cxCalc, View.MappingControls.Interfaces;

type
  TfrmCad_Bancos = class(TfrmBaseCadastro)
    edtDescricao: TcxTextEdit;
    lciDescricao: TdxLayoutItem;
    edtPercentual: TcxCalcEdit;
    lciPercentual: TdxLayoutItem;
    edtPrazo: TcxTextEdit;
    lciPrazo: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    lcg2: TdxLayoutAutoCreatedGroup;
  private
    { Private declarations }
    FMap : IMapCtrls;
  protected
    function GetJson : IJson; override;
    function Dao : IDao; override;
    function GetMap : IMapCtrls; override;
  public
    { Public declarations }
  end;


implementation
uses
  Dao.Base, View.MappingControls.Edits, View.MappingControls.Base;

{$R *.dfm}

{ TfrmCad_Bancos }

function TfrmCad_Bancos.Dao: IDao;
begin
  Result := TDao.New( '/bancos' );
end;

function TfrmCad_Bancos.GetJson: IJson;
begin
  Result := inherited;
end;

function TfrmCad_Bancos.GetMap: IMapCtrls;
begin
  if not Assigned( FMap ) then begin
    FMap := TMapCtrls.New
      .Add( 'id', TMapEdit.New( edtID, True ) )
      .Add( 'descricao', TMapEdit.New( edtDescricao ) )
      .Add( 'prazo', TMapEdit.New( edtPrazo ) )
      .Add( 'perc', TMapEdit.New( edtPercentual ) );
  end;
  Result := FMap;
end;

end.
