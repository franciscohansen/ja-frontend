unit untConsultaFollowUp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseModal, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutcxEditAdapters, cxContainer, cxEdit, Vcl.ComCtrls,
  dxCore, cxDateUtils, cxCustomData, cxStyles, dxScrollbarAnnotations, cxTL,
  cxTLdxBarBuiltInMenu, cxInplaceContainer, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, dxLayoutControlAdapters, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, System.DateUtils, cxMemo, cxRichEdit;

type
  TfrmConsultaFollowUp = class(TfrmBaseModal)
    edtDataInicial: TcxDateEdit;
    lciDataInicial: TdxLayoutItem;
    edtDataFinal: TcxDateEdit;
    lciDataFinal: TdxLayoutItem;
    Grid: TcxTreeList;
    lciGrid: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    btnBuscar: TcxButton;
    lciBuscar: TdxLayoutItem;
    GridData: TcxTreeListColumn;
    GridMotivo: TcxTreeListColumn;
    GridAssunto: TcxTreeListColumn;
    GridResumo: TcxTreeListColumn;
    edtResumo: TcxRichEdit;
    lciResumo: TdxLayoutItem;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GridFocusedNodeChanged(Sender: TcxCustomTreeList;
      APrevFocusedNode, AFocusedNode: TcxTreeListNode);
  private
    FIDCliente: Integer;
    procedure SetIDCliente(const Value: Integer);
    { Private declarations }
  public
    { Public declarations }
    property IDCliente : Integer read FIDCliente write SetIDCliente;
  end;


implementation

uses
  Exceptions, Dao.FollowUp, Json.Interfaces, Util.Format;

{$R *.dfm}

procedure TfrmConsultaFollowUp.btnBuscarClick(Sender: TObject);
var
  list : TArray<IJson>;
  j : IJson;
begin
  inherited;
  list := TFollowUpDao.New.FindForConsulta( IDCliente, edtDataInicial.Date, edtDataFinal.Date );
  Grid.BeginUpdate;
  try
    Grid.Clear;
    for j  in list do begin
      with Grid.Add do begin
        Values[ GridData.ItemIndex    ] := j.Item( 'dataHora' ).AsDateTime;
        Values[ GridMotivo.ItemIndex  ] := j.Item( 'motivo' ).AsString;
        Values[ GridAssunto.ItemIndex ] := j.Item( 'assunto' ).AsString;
        Values[ GridResumo.ItemIndex  ] := j.Item( 'resumo' ).AsString;
      end;
    end;
  finally
    Grid.EndUpdate;
  end;
end;

procedure TfrmConsultaFollowUp.FormCreate(Sender: TObject);
begin
  inherited;
  edtDataFinal.Date   := Date;
  edtDataInicial.Date := IncMonth( Date, -1 );
  TGridFormat.New( Grid )
    .ColumnsAutoWidth( True )
    .ShowRoot( False )
    .Editable( False );
end;

procedure TfrmConsultaFollowUp.GridFocusedNodeChanged(Sender: TcxCustomTreeList;
  APrevFocusedNode, AFocusedNode: TcxTreeListNode);
begin
  inherited;
  if Assigned( AFocusedNode ) then
    edtResumo.Lines.Text := AFocusedNode.Values[ GridResumo.ItemIndex ];
end;

procedure TfrmConsultaFollowUp.SetIDCliente(const Value: Integer);
begin
  FIDCliente := Value;
end;

end.
