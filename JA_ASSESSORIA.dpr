program JA_ASSESSORIA;

uses
  Vcl.Forms,
  untMain in 'untMain.pas' {frmMain},
  Http.Interfaces in 'src\http\Http.Interfaces.pas',
  Json.Interfaces in 'src\json\Json.Interfaces.pas',
  Json.Base in 'src\json\Json.Base.pas',
  Util.Lists in 'src\util\Util.Lists.pas',
  Exceptions in 'src\Exceptions.pas',
  Http.Base in 'src\http\Http.Base.pas',
  Cfg.Constantes in 'src\cfg\Cfg.Constantes.pas',
  untBaseGeral in 'view\untBaseGeral.pas' {frmBaseGeral},
  untBaseModal in 'view\untBaseModal.pas' {frmBaseModal},
  untLogin in 'view\untLogin.pas' {frmLogin},
  untBaseCadastro in 'view\untBaseCadastro.pas' {frmBaseCadastro},
  Dao.Interfaces in 'src\dao\Dao.Interfaces.pas',
  Cfg in 'src\cfg\Cfg.pas',
  untBaseLista in 'view\untBaseLista.pas' {frmBaseLista},
  untLst_Agendamentos in 'untLst_Agendamentos.pas' {frmLst_Agendamentos},
  Dao.Base in 'src\dao\Dao.Base.pas',
  untLst_Bancos in 'untLst_Bancos.pas' {frmLst_Bancos},
  untCad_Bancos in 'untCad_Bancos.pas' {frmCad_Bancos},
  Forms.Interfaces in 'src\forms\Forms.Interfaces.pas',
  View.MappingControls.Base in 'src\view\View.MappingControls.Base.pas',
  View.MappingControls.Edits in 'src\view\View.MappingControls.Edits.pas',
  View.MappingControls.Interfaces in 'src\view\View.MappingControls.Interfaces.pas',
  Util.Format in 'src\util\Util.Format.pas',
  untConnectionConfig in 'untConnectionConfig.pas' {frmConnectionConfig},
  untMessageBox in 'view\untMessageBox.pas' {frmMessageBox},
  View.MessageBox in 'src\view\View.MessageBox.pas',
  untLst_Vendas in 'untLst_Vendas.pas' {frmLst_Vendas},
  untCad_Vendas in 'untCad_Vendas.pas' {frmCad_Vendas},
  View.CreateControls.Base in 'src\view\View.CreateControls.Base.pas',
  View.CreateControls.Interfaces in 'src\view\View.CreateControls.Interfaces.pas',
  Model.Endereco.Base in 'src\model\Model.Endereco.Base.pas',
  Model.Endereco.Interfaces in 'src\model\Model.Endereco.Interfaces.pas',
  Model.Enums in 'src\model\Model.Enums.pas',
  Util.ConsultaLocalizacao.Base in 'src\util\Util.ConsultaLocalizacao.Base.pas',
  Util.ConsultaLocalizacao.Interfaces in 'src\util\Util.ConsultaLocalizacao.Interfaces.pas',
  Json.Builder in 'src\json\Json.Builder.pas',
  View.AtualizaUf in 'src\view\View.AtualizaUf.pas',
  Util.Strings in 'src\util\Util.Strings.pas',
  Dao.Login in 'src\dao\Dao.Login.pas',
  untLst_Juridico in 'untLst_Juridico.pas' {frmLst_Juridico},
  untPainelAdministracao in 'untPainelAdministracao.pas' {frmPainelAdministracao},
  untLst_Documentos in 'untLst_Documentos.pas' {frmLst_Documentos},
  Model.Enums.JA in 'src\model\Model.Enums.JA.pas',
  untCad_Documentos in 'untCad_Documentos.pas' {frmCad_Documentos},
  untConfiguracoes in 'untConfiguracoes.pas' {frmConfiguracoes},
  untPesquisa in 'untPesquisa.pas' {frmPesquisa},
  View.Search in 'src\view\View.Search.pas',
  Dao.Simulacao in 'src\dao\Dao.Simulacao.pas',
  Dao.Configuracao in 'src\dao\Dao.Configuracao.pas',
  Dao.Empresa in 'src\dao\Dao.Empresa.pas',
  Dao.Base.H10 in 'src\dao\Dao.Base.H10.pas',
  Dao.Juridico in 'src\dao\Dao.Juridico.pas',
  Dao.Cliente in 'src\dao\Dao.Cliente.pas',
  Util.Converter in 'src\util\Util.Converter.pas',
  untCad_Forum in 'untCad_Forum.pas' {frmCad_Forum},
  untCad_OficialJustica in 'untCad_OficialJustica.pas' {frmCad_OficialJustica},
  untCad_Acompanhamento in 'untCad_Acompanhamento.pas' {frmCad_Acompanhamento},
  untUsuarios in 'untUsuarios.pas' {frmUsuarios},
  untSenhaAdmin in 'untSenhaAdmin.pas' {frmSenhaAdmin},
  Dao.FollowUp in 'src\dao\Dao.FollowUp.pas',
  untNewAcompanhamento in 'untNewAcompanhamento.pas' {frmNewAcompanhamento},
  untConsultaFollowUp in 'untConsultaFollowUp.pas' {frmConsultaFollowUp},
  Dao.Financeiro.H10 in 'src\dao\Dao.Financeiro.H10.pas',
  Dao.Localizacao.H10 in 'src\dao\Dao.Localizacao.H10.pas',
  untImprimir in 'untImprimir.pas' {frmImprimir},
  Dao.Report in 'src\dao\Dao.Report.pas';

{$R *.res}

begin
  Application.Initialize;
  if TfrmLogin.DoLogin then begin
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TfrmMain, frmMain);
  Application.Title := 'JA Assessoria e Cobranšas';
  Application.Run;
  end;
end.
