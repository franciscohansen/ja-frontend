unit untLst_Documentos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseLista, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles,
  dxScrollbarAnnotations, cxTL, cxTextEdit, cxTLdxBarBuiltInMenu, Vcl.Menus,
  cxContainer, cxEdit, dxLayoutContainer, dxLayoutControlAdapters,
  dxLayoutcxEditAdapters, cxClasses, cxMaskEdit, cxDropDownEdit, Vcl.StdCtrls,
  cxButtons, cxInplaceContainer, dxLayoutControl, Dao.Interfaces,
  Json.Interfaces, Forms.Interfaces;

type
  TfrmLst_Documentos = class(TfrmBaseLista)
    GridBase_Descricao: TcxTreeListColumn;
    GridBase_Tipo: TcxTreeListColumn;
  private
    { Private declarations }
  protected
    function Dao : IDao; override;
    procedure JsonToNode( AJson : IJson; ANode : TcxTreeListNode ); override;
    function FormInstance : IFormCadastro; override;
  public
    { Public declarations }
  end;


implementation

uses
  Dao.Base, Json.Base, Model.Enums.JA, untCad_Documentos;

{$R *.dfm}

{ TfrmLst_Documentos }

function TfrmLst_Documentos.Dao: IDao;
begin
  Result := TDao.New( '/documentos' );
end;

function TfrmLst_Documentos.FormInstance: IFormCadastro;
begin
  Result := TfrmCad_Documentos.Create( Self.Owner );
end;

procedure TfrmLst_Documentos.JsonToNode(AJson: IJson; ANode: TcxTreeListNode);
begin
  inherited;
  ANode.Values[ GridBase_Descricao.ItemIndex  ] := AJson.Item( 'descricao' ).AsString;
  ANode.Values[ GridBase_Tipo.ItemIndex       ] := TTipoDocumento.ValueOf( AJson.Item( 'tipo' ).AsString ).Description;
end;

end.
