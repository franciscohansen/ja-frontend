unit untCad_Documentos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseCadastro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxContainer, cxEdit,
  dxLayoutControlAdapters, dxLayoutcxEditAdapters, dxLayoutContainer, cxClasses,
  cxTextEdit, Vcl.StdCtrls, cxButtons, dxLayoutControl, cxMaskEdit,
  cxDropDownEdit, cxImageComboBox, View.MappingControls.Interfaces,
  View.MappingControls.Edits, View.MappingControls.Base, Dao.Interfaces,
  Json.Interfaces;

type
  TfrmCad_Documentos = class(TfrmBaseCadastro)
    edtDescricao: TcxTextEdit;
    lciDescricao: TdxLayoutItem;
    cbbTipo: TcxImageComboBox;
    lciTipo: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
      FMap : IMapCtrls;
  protected
    function GetJson : IJson; override;
    function Dao : IDao; override;
    function GetMap : IMapCtrls; override;
  public
    { Public declarations }
  end;


implementation

uses
  Dao.Base, Model.Enums.JA;

{$R *.dfm}

{ TfrmCad_Documentos }

function TfrmCad_Documentos.Dao: IDao;
begin
  Result := TDao.New( '/documentos' );
end;

procedure TfrmCad_Documentos.FormCreate(Sender: TObject);
var
  tipo : TTipoDocumento;
begin
  inherited;
  cbbTipo.Properties.BeginUpdate;
  try
    cbbTipo.Properties.Items.Clear;
    for tipo := Low( TTipoDocumento ) to High( TTipoDocumento ) do begin
      with cbbTipo.Properties.Items.Add do begin
        Description := tipo.Description;
        Value := tipo.Value;
      end;
    end;
  finally
    cbbTipo.Properties.EndUpdate;
  end;
end;

function TfrmCad_Documentos.GetJson: IJson;
begin
  Result := inherited;
end;

function TfrmCad_Documentos.GetMap: IMapCtrls;
begin
  if not Assigned( FMap ) then begin
    FMap := TMapCtrls.New
      .Add( 'id', TMapEdit.New( edtID, True ) )
      .Add( 'descricao', TMapEdit.New( edtDescricao ) )
      .Add( 'tipo', TMapEdit.New( cbbTipo ) );
  end;
  Result := FMap;
end;

end.
