inherited frmLst_Juridico: TfrmLst_Juridico
  Caption = 'Jur'#237'dico'
  ClientHeight = 585
  ClientWidth = 1126
  ExplicitWidth = 1142
  ExplicitHeight = 624
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Width = 1126
    Height = 585
    ExplicitWidth = 1126
    ExplicitHeight = 585
    inherited GridBase: TcxTreeList
      Top = 82
      Width = 1106
      Height = 462
      OptionsView.ColumnAutoWidth = False
      Styles.OnGetContentStyle = GridBaseStylesGetContentStyle
      TabOrder = 8
      OnFocusedNodeChanged = GridBaseFocusedNodeChanged
      ExplicitTop = 82
      ExplicitWidth = 1106
      ExplicitHeight = 462
      inherited GridBase_ID: TcxTreeListColumn
        Position.ColIndex = 2
      end
      object GridBase_Cliente: TcxTreeListColumn
        Caption.Text = 'Cliente'
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_CPF: TcxTreeListColumn
        Caption.Text = 'CPF'
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Banco: TcxTreeListColumn
        Caption.Text = 'Banco'
        Width = 100
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Veiculo: TcxTreeListColumn
        Caption.Text = 'Ve'#237'culo'
        Width = 100
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Placa: TcxTreeListColumn
        Caption.Text = 'Placa'
        Width = 100
        Position.ColIndex = 7
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Renavam: TcxTreeListColumn
        Caption.Text = 'RENAVAM'
        Width = 100
        Position.ColIndex = 8
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_RG: TcxTreeListColumn
        Caption.Text = 'RG'
        Width = 100
        Position.ColIndex = 9
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Endereco: TcxTreeListColumn
        Caption.Text = 'Endere'#231'o'
        Width = 100
        Position.ColIndex = 10
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Nro: TcxTreeListColumn
        Caption.Text = 'Nro.'
        Width = 100
        Position.ColIndex = 11
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Bairro: TcxTreeListColumn
        Caption.Text = 'Bairro'
        Width = 100
        Position.ColIndex = 12
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_UF: TcxTreeListColumn
        Caption.Text = 'UF'
        Width = 100
        Position.ColIndex = 14
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Cidade: TcxTreeListColumn
        Caption.Text = 'Cidade'
        Width = 100
        Position.ColIndex = 13
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_DataFechamento: TcxTreeListColumn
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mm/yyyy'
        Properties.EditFormat = 'dd/mm/yyyy'
        Caption.Text = 'Data Fechamento'
        Width = 100
        Position.ColIndex = 15
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_DataQuitacao: TcxTreeListColumn
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mm/yyyy'
        Properties.EditFormat = 'dd/mm/yyyy'
        Caption.Text = 'Data Quita'#231#227'o'
        Width = 100
        Position.ColIndex = 16
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Obs: TcxTreeListColumn
        Visible = False
        Caption.Text = 'Observa'#231#245'es'
        Width = 100
        Position.ColIndex = 17
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Leituras: TcxTreeListColumn
        Caption.Text = 'Leituras'
        Width = 100
        Position.ColIndex = 18
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Cor: TcxTreeListColumn
        PropertiesClassName = 'TdxColorEditProperties'
        Options.Sizing = False
        Width = 20
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    inherited btnNovo: TcxButton
      Left = 10000
      Top = 10000
      TabOrder = 26
      Visible = False
      ExplicitLeft = 10000
      ExplicitTop = 10000
    end
    inherited btnEditar: TcxButton
      Left = 10000
      Top = 10000
      TabOrder = 28
      Visible = False
      ExplicitLeft = 10000
      ExplicitTop = 10000
    end
    inherited btnExcluir: TcxButton
      Left = 10000
      Top = 10000
      TabOrder = 25
      Visible = False
      ExplicitLeft = 10000
      ExplicitTop = 10000
    end
    inherited edtBusca: TcxTextEdit
      ExplicitWidth = 1106
      Width = 1106
    end
    inherited btnBuscar: TcxButton
      Left = 1041
      Top = 51
      TabOrder = 7
      ExplicitLeft = 1041
      ExplicitTop = 51
    end
    inherited btnAnterior: TcxButton
      Left = 960
      Top = 550
      TabOrder = 22
      ExplicitLeft = 960
      ExplicitTop = 550
    end
    inherited btnProxima: TcxButton
      Left = 1041
      Top = 550
      TabOrder = 23
      ExplicitLeft = 1041
      ExplicitTop = 550
    end
    inherited cbbPagina: TcxComboBox
      Left = 865
      Top = 550
      TabOrder = 20
      ExplicitLeft = 865
      ExplicitTop = 550
    end
    inherited btnAbrir: TcxButton
      Left = 10000
      Top = 10000
      TabOrder = 24
      Visible = False
      ExplicitLeft = 10000
      ExplicitTop = 10000
    end
    inherited btnImprimir: TcxButton
      Left = 10000
      Top = 10000
      TabOrder = 27
      Visible = False
      ExplicitLeft = 10000
      ExplicitTop = 10000
    end
    inherited lbTotalPaginas: TcxLabel
      Left = 921
      Top = 556
      ExplicitLeft = 921
      ExplicitTop = 556
    end
    object cbbSituacao: TcxImageComboBox [12]
      Left = 10
      Top = 55
      AutoSize = False
      Properties.Items = <>
      Properties.OnChange = cbbSituacaoPropertiesChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 1
      Height = 21
      Width = 166
    end
    object edtDataInicial: TcxDateEdit [13]
      Left = 182
      Top = 55
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 2
      Width = 166
    end
    object edtDataFinal: TcxDateEdit [14]
      Left = 354
      Top = 55
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 3
      Width = 166
    end
    object cbbUf: TcxImageComboBox [15]
      Left = 526
      Top = 55
      Properties.Items = <>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 4
      Width = 165
    end
    object cbbTipo: TcxImageComboBox [16]
      Left = 697
      Top = 55
      Properties.Items = <>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 5
      Width = 166
    end
    object cbbBanco: TcxImageComboBox [17]
      Left = 869
      Top = 55
      Properties.Items = <>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 6
      Width = 166
    end
    object cbox1: TcxCheckBox [18]
      Left = 691
      Top = 550
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 15
    end
    object cbox2: TcxCheckBox [19]
      Left = 710
      Top = 550
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 16
    end
    object cbox3: TcxCheckBox [20]
      Left = 729
      Top = 550
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 17
    end
    object cbox4: TcxCheckBox [21]
      Left = 748
      Top = 550
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 18
    end
    object cbox5: TcxCheckBox [22]
      Left = 767
      Top = 550
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 19
    end
    object btnAcompanhamento: TcxButton [23]
      Left = 10
      Top = 550
      Width = 120
      Height = 25
      Caption = 'Acompanhamento'
      TabOrder = 9
      OnClick = btnAcompanhamentoClick
    end
    object btnVisualizarAcompanhamento: TcxButton [24]
      Left = 136
      Top = 550
      Width = 150
      Height = 25
      Caption = 'Visualizar Acompanhamento'
      TabOrder = 10
      OnClick = btnAcompanhamentoClick
    end
    object btnFinalizar: TcxButton [25]
      Left = 292
      Top = 550
      Width = 75
      Height = 25
      Caption = 'Finalizar'
      TabOrder = 11
      OnClick = btnFinalizarClick
    end
    object btnArquivar: TcxButton [26]
      Left = 373
      Top = 550
      Width = 75
      Height = 25
      Caption = 'Arquivar'
      TabOrder = 12
      OnClick = btnArquivarClick
    end
    object btnOutros: TcxButton [27]
      Left = 610
      Top = 550
      Width = 75
      Height = 25
      Caption = 'Outros'
      DropDownMenu = pmOutros
      Kind = cxbkDropDown
      TabOrder = 14
    end
    object btnDesmarcarFinalizado: TcxButton [28]
      Left = 454
      Top = 550
      Width = 150
      Height = 25
      Caption = 'Desmarcar Como Finalizado'
      TabOrder = 13
      OnClick = btnDesmarcarFinalizadoClick
    end
    inherited lcgRoot: TdxLayoutGroup
      ItemIndex = 3
    end
    inherited lciBase: TdxLayoutItem
      Index = 2
    end
    inherited lciNovo: TdxLayoutItem
      Parent = nil
      Visible = False
      Index = -1
    end
    inherited lciEditar: TdxLayoutItem
      Parent = nil
      Visible = False
      Index = -1
    end
    inherited lciExcluir: TdxLayoutItem
      Parent = nil
      Visible = False
      Index = -1
    end
    inherited lcg1: TdxLayoutAutoCreatedGroup
      Index = 3
    end
    inherited lciBusca: TdxLayoutItem
      Parent = lcgRoot
      AlignVert = avTop
    end
    inherited lciBuscar: TdxLayoutItem
      Parent = lcg3
      AlignVert = avBottom
      Index = 6
    end
    inherited lcg2: TdxLayoutAutoCreatedGroup
      Parent = nil
      Index = -1
      Special = True
    end
    inherited lciAnterior: TdxLayoutItem
      Index = 13
    end
    inherited lciProxima: TdxLayoutItem
      Index = 14
    end
    inherited lciPagina: TdxLayoutItem
      Index = 11
    end
    inherited lciAbrir: TdxLayoutItem
      Parent = nil
      Visible = False
      Index = -1
    end
    inherited lciImprimir: TdxLayoutItem
      Parent = nil
      Index = -1
    end
    inherited lciTotalPaginas: TdxLayoutItem
      Index = 12
    end
    object lciSituacao: TdxLayoutItem
      Parent = lcg3
      AlignHorz = ahClient
      CaptionOptions.Text = 'Situa'#231#227'o'
      CaptionOptions.Layout = clTop
      Control = cbbSituacao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciDataInicial: TdxLayoutItem
      Parent = lcg3
      AlignHorz = ahClient
      CaptionOptions.Text = 'Data Inicial'
      CaptionOptions.Layout = clTop
      Control = edtDataInicial
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg3: TdxLayoutAutoCreatedGroup
      Parent = lcgRoot
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object lciDataFinal: TdxLayoutItem
      Parent = lcg3
      AlignHorz = ahClient
      CaptionOptions.Text = 'Data Final'
      CaptionOptions.Layout = clTop
      Control = edtDataFinal
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciUf: TdxLayoutItem
      Parent = lcg3
      AlignHorz = ahClient
      CaptionOptions.Text = 'UF'
      CaptionOptions.Layout = clTop
      Control = cbbUf
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciTipo: TdxLayoutItem
      Parent = lcg3
      AlignHorz = ahClient
      CaptionOptions.Text = 'Tipo'
      CaptionOptions.Layout = clTop
      Control = cbbTipo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object lciBanco: TdxLayoutItem
      Parent = lcg3
      AlignHorz = ahClient
      CaptionOptions.Text = 'Banco'
      CaptionOptions.Layout = clTop
      Control = cbbBanco
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object lciCbox1: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cbox1
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 13
      ControlOptions.ShowBorder = False
      Index = 6
    end
    object lciCbox2: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cbox2
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 13
      ControlOptions.ShowBorder = False
      Index = 7
    end
    object lciCbox3: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cbox3
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 13
      ControlOptions.ShowBorder = False
      Index = 8
    end
    object lciCbox4: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cbox4
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 13
      ControlOptions.ShowBorder = False
      Index = 9
    end
    object lciCbox5: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cbox5
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 13
      ControlOptions.ShowBorder = False
      Index = 10
    end
    object lciAcompanhamento: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnAcompanhamento
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 120
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciVisualizarAcompanhamento: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnVisualizarAcompanhamento
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 150
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciFinalizar: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnFinalizar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciArquivar: TdxLayoutItem
      Parent = lcg1
      Visible = False
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnArquivar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciOutros: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnOutros
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object lciDesmarcarFinalizado: TdxLayoutItem
      Parent = lcg1
      Visible = False
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnDesmarcarFinalizado
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 150
      ControlOptions.ShowBorder = False
      Index = 4
    end
  end
  inherited stRepoLista: TcxStyleRepository
    PixelsPerInch = 96
    object stlUnreadOdd: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object stlUnreadEven: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = cl3DLight
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
  object pmOutros: TPopupMenu
    Left = 1024
    Top = 128
    object mniForum: TMenuItem
      Caption = 'F'#243'rum'
      OnClick = mniForumClick
    end
    object mniOficial: TMenuItem
      Caption = 'Oficial de Justi'#231'a'
      OnClick = mniOficialClick
    end
  end
end
