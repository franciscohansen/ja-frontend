unit untCad_Acompanhamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseModal, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutcxEditAdapters, cxContainer, cxEdit, cxCustomListBox,
  cxListBox, cxLabel, Vcl.ComCtrls, dxCore, cxDateUtils, cxMemo, cxCheckBox,
  cxButtonEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  dxLayoutControlAdapters, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxCustomData,
  cxStyles, dxScrollbarAnnotations, cxTL, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, cxRichEdit, View.MappingControls.Interfaces,
  Json.Interfaces, System.StrUtils, cxCheckListBox, View.Search, Util.Lists,
  cxCalc, cxImageComboBox, Winapi.ShellAPI, Vcl.Clipbrd, Model.Enums.JA,
  System.DateUtils;

type
  TfrmCad_Acompanhamento = class(TfrmBaseModal)
    lbNome: TcxLabel;
    lciNome: TdxLayoutItem;
    lbProcurador: TcxLabel;
    lciProcurador: TdxLayoutItem;
    lbCondutor: TcxLabel;
    lciCondutor: TdxLayoutItem;
    lbVeiculo: TcxLabel;
    lciVeiculo: TdxLayoutItem;
    lcgDados: TdxLayoutGroup;
    lcgME: TdxLayoutGroup;
    lcgCB: TdxLayoutGroup;
    lcgCA: TdxLayoutGroup;
    lcgRE: TdxLayoutGroup;
    lcgEE: TdxLayoutGroup;
    edtME_Data: TcxDateEdit;
    lciME_Data: TdxLayoutItem;
    edtME_Nro: TcxTextEdit;
    lciME_Nro: TdxLayoutItem;
    edtME_IDDecisao: TcxTextEdit;
    lciME_IDDecisao: TdxLayoutItem;
    edtME_ResumoDecisao: TcxTextEdit;
    lciME_ResumoDecisao: TdxLayoutItem;
    edtME_Forum: TcxButtonEdit;
    lciME_Forum: TdxLayoutItem;
    edtME_Oficial: TcxButtonEdit;
    lciME_Oficial: TdxLayoutItem;
    cboxCB_Liberacao: TcxCheckBox;
    lciCB_Liberacao: TdxLayoutItem;
    cboxCB_Arquivado: TcxCheckBox;
    lciCB_Arquivado: TdxLayoutItem;
    edtCB_Data: TcxDateEdit;
    lciCB_Data: TdxLayoutItem;
    edtCB_ClasseProcesso: TcxTextEdit;
    lciClasseProcesso: TdxLayoutItem;
    edtCB_Nro: TcxTextEdit;
    lciCB_Nro: TdxLayoutItem;
    edtCB_Forum: TcxButtonEdit;
    lciCB_Forum: TdxLayoutItem;
    edtCB_Oficial: TcxButtonEdit;
    lciCB_Oficial: TdxLayoutItem;
    edtCA_Data: TcxDateEdit;
    lciCA_Data: TdxLayoutItem;
    edtCA_Endereco: TcxTextEdit;
    lciCA_Endereco: TdxLayoutItem;
    edtCA_Motivo: TcxTextEdit;
    lciCA_Motivo: TdxLayoutItem;
    edtCA_Oficial: TcxButtonEdit;
    lciCA_Oficial: TdxLayoutItem;
    mmoCA_Observacoes: TcxMemo;
    lciCA_Observacoes: TdxLayoutItem;
    edtRE_Data: TcxDateEdit;
    lciRE_Data: TdxLayoutItem;
    edtRE_Forum: TcxButtonEdit;
    lciRE_Forum: TdxLayoutItem;
    edtRE_IDDecisao: TcxTextEdit;
    lciRE_IDDecisao: TdxLayoutItem;
    mmoRE_Observacoes: TcxMemo;
    lciRE_Observacoes: TdxLayoutItem;
    cboxEE_Bacenjud: TcxCheckBox;
    lciEE_Bacenjud: TdxLayoutItem;
    edtEE_Data: TcxDateEdit;
    lciEE_Data: TdxLayoutItem;
    edtEE_Forum: TcxButtonEdit;
    lciEE_Forum: TdxLayoutItem;
    edtEE_IDDecisao: TcxTextEdit;
    lciEE_IDDecisao: TdxLayoutItem;
    mmoEE_Observacao: TcxMemo;
    lciEE_Observacao: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    lcg2: TdxLayoutAutoCreatedGroup;
    lcg3: TdxLayoutAutoCreatedGroup;
    lcg4: TdxLayoutAutoCreatedGroup;
    lcg5: TdxLayoutAutoCreatedGroup;
    lcg6: TdxLayoutAutoCreatedGroup;
    lcg7: TdxLayoutAutoCreatedGroup;
    lcg8: TdxLayoutAutoCreatedGroup;
    lcg9: TdxLayoutAutoCreatedGroup;
    lcg10: TdxLayoutAutoCreatedGroup;
    lcgBase: TdxLayoutGroup;
    lcgFollowUps: TdxLayoutGroup;
    lcgObservacoes: TdxLayoutGroup;
    lcgFinanceiro: TdxLayoutGroup;
    lcgMaisDados: TdxLayoutGroup;
    cboxCadastroPush: TcxCheckBox;
    lciCadastroPush: TdxLayoutItem;
    GridFollowUp: TcxTreeList;
    lciFollowUp: TdxLayoutItem;
    lcg11: TdxLayoutAutoCreatedGroup;
    GridFU_Data: TcxTreeListColumn;
    GridFU_Resumo: TcxTreeListColumn;
    GridFU_Json: TcxTreeListColumn;
    mmoObservacoes: TcxMemo;
    lciObservacoes: TdxLayoutItem;
    lstSituacao: TcxCheckListBox;
    lciSituacao: TdxLayoutItem;
    btnFechar: TcxButton;
    lciFechar: TdxLayoutItem;
    btnOk: TcxButton;
    lciOk: TdxLayoutItem;
    lcg12: TdxLayoutAutoCreatedGroup;
    cbbTipo: TcxImageComboBox;
    lciTipo: TdxLayoutItem;
    cbbVencimento: TcxImageComboBox;
    lciVencimento: TdxLayoutItem;
    btnFiltrar: TcxButton;
    lciFiltrar: TdxLayoutItem;
    lcg13: TdxLayoutAutoCreatedGroup;
    GridFinanceiro: TcxTreeList;
    lciFinanceiro: TdxLayoutItem;
    GridF_Codigo: TcxTreeListColumn;
    GridF_Descricao: TcxTreeListColumn;
    GridF_Documento: TcxTreeListColumn;
    GridF_Vcto: TcxTreeListColumn;
    GridF_Pgto: TcxTreeListColumn;
    GridF_Valor: TcxTreeListColumn;
    GridF_Recebido: TcxTreeListColumn;
    GridF_Json: TcxTreeListColumn;
    GridTelefone: TcxTreeList;
    lciFones: TdxLayoutItem;
    GridT_DDD: TcxTreeListColumn;
    GridT_Fone: TcxTreeListColumn;
    GridT_Observacoes: TcxTreeListColumn;
    GridT_WhatsApp: TcxTreeListColumn;
    GridT_PageBot: TcxTreeListColumn;
    GridT_Copiar: TcxTreeListColumn;
    edtEmail: TcxTextEdit;
    lciEmail: TdxLayoutItem;
    btnEmailCopiar: TcxButton;
    lciEmailCopiar: TdxLayoutItem;
    btnEmailAbrir: TcxButton;
    lciEmailAbrir: TdxLayoutItem;
    lcg14: TdxLayoutAutoCreatedGroup;
    btnFollowUp_Carregar: TcxButton;
    lciFollowUp_Carregar: TdxLayoutItem;
    btnFollowUp_Consultar: TcxButton;
    lciFollowUp_Consultar: TdxLayoutItem;
    lcg15: TdxLayoutAutoCreatedGroup;
    btnCopiarNome: TcxButton;
    lciCopiarNome: TdxLayoutItem;
    btnCopiarCpf: TcxButton;
    lciCopiarCpf: TdxLayoutItem;
    btnCopiarRenavam: TcxButton;
    lciCopiarRenavam: TdxLayoutItem;
    btnCopiarPlaca: TcxButton;
    lciCopiarPlaca: TdxLayoutItem;
    procedure FormCreate(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure GridT_WhatsAppPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure GridT_PageBotPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure GridT_CopiarPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure mniNomeClick(Sender: TObject);
    procedure mniCPFClick(Sender: TObject);
    procedure mniRENAVAMClick(Sender: TObject);
    procedure mniPlacaClick(Sender: TObject);
    procedure btnEmailAbrirClick(Sender: TObject);
    procedure btnEmailCopiarClick(Sender: TObject);
    procedure btnFollowUp_CarregarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnFollowUp_ConsultarClick(Sender: TObject);
    procedure lstSituacaoClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
  private
    FJson,
    FJsonCA,
    FJsonCB,
    FJsonEE,
    FJsonME,
    FJsonRE: IJson;

    FMap,
    FMapCA,
    FMapCB,
    FMapEE,
    FMapME,
    FMapRE: IMapCtrls;

    FPForumME,
    FPOficialME,
    FPForumCB,
    FPOficialCB,
    FPOficialCA,
    FPForumRE,
    FPForumEE : IPesquisa;

    FFinanceiro : IList<IJson>;

    FID: Integer;
    FAbrindo, FLancouFollowUp: Boolean;
    FContato: IJson;
    FIDContato: Integer;
    FParcelas,
    FAdesao : TArray<IJson>;

    function GetMap : IMapCtrls;
    function GetMapCA : IMapCtrls;
    function GetMapCB : IMapCtrls;
    function GetMapEE : IMapCtrls;
    function GetMapME : IMapCtrls;
    function GetMapRE : IMapCtrls;


    function GetJson : IJson;
    function GetJsonCA : IJson;
    function GetJsonCB : IJson;
    function GetJsonEE : IJson;
    function GetJsonME : IJson;
    function GetJsonRE : IJson;

    function GetPForumME: IPesquisa;
    function GetPForumCB: IPesquisa;
    function GetPForumRE: IPesquisa;
    function GetPForumEE: IPesquisa;
    function GetPOficialCA: IPesquisa;
    function GetPOficialCB: IPesquisa;
    function GetPOficialME: IPesquisa;
    procedure SetID(const Value: Integer);
    procedure SetJson( AJson : IJson );
    procedure SetAbrindo(const Value: Boolean);

    function PegaFone : string;
    procedure SetContato(const Value: IJson);
    procedure SetIDContato(const Value: Integer);

    property Contato : IJson read FContato write SetContato;

    function Ativo( ASituacao : TJuridicoSituacao ) : Boolean;

    procedure MontaFinanceiro;
    { Private declarations }
  public
    { Public declarations }
    property ID : Integer read FID write SetID;
    property IDContato : Integer read FIDContato write SetIDContato;
    property Abrindo : Boolean read FAbrindo write SetAbrindo;
  end;

implementation

uses
  Util.Format, View.MappingControls.Edits, View.MappingControls.Base,
  Dao.Juridico, Dao.Base, Dao.Simulacao, Dao.FollowUp,
  Dao.Base.H10, untNewAcompanhamento, View.MessageBox, Json.Base,
  untConsultaFollowUp, Dao.Financeiro.H10;

{$R *.dfm}

function TfrmCad_Acompanhamento.Ativo(ASituacao: TJuridicoSituacao): Boolean;
var
  I: Integer;
begin
  Result := false;
  for I := 0 to lstSituacao.Count -1 do begin
    if Ord( ASituacao ) = lstSituacao.Items.Items[ i ].Tag then
      Exit( lstSituacao.Items.Items[ i ].State = cbsChecked );
  end;
end;

procedure TfrmCad_Acompanhamento.btnEmailAbrirClick(Sender: TObject);
const
  BASE_URL = 'https://webmail-seguro.com.br/';
begin
  inherited;
  ShellExecute( 0, 'open', PChar( BASE_URL ), '', '', SW_SHOW );
end;

procedure TfrmCad_Acompanhamento.btnEmailCopiarClick(Sender: TObject);
var
  dc : IJson;
begin
  inherited;
  dc := Contato.Item( 'dadosCliente' ).AsJson;
  if Assigned( dc ) and ( not dc.IsEmpty ) then
    Clipboard.AsText := dc.Item( 'email' ).AsString;
end;

procedure TfrmCad_Acompanhamento.btnFecharClick(Sender: TObject);
begin
  inherited;
  if FAbrindo then
    Close;
end;

procedure TfrmCad_Acompanhamento.btnFiltrarClick(Sender: TObject);
var
  l : IList<IJson>;
  i: Integer;
  j : IJson;
begin
  inherited;
  if cbbTipo.EditValue = 'ACORDO' then
    l := TList<IJson>.New( FParcelas )
  else
    l := TList<IJson>.New( FAdesao );
  if cbbVencimento.EditValue <> 'TODAS' then begin
    if cbbVencimento.EditValue = 'VENCIDAS' then begin
      l := l.Filter(
        function( AObj : IJson ): Boolean
        begin
          Result := ( StartOfTheDay( AObj.Item( 'vencimento' ).AsDate ) > StartOfTheDay( Date ) ) or
            ( AObj.Item( 'baixa' ).AsDate > 0 );
        end
      );
    end
    else begin
      l := l.Filter(
        function( AObj : IJson ): Boolean
        begin
          Result := ( StartOfTheDay( AObj.Item( 'vencimento' ).AsDate ) <= StartOfTheDay( Date ) ) or
            ( AObj.Item( 'baixa' ).AsDate <= 0 );
        end
      );
    end;
  end;
  GridFinanceiro.BeginUpdate;
  try
    GridFinanceiro.Clear;
    for i := 0 to l.Count -1 do begin
      j := l.Item( i );
      with GridFinanceiro.Add do begin
        Values[ GridF_Codigo.ItemIndex  ] := j.ID;
        Values[ GridF_Vcto.ItemIndex    ] := j.Item( 'vencimento' ).AsDateTime;
        Values[ GridF_Descricao.ItemIndex ] := j.Item( 'descricao' ).AsString;
        if j.Item( 'baixa' ).AsDateTime > 0 then
          Values[ GridF_Pgto.ItemIndex      ] := j.Item( 'baixa' ).AsDateTime;

      end;
    end;
  finally
    GridFinanceiro.EndUpdate;
  end;
end;

procedure TfrmCad_Acompanhamento.btnFollowUp_CarregarClick(Sender: TObject);
var
  a : TArray<IJson>;
  j : IJson;
begin
  inherited;
  GridFollowUp.BeginUpdate;
  try
    GridFollowUp.Clear;
    a := TFollowUpDao.New.FindByIdJuridico( FJson.ID );
    for j in a do begin
      with GridFollowUp.Add do begin
        Values[ GridFU_Data.ItemIndex   ] := j.Item( 'dataHora' ).AsDate;
        Values[ GridFU_Resumo.ItemIndex ] := j.Item( 'resumo' ).AsString;
        Values[ GridFU_Json.ItemIndex   ] := j.AsJsonString;
      end;
    end;
  finally
    GridFollowUp.EndUpdate;
  end;
end;

procedure TfrmCad_Acompanhamento.btnFollowUp_ConsultarClick(Sender: TObject);
var
  frm : TfrmConsultaFollowUp;
begin
  inherited;
  frm := TfrmConsultaFollowUp.Create( Self );
  try
    if FContato.Contains( 'dadosCliente' ) then
      frm.IDCliente := FContato.Item( 'dadosCliente.cliente.id' ).AsInteger;
    frm.ShowModal;
  finally
    frm.Release;
  end;
end;

procedure TfrmCad_Acompanhamento.btnOkClick(Sender: TObject);
var
  frm : TfrmNewAcompanhamento;
begin
  inherited;
  if not Abrindo then begin
    frm := TfrmNewAcompanhamento.Create( Self );
    try
      frm.Juridico  := FJson;
      if FContato.Contains( 'dadosCliente' ) then begin
        frm.IDCliente := FContato.Item( 'dadosCliente.cliente.idHabil' ).AsInteger;
        frm.Cliente := FContato.Item( 'dadosCliente.cliente' ).AsJson;
      end;
      frm.DataHora := Now;
      if frm.ShowModal <> mrOk then begin
        TMsgWarning.New( '� necess�rio lan�ar o acompanhamento para continuar!' ).Show;
        Abort;
      end
      else
        FLancouFollowUp := True;
    finally
      frm.Release;
    end;
    FJson := GetJson;
    FJson.AddOrSetJson( 'contato', TJson.New.AddInt( 'id', FContato.ID ) );
    FJson := TJuridicoDao.New.Dao.Save( FJson );
  end;
  ModalResult := mrOk;
end;

procedure TfrmCad_Acompanhamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if ( not Abrindo ) and ( not FLancouFollowUp ) then
    btnOk.Click;
end;

procedure TfrmCad_Acompanhamento.FormCreate(Sender: TObject);
var
  js: TJuridicoSituacao;
  cbox : TcxCheckListBoxItem;
begin
  inherited;
  FLancouFollowUp := False;
  LCBaseModal.BeginUpdate;
  try
    TGridFormat.New( [ GridFollowUp, GridFinanceiro ] )
      .ShowRoot( False )
      .Editable( False )
      .ColumnsAutoWidth( True );
    TGridFormat.New([ GridTelefone ])
      .ShowRoot( False )
      .Editable( True )
      .Deletable( False )
      .ShowNavigationBar( False )
      .ColumnsAutoWidth( True );

    lstSituacao.Clear;
    for js := Low( TJuridicoSituacao ) to High( TJuridicoSituacao ) do begin
      cbox := lstSituacao.Items.Add;
      cbox.Text    := js.Description;
      cbox.Tag     := Ord( js );
      cbox.State   := cbsUnchecked;
      cbox.Enabled := not ( js in [ jsContratoCancelado, jsFinalizado, jsArquivado ] );
    end;
  finally
    LCBaseModal.EndUpdate;
  end;
end;

function TfrmCad_Acompanhamento.GetJson: IJson;
var
  l : IList<IJson>;
procedure Add( AJson : IJson );
begin
  if Assigned( AJson ) then
    l.Add( AJson );
end;
begin
  FJson := TManipulateCtrls.New( GetMap, FJson ).ToJson;
  l := TList<IJson>.New( FJson.Item( 'situacoes' ).AsList );
  l := l.Filter(
      function( AObj : IJson ): Boolean
      begin
        Result := AnsiIndexStr( AObj.Item( 'situacao' ).AsString, [
          jsComBusca.Value,
          jsMandadoExpedido.Value,
          jsCarroApreendido.Value,
          jsRenajud.Value,
          jsEmExecucao.Value
        ] ) < 0;
      end
    );
  Add( GetJsonCA );
  Add( GetJsonCB );
  Add( GetJsonEE );
  Add( GetJsonME );
  Add( GetJsonRE );

  FJson.AddOrSetJsonArray( 'situacoes', l.ToArray );
  Result := FJson;
end;

function TfrmCad_Acompanhamento.GetJsonCA: IJson;
begin
  if Assigned( FJsonCA ) then begin
    FJsonCA := TManipulateCtrls.New( GetMapCA, FJsonCA ).ToJson
      .AddOrSetBoolean( 'ativo', Ativo( jsCarroApreendido ) );
    if Assigned( GetPOficialCA.GetJson ) then
      FJsonCA.AddOrSetJson( 'oficialJustica', GetPOficialCA.GetJson );
    Result := FJsonCA;
  end;
end;

function TfrmCad_Acompanhamento.GetJsonCB: IJson;
begin
  if Assigned( FJsonCB ) then begin
    FJsonCB := TManipulateCtrls.New( GetMapCB, FJsonCB ).ToJson
      .AddOrSetBoolean( 'ativo', Ativo( jsComBusca ) );

    if Assigned( GetPOficialCB.GetJson ) then
      FJsonCB.AddOrSetJson( 'oficialJustica', GetPOficialCB.GetJson );
    if Assigned( GetPForumCB.GetJson ) then
      FJsonCB.AddOrSetJson( 'forum', GetPForumCB.GetJson );
    Result := FJsonCB;
  end;
end;

function TfrmCad_Acompanhamento.GetJsonEE: IJson;
begin
  if Assigned( FJsonEE ) then begin
    FJsonEE := TManipulateCtrls.New( GetMapEE, FJsonEE ).ToJson
      .AddOrSetBoolean( 'ativo', Ativo( jsEmExecucao ) );
    if Assigned( GetPForumEE.GetJson ) then
      FJsonEE.AddOrSetJson( 'forum', GetPForumEE.GetJson );
    Result := FJsonEE;
  end;
end;

function TfrmCad_Acompanhamento.GetJsonME: IJson;
begin
  if Assigned( FJsonME ) then begin
    FJsonME := TManipulateCtrls.New( GetMapME, FJsonME ).ToJson
      .AddOrSetBoolean( 'ativo', Ativo( jsMandadoExpedido ) );
    if Assigned( GetPForumME.GetJson ) then
      FJsonME.AddOrSetJson( 'forum', GetPForumME.GetJson );

    if Assigned( GetPOficialME.GetJson ) then
      FJsonME.AddOrSetJson( 'oficialJustica', GetPOficialME.GetJson );
    Result := FJsonME;
  end;
end;

function TfrmCad_Acompanhamento.GetJsonRE: IJson;
begin
  if Assigned( FJsonRE ) then begin
    FJsonRE := TManipulateCtrls.New( GetMapRE, FJsonRE ).ToJson
      .AddOrSetBoolean( 'ativo', Ativo( jsRenajud ) );

    if Assigned( GetPForumRE.GetJson ) then
      FJsonRE.AddOrSetJson( 'forum', GetPForumRE.GetJson );
    Result := FJsonRE;
  end;
end;

function TfrmCad_Acompanhamento.GetMap: IMapCtrls;
begin
  if not Assigned( FMap ) then begin
    FMap := TMapCtrls.New
      .Add( 'cadastroPush', TMapEdit.New( cboxCadastroPush ) )
      .Add( 'observacoes', TMapEdit.New( mmoObservacoes ) );
  end;
  Result := FMap;
end;

function TfrmCad_Acompanhamento.GetMapCA: IMapCtrls;
begin
  if not Assigned( FMapCA ) then begin
    FMapCA := TMapCtrls.New
      .Add( 'data', TMapEdit.New( edtCA_Data ) )
      .Add( 'observacoes', TMapEdit.New( mmoCA_Observacoes ) )
      .Add( 'carroApreendido', TMapCtrls.New
        .Add( 'endereco', TMapEdit.New( edtCA_Endereco ) )
        .Add( 'motivo', TMapEdit.New( edtCA_Motivo ) )
    );
  end;
  Result := FMapCA;
end;

function TfrmCad_Acompanhamento.GetMapCB: IMapCtrls;
begin
  if not Assigned( FMapCB ) then begin
    FMapCB := TMapCtrls.New
      .Add( 'data', TMapEdit.New( edtCB_Data ) )
      .Add( 'busca', TMapCtrls.New
        .Add( 'aguardandoLiberacao', TMapEdit.New( cboxCB_Liberacao ) )
        .Add( 'arquivado', TMapEdit.New( cboxCB_Arquivado ) )
        .Add( 'classeProcessual', TMapEdit.New( edtCB_ClasseProcesso ) )
        .Add( 'nroProcesso', TMapEdit.New( edtCB_Nro ) )
      );
  end;
  Result := FMapCB;
end;

function TfrmCad_Acompanhamento.GetMapEE: IMapCtrls;
begin
  if not Assigned( FMapEE ) then begin
    FMapEE := TMapCtrls.New
      .Add( 'data', TMapEdit.New( edtEE_Data ) )
      .Add( 'observacoes', TMapEdit.New( mmoEE_Observacao ) )
      .Add( 'execucao', TMapCtrls.New
        .Add( 'bacenjud', TMapEdit.New( cboxEE_Bacenjud ) )
      );
  end;
  Result := FMapEE;
end;

function TfrmCad_Acompanhamento.GetMapME: IMapCtrls;
begin
  if not Assigned( FMapME ) then begin
    FMapME := TMapCtrls.New
      .Add( 'data', TMapEdit.New( edtME_Data ) )
      .Add( 'mandado', TMapCtrls.New
        .Add( 'nro', TMapEdit.New( edtME_Nro ) )
        .Add( 'idDecisao', TMapEdit.New( edtME_IDDecisao ) )
        .Add( 'resumoDecisao', TMapEdit.New( edtME_ResumoDecisao ) )
      );
  end;
  Result := FMapME;
end;

function TfrmCad_Acompanhamento.GetMapRE: IMapCtrls;
begin
  if not Assigned( FMapRE ) then begin
    FMapRE := TMapCtrls.New
      .Add( 'data', TMapEdit.New( edtRE_Data ) )
      .Add( 'observacoes', TMapEdit.New( mmoRE_Observacoes ) )
      .Add( 'renajud', TMapCtrls.New
        .Add( 'idDecisao', TMapEdit.New( edtRE_IDDecisao ) )
      );
  end;
  Result := FMapRE;
end;

function TfrmCad_Acompanhamento.GetPOficialCA: IPesquisa;
begin
  if not Assigned( FPOficialCA ) then begin
    FPOficialCA := TPesquisa.New( edtCA_Oficial, TDao.New( '/oficial-justica' ), 'nome' );
  end;
  Result := FPOficialCA;
end;

function TfrmCad_Acompanhamento.GetPOficialCB: IPesquisa;
begin
  if not Assigned( FPOficialCB ) then begin
    FPOficialCB := TPesquisa.New( edtCB_Oficial, TDao.New( '/oficial-justica' ), 'nome' );
  end;
  Result := FPOficialCB;
end;

function TfrmCad_Acompanhamento.GetPOficialME: IPesquisa;
begin
  if not Assigned( FPOficialME ) then begin
    FPOficialME := TPesquisa.New( edtME_Oficial, TDao.New( '/oficial-justica' ), 'nome' );
  end;
  Result := FPOficialME;
end;

procedure TfrmCad_Acompanhamento.GridT_CopiarPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
  Clipboard.AsText := PegaFone;
end;

procedure TfrmCad_Acompanhamento.GridT_PageBotPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
const
  BASE_URL = 'https://pagebot.com.br/admin/atendimentos';
begin
  inherited;
  ShellExecute( 0, 'open', PChar( BASE_URL ), '', '', SW_SHOW );
end;

procedure TfrmCad_Acompanhamento.GridT_WhatsAppPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
const
  BASE_URL = 'https://api.whatsapp.com/send?phone=%s';
begin
  inherited;
  ShellExecute( 0, 'open', PChar( Format( BASE_URL, [ PegaFone ] ) ), '', '', SW_SHOW );
end;

procedure TfrmCad_Acompanhamento.lstSituacaoClick(Sender: TObject);
begin
  inherited;
  lcgCB.Visible := Ativo( jsComBusca );
  lcgCA.Visible := Ativo( jsCarroApreendido );
  lcgME.Visible := Ativo( jsMandadoExpedido );
  lcgEE.Visible := Ativo( jsEmExecucao );
  lcgRE.Visible := Ativo( jsRenajud );
end;

procedure TfrmCad_Acompanhamento.mniCPFClick(Sender: TObject);
var
  dc : IJson;
begin
  inherited;
  dc := Contato.Item( 'dadosCliente' ).AsJson;
  if Assigned( dc ) and ( not dc.IsEmpty ) then
    Clipboard.AsText := dc.Item( 'cpfCnpj' ).AsString;
end;

procedure TfrmCad_Acompanhamento.mniNomeClick(Sender: TObject);
begin
  inherited;
  Clipboard.AsText := lbNome.Caption;
end;

procedure TfrmCad_Acompanhamento.mniPlacaClick(Sender: TObject);
var
  veic : IJson;
begin
  inherited;
  veic := Contato.Item( 'dadosAdicionais' ).AsJson;
  if Assigned( veic ) and ( not veic.IsEmpty ) then begin
    veic := veic.Item( 'veiculo' ).AsJson;
    if Assigned( veic ) and ( not veic.IsEmpty ) then
      Clipboard.AsText := veic.Item( 'placa' ).AsString;
  end;
end;

procedure TfrmCad_Acompanhamento.mniRENAVAMClick(Sender: TObject);
var
  veic : IJson;
begin
  inherited;
  veic := Contato.Item( 'dadosAdicionais' ).AsJson;
  if Assigned( veic ) and ( not veic.IsEmpty ) then begin
    veic := veic.Item( 'veiculo' ).AsJson;
    if Assigned( veic ) and ( not veic.IsEmpty ) then
      Clipboard.AsText := veic.Item( 'renavam' ).AsString;
  end;
end;

procedure TfrmCad_Acompanhamento.MontaFinanceiro;
var
  dao : IDaoFinanceiroH10;
begin
  Screen.Cursor := crHourGlass;
  try
    dao := TDaoFinanceiroH10.New;
    FParcelas := dao.FindByReferencia( FContato.Item( 'refVendaParcela' ).AsInteger );
    FAdesao   := dao.FindByReferencia( FContato.Item( 'refVendaAdesao' ).AsInteger );
    btnFiltrar.Click;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TfrmCad_Acompanhamento.PegaFone: string;
var
  n : TcxTreeListNode;
begin
  n := GridTelefone.FocusedNode;
  Result := '+55' + n.Values[ GridT_DDD.ItemIndex ] + n.Values[ GridT_Fone.ItemIndex ];
  Result := TFormat.ClearFormat( Result );
end;

function TfrmCad_Acompanhamento.GetPForumCB: IPesquisa;
begin
  if not Assigned( FPForumCB ) then begin
    FPForumCB := TPesquisa.New( edtCB_Forum, TDao.New( '/forum' ), 'nome' );
  end;
  Result := FPForumCB;
end;

function TfrmCad_Acompanhamento.GetPForumEE: IPesquisa;
begin
  if not Assigned( FPForumEE ) then begin
    FPForumEE := TPesquisa.New( edtEE_Forum, TDao.New( '/forum' ), 'nome' );
  end;
  Result := FPForumEE;
end;

function TfrmCad_Acompanhamento.GetPForumME: IPesquisa;
begin
  if not Assigned( FPForumME ) then begin
    FPForumME := TPesquisa.New( edtME_Forum, TDao.New( '/forum' ), 'nome' );
  end;
  Result := FPForumME;
end;

function TfrmCad_Acompanhamento.GetPForumRE: IPesquisa;
begin
  if not Assigned( FPForumRE ) then begin
    FPForumRE := TPesquisa.New( edtRE_Forum, TDao.New( '/forum' ), 'nome' );
  end;
  Result := FPForumRE;
end;

procedure TfrmCad_Acompanhamento.SetAbrindo(const Value: Boolean);
begin
  FAbrindo := Value;
  lciFechar.Enabled := False;
end;

procedure TfrmCad_Acompanhamento.SetContato(const Value: IJson);
var
  dc, fone, da, veic, proc : IJson;
begin
  FContato := Value;
  dc := FContato.Item( 'dadosCliente' ).AsJson;
  da := FContato.Item( 'dadosAdicionais' ).AsJson;
  if Assigned( da ) and ( not da.IsEmpty ) then begin
    veic := da.Item( 'veiculo' ).AsJson;
    if Assigned( veic ) and ( not veic.IsEmpty ) then begin
      lbCondutor.Caption  := veic.Item( 'nomeCondutor' ).AsString;
      lbVeiculo.Caption   := veic.Item( 'modelo' ).AsString + ' ' + veic.Item( 'placa' ).AsString;
    end;
  end;
  proc := FContato.Item( 'procurador' ).AsJson;
  if Assigned( proc ) and ( not proc.IsEmpty ) then
    lbProcurador.Caption := proc.Item( 'nome' ).AsString;

  try
    GridTelefone.BeginUpdate;
    GridTelefone.Clear;
    if Assigned( dc ) and ( not dc.IsEmpty ) then begin
      lbNome.Caption := dc.Item( 'nome' ).AsString;
      for fone in dc.Item( 'telefones' ).AsList do begin
        with GridTelefone.Add do begin
          Values[ GridT_DDD.ItemIndex         ] := fone.Item( 'ddd' ).AsString;
          Values[ GridT_Fone.ItemIndex        ] := fone.Item( 'fone' ).AsString;
          Values[ GridT_Observacoes.ItemIndex ] := fone.Item( 'observacoes' ).AsString;
        end;
      end;
    end;
  finally
    GridTelefone.EndUpdate;
  end;
  MontaFinanceiro;
end;

procedure TfrmCad_Acompanhamento.SetID(const Value: Integer);
begin
  FID := Value;
  SetJson( TJuridicoDao.New.Dao.FindById( Value ) );
end;

procedure TfrmCad_Acompanhamento.SetIDContato(const Value: Integer);
begin
  FIDContato := Value;
  Contato := TSimulacaoDao.New.Dao.FindById( FIDContato );
end;

procedure TfrmCad_Acompanhamento.SetJson(AJson: IJson);
var
  j : IJson;
  js : TJuridicoSituacao;
  i : Integer;
  situacoes : TArray<IJson>;
begin
  GetPForumME;
  GetPForumCB;
  GetPForumRE;
  GetPForumEE;
  GetPOficialCA;
  GetPOficialCB;
  GetPOficialME;
  FJson := AJson;
  if not Assigned( FJson ) then
    Exit;

  for i := 0 to lstSituacao.Count -1 do
    lstSituacao.Items.Items[ i ].State := cbsUnchecked;

  TManipulateCtrls.New( GetMap, FJson ).ToCtrl( FAbrindo );

  situacoes := TJuridicoSituacaoDao.New.FindByIdJuridico( FJson.ID );
  FJson.AddOrSetJsonArray( 'situacoes', situacoes );

  for j in situacoes do begin
    js := TJuridicoSituacao.ValueOf( j.Item( 'situacao' ).AsString );
    case js of
      jsComBusca        : begin
        FJsonCB := j;
        TManipulateCtrls.New( GetMapCB, FJsonCB ).ToCtrl( FAbrindo );
        GetPForumCB.SetJson( FJsonCB.Item( 'forum' ).AsJson );
        GetPOficialCB.SetJson( FJsonCB.Item( 'oficialJustica' ).AsJson );
        lcgCB.Visible := j.Item( 'ativo' ).AsBoolean;
      end;
      jsMandadoExpedido : begin
        FJsonME := j;
        TManipulateCtrls.New( GetMapME, FJsonME ).ToCtrl( FAbrindo );
        GetPForumME.SetJson( FJsonME.Item( 'forum' ).AsJson );
        GetPOficialME.SetJson( FJsonME.Item( 'oficialJustica' ).AsJson );
        lcgME.Visible := j.Item( 'ativo' ).AsBoolean;
      end;
      jsCarroApreendido : begin
        FJsonCA := j;
        TManipulateCtrls.New( GetMapCA, FJsonCA ).ToCtrl( FAbrindo );
        GetPOficialCA.SetJson( FJsonCA.Item( 'oficialJustica' ).AsJson );
        lcgCA.Visible := j.Item( 'ativo' ).AsBoolean;
      end;
      jsRenajud         : begin
        FJsonRE := j;
        TManipulateCtrls.New( GetMapRE, FJsonRE ).ToCtrl( FAbrindo );
        GetPForumRE.SetJson( FJsonRE.Item( 'forum' ).AsJson );
        lcgRE.Visible := j.Item( 'ativo' ).AsBoolean;
      end;
      jsEmExecucao      : begin
        FJsonEE := j;
        TManipulateCtrls.New( GetMapEE, FJsonEE ).ToCtrl( FAbrindo );
        GetPForumEE.SetJson( FJsonEE.Item( 'forum' ).AsJson );
        lcgEE.Visible := j.Item( 'ativo' ).AsBoolean;
      end;
    end;
    for i := 0 to lstSituacao.Count -1 do begin
      if lstSituacao.Items.Items[ i ].Tag = Ord(js) then begin
        if j.Item( 'ativo' ).AsBoolean then
          lstSituacao.Items.Items[ i ].State := cbsChecked
        else
          lstSituacao.Items.Items[ i ].State := cbsUnchecked;
      end;
    end;
  end;
end;

end.
