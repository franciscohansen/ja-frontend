inherited frmPainelAdministracao: TfrmPainelAdministracao
  Caption = 'Painel de Administra'#231#227'o'
  ClientHeight = 173
  ClientWidth = 379
  ExplicitWidth = 395
  ExplicitHeight = 212
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Width = 379
    Height = 173
    ExplicitWidth = 379
    ExplicitHeight = 173
    object btnBancos: TcxButton [0]
      Left = 10
      Top = 10
      Width = 359
      Height = 25
      Caption = 'Bancos'
      TabOrder = 0
      OnClick = btnBancosClick
    end
    object btnDocumentos: TcxButton [1]
      Left = 10
      Top = 41
      Width = 359
      Height = 25
      Caption = 'Documentos'
      TabOrder = 1
      OnClick = btnDocumentosClick
    end
    object btnConfiguracoes: TcxButton [2]
      Left = 10
      Top = 72
      Width = 359
      Height = 25
      Caption = 'Configura'#231#245'es'
      TabOrder = 2
      OnClick = btnConfiguracoesClick
    end
    object btnUsuarios: TcxButton [3]
      Left = 10
      Top = 103
      Width = 359
      Height = 25
      Caption = 'Usu'#225'rios'
      TabOrder = 3
      OnClick = btnUsuariosClick
    end
    object btnCorrigeJuridico: TcxButton [4]
      Left = 10
      Top = 134
      Width = 359
      Height = 25
      Caption = 'Corrige Jur'#237'dicos Faltantes'
      TabOrder = 4
      OnClick = btnCorrigeJuridicoClick
    end
    object lciBancos: TdxLayoutItem
      Parent = lcgRoot
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnBancos
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciDocumentos: TdxLayoutItem
      Parent = lcgRoot
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnDocumentos
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciConfiguracoes: TdxLayoutItem
      Parent = lcgRoot
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnConfiguracoes
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciUsuarios: TdxLayoutItem
      Parent = lcgRoot
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnUsuarios
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciCorrigeJuridico: TdxLayoutItem
      Parent = lcgRoot
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCorrigeJuridico
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 4
    end
  end
end
