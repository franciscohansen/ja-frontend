inherited frmUsuarios: TfrmUsuarios
  Caption = 'Usu'#225'rios'
  ClientHeight = 298
  ClientWidth = 508
  OnShow = FormShow
  ExplicitWidth = 524
  ExplicitHeight = 337
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseModal: TdxLayoutControl
    Width = 508
    Height = 298
    object GridUsuarios: TcxTreeList [0]
      Left = 10
      Top = 28
      Width = 488
      Height = 229
      Bands = <
        item
        end>
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 0
      object GridU_Nome: TcxTreeListColumn
        Caption.Text = 'Nome'
        Options.Editing = False
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridU_Vendas: TcxTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Caption.Text = 'Acessa Vendas?'
        Options.Sizing = False
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridU_Juridico: TcxTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Caption.Text = 'Acessa Jur'#237'dico?'
        Options.Sizing = False
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridU_Negociacoes: TcxTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Caption.Text = 'Acessa Negocia'#231#245'es?'
        Options.Sizing = False
        Width = 120
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridU_Json: TcxTreeListColumn
        Visible = False
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object btnSalvar: TcxButton [1]
      Left = 342
      Top = 263
      Width = 75
      Height = 25
      Caption = 'Salvar'
      TabOrder = 1
      OnClick = btnSalvarClick
    end
    object btnFechar: TcxButton [2]
      Left = 423
      Top = 263
      Width = 75
      Height = 25
      Caption = 'Fechar'
      TabOrder = 2
      OnClick = btnFecharClick
    end
    inherited lcgBaseModal: TdxLayoutGroup
      ItemIndex = 1
    end
    object lciUsuarios: TdxLayoutItem
      Parent = lcgBaseModal
      AlignVert = avClient
      CaptionOptions.Text = 'Usu'#225'rios'
      CaptionOptions.Layout = clTop
      Control = GridUsuarios
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciSalvar: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnSalvar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciFechar: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnFechar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      LayoutDirection = ldHorizontal
      Index = 1
    end
  end
end
