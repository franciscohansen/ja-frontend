unit untLst_Bancos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseLista, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles,
  dxScrollbarAnnotations, cxTL, cxTextEdit, cxTLdxBarBuiltInMenu, Vcl.Menus,
  cxContainer, cxEdit, dxLayoutContainer, dxLayoutControlAdapters,
  dxLayoutcxEditAdapters, cxClasses, cxMaskEdit, cxDropDownEdit, Vcl.StdCtrls,
  cxButtons, cxInplaceContainer, dxLayoutControl, Dao.Interfaces,
  Json.Interfaces, cxCalc, Forms.Interfaces;

type
  TfrmLst_Bancos = class(TfrmBaseLista)
    GridBase_Descricao: TcxTreeListColumn;
    GridBase_Perc: TcxTreeListColumn;
    GridBase_Prazo: TcxTreeListColumn;
  private
    { Private declarations }
  protected
    function Dao : IDao; override;
    procedure JsonToNode( AJson : IJson; ANode : TcxTreeListNode ); override;
    function FormInstance : IFormCadastro; override;
  public
    { Public declarations }
  end;


implementation

uses
  Dao.Base, untCad_Bancos;

{$R *.dfm}

{ TfrmLst_bancos }

function TfrmLst_Bancos.Dao: IDao;
begin
  Result := TDao.New( '/bancos' );
end;

function TfrmLst_Bancos.FormInstance: IFormCadastro;
begin
  Result := TfrmCad_Bancos.Create( Self.Owner );
end;

procedure TfrmLst_Bancos.JsonToNode(AJson: IJson; ANode: TcxTreeListNode);
begin
  inherited;
  ANode.Values[ GridBase_Descricao.ItemIndex  ] := AJson.Item( 'descricao' ).AsString;
  ANode.Values[ GridBase_Perc.ItemIndex       ] := AJson.Item( 'perc' ).AsFloat;
  ANode.Values[ GridBase_Prazo.ItemIndex      ] := AJson.Item( 'prazo' ).AsString;
end;

end.
