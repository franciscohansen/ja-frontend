unit untNewAcompanhamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseModal, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutcxEditAdapters, cxContainer, cxEdit, cxMemo,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, dxLayoutControlAdapters, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, Json.Interfaces;

type
  TfrmNewAcompanhamento = class(TfrmBaseModal)
    cbbMotivo: TcxComboBox;
    lciMotivo: TdxLayoutItem;
    mmoResumo: TcxMemo;
    lciResumo: TdxLayoutItem;
    btnOk: TcxButton;
    lciOk: TdxLayoutItem;
    btnFechar: TcxButton;
    lciFechar: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    procedure btnFecharClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FJuridico: IJson;
    FIDCliente: Integer;
    FCliente : IJson;
    FDataHora: TDateTime;
    procedure SetIDCliente(const Value: Integer);
    procedure SetJuridico(const Value: IJson);
    procedure SetDataHora(const Value: TDateTime);
    procedure SetCliente(const Value: IJson);
    { Private declarations }
  public
    { Public declarations }
    property Juridico: IJson read FJuridico write SetJuridico;
    property IDCliente : Integer read FIDCliente write SetIDCliente;
    property Cliente : IJson read FCliente write SetCliente;
    property DataHora : TDateTime read FDataHora write SetDataHora;
  end;


implementation

uses
  View.MessageBox, Json.Base, Dao.FollowUp, Cfg, Util.Format;

{$R *.dfm}

procedure TfrmNewAcompanhamento.btnFecharClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmNewAcompanhamento.btnOkClick(Sender: TObject);
var
  j : IJson;
  d : IFollowUpDao;
begin
  inherited;
  if ( cbbMotivo.Text = '' ) or ( mmoResumo.Lines.Text = '' ) then begin
    TMsgWarning.New( 'Preencha os dados do acompanhamento!' ).Show;
    Abort;
  end;


  d := TFollowUpDao.New;
  j := d.Dao.NewRecord
    .AddOrSetString( 'motivo', cbbMotivo.Text )
    .AddOrSetString( 'assunto', cbbMotivo.Text )
    .AddOrSetString( 'resumo', mmoResumo.Lines.Text + #13#10 + 'Data e Hora do Acompanhamento: ' + TFormat.DateTime( FDataHora ) )
    .AddOrSetJson( 'juridico', FJuridico )

    .AddOrSetInt( 'idEmpresa', TCfg.GetInstance.Empresa.ID )
    .AddOrSetDateTime( 'dataHora', FDataHora )
    .AddOrSetString( 'tipo', 'JURIDICO' )
    .AddOrSetString( 'prioridade', 'ALTA' );
  if Assigned(FCliente) then begin
    j.AddOrSetJson( 'cliente', FCliente ) ;
  end;

  d.Dao.Save( j );
  ModalResult := mrOk;
end;

procedure TfrmNewAcompanhamento.FormCreate(Sender: TObject);
var
  a : TArray<IJson>;
  j : IJson;
begin
  inherited;
  a := TFollowUpDao.New.FindMotivos;
  cbbMotivo.Properties.BeginUpdate;
  try
    cbbMotivo.Properties.Items.Clear;
    for j in a do begin
      cbbMotivo.Properties.Items.Add( j.Item( 'motivo' ).AsString );
    end;
  finally
    cbbMotivo.Properties.EndUpdate;
  end;
end;

procedure TfrmNewAcompanhamento.SetCliente(const Value: IJson);
begin
  FCliente := Value;
end;

procedure TfrmNewAcompanhamento.SetDataHora(const Value: TDateTime);
begin
  FDataHora := Value;
end;

procedure TfrmNewAcompanhamento.SetIDCliente(const Value: Integer);
begin
  FIDCliente := Value;
end;

procedure TfrmNewAcompanhamento.SetJuridico(const Value: IJson);
begin
  FJuridico := Value;
end;

end.
