unit untCad_OficialJustica;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseCadastro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxContainer, cxEdit,
  dxLayoutControlAdapters, dxLayoutcxEditAdapters, dxLayoutContainer, cxClasses,
  cxTextEdit, Vcl.StdCtrls, cxButtons, dxLayoutControl, cxMaskEdit, cxButtonEdit,
  Dao.Interfaces, Http.Interfaces, Json.Interfaces,
  View.MappingControls.Interfaces, View.CreateControls.Interfaces, View.Search;

type
  TfrmCad_OficialJustica = class(TfrmBaseCadastro)
    edtNome: TcxTextEdit;
    lciNome: TdxLayoutItem;
    edtFone: TcxTextEdit;
    lciFone: TdxLayoutItem;
    edtEmail: TcxTextEdit;
    lciEmail: TdxLayoutItem;
    edtForum: TcxButtonEdit;
    lciForum: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    lcg2: TdxLayoutAutoCreatedGroup;
  private
    { Private declarations }
    FMap : IMapCtrls;
    FCtrl : ICreateControls;
    FForum : IPesquisa;
    function GetPesquisaForum : IPesquisa;
  protected
    function GetJson : IJson; override;
    procedure SetJson(const Value: IJson); override;
    function Dao : IDao; override;
    function GetMap : IMapCtrls; override;
  public
    { Public declarations }
  end;


implementation

uses
  Json.Base, Http.Base, Exceptions, Dao.Base, Cfg.Constantes, Cfg,
  View.MappingControls.Edits, View.MappingControls.Base,
  View.CreateControls.Base, untPesquisa;

{$R *.dfm}

{ TfrmCad_OficialJustica }

function TfrmCad_OficialJustica.Dao: IDao;
begin
  Result := TDao.New( '/oficial-justica' );
end;

function TfrmCad_OficialJustica.GetJson: IJson;
begin
  Result := inherited;
  Result.AddOrSetJson( 'forum', GetPesquisaForum.GetJson );
end;

function TfrmCad_OficialJustica.GetMap: IMapCtrls;
begin
  if not Assigned( FMap ) then begin
    FMap := TMapCtrls.New
      .Add( 'id', TMapEdit.New( edtID, True ) )
      .Add( 'nome', TMapEdit.New( edtNome ) )
      .Add( 'telefone', TMapEdit.New( edtFone ) )
      .Add( 'email', TMapEdit.New( edtEmail ) )
  end;
  Result := FMap;
end;

function TfrmCad_OficialJustica.GetPesquisaForum: IPesquisa;
begin
  if not Assigned( FForum ) then begin
    FForum := TPesquisa.New( edtForum, TDao.New( '/forum' ), 'descricao' );
  end;
  Result := FForum;
end;

procedure TfrmCad_OficialJustica.SetJson(const Value: IJson);
begin
  inherited;
  GetPesquisaForum;
  if FJson.Contains( 'forum' ) and ( not FJson.Item( 'forum' ).AsJson.IsEmpty ) then begin
    GetPesquisaForum.SetJson( FJson.Item( 'forum' ).AsJson );
  end;
end;

end.
