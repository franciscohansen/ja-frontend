unit untConnectionConfig;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseModal, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutcxEditAdapters, dxLayoutControlAdapters, cxContainer,
  cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxMaskEdit, cxSpinEdit, cxTextEdit,
  Json.Interfaces;

type
  TfrmConnectionConfig = class(TfrmBaseModal)
    edtHostServidor: TcxTextEdit;
    lciHostServidor: TdxLayoutItem;
    edtPortServidor: TcxSpinEdit;
    lciPortServidor: TdxLayoutItem;
    edtHostHabil: TcxTextEdit;
    lciHostHabil: TdxLayoutItem;
    edtPortHabil: TcxSpinEdit;
    lciPortHabil: TdxLayoutItem;
    btnOk: TcxButton;
    lciOk: TdxLayoutItem;
    btnCancelar: TcxButton;
    lciCancelar: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    lcg2: TdxLayoutAutoCreatedGroup;
    lcg3: TdxLayoutAutoCreatedGroup;
    edtBuild: TcxTextEdit;
    lciBuild: TdxLayoutItem;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
  function Validate : string;
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
  Cfg, View.MessageBox, Dao.Configuracao, Json.Base;

{$R *.dfm}

procedure TfrmConnectionConfig.btnCancelarClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmConnectionConfig.btnOkClick(Sender: TObject);
var
  cfg : ICfg;
  s : String;
  j : IJson;
begin
  inherited;
  s := Validate;
  if s <> '' then begin
    TMsgWarning.New( 'Houveram erros de valida��o. Os seguintes campo s�o obrigat�rios:' + #13#10 + s, True ).Show;
    Abort;
  end;
  cfg := TCfg.GetInstance;
  cfg.SetIniValue( 'CONEXAO', 'HOST', edtHostServidor.Text )
    .SetIniValue( 'CONEXAO', 'PORT', edtPortServidor.Text )
    .SetIniValue( 'CONEXAO', 'H10_HOST', edtHostHabil.Text )
    .SetIniValue( 'CONEXAO', 'H10_PORT', edtPortHabil.Text )
    .SetIniValue( 'HABIL', 'BUILD', edtBuild.Text );

  with TConfiguracaoDao.New do begin
    j := Get;
    j.AddOrSetString( 'hostH10', edtHostHabil.Text )
      .AddOrSetInt( 'portH10', edtPortHabil.Value );
    Dao.Save( j );
  end;
  ModalResult := mrOk;
end;

procedure TfrmConnectionConfig.FormCreate(Sender: TObject);
var
  cfg : ICfg;
begin
  inherited;
  cfg := TCfg.GetInstance;
  edtHostServidor.Text := cfg.Host;
  edtPortServidor.Value := cfg.Port;
  edtHostHabil.Text := cfg.H10Host;
  edtPortHabil.Value := cfg.H10Port;
  edtBuild.Text := cfg.GetIniValue( 'HABIL', 'BUILD' );
end;

function TfrmConnectionConfig.Validate: string;
var
  sb : TStringBuilder;
begin
  sb := TStringBuilder.Create;
  try
    if edtHostServidor.Text = '' then
      sb.Append( 'Host do Servidor' ).Append( #13#10 );
    if edtPortServidor.Value <= 0 then
      sb.Append( 'Porta do Servidor' ).Append( #13#10 );
    if edtHostHabil.Text = '' then
      sb.Append( 'Host do H�bil 10' ).Append( #13#10 );
    if edtPortHabil.Value <= 0 then
      sb.Append( 'Porta do H�bil 10' ).Append( #13#10 );
    if edtBuild.Text = '' then
      sb.Append( 'Build do H�bil 10' );
    Result := sb.ToString;
  finally
    sb.Free;
  end;
end;

end.
