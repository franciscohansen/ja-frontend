unit untPesquisa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseModal, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutcxEditAdapters, dxLayoutControlAdapters, cxContainer,
  cxEdit, Vcl.Menus, cxCustomData, cxStyles, dxScrollbarAnnotations, cxTL,
  cxTLdxBarBuiltInMenu, cxInplaceContainer, Vcl.StdCtrls, cxButtons, cxTextEdit,
  Dao.Interfaces, Json.Interfaces, Http.Interfaces, Util.Lists, System.Math;

type
  TfrmPesquisa = class(TfrmBaseModal)
    edtBusca: TcxTextEdit;
    lciBusca: TdxLayoutItem;
    btnBuscar: TcxButton;
    lciBuscar: TdxLayoutItem;
    GridLista: TcxTreeList;
    lciLista: TdxLayoutItem;
    btnCarregarMais: TcxButton;
    lciCarregarMais: TdxLayoutItem;
    btnOk: TcxButton;
    lciOk: TdxLayoutItem;
    btnFechar: TcxButton;
    lciFechar: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    lcg2: TdxLayoutAutoCreatedGroup;
    Grid_Json: TcxTreeListColumn;
    Grid_Codigo: TcxTreeListColumn;
    Grid_Descricao: TcxTreeListColumn;
    procedure btnFecharClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCarregarMaisClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FDao: IDao;
    FParams: IUrlParams;
    FCount: Integer;
    FList : IList<IJson>;
    FPages: Integer;
    FCurrentPage: Integer;
    FField: string;
    procedure SetDao(const Value: IDao);
    function GetJson: IJson;
    procedure SetParams(const Value: IUrlParams);
    procedure SetCount(const Value: Integer);
    procedure Next;
    procedure SetPages(const Value: Integer);
    procedure SetCurrentPage(const Value: Integer);

    procedure PageToGrid( APage : TArray<IJson> );
    procedure SetField(const Value: string);
    { Private declarations }
  protected
    property Count: Integer read FCount write SetCount;
    property Pages: Integer read FPages write SetPages;
    property CurrentPage : Integer read FCurrentPage write SetCurrentPage;
  public
    { Public declarations }
    property Dao : IDao read FDao write SetDao;
    property Field : string read FField write SetField;
    property Params : IUrlParams read FParams write SetParams;
    property Json : IJson read GetJson;
  end;


implementation

uses
  Json.Base, View.MessageBox, Cfg.Constantes;

{$R *.dfm}

{ TfrmPesquisa }

procedure TfrmPesquisa.btnBuscarClick(Sender: TObject);
 var
  dPage : Real;
begin
  inherited;
  GridLista.Clear;
  Count := FDao.RecordCount( Trim( edtBusca.Text ), Params );
  dPage := Count / TAppConstantes.P_SIZE;
  Pages := Floor( dPage );
  if( dPage - Pages ) > 0 then
    Pages := Pages + 1;
  CurrentPage := 0;
end;

procedure TfrmPesquisa.btnCarregarMaisClick(Sender: TObject);
begin
  inherited;
  Next;
end;

procedure TfrmPesquisa.btnFecharClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmPesquisa.btnOkClick(Sender: TObject);
begin
  inherited;
  if Assigned( GridLista.FocusedNode ) then
    ModalResult := mrOk
  else
    TMsgWarning.New( 'Selecione um item para continuar.' ).Show;
end;

procedure TfrmPesquisa.FormCreate(Sender: TObject);
begin
  inherited;
  FList := TList<IJson>.New;
end;

procedure TfrmPesquisa.FormDestroy(Sender: TObject);
begin
  inherited;
  FList := nil;
end;

procedure TfrmPesquisa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    btnBuscar.Click;
end;

function TfrmPesquisa.GetJson: IJson;
begin
  Result := TJson.New( GridLista.FocusedNode.Values[ Grid_Json.ItemIndex ] );
end;

procedure TfrmPesquisa.Next;
begin
  if CurrentPage < Pages then begin
    CurrentPage := CurrentPage + 1;
  end;
end;

procedure TfrmPesquisa.PageToGrid(APage: TArray<IJson>);
var
  j : IJson;
begin
  try
    GridLista.BeginUpdate;
    for j in APage do begin
      with GridLista.Add do begin
        Values[ Grid_Json.ItemIndex       ] := j.AsJsonString;
        Values[ Grid_Codigo.ItemIndex     ] := j.ID;
        if j.Contains( Field ) then
          Values[ Grid_Descricao.ItemIndex  ] := j.Item( Field ).AsString
        else
          Values[ Grid_Descricao.ItemIndex  ] := '(sem descri��o)';
      end;
    end;
  finally
    GridLista.EndUpdate;
  end;
end;

procedure TfrmPesquisa.SetCount(const Value: Integer);
begin
  FCount := Value;
end;

procedure TfrmPesquisa.SetCurrentPage(const Value: Integer);
begin
  FCurrentPage := Value;
  PageToGrid( FDao.FindAll( Trim( edtBusca.Text ), FCurrentPage, TAppConstantes.P_SIZE, Params ) );
  btnCarregarMais.Enabled := FCurrentPage < Pages;
end;

procedure TfrmPesquisa.SetDao(const Value: IDao);
begin
  FDao := Value;
end;

procedure TfrmPesquisa.SetField(const Value: string);
begin
  FField := Value;
end;

procedure TfrmPesquisa.SetPages(const Value: Integer);
begin
   FPages := Value;
end;

procedure TfrmPesquisa.SetParams(const Value: IUrlParams);
begin
  FParams := Value;
end;

end.
