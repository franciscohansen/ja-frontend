unit untSenhaAdmin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseModal, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutcxEditAdapters, dxLayoutControlAdapters, cxContainer,
  cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxTextEdit;

type
  TfrmSenhaAdmin = class(TfrmBaseModal)
    edtUsuario: TcxTextEdit;
    lciUsuario: TdxLayoutItem;
    edtSenha: TcxTextEdit;
    lciSenha: TdxLayoutItem;
    btnOk: TcxButton;
    lciOk: TdxLayoutItem;
    btnFechar: TcxButton;
    lciFechar: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    class function PedeAdmin : Boolean;
  end;


implementation

uses
  Cfg, Http.Interfaces, Http.Base, Json.Base, Cfg.Constantes;

{$R *.dfm}

{ TfrmSenhaAdmin }

procedure TfrmSenhaAdmin.btnOkClick(Sender: TObject);
var
  r : IHttpResponse;
begin
  inherited;
  r := THttpRequest.New(
    TUrl.New(
      TCfg.GetInstance.H10API + '/usuario/valida-usuario-admin' ,
      TUrlParams.New//TUrlParams.NewWithToken( TCfgFactory.GetInstance )
        .AddParam( 'login', edtUsuario.Text )
        .AddParam( 'senha', edtSenha.Text )
    )
  ).DoPost;
  if r.Status = stOk then
    ModalResult := mrOk
  else
    ModalResult := mrCancel;
end;

class function TfrmSenhaAdmin.PedeAdmin: Boolean;
var
  frm : TfrmSenhaAdmin;
begin
  if TCfg.GetInstance.Usuario.Item( 'administrador' ).AsBoolean then
    Result := True
  else begin
    frm := TfrmSenhaAdmin.Create( nil );
    try 
      Result := frm.ShowModal = mrOk;
    finally
      frm.Release;
    end;
  end;
end;

end.
