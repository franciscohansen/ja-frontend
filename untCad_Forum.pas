unit untCad_Forum;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseCadastro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxContainer, cxEdit,
  dxLayoutControlAdapters, dxLayoutcxEditAdapters, dxLayoutContainer, cxClasses,
  cxTextEdit, Vcl.StdCtrls, cxButtons, dxLayoutControl, cxMemo, cxRichEdit,
  Dao.Interfaces, Http.Interfaces, Json.Interfaces,
  View.MappingControls.Interfaces, View.CreateControls.Interfaces;

type
  TfrmCad_Forum = class(TfrmBaseCadastro)
    edtDescricao: TcxTextEdit;
    lciDescricao: TdxLayoutItem;
    lcgEndereco: TdxLayoutGroup;
    edtFone: TcxTextEdit;
    lciFone: TdxLayoutItem;
    lcg1: TdxLayoutAutoCreatedGroup;
    mmoObservacoes: TcxMemo;
    lciObservacoes: TdxLayoutItem;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FMap : IMapCtrls;
    FCtrl : ICreateControls;
  protected
    function GetJson : IJson; override;
    function Dao : IDao; override;
    function GetMap : IMapCtrls; override;
  public
    { Public declarations }
  end;


implementation

uses
  Json.Base, Http.Base, Exceptions, Dao.Base, Cfg.Constantes, Cfg,
  View.MappingControls.Edits, View.MappingControls.Base,
  View.CreateControls.Base;

{$R *.dfm}

{ TfrmCad_Forum }

function TfrmCad_Forum.Dao: IDao;
begin
  Result := TDao.New( '/forum' );
end;

procedure TfrmCad_Forum.FormCreate(Sender: TObject);
begin
  inherited;
  LCBaseGeral.BeginUpdate;
  try
    FCtrl := TCreateControls.New( Self )
      .Endereco( lcgEndereco );
  finally
    LCBaseGeral.EndUpdate;
  end;
end;

function TfrmCad_Forum.GetJson: IJson;
begin
  Result := inherited;
end;

function TfrmCad_Forum.GetMap: IMapCtrls;
begin
  if not Assigned( FMAp ) then begin
    FMap := TMapCtrls.New
      .Add( 'id', TMapEdit.New( edtID, True ) )
      .Add( 'nome', TMapEdit.New( edtDescricao ) )
      .Add( 'endereco', TMapEndereco.New( Self ) )
      .Add( 'telefone', TMapEdit.New( edtFone ) )
      .Add( 'observacoes', TMapEdit.New( mmoObservacoes ) );
  end;
  Result := FMap;
end;

end.
