inherited frmConfiguracoes: TfrmConfiguracoes
  Caption = 'Configura'#231#245'es'
  ClientHeight = 112
  ClientWidth = 503
  ExplicitWidth = 519
  ExplicitHeight = 151
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Width = 503
    Height = 112
    ExplicitHeight = 194
    inherited btnSalvar: TcxButton
      Left = 337
      Top = 77
      TabOrder = 3
      ExplicitLeft = 337
      ExplicitTop = 77
    end
    inherited btnFechar: TcxButton
      Left = 418
      Top = 77
      TabOrder = 4
      ExplicitLeft = 418
      ExplicitTop = 77
    end
    object edtComissaoPadrao: TcxCalcEdit [3]
      Left = 137
      Top = 28
      AutoSize = False
      EditValue = 0.000000000000000000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 1
      Height = 21
      Width = 175
    end
    object edtPercentualQuitacao: TcxCalcEdit [4]
      Left = 318
      Top = 28
      EditValue = 0.000000000000000000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 2
      Width = 175
    end
    inherited lciID: TdxLayoutItem
      AlignVert = avTop
      Visible = False
    end
    inherited lcgContent: TdxLayoutGroup
      LayoutDirection = ldHorizontal
    end
    object lciComissaoPadrao: TdxLayoutItem
      Parent = lcgContent
      AlignHorz = ahClient
      AlignVert = avTop
      CaptionOptions.Text = 'Comiss'#227'o Padr'#227'o'
      CaptionOptions.Layout = clTop
      Control = edtComissaoPadrao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 324
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciPercentualQuitacao: TdxLayoutItem
      Parent = lcgContent
      AlignHorz = ahClient
      AlignVert = avTop
      CaptionOptions.Text = 'Percentual Quita'#231#227'o'
      CaptionOptions.Layout = clTop
      Control = edtPercentualQuitacao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 324
      ControlOptions.ShowBorder = False
      Index = 2
    end
  end
end
