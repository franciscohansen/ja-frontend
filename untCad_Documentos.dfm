inherited frmCad_Documentos: TfrmCad_Documentos
  Caption = 'Documentos'
  ClientHeight = 114
  ExplicitHeight = 153
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    Height = 114
    inherited btnSalvar: TcxButton
      Top = 79
      TabOrder = 3
      ExplicitTop = 79
    end
    inherited btnFechar: TcxButton
      Top = 79
      TabOrder = 4
      ExplicitTop = 79
    end
    object edtDescricao: TcxTextEdit [3]
      Left = 137
      Top = 28
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Text = 'edtDescricao'
      Height = 21
      Width = 527
    end
    object cbbTipo: TcxImageComboBox [4]
      Left = 670
      Top = 28
      AutoSize = False
      Properties.Items = <>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 2
      Height = 21
      Width = 121
    end
    inherited lciID: TdxLayoutItem
      Parent = lcg1
    end
    object lciDescricao: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Descri'#231#227'o'
      CaptionOptions.Layout = clTop
      Control = edtDescricao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciTipo: TdxLayoutItem
      Parent = lcg1
      CaptionOptions.Text = 'Tipo'
      CaptionOptions.Layout = clTop
      Control = cbbTipo
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgContent
      LayoutDirection = ldHorizontal
      Index = 0
    end
  end
end
