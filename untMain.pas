unit untMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ToolWin, Vcl.ActnMan, Vcl.ActnCtrls,
  Vcl.ActnMenus, Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxClasses, dxLayoutContainer, dxLayoutControl, StdStyleActnCtrls,
  dxLayoutcxEditAdapters, cxContainer, cxEdit, cxLabel, Json.Interfaces,
  dxGDIPlusClasses;

type
  TfrmMain = class(TForm)
    pnlMenuCollapsed: TPanel;
    pnlMenuExpanded: TPanel;
    actmmb1: TActionMainMenuBar;
    lcgMC_Root: TdxLayoutGroup;
    LCMenuCollapsed: TdxLayoutControl;
    lcgME_Root: TdxLayoutGroup;
    LCMenuExpanded: TdxLayoutControl;
    imgLogo: TImage;
    lciLogo: TdxLayoutItem;
    lbUsername: TcxLabel;
    lciUsername: TdxLayoutItem;
    lbEmpresa: TcxLabel;
    lciEmpresa: TdxLayoutItem;
    sep1: TdxLayoutSeparatorItem;
    lbAgendamentos: TcxLabel;
    lciAgendamentos: TdxLayoutItem;
    lbSimulacoes: TcxLabel;
    lciSimulacoes: TdxLayoutItem;
    lbVendas: TcxLabel;
    lciVendas: TdxLayoutItem;
    sep2: TdxLayoutSeparatorItem;
    lbJuridico: TcxLabel;
    lciJuridico: TdxLayoutItem;
    lbNegociacoes: TcxLabel;
    lciNegociacoes: TdxLayoutItem;
    lbLancarVendas: TcxLabel;
    lciLancarVendas: TdxLayoutItem;
    sep3: TdxLayoutSeparatorItem;
    lbConfiguracoes: TcxLabel;
    lciConfiguracoes: TdxLayoutItem;
    imgCollapse: TImage;
    lciCollapse: TdxLayoutItem;
    procedure lbSimulacoesClick(Sender: TObject);
    procedure lbConfiguracoesClick(Sender: TObject);
    procedure lbJuridicoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation
uses
  untLogin, untLst_Bancos, untLst_Vendas, untPainelAdministracao,
  untLst_Juridico, Cfg, Dao.Empresa, View.MessageBox;

{$R *.dfm}

procedure TfrmMain.FormShow(Sender: TObject);
var
  jEmpresa : IJson;
begin
  lbUsername.Caption := TCfg.GetInstance.Usuario.Item( 'nome' ).AsString;
  lciConfiguracoes.Visible := TCfg.GetInstance.Usuario.Item( 'administrador' ).AsBoolean;
  with TH10EmpresaDao.New do begin
    jEmpresa := Dao.FindById( TCfg.GetInstance.Empresa.ID );
    lbEmpresa.Caption := jEmpresa.Item( 'nome' ).AsString;
  end;
end;

procedure TfrmMain.lbConfiguracoesClick(Sender: TObject);
var
  frm : TfrmPainelAdministracao;
begin
  frm := TfrmPainelAdministracao.Create( Self );
  frm.Show;
end;

procedure TfrmMain.lbJuridicoClick(Sender: TObject);
var
  frm : TfrmLst_Juridico;
begin
  if not TCfg.GetInstance.AcessaJuridico then begin
    TMsgWarning.New( 'Seu usu�rio n�o possui acesso ao modulo que voc� est� tentando acessar!' ).Show;
    Abort;
  end;
  frm := TfrmLst_Juridico.Create( Self );
  frm.Show;
end;

procedure TfrmMain.lbSimulacoesClick(Sender: TObject);
var
  frm : TfrmLst_Vendas;
begin
  if not TCfg.GetInstance.AcessaVendas then begin
    TMsgWarning.New( 'Seu usu�rio n�o possui acesso ao modulo que voc� est� tentando acessar!' ).Show;
    Abort;
  end;
  frm := TfrmLst_Vendas.Create( Self );
  frm.Show;
end;

end.
