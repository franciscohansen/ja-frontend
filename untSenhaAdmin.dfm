inherited frmSenhaAdmin: TfrmSenhaAdmin
  Caption = 'Informe a Senha do Administrador'
  ClientHeight = 137
  ClientWidth = 313
  ExplicitWidth = 329
  ExplicitHeight = 176
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseModal: TdxLayoutControl
    Width = 313
    Height = 137
    object edtUsuario: TcxTextEdit [0]
      Left = 10
      Top = 28
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
      Width = 293
    end
    object edtSenha: TcxTextEdit [1]
      Left = 10
      Top = 73
      Properties.EchoMode = eemPassword
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Width = 293
    end
    object btnOk: TcxButton [2]
      Left = 147
      Top = 100
      Width = 75
      Height = 25
      Caption = 'OK'
      TabOrder = 2
      OnClick = btnOkClick
    end
    object btnFechar: TcxButton [3]
      Left = 228
      Top = 100
      Width = 75
      Height = 25
      Caption = 'Fechar'
      TabOrder = 3
    end
    inherited lcgBaseModal: TdxLayoutGroup
      ItemIndex = 2
    end
    object lciUsuario: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Usu'#225'rio'
      CaptionOptions.Layout = clTop
      Control = edtUsuario
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciSenha: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Senha'
      CaptionOptions.Layout = clTop
      Control = edtSenha
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciOk: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnOk
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciFechar: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnFechar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      LayoutDirection = ldHorizontal
      Index = 2
    end
  end
end
