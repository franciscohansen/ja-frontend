unit untLst_Vendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseLista, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles,
  dxScrollbarAnnotations, cxTL, cxTextEdit, cxTLdxBarBuiltInMenu, Vcl.Menus,
  cxContainer, cxEdit, dxLayoutContainer, dxLayoutControlAdapters,
  dxLayoutcxEditAdapters, cxClasses, cxMaskEdit, cxDropDownEdit, Vcl.StdCtrls,
  cxButtons, cxInplaceContainer, dxLayoutControl, cxCalendar, cxCheckBox,
  cxImageComboBox, Dao.Interfaces,
  Json.Interfaces, Forms.Interfaces, cxCalc, Http.Interfaces, cxLabel;

type
  TfrmLst_Vendas = class(TfrmBaseLista)
    GridBase_Cliente: TcxTreeListColumn;
    GridBase_Data: TcxTreeListColumn;
    GridBase_Inativo: TcxTreeListColumn;
    cbbFiltro: TcxImageComboBox;
    lciFiltro: TdxLayoutItem;
    btnInativar: TcxButton;
    lciInativar: TdxLayoutItem;
    btnCancelar: TcxButton;
    dxLayoutItem1: TdxLayoutItem;
    procedure btnInativarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    function Dao : IDao; override;
    procedure JsonToNode( AJson : IJson; ANode : TcxTreeListNode ); override;
    function FormInstance : IFormCadastro; override;
    function GetParams : IUrlParams; override;
  public
    { Public declarations }
  end;


implementation
uses
  Dao.Base, untCad_Vendas, Dao.Simulacao, Http.Base, View.MessageBox;

{$R *.dfm}

{ TfrmLst_Vendas }

procedure TfrmLst_Vendas.btnInativarClick(Sender: TObject);
begin
  inherited;
  if not Assigned( GridBase.FocusedNode ) then begin
    TMsgInformation.New( 'Selecione um item para continuar' ).Show;
    Abort;
  end;

  if TMsgQuestion.New( 'Confirma a opera��o?' ).Show.No then
    Abort;

  if TSimulacaoDao.New.Inativar( GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ] ) then
    btnBuscar.Click;
end;

function TfrmLst_Vendas.Dao: IDao;
begin
  Result := TSimulacaoDao.New.Dao;
end;

function TfrmLst_Vendas.FormInstance: IFormCadastro;
begin
  Result := TfrmCad_Vendas.Create( Self.Owner );
end;

function TfrmLst_Vendas.GetParams: IUrlParams;
begin
  Result := inherited;
  Result.AddHeader( 'situacao', cbbFiltro.EditValue );
end;

procedure TfrmLst_Vendas.JsonToNode(AJson: IJson; ANode: TcxTreeListNode);
var
  sNome : string;
begin
  inherited;
  sNome := AJson.Item( 'dadosCliente.nome' ).AsString;
  if sNome.IsEmpty then
    sNome := '(sem cliente)';
  ANode.Values[ GridBase_Cliente.ItemIndex  ] := sNome;
  ANode.Values[ GridBase_Data.ItemIndex     ] := AJson.Item( 'dataLcto' ).AsDate;
  ANode.Values[ GridBase_Inativo.ItemIndex  ] := AJson.Item( 'inativo' ).AsBoolean;
end;

end.
