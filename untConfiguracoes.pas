unit untConfiguracoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseCadastro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxContainer, cxEdit,
  dxLayoutControlAdapters, dxLayoutcxEditAdapters, dxLayoutContainer, cxClasses,
  cxTextEdit, Vcl.StdCtrls, cxButtons, dxLayoutControl, cxMaskEdit,
  cxDropDownEdit, cxCalc, View.MappingControls.Interfaces,
  View.MappingControls.Edits, View.MappingControls.Base, Dao.Interfaces,
  Json.Interfaces;

type
  TfrmConfiguracoes = class(TfrmBaseCadastro)
    edtComissaoPadrao: TcxCalcEdit;
    lciComissaoPadrao: TdxLayoutItem;
    edtPercentualQuitacao: TcxCalcEdit;
    lciPercentualQuitacao: TdxLayoutItem;
  private
    { Private declarations }
  FMap : IMapCtrls;
  protected
    function GetJson : IJson; override;
    function Dao : IDao; override;
    function GetMap : IMapCtrls; override;
  public
    { Public declarations }
  end;


implementation

uses
  Dao.Base, Dao.Configuracao;

{$R *.dfm}

{ TfrmConfiguracoes }

function TfrmConfiguracoes.Dao: IDao;
begin
  Result := TConfiguracaoDao.New.Dao;
end;

function TfrmConfiguracoes.GetJson: IJson;
begin
  Result := inherited;
end;

function TfrmConfiguracoes.GetMap: IMapCtrls;
begin
  if not Assigned( FMap ) then begin
    FMap := TMapCtrls.New
      .Add( 'percentualQuitacao', TMapEdit.New( edtPercentualQuitacao ) )
      .Add( 'comissaoPadrao', TMapEdit.New( edtComissaoPadrao ) );
  end;
  Result := FMap;
end;

end.
