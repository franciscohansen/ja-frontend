inherited frmConnectionConfig: TfrmConnectionConfig
  Caption = 'Configura'#231#227'o de Conex'#227'o'
  ClientHeight = 198
  ClientWidth = 665
  OnCreate = FormCreate
  ExplicitWidth = 681
  ExplicitHeight = 237
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseModal: TdxLayoutControl
    Width = 665
    Height = 198
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 665
    ExplicitHeight = 198
    object edtHostServidor: TcxTextEdit [0]
      Left = 10
      Top = 28
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 2
      Text = 'edtHostServidor'
      Width = 518
    end
    object edtPortServidor: TcxSpinEdit [1]
      Left = 534
      Top = 28
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 3
      Height = 21
      Width = 121
    end
    object edtHostHabil: TcxTextEdit [2]
      Left = 10
      Top = 73
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 4
      Text = 'edtHostHabil'
      Width = 518
    end
    object edtPortHabil: TcxSpinEdit [3]
      Left = 534
      Top = 73
      AutoSize = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      TabOrder = 5
      Height = 21
      Width = 121
    end
    object btnOk: TcxButton [4]
      Left = 499
      Top = 163
      Width = 75
      Height = 25
      Caption = 'OK'
      TabOrder = 0
      OnClick = btnOkClick
    end
    object btnCancelar: TcxButton [5]
      Left = 580
      Top = 163
      Width = 75
      Height = 25
      Caption = 'Fechar'
      TabOrder = 1
      OnClick = btnCancelarClick
    end
    object edtBuild: TcxTextEdit [6]
      Left = 10
      Top = 118
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 6
      Text = 'edtBuild'
      Width = 645
    end
    inherited lcgBaseModal: TdxLayoutGroup
      ItemIndex = 3
    end
    object lciHostServidor: TdxLayoutItem
      Parent = lcg2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Host Servidor'
      CaptionOptions.Layout = clTop
      Control = edtHostServidor
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciPortServidor: TdxLayoutItem
      Parent = lcg2
      AlignVert = avClient
      CaptionOptions.Text = 'Porta'
      CaptionOptions.Layout = clTop
      Control = edtPortServidor
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciHostHabil: TdxLayoutItem
      Parent = lcg3
      AlignHorz = ahClient
      CaptionOptions.Text = 'Host H'#225'bil 10'
      CaptionOptions.Layout = clTop
      Control = edtHostHabil
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciPortHabil: TdxLayoutItem
      Parent = lcg3
      AlignVert = avClient
      CaptionOptions.Text = 'Porta'
      CaptionOptions.Layout = clTop
      Control = edtPortHabil
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciOk: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnOk
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciCancelar: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCancelar
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      AlignVert = avBottom
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lcg2: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object lcg3: TdxLayoutAutoCreatedGroup
      Parent = lcgBaseModal
      LayoutDirection = ldHorizontal
      Index = 2
    end
    object lciBuild: TdxLayoutItem
      Parent = lcgBaseModal
      CaptionOptions.Text = 'Build H'#225'bil 10'
      CaptionOptions.Layout = clTop
      Control = edtBuild
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
  end
end
