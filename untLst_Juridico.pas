unit untLst_Juridico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, untBaseLista, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles,
  dxScrollbarAnnotations, cxTL, cxTextEdit, cxTLdxBarBuiltInMenu, Vcl.Menus,
  cxContainer, cxEdit, dxLayoutContainer, dxLayoutControlAdapters,
  dxLayoutcxEditAdapters, cxClasses, cxMaskEdit, cxDropDownEdit, Vcl.StdCtrls,
  cxButtons, cxInplaceContainer, dxLayoutControl, Dao.Interfaces,
  Http.Interfaces, Json.Interfaces, Forms.Interfaces, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxCalendar, cxImageComboBox, System.DateUtils, Util.Lists,
  cxCheckBox, dxColorEdit, cxRichEdit, cxCalc, cxButtonEdit, System.Math,
  cxLabel;

type
  TfrmLst_Juridico = class(TfrmBaseLista)
    GridBase_Cliente: TcxTreeListColumn;
    GridBase_CPF: TcxTreeListColumn;
    GridBase_Banco: TcxTreeListColumn;
    GridBase_Veiculo: TcxTreeListColumn;
    GridBase_Placa: TcxTreeListColumn;
    GridBase_Renavam: TcxTreeListColumn;
    GridBase_RG: TcxTreeListColumn;
    GridBase_Endereco: TcxTreeListColumn;
    GridBase_Nro: TcxTreeListColumn;
    GridBase_Bairro: TcxTreeListColumn;
    GridBase_UF: TcxTreeListColumn;
    GridBase_DataFechamento: TcxTreeListColumn;
    GridBase_DataQuitacao: TcxTreeListColumn;
    GridBase_Obs: TcxTreeListColumn;
    GridBase_Leituras: TcxTreeListColumn;
    GridBase_Cidade: TcxTreeListColumn;
    cbbSituacao: TcxImageComboBox;
    lciSituacao: TdxLayoutItem;
    edtDataInicial: TcxDateEdit;
    lciDataInicial: TdxLayoutItem;
    lcg3: TdxLayoutAutoCreatedGroup;
    edtDataFinal: TcxDateEdit;
    lciDataFinal: TdxLayoutItem;
    cbbUf: TcxImageComboBox;
    lciUf: TdxLayoutItem;
    cbbTipo: TcxImageComboBox;
    lciTipo: TdxLayoutItem;
    cbbBanco: TcxImageComboBox;
    lciBanco: TdxLayoutItem;
    cbox1: TcxCheckBox;
    lciCbox1: TdxLayoutItem;
    cbox2: TcxCheckBox;
    lciCbox2: TdxLayoutItem;
    cbox3: TcxCheckBox;
    lciCbox3: TdxLayoutItem;
    cbox4: TcxCheckBox;
    lciCbox4: TdxLayoutItem;
    cbox5: TcxCheckBox;
    lciCbox5: TdxLayoutItem;
    btnAcompanhamento: TcxButton;
    lciAcompanhamento: TdxLayoutItem;
    GridBase_Cor: TcxTreeListColumn;
    btnVisualizarAcompanhamento: TcxButton;
    lciVisualizarAcompanhamento: TdxLayoutItem;
    btnFinalizar: TcxButton;
    lciFinalizar: TdxLayoutItem;
    btnArquivar: TcxButton;
    lciArquivar: TdxLayoutItem;
    btnOutros: TcxButton;
    lciOutros: TdxLayoutItem;
    pmOutros: TPopupMenu;
    mniForum: TMenuItem;
    mniOficial: TMenuItem;
    btnDesmarcarFinalizado: TcxButton;
    lciDesmarcarFinalizado: TdxLayoutItem;
    stlUnreadOdd: TcxStyle;
    stlUnreadEven: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure GridBaseFocusedNodeChanged(Sender: TcxCustomTreeList;
      APrevFocusedNode, AFocusedNode: TcxTreeListNode);
    procedure cbbSituacaoPropertiesChange(Sender: TObject);
    procedure btnDesmarcarFinalizadoClick(Sender: TObject);
    procedure btnArquivarClick(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure mniForumClick(Sender: TObject);
    procedure mniOficialClick(Sender: TObject);
    procedure btnAcompanhamentoClick(Sender: TObject);
    procedure GridBaseStylesGetContentStyle(Sender: TcxCustomTreeList;
      AColumn: TcxTreeListColumn; ANode: TcxTreeListNode; var AStyle: TcxStyle);
    procedure GridBaseDblClick(Sender: TObject);
  private
    { Private declarations }
    FSituacoes,
    FVisualizacoes : IMap<Integer, IJson>;
    procedure PreencheCombos;
    procedure ClearCbox;
    procedure FillCbox( ANumber : Integer );
    function Cor( AJson : IJson ) : Integer;
    procedure MarcaLido( AID : Integer );
    procedure NextNode;
  protected
    function Dao : IDao; override;
    procedure JsonToNode( AJson : IJson; ANode : TcxTreeListNode ); override;
    function FormInstance : IFormCadastro; override;
    function GetParams : IUrlParams; override;
    function FindAll( ASearch : string; AStart, ALength : Integer; AParams : IURlParams ) :TArray<IJson>; override;
    function RecordCount( ASearch : string; AParams : IUrlParams ) : Integer; override;
  public
    { Public declarations }
  end;


implementation

uses
  Cfg, Cfg.Constantes, Dao.Base, Exceptions, Http.Base, Json.Base, Dao.Juridico,
  Model.Enums.JA, Dao.Cliente, Util.Format, Util.Converter, View.MessageBox,
  untCad_OficialJustica, untCad_Forum, untCad_Acompanhamento, Dao.Simulacao,
  untSenhaAdmin;

{$R *.dfm}

{ TfrmLst_Juridico }

procedure TfrmLst_Juridico.btnAcompanhamentoClick(Sender: TObject);
var
  frm: TfrmCad_Acompanhamento;
  j : IJson;
begin
  inherited;
  if not Assigned( GridBase.FocusedNode ) then begin
    TMsgWarning.New( 'Selecione um item para continuar.' ).Show;
    Abort;
  end;
  frm := TfrmCad_Acompanhamento.Create( Self );
  try
    frm.Abrindo := Sender = btnVisualizarAcompanhamento;
    frm.ID      := GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ];
    frm.IDContato := TJson.New( GridBAse.FocusedNode.Values[ GridBase_Json.ItemIndex ] ).ID;
    if frm.ShowModal = mrOk then begin
      if Sender <> btnVisualizarAcompanhamento then begin
        FSituacoes.AddOrReplace( frm.ID, TJuridicoDao.New.SituacaoAtiva( frm.ID ) );
        GridBase.FocusedNode.Values[ GridBase_Cor.ItemIndex ] := Cor( FSituacoes.Item( frm.ID ) );
        MarcaLido( GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ] );
      end;
    end;
  finally
    frm.Release;
  end;
end;

procedure TfrmLst_Juridico.btnArquivarClick(Sender: TObject);
var
  j : IJson;
begin
  inherited;
  if not TfrmSenhaAdmin.PedeAdmin then begin
    TMsgError.New( 'Voc� n�o tem permiss�o para esta opera��o.' ).Show;
    Exit;
  end;
  if Assigned( GridBase.FocusedNode ) then begin
    j := TJuridicoDao.New.MudaSituacao(
      GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ],
      jsArquivado,
      True
    );
    FSituacoes.AddOrReplace( GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ], j );
    GridBaseFocusedNodeChanged( GridBase, GridBase.FocusedNode, GridBase.FocusedNode );
  end
  else begin
    TMsgWarning.New( 'Selecione um item para continuar' ).Show;
  end;
end;

procedure TfrmLst_Juridico.btnDesmarcarFinalizadoClick(Sender: TObject);
var
  j : IJson;
begin
  inherited;
  if not TfrmSenhaAdmin.PedeAdmin then begin
    TMsgError.New( 'Voc� n�o tem permiss�o para esta opera��o.' ).Show;
    Exit;
  end;
  if Assigned( GridBase.FocusedNode ) then begin
    j := TJuridicoDao.New.MudaSituacao(
      GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ],
      jsFinalizado,
      False
    );
    FSituacoes.AddOrReplace( GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ], j );
    GridBaseFocusedNodeChanged( GridBase, GridBase.FocusedNode, GridBase.FocusedNode );
  end
  else begin
    TMsgWarning.New( 'Selecione um item para continuar' ).Show;
  end;
end;

procedure TfrmLst_Juridico.btnFinalizarClick(Sender: TObject);
var
  j : IJson;
begin
  inherited;
  if not TfrmSenhaAdmin.PedeAdmin then begin
    TMsgError.New( 'Voc� n�o tem permiss�o para esta opera��o.' ).Show;
    Exit;
  end;
  if Assigned( GridBase.FocusedNode ) then begin
    j := TJuridicoDao.New.MudaSituacao(
      GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ],
      jsFinalizado,
      True
    );
    FSituacoes.AddOrReplace( GridBase.FocusedNode.Values[ GridBase_ID.ItemIndex ], j );
    GridBaseFocusedNodeChanged( GridBase, GridBase.FocusedNode, GridBase.FocusedNode );
  end
  else begin
    TMsgWarning.New( 'Selecione um item para continuar' ).Show;
  end;
end;



procedure TfrmLst_Juridico.cbbSituacaoPropertiesChange(Sender: TObject);
var
  js : TJuridicoSituacao;
begin
  inherited;
  js := TJuridicoSituacao.ValueOf( cbbSituacao.EditValue );
  GridBase_DataQuitacao.Visible := js = jsFinalizado;
  GridBase_DataFechamento.Visible := js <> jsFinalizado;
  lciArquivar.Visible := js = jsFinalizado;
end;

procedure TfrmLst_Juridico.ClearCbox;
begin
  cbox1.Checked := False;
  cbox2.Checked := False;
  cbox3.Checked := False;
  cbox4.Checked := False;
  cbox5.Checked := False;
end;

function TfrmLst_Juridico.Cor(AJson: IJson): Integer;
begin
  Result := clBlue;
  if AJson.Item( 'observacoes' ).AsString <> '' then
    Result := clGreen;

  case TJuridicoSituacao.ValueOf( AJson.Item( 'situacao' ).AsString ) of
    jsComBusca          : Exit( clRed );
    jsContratoCancelado : Exit( clGray );
    jsMandadoExpedido   : Exit( clYellow );
    jsFinalizado        : Exit( clWebDarkOrange );
    jsArquivado         : Exit( $663399 );
  end;
end;

function TfrmLst_Juridico.Dao: IDao;
begin
  Result := TJuridicoDao.New.Dao;
end;

procedure TfrmLst_Juridico.FillCbox(ANumber: Integer);
begin
  cbox1.Checked := ANumber >= 1;
  cbox2.Checked := ANumber >= 2;
  cbox3.Checked := ANumber >= 3;
  cbox4.Checked := ANumber >= 4;
  cbox5.Checked := ANumber >= 5;
end;

function TfrmLst_Juridico.FindAll(ASearch: string; AStart, ALength: Integer;
  AParams: IURlParams): TArray<IJson>;
begin
  Result := TJuridicoDao.New.LocateFindAll( ASearch, AStart, ALength, AParams );
end;

procedure TfrmLst_Juridico.FormCreate(Sender: TObject);
begin
  inherited;
  PreencheCombos;
  FSituacoes := TMap<Integer, IJson>.New;
  FVisualizacoes := TMap<Integer, IJson>.New;
  edtDataInicial.Date := IncMonth( Date, -1 );
  edtDataFinal.Date   := Date;
end;

function TfrmLst_Juridico.FormInstance: IFormCadastro;
begin
end;

function TfrmLst_Juridico.GetParams: IUrlParams;
begin
  Result := inherited;
  if cbbSituacao.EditValue <> 'TODOS' then
    Result.AddHeader( 'situacao', cbbSituacao.EditValue );
  if ( edtDataInicial.Date > 0 ) and ( edtDataFinal.Date > 0 ) then begin
    Result.AddHeader( 'data-inicial', TJavaConverter.ToDate( edtDataInicial.Date ) )
          .AddHeader( 'data-final', TJavaConverter.ToDate( EndOfTheDay(edtDataFinal.Date), True ) );
  end;
  if cbbUf.EditValue <> 'TODOS' then
    Result.AddHeader('uf', cbbUf.EditValue );
  if cbbBanco.EditValue <> 'TODOS' then
    Result.AddHeader( 'banco', TJson.New( cbbBanco.EditValue ).ID.ToString );

end;

procedure TfrmLst_Juridico.GridBaseDblClick(Sender: TObject);
begin
  btnAcompanhamento.Click;
end;

procedure TfrmLst_Juridico.GridBaseFocusedNodeChanged(Sender: TcxCustomTreeList;
  APrevFocusedNode, AFocusedNode: TcxTreeListNode);
var
  jV, jS : IJson;
  dao : IJuridicoDao;
  iID : Integer;
begin
  inherited;
  ClearCbox;
  if Assigned( AFocusedNode ) then begin
    dao := TJuridicoDao.New;
    iID := AFocusedNode.Values[ GridBase_ID.ItemIndex ];
    if FVisualizacoes.ContainsKey( iID ) then begin
      jV := FVisualizacoes.Item( iID );
    end
    else begin
      jV := dao.Visualizacoes( iID );
      if Assigned( jV ) then
        FVisualizacoes.Add( iID, jV );
    end;
    if Assigned( jV ) then begin
      FillCbox( jV.Item( 'nroVisualizacoes' ).AsInteger );
    end;
    if FSituacoes.ContainsKey( iID ) then begin
      jS := FSituacoes.Item( iID );
    end
    else begin
      jS := dao.SituacaoAtiva( iID );
      if Assigned( jS ) then
        FSituacoes.Add( iID, jS );
    end;
    if Assigned( jS ) then begin
      lciDesmarcarFinalizado.Visible := jS.Item( 'situacao' ).AsString = jsFinalizado.Value;
    end;
  end;
end;

procedure TfrmLst_Juridico.GridBaseStylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn; ANode: TcxTreeListNode;
  var AStyle: TcxStyle);
var
  id, count : Integer;
begin
  inherited;
  if not Assigned( ANode ) then
    Exit;
  id := ANode.Values[ GridBase_ID.ItemIndex ];
  count := 0;
  if FVisualizacoes.ContainsKey( id ) then
    count := FVisualizacoes.Item( id ).Item( 'nroVisualizacoes' ).AsInteger;

  if Odd( ANode.VisibleIndex ) then begin
    AStyle := stlUnreadOdd;
    if count > 0 then
      AStyle := stlOdd;
  end
  else begin
    AStyle := stlUnreadEven;
    if count > 0 then
      AStyle := stlEven;
  end;
end;

procedure TfrmLst_Juridico.JsonToNode(AJson: IJson; ANode: TcxTreeListNode);
procedure Endereco( AEndereco : IJson );
begin
  if AEndereco.IsEmpty then
    Exit;

  with AEndereco do begin
    ANode.Values[ GridBase_Endereco.ItemIndex ] := Item( 'logradouro' ).AsString;
    ANode.Values[ GridBase_Nro.ItemIndex      ] := Item( 'numero' ).AsString;
    ANode.Values[ GridBase_Bairro.ItemIndex   ] := Item( 'bairro' ).AsString;
    ANode.Values[ GridBase_Cidade.ItemIndex   ] := Item( 'municipio' ).AsString;
    ANode.Values[ GridBase_UF.ItemIndex       ] := Item( 'uf' ).AsString;
  end;
end;
procedure DadosCliente( ADados : IJson );
begin
  if ADados.IsEmpty then
    Exit;
  with ADados do begin
    ANode.Values[ GridBase_Cliente.ItemIndex  ] := Item( 'nome' ).AsString;
    ANode.Values[ GridBase_RG.ItemIndex       ] := Item( 'rgIe' ).AsString;
    ANode.Values[ GridBase_CPF.ItemIndex      ] := Item( 'cpfCnpj' ).AsString;
    Endereco( Item( 'enderecoCarne' ).AsJson );
  end;
end;
var
  j : IJson;
  idJuridico : Integer;
  dao : IJuridicoDao;
begin
  inherited;
  idJuridico := AJson.Item( 'juridico.id' ).AsInteger;
  ANode.Values[ GridBase_ID.ItemIndex             ] := idJuridico;
  ANode.Values[ GridBase_DataFechamento.ItemIndex ] := AJson.Item( 'dataVendaParcela' ).AsDate;
  ANode.Values[ GridBase_Cliente.ItemIndex        ] := '(sem dados de cliente)';
  dao := TJuridicoDao.New;
  if AJson.Contains( 'dadosCliente' ) then begin
    DadosCliente( AJson.Item( 'dadosCliente' ).AsJson );
  end;
  if AJson.Contains( 'dadosAdicionais' ) then begin
    j := AJson.Item( 'dadosAdicionais' ).AsJson;
    if not j.IsEmpty then begin
      ANode.Values[ GridBase_Veiculo.ItemIndex ] := '(sem dados)';
      if j.Contains( 'veiculo' ) and ( not j.Item( 'veiculo' ).AsJson.IsEmpty ) then begin
        ANode.Values[ GridBase_Veiculo.ItemIndex  ] := j.Item( 'veiculo.modelo' ).AsString;
        ANode.Values[ GridBase_Placa.ItemIndex    ] := j.Item( 'veiculo.placa' ).AsString;
        ANode.Values[ GridBase_Renavam.ItemIndex  ] := j.Item( 'veiculo.renavam' ).AsString;
      end;
    end;
  end;
  if AJson.Contains( 'simulacao' ) and ( not AJson.Item( 'simulacao' ).AsJson.IsEmpty ) then begin
    j := AJson.Item( 'simulacao' ).AsJson;
    ANode.Values[ GridBase_Banco.ItemIndex ] := '(sem informa��es)';
    if j.Contains( 'banco' ) and ( not j.Item( 'banco' ).AsJson.IsEmpty ) then
      ANode.Values[ GridBase_Banco.ItemIndex ] := j.Item( 'banco.descricao' ).AsString;
  end;
  j := nil;
  if not FSituacoes.ContainsKey( idJuridico ) then begin
    j := dao.SituacaoAtiva( idJuridico );
    if Assigned( j ) then begin
      FSituacoes.Add( idJuridico, j );
    end;
  end
  else begin
    j := FSituacoes.Item( idJuridico );
  end;
  if Assigned( j ) then begin
    ANode.Values[ GridBase_Cor.ItemIndex ] := Cor( j );
    ANode.Values[ GridBase_DataQuitacao.ItemIndex ] := j.Item( 'data' ).AsDate;
  end;

  j := nil;
  if not FVisualizacoes.ContainsKey( idJuridico ) then begin
    j := dao.Visualizacoes( idJuridico );
    if Assigned( j ) then begin
      FVisualizacoes.Add( idJuridico, j );
    end;
  end
  else begin
    j := FVisualizacoes.Item( idJuridico );
  end;
  if Assigned( j ) then begin
    ANode.Values[ GridBase_Leituras.ItemIndex ] := j.Item( 'nroVisualizacoes' ).AsInteger;
  end;
end;

procedure TfrmLst_Juridico.MarcaLido(AID: Integer);
var
  j : IJson;
begin
  j := TJuridicoDao.New.MarcaLeitura( AID );
  if Assigned( j ) then begin
    FVisualizacoes.AddOrReplace( AID, j );
    GridBaseFocusedNodeChanged( GridBase, GridBase.FocusedNode, GridBase.FocusedNode );
  end;
end;

procedure TfrmLst_Juridico.mniForumClick(Sender: TObject);
var
  frm : TfrmCad_Forum;
begin
  inherited;
  frm := TfrmCad_Forum.Create( Self.Owner );
  frm.Insert;
end;

procedure TfrmLst_Juridico.mniOficialClick(Sender: TObject);
var
  frm : TfrmCad_OficialJustica;
begin
  inherited;
  frm := TfrmCad_OficialJustica.Create( Self.Owner );
  frm.Insert;
end;

procedure TfrmLst_Juridico.NextNode;
begin
  if not Assigned( GridBase.FocusedNode ) then
    Exit;
  if GridBase.FocusedNode.VisibleIndex < GridBase.Count then
    GridBase.FocusedNode := GridBase.Items[ GridBase.FocusedNode.VisibleIndex + 1 ];
end;

procedure TfrmLst_Juridico.PreencheCombos;
var
  tc : TTipoContrato;
  ts : TJuridicoSituacao;
  l : IList<IJson>;
  ufs : IList<Variant>;
  i: Integer;
  j : IJson;
begin
  Screen.Cursor := crHourGlass;
  try
    cbbTipo.Properties.BeginUpdate;
    try
      cbbTipo.Properties.Items.Clear;
      with cbbTipo.Properties.Items.Add do begin
        Description := 'Todos';
        Value := 'TODOS';
      end;
      for tc := Low( TTipoContrato ) to High( TTipoContrato ) do begin
        with cbbTipo.Properties.Items.Add do begin
          Description   := tc.Description;
          Value         := tc.Value;
        end;
      end;
    finally
      cbbTipo.Properties.EndUpdate;
      cbbTipo.ItemIndex := 0;
    end;

    cbbSituacao.Properties.Beginupdate;
    try
      cbbSituacao.Properties.Items.Clear;
      with cbbSituacao.Properties.Items.Add do begin
        Description := 'Todos';
        Value := 'TODOS';
      end;
      for ts := Low( TJuridicoSituacao ) to High( TJuridicoSituacao ) do begin
        with cbbSituacao.Properties.Items.Add do begin
          Description := ts.Description;
          Value       := ts.Value;
        end;
      end;
    finally
      cbbSituacao.Properties.Items.Add;
      cbbSituacao.ItemIndex := 0;
    end;
    l := TList<IJson>.New( TDao.New( '/bancos' ).FindAll(0, 0) );
    cbbBanco.Properties.BeginUpdate;
    try
      cbbBanco.Properties.Items.Clear;
      with cbbBanco.Properties.Items.Add do begin
        Description := 'Todos';
        Value := 'TODOS';
      end;
      if l.Count > 0 then begin
        for i := 0 to l.Count -1 do begin
          j := l.Item( i );
          with cbbBanco.Properties.Items.Add do begin
            Description := j.Item( 'descricao' ).AsString;
            Value       := j.AsJsonString;
          end;
        end;
      end;
    finally
      cbbBanco.Properties.EndUpdate;
      cbbBanco.ItemIndex := 0;
    end;
    ufs := TList<Variant>.New( TClienteDao.New.UFs );

    cbbUf.Properties.BeginUpdate;
    try
      cbbUf.Properties.Items.Clear;
      with cbbUf.Properties.Items.Add do begin
        Description := 'Todos';
        Value       := 'TODOS';
      end;
      for i := 0 to ufs.Count -1 do begin
        with cbbUf.Properties.Items.Add do begin
          Description := ufs.Item( i );
          Value       := ufs.Item( i );
        end;
      end;
    finally
      cbbUf.Properties.EndUpdate;
      cbbUF.ItemIndex := 0;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TfrmLst_Juridico.RecordCount(ASearch: string;
  AParams: IUrlParams): Integer;
begin
  Result := TJuridicoDao.New.LocateRecordCount( ASearch, AParams );
end;

end.
