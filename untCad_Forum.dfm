inherited frmCad_Forum: TfrmCad_Forum
  Caption = 'F'#243'rum'
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    inherited btnSalvar: TcxButton
      TabOrder = 4
    end
    inherited btnFechar: TcxButton
      TabOrder = 5
    end
    object edtDescricao: TcxTextEdit [3]
      Left = 137
      Top = 28
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Text = 'edtDescricao'
      Width = 654
    end
    object edtFone: TcxTextEdit [4]
      Left = 10
      Top = 84
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 2
      Text = 'edtFone'
      Width = 781
    end
    object mmoObservacoes: TcxMemo [5]
      Left = 10
      Top = 129
      Lines.Strings = (
        'mmoObservacoes')
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 3
      Height = 408
      Width = 781
    end
    inherited lciID: TdxLayoutItem
      Parent = lcg1
    end
    object lciDescricao: TdxLayoutItem
      Parent = lcg1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Descri'#231#227'o'
      CaptionOptions.Layout = clTop
      Control = edtDescricao
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcgEndereco: TdxLayoutGroup
      Parent = lcgContent
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 1
    end
    object lciFone: TdxLayoutItem
      Parent = lcgContent
      CaptionOptions.Text = 'Telefone'
      CaptionOptions.Layout = clTop
      Control = edtFone
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lcg1: TdxLayoutAutoCreatedGroup
      Parent = lcgContent
      LayoutDirection = ldHorizontal
      Index = 0
    end
    object lciObservacoes: TdxLayoutItem
      Parent = lcgContent
      AlignVert = avClient
      CaptionOptions.Text = 'Observa'#231#245'es'
      CaptionOptions.Layout = clTop
      Control = mmoObservacoes
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 3
    end
  end
end
