inherited frmLst_Agendamentos: TfrmLst_Agendamentos
  Caption = 'Agendamentos'
  PixelsPerInch = 96
  TextHeight = 13
  inherited LCBaseGeral: TdxLayoutControl
    inherited GridBase: TcxTreeList
      object GridBase_Descricao: TcxTreeListColumn
        Caption.Text = 'Descri'#231#227'o'
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Data: TcxTreeListColumn
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mm/yyyy'
        Properties.EditFormat = 'dd/mm/yyyy'
        Caption.Text = 'Data'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Status: TcxTreeListColumn
        Caption.Text = 'Status'
        Options.Sizing = False
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object GridBase_Usuario: TcxTreeListColumn
        Caption.Text = 'Usu'#225'rio'
        Width = 100
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    inherited btnExcluir: TcxButton
      Left = 172
      TabOrder = 5
      ExplicitLeft = 172
    end
    inherited btnAnterior: TcxButton
      TabOrder = 7
    end
    inherited btnProxima: TcxButton
      TabOrder = 8
    end
    inherited cbbPagina: TcxComboBox
      Properties.OnChange = nil
      TabOrder = 6
    end
    inherited btnAbrir: TcxButton
      Left = 253
      TabOrder = 9
      ExplicitLeft = 253
    end
    inherited btnImprimir: TcxButton
      TabOrder = 10
    end
    inherited lciExcluir: TdxLayoutItem
      Index = 2
    end
    inherited lciAnterior: TdxLayoutItem
      Index = 4
    end
    inherited lciProxima: TdxLayoutItem
      Index = 5
    end
    inherited lciPagina: TdxLayoutItem
      Index = 3
    end
    inherited lciAbrir: TdxLayoutItem
      Index = 6
    end
    inherited lciImprimir: TdxLayoutItem
      Index = 7
    end
  end
end
